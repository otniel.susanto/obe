<?php

namespace App\ExcelTemplates;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use App\Models;
use Illuminate\Support\Facades\Auth;

class MataKuliahTemplate implements FromView, ShouldAutoSize, WithEvents
{
    public function view(): View
    {
        $user = Auth::user();
        $program_studi = $user->programStudi;
        return view('mata_kuliah.template', [
            'program_studi' => $program_studi
        ]);
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $user = Auth::user();
                $drop_column = 'B';

                // set dropdown list for first data row
                $validation = $event->sheet->getCell("{$drop_column}8")->getDataValidation();
                $validation->setType(DataValidation::TYPE_LIST );
                $validation->setErrorStyle(DataValidation::STYLE_INFORMATION );
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pilih data dari list');
                // $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1("J8:J" . $event->sheet->getHighestRow());

                // clone validation to remaining rows
                for ($i = 9; $i <= 100; $i++) {
                    $event->sheet->getCell("{$drop_column}{$i}")->setDataValidation(clone $validation);
                }

                #region DropDown Jenis Mata Kuliah
                $drop_column = 'C';

                // set dropdown options
                $optionsObject = Models\JenisMataKuliah::select('nama')->get();
                $options = [];
                foreach($optionsObject as $item){
                    array_push($options, $item->nama);
                }

                // set dropdown list for first data row
                $validation = $event->sheet->getCell("{$drop_column}8")->getDataValidation();
                $validation->setType(DataValidation::TYPE_LIST );
                $validation->setErrorStyle(DataValidation::STYLE_INFORMATION );
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pilih data dari list');
                // $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1(sprintf('"%s"',implode(',',$options)));
                // clone validation to remaining rows
                for ($i = 9; $i <= 100; $i++) {
                    $event->sheet->getCell("{$drop_column}{$i}")->setDataValidation(clone $validation);
                }
                #endregion
            },
        ];
    }
}
