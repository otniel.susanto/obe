<?php

namespace App\ExcelTemplates;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use App\Models;
use Illuminate\Support\Facades\Auth;

class MahasiswaTemplate implements FromView, WithEvents
{
    public function view(): View
    {
        return view('mahasiswa.template', [
            
        ]);
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {   
                // $user = Auth::user();        
                // $drop_column = 'C';

                // // set dropdown options
                // $optionsObject = $user->programStudi;
                // $options = [];
                // foreach($optionsObject as $item){
                //     array_push($options, $item->nama);
                // }

                // // set dropdown list for first data row
                // $validation = $event->sheet->getCell("{$drop_column}2")->getDataValidation();
                // $validation->setType(DataValidation::TYPE_LIST );
                // $validation->setErrorStyle(DataValidation::STYLE_INFORMATION );
                // $validation->setAllowBlank(false);
                // $validation->setShowInputMessage(true);
                // $validation->setShowErrorMessage(true);
                // $validation->setShowDropDown(true);
                // $validation->setErrorTitle('Input error');
                // $validation->setError('Data tidak ditemukan di dalam list');
                // $validation->setPromptTitle('Pilih data dari list');
                // // $validation->setPrompt('Please pick a value from the drop-down list.');
                // $validation->setFormula1(sprintf('"%s"',implode(',',$options)));

                // // clone validation to remaining rows
                // for ($i = 3; $i <= 100; $i++) {
                //     $event->sheet->getCell("{$drop_column}{$i}")->setDataValidation(clone $validation);
                // }

                // // set columns to autosize
                // for ($i = 1; $i <= 100; $i++) {
                //     $column = Coordinate::stringFromColumnIndex($i);
                //     $event->sheet->getColumnDimension($column)->setAutoSize(true);
                // }
            },
        ];
    }
}
