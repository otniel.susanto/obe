<?php

namespace App\ExcelTemplates;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LevelKontribusiTemplate implements FromView, ShouldAutoSize, WithEvents
{
    protected $id;

    function __construct($id) {
        $this->id = $id;
    }

    public function view(): View
    {
        $tahun_ajaran = Models\TahunAjaran::firstWhere('status', 'Aktif');
        $program_studi = Models\ProgramStudi::find($this->id);
        $level = Models\Level::all();
        $cpl = Models\Cpl::where('program_studi_id', $this->id)->orderBy('cpl.id')->get();
        $jenis_cpl = Models\JenisCpl::select([
            'jenis_cpl.nama as nama', 
            DB::raw('COUNT(*) as jumlah')
        ])
        ->join('kategori_cpl', 'kategori_cpl.jenis_cpl_id', 'jenis_cpl.id')
        ->join('cpl', 'cpl.kategori_cpl_id', 'kategori_cpl.id')
        ->where('cpl.program_studi_id', $this->id)
        ->groupBy('jenis_cpl.id', 'jenis_cpl.nama')
        ->orderBy('jenis_cpl.id')
        ->get();
        return view('level_kontribusi.template',[
            'level' => $level,
            // 'mata_kuliah' => $mata_kuliah,
            'cpl' => $cpl,
            'jenis_cpl' => $jenis_cpl,
            'tahun_ajaran' => $tahun_ajaran,
            'program_studi' => $program_studi,
        ]);
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                #region Drop Down Tahun Ajaran
                // set dropdown options
                $optionsObject = Models\TahunAjaran::select('keterangan')->orderBy('id','desc')->limit(3)->get();
                $options = [];
                foreach($optionsObject as $item){
                    array_push($options, $item->keterangan);
                }
                // set dropdown list for first data row
                $validation = $event->sheet->getCell("D3")->getDataValidation();
                $validation->setType(DataValidation::TYPE_LIST );
                $validation->setErrorStyle(DataValidation::STYLE_INFORMATION );
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pilih data dari list');
                // $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1(sprintf('"%s"',implode(',',$options)));
                #endregion 

                #region FreezePane dan COUNTA
                $event->sheet->freezePane('F9');
                $event->sheet->getColumnDimension('A')->setWidth(1.68);
                $event->sheet->getColumnDimension('J')->setWidth(10);
                $event->sheet->getColumnDimension('K')->setAutoSize(false);
                $event->sheet->getColumnDimension('K')->setWidth(10);
                $event->sheet->getRowDimension('5')->setRowHeight(9.3);
                $lastCol = $event->sheet->getHighestColumn();
                for($i = 9; $i <= 50; $i++){
                    $event->sheet->setCellValue('G'. $i, 
                    '=COUNTA(H'.$i . ':'. $lastCol . $i . ')/2');
                }
                #endregion
                
                #region Print CPL
                $cpl = Models\Cpl::select([
                    'cpl.keterangan as keterangan'
                ])
                ->join('kategori_cpl', 'kategori_cpl.id', 'cpl.kategori_cpl_id')
                ->join('jenis_cpl', 'jenis_cpl.id', 'kategori_cpl.jenis_cpl_id')
                ->where('program_studi_id', $this->id)
                ->orderBy('cpl.id')
                ->get();
                $column = 'H';
                foreach($cpl as $item){
                    $event->sheet->getDelegate()
                    ->getComment($column.'8')->getText()->createTextRun($item->keterangan);
                    $column++;
                    $column++;
                }
                #endregion
            },
        ];
    }
}
