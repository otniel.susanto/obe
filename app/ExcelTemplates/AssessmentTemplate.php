<?php

namespace App\ExcelTemplates;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AssessmentTemplate implements FromView, ShouldAutoSize, WithEvents
{
    protected $id;

    function __construct($id) {
        $this->id = $id;
    }

    public function view(): View
    {
        $assessment = Models\Assessment::find($this->id);
        $isi_soal = $assessment->isiSoal->sortBy('nomor_soal');
        return view('assessment.template',[
            'assessment' => $assessment,
            'isi_soal' => $isi_soal
        ]);
    }
    
    public function registerEvents(): array
    {
        $assessment = Models\Assessment::find($this->id);
        $lastSoalColumn = 'E';
        foreach($assessment->isiSoal as $item){
            $lastSoalColumn++;
        }
        return [
            AfterSheet::class => function(AfterSheet $event) use($lastSoalColumn) {
                $event->sheet->freezePane('D4');
                for($i = 11; $i <= $event->sheet->getHighestRow(); $i++){
                    $event->sheet->setCellValue('E'. $i, 
                    '=SUM(F'.$i . ':'. $lastSoalColumn . $i . ')');
                }
            },
        ];
    }
}
