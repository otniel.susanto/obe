<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CourseScoringSheetExport implements FromView, ShouldAutoSize, WithEvents
{
    protected $program_studi_id;
    protected $mata_kuliah_id;
    protected $tahun_ajaran_id;
    protected $kp;

    function __construct($program_studi_id, $mata_kuliah_id, $tahun_ajaran_id, $kp) {
        $this->program_studi_id = $program_studi_id;
        $this->mata_kuliah_id = $mata_kuliah_id;
        $this->tahun_ajaran_id = $tahun_ajaran_id;
        $this->kp = $kp;
    }

    public function view(): View
    {
        $kelas = Models\Kelas::where('mata_kuliah_id', $this->mata_kuliah_id)
        ->where('tahun_ajaran_id', $this->tahun_ajaran_id);
        if($this->kp !== null){
            // $kelas = $kelas->where('kp', $this->kp);
            $stringKp = str_replace(" ", "", $this->kp);
            $arrayKp = explode(",", $stringKp);
            $kelas = $kelas->whereIn('kp', $arrayKp);
        }
        $kelas = $kelas->get();
        $kelompok_nilai = Models\KelompokNilai::all();
        return view('course_scoring_sheet.export',[
            'data' => $kelas,
            'kelompok_nilai' => $kelompok_nilai,
        ]);
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('E17');
            },
        ];
    }
}
