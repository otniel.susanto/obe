<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RekapCPLExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $mata_kuliah_id;
    protected $program_studi_id;
    protected $tahun_ajaran_id;
    protected $kp;
    function __construct($mata_kuliah_id, $program_studi_id, $tahun_ajaran_id, $kp) {
        $this->mata_kuliah_id = $mata_kuliah_id;
        $this->program_studi_id = $program_studi_id;
        $this->tahun_ajaran_id = $tahun_ajaran_id;
        $this->kp = $kp;
    }

    public function view(): View
    {
        $program_studi = Models\ProgramStudi::find($this->program_studi_id);
        $mata_kuliah = Models\MataKuliah::find($this->mata_kuliah_id);
        $tahun_ajaran = Models\TahunAjaran::find($this->tahun_ajaran_id);
        if($tahun_ajaran !== null){
            $kelas = Models\Kelas::where('tahun_ajaran_id', $this->tahun_ajaran_id);
            $cpl = Models\Cpl::select([
                DB::raw('DISTINCT cpl.id as id'),
                'cpl.kode as kode',
                'cpl.program_studi_id as program_studi_id',
                'kelas.id as kelas_id',
                'level_kontribusi.skor_maks as skor_maks',
            ])
            ->join('level_kontribusi', 'cpl.id', '=', 'level_kontribusi.cpl_id')
            ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
            ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
            ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
            ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
            ->where('kelas.tahun_ajaran_id', $this->tahun_ajaran_id)
            ->orderBy('cpl.program_studi_id')
            ->orderBy('cpl.id');
            $level_kontribusi = Models\LevelKontribusi::select([
                DB::raw('DISTINCT level_kontribusi.id as id'),
                'cpl.id as cpl_id',
                'cpl.program_studi_id as program_studi_id',
                'kelas.id as kelas_id',
                'skor_mahasiswa.mahasiswa_id as mahasiswa_id',
                'skor_mahasiswa.skor as skor',
            ])
            ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
            ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
            ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
            ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
            ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
            ->join('kelas_mahasiswa', 'kelas.id', '=', 'kelas_mahasiswa.kelas_id')
            ->join('skor_mahasiswa', 'level_kontribusi.id', '=', 'skor_mahasiswa.level_kontribusi_id')
            ->where('kelas.tahun_ajaran_id', $this->tahun_ajaran_id)
            ->orderBy('cpl.id');
            if(isset($this->kp) && $this->kp != ""){
                $stringKp = str_replace(" ", "", $this->kp);
                $arrayKp = explode(",", $stringKp);
                $cpl = $cpl->whereIn('kelas.kp', $arrayKp);
                $level_kontribusi = $level_kontribusi->whereIn('kelas.kp', $arrayKp);
                $kelas = $kelas->whereIn('kp', $arrayKp);
            }
            if($this->mata_kuliah_id != "" && $this->mata_kuliah_id !== null){
                $kelas = $kelas->where('mata_kuliah_id', $this->mata_kuliah_id);
                $level_kontribusi = $level_kontribusi->where('kelas.mata_kuliah_id', $this->mata_kuliah_id);
                $cpl = $cpl->where('kelas.mata_kuliah_id', $this->mata_kuliah_id);
            }
            if($this->program_studi_id != "" && $this->program_studi_id !== null){
                $level_kontribusi = $level_kontribusi->where('cpl.program_studi_id', $this->program_studi_id);
                $cpl = $cpl->where('cpl.program_studi_id', $this->program_studi_id);
            }
            $level_kontribusi = $level_kontribusi->get();
            $cpl = $cpl->get();
            $cpl_unique = $cpl->unique('id');
            $kelas = $kelas->get();
            $arrayLevelKontribusi = [];
            foreach($level_kontribusi as $itemLevelKontribusi){
                $arrayLevelKontribusi[$itemLevelKontribusi->kelas_id][$itemLevelKontribusi->cpl_id][$itemLevelKontribusi->mahasiswa_id] = $itemLevelKontribusi->skor;
            }
        }
        else{
            throw new Exception("Tahun ajaran tidak ditemukan!");
        }
        $kelompok_nilai = Models\KelompokNilai::all();
        return view('rekap_cpl.export',[
            'program_studi' => $program_studi,
            'mata_kuliah' => $mata_kuliah,
            'tahun_ajaran' => $tahun_ajaran,
            'cpl' => $cpl,
            'cpl_unique' => $cpl_unique,
            'level_kontribusi' => $arrayLevelKontribusi,
            'kelas' => $kelas,
            'kelompok_nilai' => $kelompok_nilai,
        ]);
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                if($this->program_studi_id != "" && $this->program_studi_id !== null &&
                $this->mata_kuliah_id != "" && $this->mata_kuliah_id !== null){
                    $event->sheet->freezePane('E9');
                }
                else{
                    if($this->program_studi_id != "" && $this->program_studi_id !== null){
                        $event->sheet->freezePane('E8');
                    }
                    else if($this->mata_kuliah_id != "" && $this->mata_kuliah_id !== null){
                        $event->sheet->freezePane('E8');
                    }
                }
            },
        ];
    }
}
