<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CourseScoreCardExport implements FromView, WithEvents
{
    protected $mata_kuliah_id;
    protected $program_studi_id;
    protected $tahun_ajaran_id;
    protected $kp;
    function __construct($mata_kuliah_id, $program_studi_id, $tahun_ajaran_id, $kp) {
        $this->mata_kuliah_id = $mata_kuliah_id;
        $this->program_studi_id = $program_studi_id;
        $this->tahun_ajaran_id = $tahun_ajaran_id;
        $this->kp = $kp;
    }

    public function view(): View
    {
        $kelas = Models\Kelas::where('program_studi_id', $this->program_studi_id)
        ->where('mata_kuliah_id', $this->mata_kuliah_id)
        ->where('tahun_ajaran_id', $this->tahun_ajaran_id);
        $cpl = Models\Cpl::select([
            DB::raw('DISTINCT cpl.id as id'),
            'cpl.kode as kode',
            'cpl.program_studi_id as program_studi_id',
            'level_kontribusi.skor_maks as skor_maks',
        ])
        ->join('level_kontribusi', 'cpl.id', '=', 'level_kontribusi.cpl_id')
        ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
        ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
        ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
        ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
        ->where('kelas.program_studi_id', $this->program_studi_id)
        ->where('kelas.mata_kuliah_id', $this->mata_kuliah_id)
        ->where('kelas.tahun_ajaran_id', $this->tahun_ajaran_id)
        ->orderBy('cpl.program_studi_id')
        ->orderBy('cpl.id');
        $level_kontribusi = Models\LevelKontribusi::select([
            DB::raw('DISTINCT level_kontribusi.id as id'),
            'cpl.id as cpl_id',
            'cpl.program_studi_id as program_studi_id',
            'skor_mahasiswa.mahasiswa_id as mahasiswa_id',
            'skor_mahasiswa.skor as skor',
        ])
        ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
        ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
        ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
        ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
        ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
        ->join('kelas_mahasiswa', 'kelas.id', '=', 'kelas_mahasiswa.kelas_id')
        ->join('skor_mahasiswa', 'level_kontribusi.id', '=', 'skor_mahasiswa.level_kontribusi_id')
        ->where('kelas.program_studi_id', $this->program_studi_id)
        ->where('kelas.mata_kuliah_id', $this->mata_kuliah_id)
        ->where('kelas.tahun_ajaran_id', $this->tahun_ajaran_id)
        ->orderBy('cpl.id');
        if(isset($this->kp) && $this->kp != ""){
            $stringKp = str_replace(" ", "", $this->kp);
            $arrayKp = explode(",", $stringKp);
            $cpl = $cpl->whereIn('kelas.kp', $arrayKp);
            $level_kontribusi = $level_kontribusi->whereIn('kelas.kp', $arrayKp);
            $kelas = $kelas->whereIn('kp', $arrayKp);
        }
        $level_kontribusi = $level_kontribusi->get();
        $cpl = $cpl->get();
        $kelas = $kelas->get();
        $arrayLevelKontribusi = [];
        foreach($level_kontribusi as $itemLevelKontribusi){
            $arrayLevelKontribusi[$itemLevelKontribusi->cpl_id][$itemLevelKontribusi->mahasiswa_id] = $itemLevelKontribusi->skor;
        }
        $kelompok_nilai = Models\KelompokNilai::all();
        return view('course_score_card.export',[
            'cpl' => $cpl,
            'level_kontribusi' => $arrayLevelKontribusi,
            'kelas' => $kelas,
            'kelompok_nilai' => $kelompok_nilai,
        ]);
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                // $event->sheet->freezePane('');
            },
        ];
    }
}
