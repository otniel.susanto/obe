<?php

namespace App\Imports;

use App\Models;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Exception;

class MahasiswaPesertaKelasParalelImport implements ToCollection, WithStartRow, WithMultipleSheets
{
    protected $program_studi_id;
    protected $mata_kuliah_id;
    protected $tahun_ajaran_id;
    protected $startRow;
 
    function __construct($program_studi_id, $mata_kuliah_id , $tahun_ajaran_id, $startRow) {
        $this->program_studi_id = $program_studi_id;
        $this->mata_kuliah_id = $mata_kuliah_id;
        $this->tahun_ajaran_id = $tahun_ajaran_id;
        $this->startRow = $startRow;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $rps = Models\Rps::where('mata_kuliah_id', $this->mata_kuliah_id)
        ->where('tahun_ajaran_id', $this->tahun_ajaran_id)
        ->orderBy('id', 'desc')
        ->first();
        $kelas = Models\Kelas::where('mata_kuliah_id', $this->mata_kuliah_id)
        ->where('tahun_ajaran_id', $this->tahun_ajaran_id)
        ->get();
        $program_studi = Models\ProgramStudi::all();
        foreach ($rows as $row) {
            $mahasiswa = Models\Mahasiswa::where('nrp', $row[1])->first();
            if(!isset($mahasiswa)){
                $mahasiswa = new Models\Mahasiswa();
                $mahasiswa->nrp = $row[1];
                $mahasiswa->nama = $row[2];
                //Membedakan jenis kode NRP
                if(strlen($row[1]) == 9){
                    $mahasiswa->tahun_masuk = '20' . substr($row[1], 4, 2);
                    $mahasiswa->program_studi_id = $program_studi
                    ->firstWhere('kode', substr($row[1], 0, 4))->id;
                }
                else if(strlen($row[0]) == 7){
                    $mahasiswa->tahun_masuk = '20' . substr($row[1], 1, 2);
                    $mahasiswa->program_studi_id = $program_studi
                    ->firstWhere('kode', 0 . substr($row[1], 3, 1))->id;
                }
                else{
                    throw new Exception("Data Mahasiswa " . $row[1] . ' - ' . $row[2] . ' memiliki format NRP yang tidak dikenali. Bila ada format NRP baru, silakan hubungi admin');
                }
                $mahasiswa->save();
            }
            $kp = str_replace("KP:", "", $row[3]);
            $dataKelas = $kelas->firstWhere('kp', $kp);
            if($dataKelas !== null && isset($dataKelas)){
                $mahasiswa->kelas()->detach($dataKelas->id);
                $mahasiswa->kelas()->attach($dataKelas->id);
            }
        }
    }
    public function startRow(): int {
        return $this->startRow;
    }

    public function sheets(): array
    {
        return [
            0 => new MahasiswaPesertaKelasParalelImport(
                $this->program_studi_id,
                $this->mata_kuliah_id,
                $this->tahun_ajaran_id,
                $this->startRow
            )
        ];
    }
}
