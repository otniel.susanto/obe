<?php

namespace App\Imports;

use App\Models;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class NilaiImport implements ToCollection, WithStartRow, WithMultipleSheets, WithCalculatedFormulas
{
    
    protected $id;

    function __construct($id) {
           $this->id = $id;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $isi_soal = Models\IsiSoal::where('assessment_id', $this->id)->get();
        $tahun_ajaran = $isi_soal[0]->assessment->kelas[0]->tahunAjaran;
        $arrayNilaiMahasiswa = [];
        for($i = 3; $i < count($rows) ; $i++) {
            $mahasiswa = Models\Mahasiswa::firstWhere('nrp', $rows[$i][1]);
            if($mahasiswa){
                $nilai_mahasiswa = Models\NilaiMahasiswa::where('mahasiswa_id', $mahasiswa->id)
                ->where('assessment_id', $this->id)->first();
                if(!isset($nilai_mahasiswa)){
                    $nilai_mahasiswa = new Models\NilaiMahasiswa();
                    $nilai_mahasiswa->mahasiswa_id = $mahasiswa->id;
                    $nilai_mahasiswa->assessment_id = $this->id;
                    $nilai_mahasiswa->nilai = 0;
                    $nilai_mahasiswa->save();
                }
                $col = 5;
                $nilaiTotal = 0;
                $arrayIsiSoal = [];
                while($isi_soal->where('nomor_soal', str_replace("No. ", "", $rows[0][$col]))->first() !== null){
                    $itemIsiSoal = $isi_soal->where('nomor_soal', str_replace("No. ", "", $rows[0][$col]))->first();
                    $nilai = $rows[$i][$col];
                    if(!is_numeric($nilai)){
                        $nilai = 0;
                    }
                    //Cari detail_nilai_mahasiswa. Kalau ada, nilai di tabel tersebut
                    //dihitung lalu mengurangi skor pada tabel skor_mahasiswa
                    $detail_nilai_mahasiswa = $itemIsiSoal->nilaiMahasiswa->firstWhere('mahasiswa_id', $mahasiswa->id);
                    if(null !== $detail_nilai_mahasiswa){
                        $this->kurangSkorMahasiswaLevelKontribusi($itemIsiSoal, $mahasiswa, $tahun_ajaran->id,
                        $detail_nilai_mahasiswa->pivot->nilai);
                    }
                    $this->tambahSkorMahasiswaLevelKontribusi($itemIsiSoal, $mahasiswa, $tahun_ajaran->id, $nilai);
                    $arrayIsiSoal[$itemIsiSoal->id] = ['nilai'=>$nilai];
                    $nilaiTotal += $nilai;
                    $col++;
                }
                $nilai_mahasiswa->isiSoal()->sync($arrayIsiSoal);
                // $nilai_mahasiswa->nilai = $nilaiTotal;
                $nilai_mahasiswa->nilai = $rows[$i][4];
                $nilai_mahasiswa->feedback = $rows[$i][$col];
                $nilai_mahasiswa->save();
            }
        }
    }

    public function startRow(): int {
        return 8;
    }

    public function sheets(): array
    {
        return [
            0 => new NilaiImport($this->id)
        ];
    }

    private function tambahSkorMahasiswaLevelKontribusi($isi_soal, $mahasiswa, $tahun_ajaran_id, $nilaiExcel){
        $persentase = $isi_soal->assessment->komponenPenilaian->persentase / 100 * 
        $isi_soal->assessment->komponenPenilaian->kelompokNilai->persentase / 100;
        $arraySkorMahasiswa = [];
        foreach($isi_soal->cpmk as $itemCpmk){
            $countCpmk = $isi_soal->cpmk->count();
            foreach($itemCpmk->levelKontribusi
            ->where('cpl.program_studi_id', $mahasiswa->program_studi_id)
            ->where('tahun_ajaran_id', $tahun_ajaran_id) as $itemLevelKontribusi){
                //Cari tabel skor_mahasiswa / tabel pivot antara Level Kontribusi dengan Models\Mahasiswa
                $skor_mahasiswa = Models\SkorMahasiswa::where('level_kontribusi_id', $itemLevelKontribusi->id)
                ->where('mahasiswa_id', $mahasiswa->id)->first();
                if($skor_mahasiswa === null){
                    $skor_mahasiswa = new Models\SkorMahasiswa();
                    $skor_mahasiswa->level_kontribusi_id = $itemLevelKontribusi->id;
                    $skor_mahasiswa->mahasiswa_id = $mahasiswa->id;
                    $skor_mahasiswa->skor = 0;
                    $skor_mahasiswa->save();
                }
                //Tambah kolom skor pada tabel skor_mahasiswa
                $skor_mahasiswa->skor = $skor_mahasiswa->skor + 
                round($nilaiExcel / $countCpmk * ($persentase) / 
                $itemCpmk->levelKontribusi
                ->where('cpl.program_studi_id', $mahasiswa->program_studi_id)
                ->where('tahun_ajaran_id', $tahun_ajaran_id)->count(), 2);
                $skor_mahasiswa->save();
            }
        }
    }
    
    private function kurangSkorMahasiswaLevelKontribusi($isi_soal, $mahasiswa, $tahun_ajaran_id, $nilai_pengurang){
        $persentase = $isi_soal->assessment->komponenPenilaian->persentase / 100 * 
        $isi_soal->assessment->komponenPenilaian->kelompokNilai->persentase / 100;
        foreach($isi_soal->cpmk as $itemCpmk){
            $countCpmk = $isi_soal->cpmk->count();
            foreach($itemCpmk->levelKontribusi
            ->where('cpl.program_studi_id', $mahasiswa->program_studi_id)
            ->where('tahun_ajaran_id', $tahun_ajaran_id) as $itemLevelKontribusi){
                //Cari tabel skor_mahasiswa / tabel pivot antara Level Kontribusi dengan Models\Mahasiswa
                $skor_mahasiswa = Models\SkorMahasiswa::where('level_kontribusi_id', $itemLevelKontribusi->id)
                ->where('mahasiswa_id', $mahasiswa->id)->first();
                //Kurang kolom skor pada tabel skor_mahasiswa
                if($skor_mahasiswa !== null){
                    $skor_mahasiswa->skor = $skor_mahasiswa->skor - 
                    round($nilai_pengurang / $countCpmk * ($persentase) / 
                    $itemCpmk->levelKontribusi
                    ->where('cpl.program_studi_id', $mahasiswa->program_studi_id)
                    ->where('tahun_ajaran_id', $tahun_ajaran_id)->count(), 2);
                    if($skor_mahasiswa->skor < 0){
                        $skor_mahasiswa->skor = 0;
                    }
                    $skor_mahasiswa->save();
                }
            }
        }
    }
}
