<?php

namespace App\Imports;

use App\Models;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Exception;

class KelasMahasiswaImport implements ToCollection, WithStartRow, WithMultipleSheets
{
    protected $id;
    protected $startRow;
 
    function __construct($id, $startRow) {
        $this->id = $id;
        $this->startRow = $startRow;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $program_studi = Models\ProgramStudi::all();
        $arrayMahasiswa = [];
        foreach ($rows as $row) {
            $mahasiswa = Models\Mahasiswa::where('nrp', $row[1])->first();
            if($mahasiswa === null){
                $mahasiswa = new Models\Mahasiswa();
                $mahasiswa->nrp = $row[1];
                $mahasiswa->nama = $row[2];
                $mahasiswa->tahun_masuk = '20' . substr($row[1],4,2);
                //Membedakan jenis kode NRP
                if(strlen($row[1]) == 9){
                    $mahasiswa->tahun_masuk = '20' . substr($row[1], 4, 2);
                    $mahasiswa->program_studi_id = $program_studi
                    ->firstWhere('kode', substr($row[1], 0, 4))->id;
                }
                else if(strlen($row[1]) == 7){
                    $mahasiswa->tahun_masuk = '20' . substr($row[1], 1, 2);
                    $mahasiswa->program_studi_id = $program_studi
                    ->firstWhere('kode', 0 . substr($row[1], 3, 1))->id;
                }
                else{
                    throw new Exception("Data Mahasiswa " . $row[1] . ' - ' . $row[2] . ' memiliki format NRP yang tidak dikenali. Bila ada format NRP baru, silakan hubungi admin');
                }
                $mahasiswa->save();
            }
            $arrayMahasiswa[] = $mahasiswa->id;
        }
        $kelas = Models\Kelas::find($this->id);
        $kelas->mahasiswa()->sync($arrayMahasiswa);
    }
    
    public function startRow(): int {
        return $this->startRow;
    }

    public function sheets(): array
    {
        return [
            0 => new KelasMahasiswaImport($this->id, $this->startRow)
        ];
    }
}