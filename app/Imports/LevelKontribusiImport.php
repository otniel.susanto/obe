<?php

namespace App\Imports;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Exception;

class LevelKontribusiImport implements ToCollection, WithStartRow
{
    protected $error;
    protected $message;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $this->error = false;
        $this->message = [];
        $program_studi = Models\ProgramStudi::firstWhere('nama', $rows[0][3]);
        $tahun_ajaran = Models\TahunAjaran::firstWhere('keterangan', $rows[1][3]);
        if($program_studi === null){
            throw new Exception("Program Studi " . $rows[0][3] . " tidak ditemukan");
        }
        $cpl = Models\Cpl::where('program_studi_id', $program_studi->id)->get();
        $level = Models\Level::all();
        
        #region Get All RPS yang Aktif
        $rps_all = Models\Rps::select([
            DB::raw('DISTINCT rps.id as id'),
            'mata_kuliah.kode as mata_kuliah_kode'
        ])
        ->join('mata_kuliah', 'mata_kuliah.id', '=', 'rps.mata_kuliah_id')
        ->where('tahun_ajaran_id', $tahun_ajaran->id)->get();
        #endregion
        #region Get All CPMK dari ID RPS yang aktif
        $arrayIdRps = [];
        foreach($rps_all as $item){
            $arrayIdRps[] = $item->id;
        }
        $cpmk = Models\Cpmk::select([
            DB::raw('DISTINCT cpmk.id as id'),
            'cpmk.kode as kode',
            'mata_kuliah.kode as kodeMk',
            'rps.tahun_ajaran_id as tahun_ajaran_id'
        ])
        ->join('rps', 'cpmk.rps_id', '=', 'rps.id')
        ->join('mata_kuliah', 'rps.mata_kuliah_id', '=', 'mata_kuliah.id')
        ->whereIn('cpmk.rps_id', $arrayIdRps)
        ->get();
        #endregion
        #region Get Semua Level Kontribusi pada Program Studi di Tahun Ajaran yang aktif
        $level_kontribusi_all = Models\LevelKontribusi::select([
            DB::raw('DISTINCT level_kontribusi.id as id'),
            'level_kontribusi.cpl_id as cpl_id',
            'level_kontribusi.tahun_ajaran_id as tahun_ajaran_id',
            'level_kontribusi.level_id as level_id',
            'level_kontribusi.skor_maks as skor_maks',
            'level_kontribusi.passing_grade as passing_grade',
            'level_kontribusi.created_at as created_at',
            'level_kontribusi.updated_at as updated_at',
            'cpl.kode as cpl_kode',
            'mata_kuliah.kode as mata_kuliah_kode'
        ])
        ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
        ->join('level_kontribusi_cpmk', 'level_kontribusi_cpmk.level_kontribusi_id', '=', 'level_kontribusi.id')
        ->join('cpmk', 'level_kontribusi_cpmk.cpmk_id', '=', 'cpmk.id')
        ->join('rps', 'cpmk.rps_id', '=', 'rps.id')
        ->join('mata_kuliah', 'rps.mata_kuliah_id', '=', 'mata_kuliah.id')
        ->join('program_studi', 'rps.program_studi_id', '=', 'program_studi.id')
        ->where('cpl.program_studi_id', $program_studi->id)
        ->where('level_kontribusi.tahun_ajaran_id', $tahun_ajaran->id)
        ->get();
        #endregion
        #region Looping tiap baris
        for($i = 7; $i < count($rows); $i++){
            $col = 0;

            if($rows[$i][3] != "" || $rows[$i][3] !== null){
                $rps = $rps_all->where('mata_kuliah_kode', $rows[$i][3])->first();
                #region Kalau RPS ada, di-save ke database
                if($rps !== null){
                    #region Loop tiap kolom CPL pada baris kode CPL
                    while(isset($rows[6][7+$col])){
                    #region Select Level Kontribusi berdasarkan Kode CPL dan Matkul pada tahun ajaran yang berlaku
                    $level_kontribusi = $level_kontribusi_all
                    ->where('cpl_kode', $rows[6][7+$col])
                    ->where('mata_kuliah_kode', $rows[$i][3])
                    ->first();
                    #endregion
                    #region Cek apakah kolom di bawah kode CPL punya data atau tidak
                    if(!empty($rows[$i][8+$col]) && !empty($rows[$i][7+$col])){
                        if(!(isset($level_kontribusi))){
                            $level_kontribusi = new Models\LevelKontribusi();
                        }
                        $level_kontribusi->cpl_id = $cpl->firstWhere('kode', $rows[6][7+$col])->id;
                        $level_kontribusi->level_id = $level->firstWhere('level', $rows[$i][7+$col])->id;
                        $level_kontribusi->tahun_ajaran_id = $tahun_ajaran->id;

                        #region Collect ID CPMK
                        $stringReplace = str_replace(" ", "", $rows[$i][8+$col]);
                        $arrayCpmk = explode(",", $stringReplace);
                        $arrIdCpmk = [];
                        foreach($arrayCpmk as $item){
                            $itemCpmk = $cpmk->where('kode', $item)
                            ->where('kodeMk', $rows[$i][3])->first();
                            $itemCPL = $rows[6][7+$col];
                            if(isset($itemCpmk)){
                                $arrIdCpmk[] = $itemCpmk->id;
                                $itemCPL = $rows[6][7+$col];
                            }
                            else{
                                $error = true;
                                $message[] = "Mata Kuliah " . $rows[$i][3] . ' - ' . $rows[$i][4] .
                                " tidak memiliki kode " . $item . " kolom " . $itemCPL . " <br> " . 
                                "pada RPS yang aktif (Cek file Excel atau lihat menu Help > Import Level Kontribusi)<br><br>";
                            }
                        }
                        #endregion

                        #region SAVE N-N level_kontribusi_cpmk
                        if(!(empty($arrIdCpmk))){
                            $level_kontribusi->save();
                            $level_kontribusi->cpmk()->sync($arrIdCpmk);
                        }
                        #endregion
                    }
                    #endregion
                    #region Ada level tetapi tidak ada kode CPMK
                    else if(empty($rows[$i][8+$col]) && !empty($rows[$i][7+$col])){
                        $error = true;
                        $message[] = "- Mata Kuliah " . $rows[$i][3] . ' - ' . $rows[$i][4] .
                        ' memiliki data Level pada matriks namun tidak memiliki data CPMK <br>' . 
                        '(Cek file Excel atau lihat menu Help > Import Level Kontribusi)<br><br>';
                    }
                    #endregion
                    #region Ada CPMK tetapi tidak ada Level
                    else if(empty($rows[$i][7+$col]) && !empty($rows[$i][8+$col])){
                        throw new Exception("Mata Kuliah " . $rows[$i][3] . ' - ' . $rows[$i][4] .
                        ' memiliki data CPMK pada matriks namun tidak memiliki data Level<br>' . 
                        'Silakan cek kembali file yang diupload dan pastikan kolom <br>' . 
                        'Jumlah CPL (Num of SO) memiliki angka bulat (1, 2, 3, dst)<br><br>' .
                        '*Proses import dibatalkan seluruhnya (tidak ada data yang berubah)');
                    }
                    #endregion

                    #region Delete bila tidak ada hubungan antara kode CPL dengan kode Mata Kuliah 
                    else{
                        if($level_kontribusi !== null){
                            if($level_kontribusi->skorMahasiswa->count() <= 0){
                                $level_kontribusi->cpmk()->sync([]);
                                $level_kontribusi->destroy($level_kontribusi->id);
                            }
                            else{

                            }
                        }
                    }
                    #endregion
                    $col += 2 ;
                }
                #endregion
                }
                #endregion
                #region Kalau RPS tidak ada, dilewati dan disimpan ke array nama MKnya
                else{
                    $this->error = true;
                    $this->message[] = "- Mata kuliah " . $rows[$i][3] . ' - ' . $rows[$i][4] . 
                    " belum memiliki data RPS. Silakan tambah data RPS pada menu Kurikulum > RPS <br><br>";
                }
                #endregion
            }
        }
        #endregion
    }

    public function startRow(): int {
        return 2;
    }

    public function getError(){
        return $this->error;
    }
    public function getMessage(){
        return $this->message;
    }
}
