<?php

namespace App\Imports;

use App\Models;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MataKuliahImport implements ToCollection, WithStartRow, WithMultipleSheets
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $program_studi = Models\ProgramStudi::all();
        $jenis_mata_kuliah = Models\JenisMataKuliah::all();
        foreach ($rows as $row) {
            $mata_kuliah = Models\MataKuliah::where('kode', $row[3])->first();
            if($mata_kuliah === null){
                $mata_kuliah = new Models\MataKuliah();
                $mata_kuliah->kode = $row[3];
            }
            $mata_kuliah->nama = $row[4];
            $mata_kuliah->sks = $row[5];
            $mata_kuliah->save();
            $program_studi_id = $program_studi->firstWhere('nama', $row[1])->id;
            $detailMataKuliah = Models\DetailMataKuliah::where('mata_kuliah_id', $mata_kuliah->id)
            ->where('program_studi_id', $program_studi_id)->first();
            if($detailMataKuliah === null){
                $detailMataKuliah = new Models\DetailMataKuliah();
                $detailMataKuliah->program_studi_id = $program_studi_id;
                $detailMataKuliah->mata_kuliah_id = $mata_kuliah->id;
            }
            $detailMataKuliah->alur_7_semester = $row[6];
            $detailMataKuliah->alur_8_semester = $row[7];
            $detailMataKuliah->jenis_mata_kuliah_id = $jenis_mata_kuliah->firstWhere('nama', $row[2])->id;
            $detailMataKuliah->save();
        }        
    }

    public function startRow(): int {
        return 8;
    }

    public function sheets(): array
    {
        return [
            0 => new MataKuliahImport()
        ];
    }
}
