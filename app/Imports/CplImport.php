<?php

namespace App\Imports;

use App\Models;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CplImport implements ToCollection, WithStartRow, WithMultipleSheets
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $kategori_cpl = Models\KategoriCpl::all();
        $program_studi = Models\ProgramStudi::where('nama', $rows[0][1])->first();
        //Loop tiap baris
        for($i = 3; $i < count($rows); $i++){
            $cpl = Models\Cpl::where('kode', $rows[$i][1])
            ->where('program_studi_id', $program_studi->id)
            ->first();
            if(!(isset($cpl))){
                $cpl = new Models\Cpl();
                $cpl->program_studi_id = $program_studi->id;
            }
            $cpl->kode = $rows[$i][1];
            $cpl->keterangan = $rows[$i][2];
            $cpl->kategori_cpl_id = $kategori_cpl->firstWhere('nama', $rows[$i][0])->id;
            $cpl->save();
        }        
    }

    public function startRow(): int {
        return 1;
    }

    public function sheets(): array
    {
        return [
            0 => new CplImport()
        ];
    }
}