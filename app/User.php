<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'npk', 'email', 'password', 'jabatan', 'status', 'program_studi_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function mataKuliah(){
        return $this->hasMany('App\Models\ModelsMataKuliah', 'dosen_pengampu_id', 'id');
    }
    public function verifikasi(){
        return $this->hasMany('App\Models\Verifikasi', 'verified_by', 'id');
    }
    public function dosenPenguji(){
        return $this->belongsToMany('App\Models\Assessment', 'dosen_penguji', 'dosen_id', 'assessment_id');
    }
    public function programStudi(){
        return $this->belongsToMany('App\Models\ProgramStudi', 'program_studi_dosen', 'dosen_id', 'program_studi_id');
    }
}
