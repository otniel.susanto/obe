<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;

use App\ExcelTemplates\AssessmentTemplate;
use App\Imports\NilaiImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Response;
use Session;

class AssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $tahun_ajaran = Models\TahunAjaran::orderBy('id', 'desc')->get();
        $program_studi = $user->programStudi;
        $assessment = Models\Assessment::select([
            DB::raw('DISTINCT assessment.id as id'),
            'assessment.aktivitas as aktivitas',
            'assessment.tanggal as tanggal',
            'assessment.komponen_penilaian_id as komponen_penilaian_id',
            'mata_kuliah.kode as mata_kuliah_kode',
            'kelas.program_studi_id as program_studi_id',
            'tahun_ajaran.id as tahun_ajaran_id',
        ])
        ->join('assessment_kelas', 'assessment_kelas.assessment_id', '=', 'assessment.id')
        ->join('kelas', 'assessment_kelas.kelas_id', '=', 'kelas.id')
        ->join('tahun_ajaran', 'kelas.tahun_ajaran_id', '=', 'tahun_ajaran.id')
        ->join('mata_kuliah', 'kelas.mata_kuliah_id', '=', 'mata_kuliah.id');
        if(isset($request->aktivitas) && $request->aktivitas != ""){
            $assessment = $assessment->where('assessment.aktivitas', 'LIKE', '%'. $request->aktivitas . '%');
        }
        if(isset($request->program_studi_id) && $request->program_studi_id != ""){
            $assessment = $assessment->where('kelas.program_studi_id', '=', $request->program_studi_id);
        }
        if(isset($request->nama_mata_kuliah) && $request->nama_mata_kuliah != ""){
            $assessment = $assessment->where('mata_kuliah.nama', 'LIKE', '%'.$request->nama_mata_kuliah.'%');
        }
        if(isset($request->kode_mata_kuliah) && $request->kode_mata_kuliah != ""){
            $assessment = $assessment->where('mata_kuliah.kode', 'LIKE', '%'.$request->kode_mata_kuliah.'%');
        }
        if(isset($request->tahun_ajaran_id) && $request->tahun_ajaran_id != ""){
            $assessment = $assessment->where('kelas.tahun_ajaran_id', $request->tahun_ajaran_id);
        }
        else{
            $assessment = $assessment->where('tahun_ajaran.status', 'Aktif');
        }
        $assessment = $assessment
        ->orderBy('kelas.program_studi_id','asc')
        ->orderBy('tahun_ajaran.id','desc')
        ->orderBy('mata_kuliah.kode','asc')
        ->orderBy('assessment.komponen_penilaian_id','asc')
        ->orderBy('assessment.aktivitas','asc')
        ->orderBy('assessment.tanggal','desc')
        ->get();
        // dd($assessment);
        return view('assessment.index',[
            'assessment' => $assessment,
            'program_studi' => $program_studi,
            'tahun_ajaran' => $tahun_ajaran,
            'request' => $request,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $taksonomi_bloom = Models\TaksonomiBloom::all();
        $mata_kuliah = Models\MataKuliah::join('detail_mata_kuliah', 'mata_kuliah.id', '=', 'detail_mata_kuliah.mata_kuliah_id')
        ->whereIn('detail_mata_kuliah.program_studi_id', $arrayIdProgramStudi);
        $dosen = User::select([
            DB::raw('DISTINCT users.id as id'),
            'users.name as name',
            'users.npk as npk'
        ])
        ->join('program_studi_dosen', 'users.id', '=', 'program_studi_dosen.dosen_id')
        ->whereIn('program_studi_dosen.program_studi_id', $arrayIdProgramStudi)
        ->whereIn('users.jabatan', [
            "Dosen","Kepala Laboratorium", 
            "Kepala Jurusan", "Koordinator Mata Kuliah", "Dekan", "Wakil Dekan"])
        ->get();
        $tahun_ajaran = Models\TahunAjaran::firstWhere('status', 'Aktif');
        $program_studi = $user->programStudi;
        return view('assessment.create',[
            'taksonomi_bloom' => $taksonomi_bloom,
            'mata_kuliah' => $mata_kuliah,
            'dosen' => $dosen,
            'tahun_ajaran' => $tahun_ajaran,
            'program_studi' => $program_studi,
            'assessment' => null,
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'mata_kuliah_id' => 'required',
                'komponen_penilaian_id' => 'required',
            ]);
            $tahun_ajaran = Models\TahunAjaran::find($request->tahun_ajaran_id);
            if($tahun_ajaran->status != "Aktif"){
                return response()->json([ 
                    'error' => true, 
                    'message' => "Anda tidak diijinkan untuk menambah data pada tahun ajaran yang sudah tidak aktif!"
                ], 500);
            }
            else{
                DB::beginTransaction();
                $mata_kuliah = Models\MataKuliah::find($request->mata_kuliah_id);
                $komponen_penilaian = Models\KomponenPenilaian::find($request->komponen_penilaian_id);
                #region Isi Kolom2 Assessment
                $assessment = new Models\Assessment();
                $assessment->komponen_penilaian_id = $komponen_penilaian->id;
                $assessment->aktivitas = $komponen_penilaian->aktivitas;
                $assessment->jenis_penilaian = $komponen_penilaian->jenisPenilaian->nama;
                $assessment->persentase = $komponen_penilaian->persentase;
                $assessment->kelompok = $komponen_penilaian->kelompokNilai->nama;
                if($komponen_penilaian->jenisPenilaian->nama == 'Ujian'){
                    $request->validate([
                        'tanggal' => 'required',
                        'durasi' => 'required',
                        'sifat' => 'required',
                        'jenis_soal' => 'required',
                    ]);
                }
                $assessment->jenis_soal = $request->jenis_soal;
                $assessment->tanggal = date("Y-m-d H:i:s", strtotime($request->tanggal . ' ' . $request->jam));
                $assessment->durasi = $request->durasi;
                $assessment->sifat = $request->sifat;
                #endregion

                #region Save File Soal dan Save Assessment
                if($request->file('file') !== null){
                    $this->validate($request, [
                        'file' => 'required|mimes:doc,docx,pdf,zip,rar'
                    ]);
                    $file = $request->file('file');
                    $nama_file = date('dmY-His') . '-' . $file->getClientOriginalName();
                    $file->move('file-soal', $nama_file);
                    $assessment->nama_file_soal = $nama_file;
                }
                $assessment->save();
                #endregion

                #region Cari Kelas
                $kelas = Models\Kelas::select('kelas.id')
                ->where('mata_kuliah_id', $request->mata_kuliah_id)
                // ->where('program_studi_id', $request->program_studi_id)
                ->where('tahun_ajaran.status', 'Aktif')
                ->join('tahun_ajaran', 'kelas.tahun_ajaran_id', 'tahun_ajaran.id')
                ->get();
                #endregion
                
                #region Menyiapkan data nilai mahasiswa dengan nilai 0
                $arrayMahasiswa = [];
                foreach($kelas as $itemKelas){
                    foreach($itemKelas->mahasiswa as $itemMahasiswa){
                        $nilai_mahasiswa = new Models\NilaiMahasiswa();
                        $nilai_mahasiswa->assessment_id = $assessment->id;
                        $nilai_mahasiswa->mahasiswa_id = $itemMahasiswa->id;
                        $nilai_mahasiswa->nilai = 0;
                        $nilai_mahasiswa->save();
                        $arrayMahasiswa[] = $itemMahasiswa->id;
                    }
                }
                $assessment->kelas()->sync($kelas);
                #endregion
                
                #region Save Isi Soal
                foreach(json_decode($request->detail, true) as $item){
                    $isi_soal = new Models\IsiSoal();
                    $isi_soal->assessment_id = $assessment->id;
                    $isi_soal->nomor_soal = $item['nomor_soal'];
                    $isi_soal->skor_maks = $item['skor_maks'];
                    $isi_soal->keterangan = $item['keterangan'];
                    $isi_soal->rubrik = $item['rubrik'];
                    if($item['taksonomi_bloom_id'] != "" && isset($item['taksonomi_bloom_id'])){
                        $isi_soal->taksonomi_bloom_id  = $item['taksonomi_bloom_id'];
                    }
                    $isi_soal->kompetensi = $item['kompetensi'];
                    $isi_soal->save();
                    //Mengisi tabel pivot antara isi_soal dengan cpmk
                    $isi_soal->cpmk()->sync($item['cpmk']);
                    //Hitung Skor Maksimal untuk Tabel Level Kontribusi
                    $this->tambahSkorMaksLevelKontribusi($assessment, $isi_soal, $request->tahun_ajaran_id);
                }
                #endregion

                #region Input tabel dosen_penguji
                $arrayDosen = [];
                foreach(json_decode($request->dosen_penguji, true) as $itemDosen){
                    $arrayDosen[] = $itemDosen['id'];
                }
                $assessment->dosenPenguji()->sync($arrayDosen);
                #endregion
                
                DB::commit();
            }
            return response()->json([ 
                'error' => false, 
                'message' => "Data assessment telah disimpan"
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $assessment = Models\Assessment::find($id);
        return view('assessment.show',[
            'assessment' => $assessment,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $taksonomi_bloom = Models\TaksonomiBloom::all();
        $dosen = User::select([
            DB::raw('DISTINCT users.id as id'),
            'users.name as name',
            'users.npk as npk'
        ])
        ->join('program_studi_dosen', 'users.id', '=', 'program_studi_dosen.dosen_id')
        ->whereIn('program_studi_dosen.program_studi_id', $arrayIdProgramStudi)
        ->whereIn('users.jabatan', [
            "Dosen","Kepala Laboratorium", 
            "Kepala Jurusan", "Koordinator Mata Kuliah", "Dekan", "Wakil Dekan"])
        ->get();
        $tahun_ajaran = Models\TahunAjaran::firstWhere('status', 'Aktif');
        $assessment = Models\Assessment::find($id);
        return view('assessment.edit',[
            'tahun_ajaran' => $tahun_ajaran,
            'taksonomi_bloom' => $taksonomi_bloom,
            'dosen' => $dosen,
            'assessment' => $assessment,
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $assessment = Models\Assessment::find($id);
            $assessment->jenis_soal = $request->jenis_soal;
            $assessment->tanggal = date("Y-m-d H:i:s", strtotime($request->tanggal . ' ' . $request->jam));
            $assessment->durasi = $request->durasi;
            $assessment->sifat = $request->sifat;
            //Save File Soal
            if($request->file('file') !== null){
                $this->validate($request, [
                    'file' => 'required|mimes:doc,docx,pdf'
                ]);
                $file = $request->file('file');
                $nama_file = date('dmY-His') . '-' . $file->getClientOriginalName();
                $file->move('file-soal', $nama_file);
                $assessment->nama_file_soal = $nama_file;
            }
            $assessment->save();
            $kelas = Models\Kelas::select('kelas.id')
            ->where('mata_kuliah_id', $request->mata_kuliah_id)
            // ->where('program_studi_id', $request->program_studi_id)
            ->where('tahun_ajaran_id', $request->tahun_ajaran_id)
            ->get();
            #region Menyiapkan data nilai mahasiswa dengan nilai 0
            $arrayMahasiswa = [];
            foreach($kelas as $itemKelas){
                foreach($itemKelas->mahasiswa as $itemMahasiswa){
                    $nilai_mahasiswa = Models\NilaiMahasiswa::where('assessment_id', $assessment->id)
                    ->where('mahasiswa_id', $itemMahasiswa->id)->first();
                    if($nilai_mahasiswa === null){
                        $nilai_mahasiswa = new Models\NilaiMahasiswa();
                        $nilai_mahasiswa->assessment_id = $assessment->id;
                        $nilai_mahasiswa->mahasiswa_id = $itemMahasiswa->id;
                        $nilai_mahasiswa->nilai = 0;
                        $nilai_mahasiswa->save();
                    }
                    $arrayMahasiswa[] = $itemMahasiswa->id;
                }
            }
            $assessment->kelas()->sync($kelas);
            #endregion
            
            #region Delete Isi Soal
            foreach(json_decode($request->isi_soal_delete) as $item){
                $isi_soal = Models\IsiSoal::find($item->id);
                $this->kurangSkorMaksLevelKontribusi($assessment, $isi_soal, $request->tahun_ajaran_id);
                $isi_soal->cpmk()->sync([]);
                $isi_soal = Models\IsiSoal::destroy($item->id);
            }
            #endregion
            #region Update Isi Soal
            foreach(json_decode($request->isi_soal_update) as $item){
                $isi_soal = Models\IsiSoal::find($item->id);
                $this->kurangSkorMaksLevelKontribusi($assessment, $isi_soal, $request->tahun_ajaran_id);
                $isi_soal->nomor_soal = $item->nomor_soal;
                $isi_soal->skor_maks = $item->skor_maks;
                $isi_soal->keterangan = $item->keterangan;
                $isi_soal->rubrik = $item->rubrik;
                if($item->taksonomi_bloom_id != "" && isset($item->taksonomi_bloom_id)){
                    $isi_soal->taksonomi_bloom_id  = $item->taksonomi_bloom_id;
                }
                $isi_soal->kompetensi = $item->kompetensi ?? null;
                $isi_soal->save();
                //Mengisi tabel pivot antara isi_soal dengan cpmk
                $isi_soal->cpmk()->sync($item->cpmk);
                //Hitung Skor Maksimal untuk Tabel Level Kontribusi
                $this->tambahSkorMaksLevelKontribusi($assessment, $isi_soal, $request->tahun_ajaran_id);
            }
            #endregion
            #region Save Isi Soal Baru
            foreach(json_decode($request->isi_soal_create) as $item){
                $isi_soal = new Models\IsiSoal();
                $isi_soal->assessment_id = $assessment->id;
                $isi_soal->nomor_soal = $item->nomor_soal;
                $isi_soal->skor_maks = $item->skor_maks;
                $isi_soal->keterangan = $item->keterangan;
                $isi_soal->rubrik = $item->rubrik;
                if($item->taksonomi_bloom_id != "" && isset($item->taksonomi_bloom_id)){
                    $isi_soal->taksonomi_bloom_id  = $item->taksonomi_bloom_id;
                }
                $isi_soal->kompetensi = $item->kompetensi ?? null;
                $isi_soal->save();
                //Mengisi tabel pivot antara isi_soal dengan cpmk
                $isi_soal->cpmk()->sync($item->cpmk);
                //Hitung Skor Maksimal untuk Tabel Level Kontribusi
                $this->tambahSkorMaksLevelKontribusi($assessment, $isi_soal, $request->tahun_ajaran_id);
            }
            #endregion
            
            #region Hapus Dosen Penguji
            foreach(json_decode($request->dosen_penguji_delete) as $itemDosen){
                $assessment->dosenPenguji()->detach($itemDosen->id);
            }
            #endregion
            #region Tambah Dosen Penguji
            foreach(json_decode($request->dosen_penguji_create) as $itemDosen){
                $assessment->dosenPenguji()->attach($itemDosen->id);
            }
            #endregion
            DB::commit();
            return response()->json([ 
                'error' => false, 
                'message' => "Data assessment telah diubah"
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $assessment = Models\Assessment::find($id);
            $kelas = $assessment->kelas;
            $nilai_mahasiswa = $assessment->nilaiMahasiswa;
            foreach($assessment->isiSoal as $itemIsiSoal){
                $this->kurangSkorMaksLevelKontribusi($assessment, $itemIsiSoal, $assessment->kelas[0]->tahun_ajaran_id);
                foreach($kelas as $itemKelas){
                    foreach($itemKelas->mahasiswa as $itemMahasiswa){
                        $itemNilaiMahasiswa = $nilai_mahasiswa->firstWhere('mahasiswa_id', $itemMahasiswa->id);
                        if($itemNilaiMahasiswa){
                            $itemDetailNilai = $itemNilaiMahasiswa->isiSoal()->firstWhere('id', $itemIsiSoal->id);
                            if($itemDetailNilai){
                                $this->kurangSkorMahasiswaLevelKontribusi($itemIsiSoal, $itemMahasiswa, $kelas[0]->tahunAjaran->id,
                                $itemDetailNilai->pivot->nilai);
                            }
                        }
                    }
                }
                $itemIsiSoal->cpmk()->sync([]);
                $itemIsiSoal->nilaiMahasiswa()->sync([]);
            }
            $assessment->kelas()->sync([]);
            $assessment->dosenPenguji()->sync([]);
            $assessment->isiSoal()->delete();
            $assessment->nilaiMahasiswa()->delete();
            $assessment = Models\Assessment::destroy($id);
            DB::commit();
            return response()->json([ 
                'error' => false, 
                'message' => "Data assessment telah dihapus", 
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([ 
                'error' => false, 
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function updateUrlJawaban(Request $request, $id){
        try{
            $this->validate($request, [
                'url_jawaban_mahasiswa' => 'required|url'
            ]);
            $assessment = Models\Assessment::find($id);
            $assessment->url_jawaban_mahasiswa = $request->url_jawaban_mahasiswa;
            $assessment->save();
            return response()->json([ 
                'error' => false, 
                'message' => "URL jawaban mahasiswa telah diubah", 
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([ 
                'error' => false, 
                'message' => $exception->getMessage()
            ], 500);
        }
    }
    public function downloadSoal($id){
        $assessment = Models\Assessment::find($id);
        $filePath = public_path(). "/file-soal/" . $assessment->nama_file_soal;
        return Response::download($filePath);
    }
    public function downloadFileNilai($id){
        $assessment = Models\Assessment::find($id);
        $filePath = public_path(). "/file-nilai/" . $assessment->nama_file_penilaian;
        return Response::download($filePath);
    }

    public function template(Request $request)
    {
        $assessment = Models\Assessment::find($request->id);
        $kelas = $assessment->kelas[0];
        $komponen_penilaian = $assessment->komponenPenilaian;
        $mata_kuliah = $kelas->mataKuliah;
        $tahun_ajaran = $kelas->tahunAjaran;
        return Excel::download(new AssessmentTemplate($request->id), 
        'Template Nilai ' . $mata_kuliah->nama . ' (' . $mata_kuliah->kode . ') - ' .
        $komponen_penilaian->aktivitas . ' - ' . str_replace('/', '_', $tahun_ajaran->keterangan) . '.xlsx');
    }

    public function getKomponenMk(Request $request)
    {
        $kelas = Models\Kelas::where('mata_kuliah_id', $request->mata_kuliah_id)
        ->where('tahun_ajaran_id', $request->tahun_ajaran_id)
        ->first();
        $komponen_penilaian = null;
        if($kelas !== null){
            $komponen_penilaian = Models\Kelas::select([
                DB::raw('DISTINCT komponen_penilaian.*'),
                'kelompok_nilai.nama as kelompok_nilai_nama'
            ])
            ->join('rps', 'rps.id', '=', 'kelas.rps_id')
            ->join('komponen_penilaian', 'rps.id', '=', 'komponen_penilaian.rps_id')
            ->join('kelompok_nilai', 'kelompok_nilai.id', '=', 'komponen_penilaian.kelompok_nilai_id')
            ->leftJoin('assessment', 'assessment.komponen_penilaian_id', '=', 'komponen_penilaian.id')
            ->where('rps.id', $kelas->rps_id)
            ->whereNull('assessment.id')
            ->get();
        }
        return response()->json([ 
            'error' => false,
            'komponen_penilaian' => $komponen_penilaian,
            'mahasiswa' => $kelas->mahasiswa[0] ?? null
        ], 200);
    }

    public function getCpmk(Request $request){
        $rps = Models\Rps::where('mata_kuliah_id', $request->mata_kuliah_id)
        ->where('tahun_ajaran_id', $request->tahun_ajaran_id)
        ->first();
        $cpmk = Models\Cpmk::select([
            DB::raw('DISTINCT id as id'),
            'kode as kode',
            'keterangan as keterangan'
        ])
        ->where('rps_id', $rps->id);
        if(isset($request->cpmk)){
            $cpmk = $cpmk->where(function ($q) use($request){
                foreach($request->cpmk as $key=>$item){
                    if($key == 0){
                        $q = $q->where('id', $item);
                    }
                    else{
                        $q = $q->orWhere('id', $item);
                    }
                }
            });
        }
        $cpmk = $cpmk->get();
        $cpl = Models\Cpmk::select([
            DB::raw('DISTINCT cpl.kode as kodeCpl'),
            'cpmk.kode as kodeCpmk',
            'level.keterangan as keterangan'
        ])
        ->join('level_kontribusi_cpmk', 'level_kontribusi_cpmk.cpmk_id', '=', 'cpmk.id')
        ->join('level_kontribusi', 'level_kontribusi_cpmk.level_kontribusi_id', '=', 'level_kontribusi.id')
        ->join('level', 'level.id', '=', 'level_kontribusi.level_id')
        ->join('cpl', 'level_kontribusi.cpl_id', '=', 'cpl.id')
        ->where('cpmk.rps_id', $rps->id)
        ->where('level_kontribusi.tahun_ajaran_id', $request->tahun_ajaran_id)
        ->get();
        
        return response()->json([ 
            'error' => false,
            'cpmk' => $cpmk,
            'cpl' => $cpl,
        ], 200);
    }
    
    public function importNilai(Request $request) {
        try{
            $this->validate($request, [
                'file' => 'required|mimes:xlsx',
                'assessment_id' => 'required'
            ]);
            $assessment = Models\Assessment::find($request->assessment_id);
            //Proses import
            Excel::import(new NilaiImport($request->assessment_id), $request->file('file'));
            //Simpan URL Google Drive Jawaban Mahasiswa
            if($request->url !== null && $request->url != ""){
                $assessment->url_jawaban_mahasiswa = $request->url;
            }
            #region Save File Soal dan Save Assessment
            $file = $request->file('file');
            $nama_file = $file->getClientOriginalName();
            $file->move('file-nilai', $nama_file);
            $assessment->nama_file_penilaian = $nama_file;
            $assessment->save();
            #endregion
            return response()->json([ 
                'error' => false, 
                'message' => 'Data nilai assessment berhasil diimport', 
            ],200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }

    public function getDosen(Request $request){
        $dosen = User::find($request->dosen_penguji_id);
        return response()->json([ 
            'error' => false,
            'dosen' => $dosen,
        ], 200);
    }

    private function tambahSkorMaksLevelKontribusi($assessment, $isi_soal, $tahun_ajaran_id){
        foreach($isi_soal->cpmk as $itemCpmk){
            foreach($itemCpmk->levelKontribusi->groupBy('cpl.program_studi_id') as $itemGroupBy){
                foreach($itemGroupBy->where('tahun_ajaran_id', $tahun_ajaran_id) as $itemLevelKontribusi){
                    $itemLevelKontribusi->skor_maks += round($isi_soal->skor_maks / $isi_soal->cpmk->count() * 
                    ($assessment->persentase / 100 * $assessment->komponenPenilaian->kelompokNilai->persentase / 100) / 
                    $itemGroupBy->count() ,2);
                    $itemLevelKontribusi->save();
                }
            }
        }
    }
    private function kurangSkorMaksLevelKontribusi($assessment, $isi_soal, $tahun_ajaran_id){
        foreach($isi_soal->cpmk as $itemCpmk){
            foreach($itemCpmk->levelKontribusi->groupBy('cpl.program_studi_id') as $itemGroupBy){
                foreach($itemGroupBy->where('tahun_ajaran_id', $tahun_ajaran_id) as $itemLevelKontribusi){
                    $itemLevelKontribusi->skor_maks = $itemLevelKontribusi->skor_maks - round($isi_soal->skor_maks / $isi_soal->cpmk->count() * 
                    ($assessment->persentase / 100 * $assessment->komponenPenilaian->kelompokNilai->persentase / 100) / 
                    $itemGroupBy->count(),2);
                    if($itemLevelKontribusi->skor_maks < 0){
                        $itemLevelKontribusi->skor_maks = 0 ;
                    }
                    $itemLevelKontribusi->save();
                }
            }
        }
    }

    private function kurangSkorMahasiswaLevelKontribusi($isi_soal, $mahasiswa, $tahun_ajaran_id, $nilai_pengurang){
        $persentase = $isi_soal->assessment->komponenPenilaian->persentase / 100 * 
        $isi_soal->assessment->komponenPenilaian->kelompokNilai->persentase / 100;
        foreach($isi_soal->cpmk as $itemCpmk){
            $countCpmk = $isi_soal->cpmk->count();
            foreach($itemCpmk->levelKontribusi
            ->where('cpl.program_studi_id', $mahasiswa->program_studi_id)
            ->where('tahun_ajaran_id', $tahun_ajaran_id) as $itemLevelKontribusi){
                //Cari tabel skor_mahasiswa / tabel pivot antara Level Kontribusi dengan Models\Mahasiswa
                $skor_mahasiswa = Models\SkorMahasiswa::where('level_kontribusi_id', $itemLevelKontribusi->id)->where('mahasiswa_id', $mahasiswa->id)->first();
                //Tambah kolom skor pada tabel skor_mahasiswa
                if($skor_mahasiswa !== null){
                    $skor_mahasiswa->skor = $skor_mahasiswa->skor - 
                    round($nilai_pengurang / $countCpmk * ($persentase) / 
                    $itemCpmk->levelKontribusi
                    ->where('cpl.program_studi_id', $mahasiswa->program_studi_id)
                    ->where('tahun_ajaran_id', $tahun_ajaran_id)->count(), 2);
                    if($skor_mahasiswa->skor < 0){
                        $skor_mahasiswa->skor = 0;
                    }
                    $skor_mahasiswa->save();
                }
            }
        }
    }

    private function JumlahCPL($mata_kuliah_id, $tahun_ajaran_id){
        //Cari Jumlah CPL yang terhubung ke RPS Matkulnya
        $level_kontribusi = Models\LevelKontribusi::select([
            DB::raw('DISTINCT cpl.id'),
        ])
        ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
        ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
        ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
        ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
        ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
        // ->where('rps.program_studi_id', $program_studi_id)
        ->where('rps.mata_kuliah_id', $mata_kuliah_id)
        ->where('level_kontribusi.tahun_ajaran_id', $tahun_ajaran_id)
        ->where('kelas.tahun_ajaran_id', $tahun_ajaran_id);
        return $level_kontribusi->count();
    }
}
