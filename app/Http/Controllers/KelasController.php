<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\Imports\KelasMahasiswaImport;
use App\Imports\MahasiswaPesertaKelasParalelImport;
use Illuminate\Support\Facades\Auth;

use App\Models;
use DB;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $kelas = Models\Kelas::select([
            DB::raw('DISTINCT kelas.id as id'),
            'kelas.tahun_ajaran_id as tahun_ajaran_id',
            'kelas.program_studi_id as program_studi_id',
            'kelas.mata_kuliah_id as mata_kuliah_id',
            'kelas.rps_id as rps_id',
            'kelas.kp as kp',
            'mata_kuliah.kode as mata_kuliah_kode'
        ])
        ->join('rps', 'kelas.rps_id', '=', 'rps.id')
        ->join('mata_kuliah', 'kelas.mata_kuliah_id', '=', 'mata_kuliah.id')
        ->join('detail_mata_kuliah', 'detail_mata_kuliah.mata_kuliah_id', '=', 'mata_kuliah.id')
        ->join('program_studi', 'kelas.program_studi_id', '=', 'program_studi.id')
        ->join('tahun_ajaran', 'kelas.tahun_ajaran_id', '=', 'tahun_ajaran.id')
        ->whereIn('kelas.program_studi_id', $arrayIdProgramStudi);
        if(isset($request->kode_mata_kuliah) && $request->kode_mata_kuliah != ""){
            $kelas = $kelas->where('mata_kuliah.kode', 'LIKE', '%' . $request->kode_mata_kuliah . '%');
        }
        if(isset($request->nama_mata_kuliah) && $request->nama_mata_kuliah != ""){
            $kelas = $kelas->where('mata_kuliah.nama', 'LIKE', '%' . $request->nama_mata_kuliah . '%');
        }
        if(isset($request->program_studi_id) && $request->program_studi_id != ""){
            $kelas = $kelas->where('detail_mata_kuliah.program_studi_id', $request->program_studi_id);
        }
        if(isset($request->tahun_ajaran_id) && $request->tahun_ajaran_id != ""){
            $kelas = $kelas->where('kelas.tahun_ajaran_id', $request->tahun_ajaran_id);
        }
        else{
            $kelas = $kelas->where('tahun_ajaran.status', 'Aktif');
        }
        if(isset($request->kp) && $request->kp != ""){
            $kelas = $kelas->where('kelas.kp', 'LIKE', $request->kp . '%');
        }
        $kelas = $kelas
        ->orderBy('mata_kuliah.kode')
        ->orderBy('kelas.kp')
        ->get();
        $program_studi = $user->programStudi;
        $tahun_ajaran = Models\TahunAjaran::orderBy('id', 'desc')->get();
        return view('kelas.index',[
            'kelas' => $kelas,
            'tahun_ajaran' => $tahun_ajaran,
            'program_studi' => $program_studi,
            'request' => $request,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $tahun_ajaran = Models\TahunAjaran::where('status', 'Aktif')->first();
        $program_studi = $user->programStudi;
        return view('kelas.create',[
            'tahun_ajaran' => $tahun_ajaran,
            'program_studi' => $program_studi,
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'kp' => 'required',
                'rps_id' => 'required',
                'tahun_ajaran_id' => 'required'
            ]);
            DB::beginTransaction();
            #region Cari RPS
            $rps = Models\Rps::find($request->rps_id);
            if($rps->tahun_ajaran_id != $request->tahun_ajaran_id){
                $rps->tahun_ajaran_id = $request->tahun_ajaran_id;
                $rps->save();
            }
            #endregion
            #region Buat Kelas Paralel Baru
            if(isset($request->kp)){
                foreach(json_decode($request->kp) as $item){
                    $kelas = Models\Kelas::where('rps_id', $rps->id)
                    ->where('mata_kuliah_id', $rps->mata_kuliah_id)
                    ->where('tahun_ajaran_id', $request->tahun_ajaran_id)
                    ->where('kp', $item->kp)
                    ->first();
                    if($kelas === null){
                        $kelas = new Models\Kelas();
                        $kelas->rps_id = $rps->id;
                        $kelas->program_studi_id = $rps->program_studi_id;
                        $kelas->mata_kuliah_id = $rps->mata_kuliah_id;
                        $kelas->tahun_ajaran_id = $request->tahun_ajaran_id;
                    }
                    $kelas->kp = $item->kp;
                    $kelas->save();
                }
            }
            #endregion
            #region Memasukkan data mahasiswa bila ada
            if($request->file('file') !== null){
                $file = $request->file('file');
                $namaFile = $file->getClientOriginalName();
                $extension = pathinfo($namaFile, PATHINFO_EXTENSION);
                $startRow = 3;
                if($extension == "csv"){
                    $startRow = 2;
                }
                Excel::import(new MahasiswaPesertaKelasParalelImport(
                    $rps->program_studi_id, 
                    $rps->mata_kuliah_id, 
                    $request->tahun_ajaran_id , $startRow
                ), $request->file('file'));
            }
            #endregion

            #region Perbarui Level Kontribusi bila Tahun Ajaran berbeda (lebih baru)
            $tahun_ajaran_terakhir = $rps->cpmk[0]->levelKontribusi
            ->sortByDesc('tahun_ajaran_id')->first() ? $rps->cpmk[0]->levelKontribusi
            ->sortByDesc('tahun_ajaran_id')->first()->tahun_ajaran_id : null;
            foreach($rps->cpmk as $itemCpmk){
                foreach($itemCpmk->levelKontribusi->where('tahun_ajaran_id', $tahun_ajaran_terakhir) as $itemlevelKontribusi){
                    if($itemCpmk->levelKontribusi->sortByDesc('tahun_ajaran_id')
                    ->where('cpl_id', $itemlevelKontribusi->cpl_id)->first()
                    ->tahun_ajaran_id < $request->tahun_ajaran_id){
                        $itemlevelKontribusi->clone($request->tahun_ajaran_id);
                    }
                }
            }
            #endregion
            DB::commit();
            return response()->json([ 
                'error' => false, 
                'message' => "Data kelas telah disimpan", 
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $kelas = Models\Kelas::find($id);
        $mahasiswa = Models\Mahasiswa::all();
        return view('kelas.show',[
            'kelas' => $kelas,
            'mahasiswa' => $mahasiswa,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $kelas = Models\Kelas::find($id);
        $program_studi = Models\ProgramStudi::find($kelas->program_studi_id);
        $mata_kuliah = Models\MataKuliah::find($kelas->mata_kuliah_id);
        $tahun_ajaran = Models\TahunAjaran::find($kelas->tahun_ajaran_id);
        return response()->json([ 
            'error' => false, 
            'kelas' => $kelas,
            'program_studi' => $program_studi,
            'mata_kuliah' => $mata_kuliah,
            'tahun_ajaran' => $tahun_ajaran,
            'user' => $user
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kp' => 'required',
            'mata_kuliah_id' => 'required',
            'tahun_ajaran_id' => 'required'
        ]);
        $kelas = Models\Kelas::find($id);
        $kelas->kp = $request->kp;
        $kelas->mata_kuliah_id = $request->mata_kuliah_id;
        $kelas->tahun_ajaran_id = $request->tahun_ajaran_id;
        $kelas->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data kelas telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Models\Kelas::find($id);
        if($kelas->assessment()->exists()){
            return response()->json([ 
                'error' => false, 
                'message' => "Data kelas tidak dapat dihapus karena memiliki data assessment", 
            ], 200);
        }
        $kelas->mahasiswa()->sync([]);
        $kelas = Models\Kelas::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data kelas telah dihapus", 
        ], 200);
    }

    public function tambahMahasiswa(Request $request){
        try{
            $kelas = Models\Kelas::find($request->kelas_id);
            $kelas->mahasiswa()->attach($request->mahasiswa_id);
            return response()->json([ 
                'error' => false, 
                'message' => "Data mahasiswa telah masuk ke dalam kelas", 
            ], 200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->message, 
            ], 500);
        }
    }

    public function kelasRps(Request $request){
        if($request->mata_kuliah_id !== null || $request->mata_kuliah_id != ""){
            $mata_kuliah = Models\MataKuliah::find($request->mata_kuliah_id);
            $rps = Models\Rps::where('mata_kuliah_id', $request->mata_kuliah_id)
            ->orderBy('id', 'desc')
            ->first();
            if($rps === null){
                return response()->json([ 
                    'error' => true, 
                    'message' => "RPS tidak ditemukan untuk mata kuliah " . $mata_kuliah->kode . ' - ' . $mata_kuliah->nama, 
                ], 500);
            }
            return response()->json([ 
                'error' => false, 
                'rps' => $rps, 
            ], 200);
        }else{
            return response()->json([ 
                'error' => true,
                'message' => "Pilih mata kuliah terlebih dahulu"
            ], 200);
        }
    }
    public function deleteMahasiswa(Request $request){
        try{
            $kelas = Models\Kelas::find($request->kelas_id);
            $kelas->mahasiswa()->detach($request->mahasiswa_id);
            return response()->json([ 
                'error' => false, 
                'message' => "Data mahasiswa telah terhapus dari kelas", 
            ], 200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->message, 
            ], 500);
        }
    }
    public function deleteAllMahasiswa(Request $request){
        try{
            $kelas = Models\Kelas::find($request->kelas_id);
            $kelas->mahasiswa()->sync([]);
            return response()->json([ 
                'error' => false, 
                'message' => "Data semua mahasiswa telah terhapus dari kelas ini", 
            ], 200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->message, 
            ], 500);
        }
    }
    public function importMahasiswa(Request $request){
        try{
            // validasi
            $this->validate($request, [
                'file' => 'required|file|mimes:csv,xls,xlsx,txt'
            ]);
            $file = $request->file('file');
            $namaFile = $file->getClientOriginalName();
            $extension = pathinfo($namaFile, PATHINFO_EXTENSION);
            $startRow = 3;
            if($extension == "csv"){
                $startRow = 2;
            }
            Excel::import(new KelasMahasiswaImport($request->kelas_id, $startRow), $request->file('file'));

            return redirect("kelas/$request->kelas_id");
        }catch(\ErrorException $error){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Data tidak ditemukan di dalam database<br>", 
            ], 500);
        }catch(\Illuminate\Database\QueryException $exception){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Data tidak ditemukan di dalam database<br>", 
            ], 500);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }
}
