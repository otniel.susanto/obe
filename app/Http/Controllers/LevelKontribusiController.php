<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use App\ExcelTemplates\LevelKontribusiTemplate;
use App\Imports\LevelKontribusiImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Response;
use Session;

class LevelKontribusiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $program_studi = $user->programStudi;
        $tahun_ajaran = Models\TahunAjaran::orderBy('id','desc')->get();
        $level = Models\Level::all();
        $cpl = Models\Cpl::select([
            DB::raw('DISTINCT cpl.id as id'),
            'cpl.kode as kode'
        ])
        ->join('kategori_cpl', 'kategori_cpl.id', 'cpl.kategori_cpl_id')
        ->join('jenis_cpl', 'jenis_cpl.id', 'kategori_cpl.jenis_cpl_id')
        ->orderBy('cpl.id');
        $jenis_cpl = Models\JenisCpl::select([
            'jenis_cpl.nama as nama', 
            DB::raw('COUNT(*) as jumlah')
        ])
        ->join('kategori_cpl', 'kategori_cpl.jenis_cpl_id', 'jenis_cpl.id')
        ->join('cpl', 'cpl.kategori_cpl_id', 'kategori_cpl.id')
        ->groupBy('jenis_cpl.id', 'jenis_cpl.nama')
        ->orderBy('jenis_cpl.id');
        $mata_kuliah = Models\MataKuliah::join('detail_mata_kuliah', 'mata_kuliah.id', '=', 'detail_mata_kuliah.mata_kuliah_id')
        ->join('rps', 'mata_kuliah.id', '=', 'rps.mata_kuliah_id')
        ->join('cpmk', 'rps.id', '=', 'cpmk.rps_id')
        ->join('level_kontribusi_cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
        ->join('level_kontribusi', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
        ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
        ->join('program_studi', 'program_studi.id', '=', 'detail_mata_kuliah.program_studi_id')
        ->join('tahun_ajaran', 'level_kontribusi.tahun_ajaran_id', '=', 'tahun_ajaran.id');
        $level_kontribusi = Models\LevelKontribusi::select([
            DB::raw('DISTINCT level_kontribusi.id as id'),
            'cpl.id as cpl_id',
            'level_kontribusi.level_id as level_id',
            'mata_kuliah.id as mata_kuliah_id',
            'level_kontribusi.skor_maks as skor_maks',
        ])
        ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
        ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
        ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
        ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
        ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'level_kontribusi.tahun_ajaran_id')
        ->join('mata_kuliah', 'mata_kuliah.id', '=', 'rps.mata_kuliah_id')
        ->join('detail_mata_kuliah', 'mata_kuliah.id', '=', 'detail_mata_kuliah.mata_kuliah_id');
        //Filter Program Studi
        if(isset($request->program_studi_id) && $request->program_studi_id != ""){
            $mata_kuliah = $mata_kuliah->where('cpl.program_studi_id', '=', $request->program_studi_id);
            $level_kontribusi = $level_kontribusi->where('cpl.program_studi_id', '=', $request->program_studi_id);
            $cpl = $cpl->where('cpl.program_studi_id', $request->program_studi_id);
            $jenis_cpl = $jenis_cpl->where('cpl.program_studi_id', $request->program_studi_id);
        }
        else{
            $mata_kuliah = $mata_kuliah->where('cpl.program_studi_id', '=', $user->programStudi[0]->id);
            $level_kontribusi = $level_kontribusi->where('cpl.program_studi_id', '=', $user->programStudi[0]->id);
            $cpl = $cpl->where('cpl.program_studi_id', $user->programStudi[0]->id);
            $jenis_cpl = $jenis_cpl->where('cpl.program_studi_id', $user->programStudi[0]->id);
        }
        //Filter Tahun Ajaran
        if(isset($request->tahun_ajaran_id) && $request->tahun_ajaran_id != ""){
            $mata_kuliah = $mata_kuliah->where('level_kontribusi.tahun_ajaran_id', '=', $request->tahun_ajaran_id);
            $level_kontribusi = $level_kontribusi->where('level_kontribusi.tahun_ajaran_id', '=', $request->tahun_ajaran_id);
        }
        //Tahun Ajaran tidak difilter, memilih Tahun Ajaran yang aktif saja
        else{
            $mata_kuliah = $mata_kuliah->where('tahun_ajaran.status', '=', 'Aktif');
            $level_kontribusi = $level_kontribusi->where('tahun_ajaran.status', '=', 'Aktif');
        }
        //Filter Nama Mata Kuliah (use LIKE)
        if(isset($request->nama_mata_kuliah) && $request->nama_mata_kuliah != ""){
            $mata_kuliah = $mata_kuliah->where('mata_kuliah.nama', 'LIKE', '%' . $request->nama_mata_kuliah . '%');
            $level_kontribusi = $level_kontribusi->where('mata_kuliah.nama', 'LIKE', '%' . $request->nama_mata_kuliah . '%');
        }
        //Filter Kode Mata Kuliah (use LIKE)
        if(isset($request->kode_mata_kuliah) && $request->kode_mata_kuliah != ""){
            $mata_kuliah = $mata_kuliah->where('mata_kuliah.kode', 'LIKE', '%' . $request->kode_mata_kuliah . '%');
            $level_kontribusi = $level_kontribusi->where('mata_kuliah.kode', 'LIKE', '%' . $request->kode_mata_kuliah . '%');
        }
        //Filter Checkbox Alur 7 Semester OFF / Null
        if($request->alur_7_semester === null){
            $mata_kuliah = $mata_kuliah->select([
                DB::raw('DISTINCT mata_kuliah.id as id'),
                'mata_kuliah.kode as kode',
                'mata_kuliah.nama as nama',
                'mata_kuliah.sks as sks',
                'detail_mata_kuliah.alur_8_semester as semester'
            ]);
            //Filter angka semester
            if(isset($request->semester) && $request->semester != ""){
                $mata_kuliah = $mata_kuliah->where('detail_mata_kuliah.alur_8_semester', '=', $request->semester)
                ->orderBy('detail_mata_kuliah.alur_8_semester');
                $level_kontribusi = $level_kontribusi->where('detail_mata_kuliah.alur_8_semester', '=', $request->semester)
                ->orderBy('detail_mata_kuliah.alur_8_semester');
            }
        }
        //Filter Checkbox Alur 7 Semester ON
        else if($request->alur_7_semester == "on"){
            $mata_kuliah = $mata_kuliah->select([
                DB::raw('DISTINCT mata_kuliah.id as id'),
                'mata_kuliah.kode as kode',
                'mata_kuliah.nama as nama',
                'mata_kuliah.sks as sks',
                'detail_mata_kuliah.alur_7_semester as semester'
            ]);
            //Filter angka semester
            if(isset($request->semester) && $request->semester != ""){
                $mata_kuliah = $mata_kuliah->where('detail_mata_kuliah.alur_7_semester', '=', $request->semester)
                ->orderBy('detail_mata_kuliah.alur_7_semester');
                $level_kontribusi = $level_kontribusi->where('detail_mata_kuliah.alur_7_semester', '=', $request->semester)
                ->orderBy('detail_mata_kuliah.alur_7_semester');;
            }
        }
        $mata_kuliah = $mata_kuliah
        ->orderBy('mata_kuliah.kode')
        ->get();
        $level_kontribusi = $level_kontribusi->get();
        $cpl = $cpl->get();
        $jenis_cpl = $jenis_cpl->get();
        // $assessment = Models\Assessment::find(3);
        // $isi_soal = $assessment->isiSoal;
        // foreach($isi_soal as $itemIsiSoal){
        //     foreach($itemIsiSoal->cpmk as $itemCpmk){
        //         foreach($itemCpmk->levelKontribusi->groupBy('cpl.program_studi_id') as $itemGroupBy){
        //             foreach($itemGroupBy->where('tahun_ajaran_id', 1) as $itemLevelKontribusi){
        //                 $stringCoba = $itemIsiSoal->skor_maks . ' / ' . $itemIsiSoal->cpmk->count() . ' * ' .
        //                 ($assessment->persentase / 100 * $assessment->komponenPenilaian->kelompokNilai->persentase / 100) . ' / ' .
        //                 $itemGroupBy->count();
        //                 dd($stringCoba);
        //             }
        //         }
        //     }
        // }
        // dd($level_kontribusi->where('mata_kuliah_id', 8));
        return view('level_kontribusi.index',[
            'level' => $level,
            'cpl' => $cpl,
            'jenis_cpl' => $jenis_cpl,
            'program_studi' => $program_studi,
            'tahun_ajaran' => $tahun_ajaran,
            'mata_kuliah' => $mata_kuliah,
            'level_kontribusi' => $level_kontribusi,
            'request' => $request,
            'user' => $user
        ]);
    }

    public function template($id)
    {
        $program_studi = Models\ProgramStudi::find($id);
        if(isset($program_studi)){
            return Excel::download(new LevelKontribusiTemplate($program_studi->id), 'Template Input Matriks CPL vs CPMK - ' . $program_studi->nama . '.xlsx');
        }
        else{
            return response()->json([
                'message' => "Program Studi tidak ditemukan!"
            ],500);
        }
    }
    
    public function import(Request $request) 
	{
        try{
            $this->validate($request, [
                'file' => 'required|file|mimes:csv,xls,xlsx,txt'
            ]);
            //proses import
            $excel = new LevelKontribusiImport();
            Excel::import($excel, $request->file('file'));
            $error = $excel->getError();
            $arrayMessage = $excel->getMessage();
            return response()->json([ 
                'error' => $error, 
                'message' => "Import Matriks CPL vs CPMK berhasil, silakan filter sesuai dengan Program Studi yang baru saja diimport",
                'arrayMessage' => $arrayMessage
            ],200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
	}
}

