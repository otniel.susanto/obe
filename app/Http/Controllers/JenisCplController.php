<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

class JenisCplController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $jenis_cpl = Models\JenisCpl::orderBy('id')->paginate(8);
        return view('jenis_cpl.index',[
            'jenis_cpl' => $jenis_cpl,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('jenis_cpl.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'nama' => 'required'
        ]);
        $jenis_cpl = new Models\JenisCpl();
        $jenis_cpl->nama = $request->nama;
        $jenis_cpl->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data jenis cpl telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $jenis_cpl = Models\JenisCpl::find($id);
        return response()->json([ 
            'error' => false, 
            'jenis_cpl' => $jenis_cpl, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis_cpl = Models\JenisCpl::find($id);
        return response()->json([ 
            'error' => false, 
            'jenis_cpl' => $jenis_cpl, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);
        $jenis_cpl = Models\JenisCpl::find($id);
        $jenis_cpl->nama = $request->nama;
        $jenis_cpl->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data jenis cpl telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenis_cpl = Models\JenisCpl::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data jenis cpl telah dihapus", 
        ], 200);
    }
}
