<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;

use App\Imports\RpsKelasMahasiswaImport;
use App\ExcelTemplates\RpsKelasMahasiswaTemplate;
use App\Imports\NilaiImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Response;
use Session;

class RPSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $program_studi = $user->programStudi;
        $arrayIdProgramStudi = [];
        foreach($program_studi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $rps = Models\Rps::select([
            DB::raw('DISTINCT rps.id as id'), 
            'rps.nama_file_rps as nama_file_rps',
            'rps.program_studi_id as program_studi_id',
            'rps.mata_kuliah_id as mata_kuliah_id',
            'mata_kuliah.kode as mata_kuliah_kode',
            'rps.dosen_pengampu_id as dosen_pengampu_id',
            'rps.updated_by as updated_by',
        ])
        ->join('tahun_ajaran', 'rps.tahun_ajaran_id', '=', 'tahun_ajaran.id')
        ->join('mata_kuliah', 'rps.mata_kuliah_id', '=', 'mata_kuliah.id')
        ->join('program_studi', 'rps.program_studi_id', '=', 'program_studi.id');
        if(isset($request->kode_mata_kuliah) && $request->kode_mata_kuliah != ""){
            $rps = $rps->where('mata_kuliah.kode', 'LIKE', '%' . $request->kode_mata_kuliah . '%');
        }
        if(isset($request->nama_mata_kuliah) && $request->nama_mata_kuliah != ""){
            $rps = $rps->where('mata_kuliah.nama', 'LIKE', '%' . $request->nama_mata_kuliah . '%');
        }
        if(isset($request->program_studi_id) && $request->program_studi_id != ""){
            $rps = $rps->where('rps.program_studi_id', $request->program_studi_id);
        }
        else{
            $rps = $rps->whereIn('rps.program_studi_id', $arrayIdProgramStudi);
        }
        if(isset($request->tahun_ajaran_id) && $request->tahun_ajaran_id != ""){
            $rps = $rps->where('rps.tahun_ajaran_id', $request->tahun_ajaran_id);
        }
        else{
            $rps = $rps->where('tahun_ajaran.status', 'Aktif');
        }
        $rps = $rps->orderBy('mata_kuliah.kode')->get();
        $tahun_ajaran = Models\TahunAjaran::orderBy('id', 'desc')->get();
        return view('rps.index',[
            'rps' => $rps,
            'tahun_ajaran' => $tahun_ajaran,
            'program_studi' => $program_studi,
            'request' => $request,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $jenis_penilaian = Models\JenisPenilaian::all();
        $mata_kuliah = Models\MataKuliah::all();
        $kelompok_nilai = Models\KelompokNilai::all();
        $dosen = User::select([
            DB::raw('DISTINCT users.id as id'),
            'users.npk as npk',
            'users.name as name'
        ])
        ->join('program_studi_dosen', 'program_studi_dosen.dosen_id', '=', 'users.id')
        ->whereIn('program_studi_dosen.program_studi_id', $arrayIdProgramStudi)
        ->whereIn('jabatan', [
            "Dosen","Kepala Laboratorium", 
            "Kepala Jurusan", "Koordinator Mata Kuliah", "Dekan", "Wakil Dekan"])
        ->get();
        $program_studi = $user->programStudi;
        return view('rps.create',[
            'jenis_penilaian' => $jenis_penilaian,
            'mata_kuliah' => $mata_kuliah,
            'kelompok_nilai' => $kelompok_nilai,
            'dosen' => $dosen,
            'program_studi' => $program_studi,
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'cpmk_create' => 'required',
                'komponen_penilaian_create' => 'required',
                'dosen_pengampu_id' => 'required',
                'program_studi_id' => 'required',
                'mata_kuliah_id' => 'required'
            ]);
            $kelompok_nilai = Models\KelompokNilai::all();
            $tahun_ajaran = Models\TahunAjaran::where('status', 'Aktif')->first();
            DB::beginTransaction();
            #region Buat RPS baru dan isi kolom2
            $rps = new Models\Rps();
            $rps->dosen_pengampu_id = $request->dosen_pengampu_id;
            $rps->updated_by = Auth::user()->id;
            $rps->program_studi_id = $request->program_studi_id;
            $rps->mata_kuliah_id = $request->mata_kuliah_id;
            $rps->tahun_ajaran_id = $tahun_ajaran->id;
            $rps->keterangan = $request->keterangan;
            $rps->nama_file_rps = $request->nama_file_rps;
            #endregion
            #region Save file RPS
            if($request->file('file') !== null){
                $this->validate($request, [
                    'file' => 'required|mimes:doc,docx,pdf'
                ]);
                $file = $request->file('file');
                $nama_file = date('dmY-His') . ' - ' . $file->getClientOriginalName();

                $file->move('file-rps', $nama_file );
                $rps->nama_file_rps = $nama_file;
            }
            $rps->save(); 
            #endregion

            #region Create CPMK
            if(isset($request->cpmk_create)){
                foreach(json_decode($request->cpmk_create) as $item){
                    $cpmk = new Models\Cpmk();
                    $cpmk->rps_id = $rps->id;
                    $cpmk->kode = $item->kode;
                    $cpmk->keterangan = $item->keterangan;
                    $cpmk->save();
                }
            }
            #endregion
            #region Create Komponen Penilaian
            if(isset($request->komponen_penilaian_create)){
                foreach(json_decode($request->komponen_penilaian_create) as $item){
                    $komponen_penilaian = new Models\KomponenPenilaian();
                    $komponen_penilaian->rps_id = $rps->id;
                    $komponen_penilaian->jenis_penilaian_id = $item->jenis_penilaian_id;
                    $komponen_penilaian->aktivitas = $item->aktivitas;
                    $komponen_penilaian->persentase = $item->persentase;
                    $komponen_penilaian->kelompok_nilai_id = $item->kelompok;
                    $komponen_penilaian->save();
                }
            }
            #endregion
            DB::commit();
            return response()->json([ 
                'error' => false, 
                'message' => "Data RPS telah disimpan",
                'rps_id' => $rps->id
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage(),
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $rps = Models\Rps::find($id);
        $kelompok_nilai = Models\KelompokNilai::all();
        $histori_rps = Models\Rps::where('program_studi_id', $rps->program_studi_id)
        ->where('mata_kuliah_id', $rps->mata_kuliah_id)
        ->get();
        // dd($histori_rps[0]->pembuatRevisi);
        return view('rps.show',[
            'rps' => $rps,
            'histori_rps' => $histori_rps,
            'kelompok_nilai' => $kelompok_nilai,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $rps = Models\Rps::find($id);
        $jenis_penilaian = Models\JenisPenilaian::all();
        $program_studi = $user->programStudi;
        $arrayIdProgramStudi = [];
        foreach($program_studi as $itemProgramStudi){
            $arrayIdProgramStudi[] = $itemProgramStudi->id;
        }
        $mata_kuliah = Models\MataKuliah::all();
        $kelompok_nilai = Models\KelompokNilai::all();
        $dosen = User::select([
            DB::raw('DISTINCT users.id as id'),
            'users.npk as npk',
            'users.name as name'
        ])
        ->join('program_studi_dosen', 'program_studi_dosen.dosen_id', '=', 'users.id')
        ->whereIn('program_studi_dosen.program_studi_id', $arrayIdProgramStudi)
        ->whereIn('jabatan', [
            "Dosen","Kepala Laboratorium", 
            "Kepala Jurusan", "Koordinator Mata Kuliah", "Dekan", "Wakil Dekan"])
        ->get();
        return view('rps.edit',[
            'jenis_penilaian' => $jenis_penilaian,
            'program_studi' => $program_studi,
            'mata_kuliah' => $mata_kuliah,
            'kelompok_nilai' => $kelompok_nilai,
            'rps' => $rps,
            'dosen' => $dosen,
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $request->validate([
                'mata_kuliah_id' => 'required',
                'dosen_pengampu_id' => 'required'
            ]);
            DB::beginTransaction();
            $tahun_ajaran = Models\TahunAjaran::where('status', 'Aktif')->first();
            #region Update RPS
            $rps = Models\Rps::find($id);
            $rps->tahun_ajaran_id = $tahun_ajaran->id;
            $rps->dosen_pengampu_id = $request->dosen_pengampu_id;
            $rps->program_studi_id = $request->program_studi_id;
            $rps->mata_kuliah_id = $request->mata_kuliah_id;
            $rps->keterangan = $request->keterangan;
            #endregion
            
            #region Save file RPS
            if($request->file('file') !== null){
                $this->validate($request, [
                    'file' => 'required|mimes:doc,docx,pdf'
                ]);
                $file = $request->file('file');
                $nama_file = date('dmY-His') . ' - ' . $file->getClientOriginalName();

                $file->move('file-rps', $nama_file );
                $rps->nama_file_rps = $nama_file;
            }
            $rps->save(); 
            #endregion
            
            #region Delete CPMK
            if(isset($request->cpmk_delete)){
                foreach(json_decode($request->cpmk_delete) as $item){
                    $cpmk = Models\Cpmk::find($item->id);
                    $cpmk->levelKontribusi()->sync([]);
                    $cpmk->destroy($item->id);
                }
            }
            #endregion
            #region Update CPMK
            if(isset($request->cpmk_update)){
                foreach(json_decode($request->cpmk_update) as $item){
                    $cpmk = Models\Cpmk::find($item->id);
                    if($cpmk !== null){
                        $cpmk->kode = $item->kode;
                        $cpmk->keterangan = $item->keterangan;
                        $cpmk->save();
                    }
                }
            }
            #endregion
            #region Create CPMK
            if(isset($request->cpmk_create)){
                foreach(json_decode($request->cpmk_create) as $item){
                    $cpmk = new Models\Cpmk();
                    $cpmk->rps_id = $rps->id;
                    $cpmk->kode = $item->kode;
                    $cpmk->keterangan = $item->keterangan;
                    $cpmk->save();
                }
            }
            #endregion
            
            #region Delete Komponen Penilaian
            if(isset($request->komponen_penilaian_delete)){
                foreach(json_decode($request->komponen_penilaian_delete) as $item){
                    $komponen_penilaian = Models\KomponenPenilaian::destroy($item->id);
                }
            }
            #endregion
            #region Update Komponen Penilaian
            if(isset($request->komponen_penilaian_update)){
                foreach(json_decode($request->komponen_penilaian_update) as $item){
                    $komponen_penilaian = Models\KomponenPenilaian::find($item->id);
                    if($komponen_penilaian !== null){
                        $komponen_penilaian->rps_id = $rps->id;
                        $komponen_penilaian->jenis_penilaian_id = $item->jenis_penilaian_id;
                        $komponen_penilaian->aktivitas = $item->aktivitas;
                        $komponen_penilaian->persentase = $item->persentase;
                        $komponen_penilaian->kelompok_nilai_id = $item->kelompok;
                        $komponen_penilaian->save();
                    }
                }
            }
            #endregion
            #region Create Komponen Penilaian
            if(isset($request->komponen_penilaian_create)){
                foreach(json_decode($request->komponen_penilaian_create) as $item){
                    $komponen_penilaian = new Models\KomponenPenilaian();
                    $komponen_penilaian->rps_id = $rps->id;
                    $komponen_penilaian->jenis_penilaian_id = $item->jenis_penilaian_id;
                    $komponen_penilaian->aktivitas = $item->aktivitas;
                    $komponen_penilaian->persentase = $item->persentase;
                    $komponen_penilaian->kelompok_nilai_id = $item->kelompok;
                    $komponen_penilaian->save();
                }
            }
            #endregion
    
            DB::commit();
            return response()->json([ 
                'error' => false, 
                'message' => "Data RPS telah diupdate", 
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage(),
                'request' => $request->kode
            ], 500);
        }
    }
    
    #region UpdateAsNew
    public function updateAsNew(Request $request, $id)
    {
        try{
            $request->validate([
                'mata_kuliah_id' => 'required',
                'dosen_pengampu_id' => 'required'
            ]);
            DB::beginTransaction();
            $tahun_ajaran = Models\TahunAjaran::where('status', 'Aktif')->first();
            $tahun_ajaran_sebelumnya = Models\TahunAjaran::where('status', 'Nonaktif')
            ->where('id', '!=', $id)->orderBy('id','desc')->first();
            $rps = 0;
            #region Update RPS
            if($tahun_ajaran_sebelumnya !== null || isset($tahun_ajaran_sebelumnya)){
                $rps_lama = Models\Rps::find($id);
                $rps = $rps_lama;
                $rps->tahun_ajaran_id = $tahun_ajaran->id;
                $rps->updated_by = Auth::user()->id;
                $rps->save();
                $rps_lama->tahun_ajaran_id = $tahun_ajaran_sebelumnya->id;
                $rps_lama->save();
                $cpmk_all = $rps_lama->cpmk->toArray();
                $komponen_penilaian_all = $rps_lama->komponenPenilaian->toArray();
            }else if($tahun_ajaran_sebelumnya === null){
                $this->update($request, $id);
            }
            #endregion
            
            #region Save file RPS
            if($request->file('file') !== null){
                $this->validate($request, [
                    'file' => 'required|mimes:doc,docx,pdf'
                ]);
                $file = $request->file('file');
                $nama_file = date('dmY-His') . ' - ' . $file->getClientOriginalName();

                $file->move('file-rps', $nama_file );
                $rps->nama_file_rps = $nama_file;
                $rps->save();
            }
            #endregion
            
            #region Komponen Penilaian
            #region Delete Komponen Penilaian
            if(isset($request->komponen_penilaian_delete)){
                foreach(json_decode($request->komponen_penilaian_delete) as $item){
                    foreach($komponen_penilaian_all as $key=>$itemKomponenPenilaian){
                        if($itemKomponenPenilaian['id'] == $item->id){
                            unset($komponen_penilaian_all[$key]);
                            break;
                        }
                    }
                }
            }
            #endregion
            #region Update Komponen Penilaian
            if(isset($request->komponen_penilaian_update)){
                foreach(json_decode($request->komponen_penilaian_update) as $item){
                    foreach($komponen_penilaian_all as $key=>$itemKomponenPenilaian){
                        if($itemKomponenPenilaian['id'] == $item->id){
                            $komponen_penilaian_all[$key]['rps_id'] = $rps->id;
                            $komponen_penilaian_all[$key]['jenis_penilaian_id'] = $item->jenis_penilaian_id;
                            $komponen_penilaian_all[$key]['aktivitas'] = $item->aktivitas;
                            $komponen_penilaian_all[$key]['persentase'] = $item->persentase;
                            $komponen_penilaian_all[$key]['kelompok_nilai_id'] = $item->kelompok;
                            break;
                        }
                    }
                }
            }
            #endregion
            #region Create Komponen Penilaian
            if(isset($request->komponen_penilaian_create)){
                foreach(json_decode($request->komponen_penilaian_create) as $item){
                    $komponen_penilaian = [];
                    $komponen_penilaian['rps_id'] = $rps->id;
                    $komponen_penilaian['jenis_penilaian_id'] = $item->jenis_penilaian_id;
                    $komponen_penilaian['aktivitas'] = $item->aktivitas;
                    $komponen_penilaian['persentase'] = $item->persentase;
                    $komponen_penilaian['kelompok_nilai_id'] = $item->kelompok;
                    $komponen_penilaian_all[] = $komponen_penilaian;
                }
            }
            #endregion
            #region Save ke DB
            foreach($komponen_penilaian_all as $key=>$itemKomponenPenilaian){
                $komponen_penilaian = new Models\KomponenPenilaian();
                $komponen_penilaian->rps_id = $komponen_penilaian_all[$key]['rps_id'];
                $komponen_penilaian->jenis_penilaian_id = $komponen_penilaian_all[$key]['jenis_penilaian_id'];
                $komponen_penilaian->aktivitas = $komponen_penilaian_all[$key]['aktivitas'];
                $komponen_penilaian->persentase = $komponen_penilaian_all[$key]['persentase'];
                $komponen_penilaian->kelompok_nilai_id = $komponen_penilaian_all[$key]['kelompok_nilai_id'];
                $komponen_penilaian->save();
            }
            #endregion
            #endregion

            #region CPMK
            #region Delete CPMK
            if(isset($request->cpmk_delete)){
                foreach(json_decode($request->cpmk_delete) as $item){
                    foreach($cpmk_all as $key=>$itemCpmk){
                        if($itemCpmk['id'] == $item->id){
                            unset($cpmk_all[$key]);
                            break;
                        }
                    }
                }
            }
            #endregion
            #region Update CPMK
            if(isset($request->cpmk_update)){
                foreach(json_decode($request->cpmk_update) as $item){
                    foreach($cpmk_all as $key=>$itemCpmk){
                        if($itemCpmk['id'] == $item->id){
                            $cpmk_all[$key]['rps_id'] = $rps->id;
                            $cpmk_all[$key]['kode'] = $item->kode;
                            $cpmk_all[$key]['keterangan'] = $item->keterangan;
                            break;
                        }
                    }
                }
            }
            #endregion
            #region Create CPMK
            if(isset($request->cpmk_create)){
                foreach(json_decode($request->cpmk_create) as $item){
                    $cpmk = [];
                    $cpmk['rps_id'] = $rps->id;
                    $cpmk['kode'] = $item->kode;
                    $cpmk['keterangan'] = $item->keterangan;
                    $cpmk_all[] = $cpmk;
                }
            }
            #endregion
            #region Save ke DB
            foreach($cpmk_all as $key=>$itemCpmk){
                $cpmk = new Models\Cpmk();
                $cpmk->rps_id = $cpmk_all[$key]['rps_id'];
                $cpmk->kode = $cpmk_all[$key]['kode'];
                $cpmk->keterangan = $cpmk_all[$key]['keterangan'];
                $cpmk->save();
            }
            #endregion
            #endregion

            DB::commit();
            return response()->json([
                'error' => false, 
                'message' => "Data RPS disimpan sebagai data baru.\n". 
                            "Data RPS yang lama memiliki penggunaan terakhir pada Tahun Ajaran sebelumnya", 
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage(),
                'request' => $request->kode
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rps = Models\Rps::find($id);
        $rps->cpmk()->delete();
        $rps->komponenPenilaian()->delete();
        $rps->kelas()->delete();
        $rps = Models\Rps::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data RPS telah dihapus", 
        ], 200);
    }

    public function downloadRPS($fileName){ 
        $filePath = public_path(). "/file-rps/" . $fileName;
        return Response::download($filePath);
        // return Storage::disk('public/storage')->download($fileName);
    }
    public function uploadRPS(Request $request){ 
        $this->validate($request, [
            'file' => 'required|mimes:doc,docx,pdf',
            'rps_id' => 'required'
        ]);
        $rps = Models\Rps::find($request->rps_id);
        $file = $request->file('file');
        $nama_file = date('dmY-His') . ' - ' . $file->getClientOriginalName();
        $file->move('file-rps', $nama_file );
        $rps->nama_file_rps = $nama_file;
        $rps->save();
        return response()->json([ 
            'error' => false, 
            'message' => "File RPS berhasil diupdate", 
        ], 200);
    }
    
    private function generateKodeCpmk($mata_kuliah){
        $cpmk = Models\Cpmk::select('kode as kode')
        ->where('mata_kuliah_id', $mata_kuliah->id)
        ->orderBy('kode', 'desc')
        ->first();
        $kodeBaru = substr($cpmk->kode,5) + 1;
        return $kodeBaru;
    }

    public function getLastRps(Request $request){
        try{
            $rps = null;
            $cpmk = null;
            $komponen_penilaian = null;
            $rps_id = Models\Kelas::select('rps_id as id');
            if(!(isset($request->rps_id)) || $request->rps_id == ""){
                $rps_id = $rps_id
                ->where('program_studi_id', $request->program_studi_id)
                ->where('mata_kuliah_id', $request->mata_kuliah_id)
                ->orderBy('tahun_ajaran_id','desc');
            }
            else if(isset($request->rps_id) && $request->rps_id != ""){
                $rps_id = Models\Rps::select('rps.id as id')->where('id',$request->rps_id);
                $rps = Models\Rps::find($request->rps_id);
                $cpmk = Models\Cpmk::where('rps_id', $request->rps_id)->get();
                $komponen_penilaian = Models\KomponenPenilaian::select([
                    DB::raw('DISTINCT komponen_penilaian.*'),
                    'kelompok_nilai.id as kelompok_nilai_id'
                ])
                ->join('kelompok_nilai', 'komponen_penilaian.kelompok_nilai_id', '=', 'kelompok_nilai.id')
                ->where('rps_id', $request->rps_id)->get();
            }
            $rps_id = $rps_id->first();
            return response()->json([ 
                'error' => false, 
                'rps_id' => $rps_id,
                'rps' => $rps,
                'cpmk' => $cpmk,
                'komponen_penilaian' => $komponen_penilaian,
            ], 200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }

    public function template($id)
    {
        return Excel::download(new RpsKelasMahasiswaTemplate($id), 'Template Input RPS Kelas Mahasiswa.xlsx');
    }
    public function import(Request $request) 
	{
        try{
            // validasi
            $this->validate($request, [
                'file-import' => 'required|mimes:csv,xls,xlsx,txt'
            ]);
            Excel::import(new RpsKelasMahasiswaImport($request->rps_id), $request->file('file-import'));

            return redirect('/rps');
        }catch(Exception $exception){
            return redirect('/rps',[ 
                'error' => true, 
                'message' => $exception->message, 
            ]);
        }
    }
}
