<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

class KategoriCplController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $kategori_cpl = Models\KategoriCpl::orderBy('id')->paginate(8);
        $jenis_cpl = Models\JenisCpl::all();
        return view('kategori_cpl.index',[
            'kategori_cpl' => $kategori_cpl,
            'jenis_cpl' => $jenis_cpl,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $jenis_cpl = Models\JenisCpl::all();
        return view('kategori_cpl.create',[
            'jenis_cpl' => $jenis_cpl,
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'jenis_cpl_id' => 'required',
            'nama' => 'required',
            'bobot' => 'required'
        ]);
        $kategori_cpl = new Models\KategoriCpl();
        $kategori_cpl->jenis_cpl_id = $request->jenis_cpl_id;
        $kategori_cpl->nama = $request->nama;
        $kategori_cpl->bobot = $request->bobot;
        $kategori_cpl->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data kategori cpl telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $kategori_cpl = Models\KategoriCpl::find($id);
        return response()->json([ 
            'error' => false, 
            'kategori_cpl' => $kategori_cpl, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori_cpl = Models\KategoriCpl::find($id);
        $jenis_cpl = Models\JenisCpl::find($kategori_cpl->jenis_cpl_id);
        return response()->json([ 
            'error' => false, 
            'kategori_cpl' => $kategori_cpl,
            'jenis_cpl' => $jenis_cpl
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'jenis_cpl_id' => 'required',
            'nama' => 'required',
            'bobot' => 'required'
        ]);
        $kategori_cpl = Models\KategoriCpl::find($id);
        $kategori_cpl->jenis_cpl_id = $request->jenis_cpl_id;
        $kategori_cpl->nama = $request->nama;
        $kategori_cpl->bobot = $request->bobot;
        $kategori_cpl->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data kategori cpl telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori_cpl = Models\KategoriCpl::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data kategori cpl telah dihapus", 
        ], 200);
    }
}
