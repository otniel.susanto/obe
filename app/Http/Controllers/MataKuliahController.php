<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use App\User;
use Illuminate\Support\Facades\Auth;
use DB;

use App\ExcelTemplates\MataKuliahTemplate;
use App\Imports\MataKuliahImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Response;
use Session;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $program_studi = $user->programStudi;
        $arrayIdProgramStudi = [];
        foreach($program_studi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $jenis_mata_kuliah = Models\JenisMataKuliah::all();
        $detail_mata_kuliah = Models\DetailMataKuliah::select([
            'detail_mata_kuliah.id as id',
            'detail_mata_kuliah.mata_kuliah_id as mata_kuliah_id',
            'detail_mata_kuliah.program_studi_id as program_studi_id',
            'detail_mata_kuliah.jenis_mata_kuliah_id as jenis_mata_kuliah_id',
            'detail_mata_kuliah.alur_7_semester as alur_7_semester',
            'detail_mata_kuliah.alur_8_semester as alur_8_semester',
        ])
        ->join('mata_kuliah', 'detail_mata_kuliah.mata_kuliah_id', '=', 'mata_kuliah.id')
        ->join('program_studi', 'detail_mata_kuliah.program_studi_id', '=', 'program_studi.id')
        ->join('jenis_mata_kuliah', 'detail_mata_kuliah.jenis_mata_kuliah_id', '=', 'jenis_mata_kuliah.id')
        ->whereIn('detail_mata_kuliah.program_studi_id', $arrayIdProgramStudi);
        if(isset($request->program_studi_id) && $request->program_studi_id != ""){
            $detail_mata_kuliah = $detail_mata_kuliah
            ->where('detail_mata_kuliah.program_studi_id', $request->program_studi_id);
        }
        if(isset($request->jenis_mata_kuliah_id) && $request->jenis_mata_kuliah_id != ""){
            $detail_mata_kuliah = $detail_mata_kuliah
            ->where('detail_mata_kuliah.jenis_mata_kuliah_id', $request->jenis_mata_kuliah_id);
        }
        if(isset($request->nama_mata_kuliah) && $request->nama_mata_kuliah != ""){
            $detail_mata_kuliah = $detail_mata_kuliah
            ->where('mata_kuliah.nama', 'LIKE', '%' . $request->nama_mata_kuliah . '%');
        }
        if(isset($request->kode_mata_kuliah) && $request->kode_mata_kuliah != ""){
            $detail_mata_kuliah = $detail_mata_kuliah
            ->where('mata_kuliah.kode', 'LIKE', '%' . $request->kode_mata_kuliah . '%');
        }
        if(isset($request->semester) && $request->semester != ""){
            $detail_mata_kuliah = $detail_mata_kuliah
            ->where(function ($q) use($request) {
                $q->where('detail_mata_kuliah.alur_7_semester', $request->semester)
                ->orWhere('detail_mata_kuliah.alur_8_semester', $request->semester);
            });
        }
        $detail_mata_kuliah = $detail_mata_kuliah->get();
        $dosen = User::where('jabatan', '!=', 'Dosen LB')->get();
        return view('mata_kuliah.index',[
            'detail_mata_kuliah' => $detail_mata_kuliah,
            'dosen' => $dosen,
            'program_studi' => $program_studi,
            'jenis_mata_kuliah' => $jenis_mata_kuliah,
            'request' => $request,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $program_studi = $user->programStudi;
        $jenis_mata_kuliah = Models\JenisMataKuliah::all();
        return view('mata_kuliah.create', [
            'program_studi' => $program_studi,
            'jenis_mata_kuliah' => $jenis_mata_kuliah,
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'kode' => 'required',
                'nama' => 'required',
                'sks' => 'required',
                'program_studi_create' => 'required'
            ]);
            DB::beginTransaction();
            $mata_kuliah = Models\MataKuliah::where('kode', $request->kode)->first();
            if($mata_kuliah === null || !isset($mata_kuliah)){
                $mata_kuliah = new Models\MataKuliah();
            }
            $mata_kuliah->kode = $request->kode;
            $mata_kuliah->nama = $request->nama;
            $mata_kuliah->sks = $request->sks;
            $mata_kuliah->save();
            foreach($request->program_studi_create as $item){
                $mata_kuliah->programStudi()->detach($item['program_studi_id']);
                $mata_kuliah->programStudi()->attach($item['program_studi_id'],[
                    'alur_7_semester' => $item['alur_7_semester'],
                    'alur_8_semester' => $item['alur_8_semester'],
                    'jenis_mata_kuliah_id' =>$item['jenis_mata_kuliah_id']
                ]);
            }
            DB::commit();
            return response()->json([ 
                'error' => false, 
                'message' => "Data mata kuliah telah disimpan", 
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $detail_mata_kuliah = Models\DetailMataKuliah::find($id);
        $jenis_mata_kuliah = Models\JenisMataKuliah::all();
        return view('mata_kuliah.show', [
            'detail_mata_kuliah' => $detail_mata_kuliah,
            'jenis_mata_kuliah' => $jenis_mata_kuliah,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $detail_mata_kuliah = Models\DetailMataKuliah::find($id);
        $jenis_mata_kuliah = Models\JenisMataKuliah::all();
        $program_studi = $user->programStudi;
        return view('mata_kuliah.edit', [
            'detail_mata_kuliah' => $detail_mata_kuliah,
            'program_studi' => $program_studi,
            'jenis_mata_kuliah' => $jenis_mata_kuliah,
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
            'sks' => 'required',
        ]);
        $mata_kuliah = Models\MataKuliah::find($id);
        $mata_kuliah->kode = $request->kode;
        $mata_kuliah->nama = $request->nama;
        $mata_kuliah->sks = $request->sks;
        $mata_kuliah->save();

        if(isset($request->detail_mata_kuliah_delete)){
            foreach($request->detail_mata_kuliah_delete as $item){
                $detail_mata_kuliah = Models\DetailMataKuliah::destroy($item['id']);
            }
        }
        if(isset($request->detail_mata_kuliah_update)){
            foreach($request->detail_mata_kuliah_update as $item){
                $detail_mata_kuliah = Models\DetailMataKuliah::find($item['id']);
                $detail_mata_kuliah->program_studi_id = $item['program_studi_id'];
                $detail_mata_kuliah->mata_kuliah_id = $item['mata_kuliah_id'];
                $detail_mata_kuliah->jenis_mata_kuliah_id = $item['jenis_mata_kuliah_id'];
                $detail_mata_kuliah->alur_7_semester = $item['alur_7_semester'];
                $detail_mata_kuliah->alur_8_semester = $item['alur_8_semester'];
                $detail_mata_kuliah->save();
            }
        }
        if(isset($request->detail_mata_kuliah_create)){
            foreach($request->detail_mata_kuliah_create as $item){
                $detail_mata_kuliah = new Models\DetailMataKuliah();
                $detail_mata_kuliah->program_studi_id = $item['program_studi_id'];
                $detail_mata_kuliah->mata_kuliah_id = $mata_kuliah->id;
                $detail_mata_kuliah->jenis_mata_kuliah_id = $item['jenis_mata_kuliah_id'];
                $detail_mata_kuliah->alur_7_semester = $item['alur_7_semester'];
                $detail_mata_kuliah->alur_8_semester = $item['alur_8_semester'];
                $detail_mata_kuliah->save();
            }
        }
        return response()->json([ 
            'error' => false, 
            'message' => "Data mata kuliah telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $mata_kuliah = Models\MataKuliah::find($id);
            if($mata_kuliah->kelas()->exists()){
                return response()->json([ 
                    'error' => true, 
                    'message' => "Data mata kuliah tidak dapat dihapus karena sudah memiliki kelas", 
                ], 200);
            }
            $mata_kuliah->programStudi()->sync([]);
            $mata_kuliah = Models\MataKuliah::destroy($id);
            DB::commit();
            return response()->json([ 
                'error' => false, 
                'message' => "Data mata kuliah telah dihapus", 
            ], 200);
        }catch(Exception $exception){
            DB::rollback();
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }

    public function template()
    {
        return Excel::download(new MataKuliahTemplate, 'Template Input Mata Kuliah.xlsx');
    }    
    
    public function import(Request $request) 
	{
        try{
            $this->validate($request, [
                'file' => 'required|file|mimes:csv,xls,xlsx,txt'
            ]);
            //proses import
            $import = Excel::import(new MataKuliahImport, $request->file('file'));
            return response()->json([ 
                'error' => false, 
                'message' => 'Data Mata Kuliah berhasil diimport',
            ],200);
        }catch(\ErrorException $error){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Data tidak ditemukan di dalam database<br>", 
            ], 500);
        }catch(\Illuminate\Database\QueryException $exception){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Data tidak ditemukan di dalam database<br>", 
            ], 500);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }
    
    public function getMataKuliah(Request $request){
        $mata_kuliah = Models\MataKuliah::select([
                'program_studi.nama as program_studi',
                'jenis_mata_kuliah.nama as jenis_mata_kuliah',
                'mata_kuliah.id as id',
                'mata_kuliah.kode as kode',
                'mata_kuliah.nama as nama',
                'mata_kuliah.sks as sks',
                'detail_mata_kuliah.alur_7_semester as alur_7_semester',
                'detail_mata_kuliah.alur_8_semester as alur_8_semester',
            ])
            ->join('detail_mata_kuliah', 'detail_mata_kuliah.mata_kuliah_id', '=', 'mata_kuliah.id')
            ->join('program_studi', 'detail_mata_kuliah.program_studi_id', '=', 'program_studi.id')
            ->join('jenis_mata_kuliah', 'detail_mata_kuliah.jenis_mata_kuliah_id', '=', 'jenis_mata_kuliah.id');
        if(isset($request->program_studi_id)){
            $mata_kuliah = $mata_kuliah
            ->where('detail_mata_kuliah.program_studi_id', $request->program_studi_id);
        }
        if(isset($request->jenis_mata_kuliah_id)){
            $mata_kuliah = $mata_kuliah
            ->where('detail_mata_kuliah.jenis_mata_kuliah_id', $request->jenis_mata_kuliah_id);
        }
        if(isset($request->mata_kuliah_id)){
            $mata_kuliah = Models\MataKuliah::where('id', $request->mata_kuliah_id);
        }
        $mata_kuliah = $mata_kuliah->orderBy('kode')->get();
        return response()->json([ 
            'error' => false, 
            'mata_kuliah' => $mata_kuliah, 
        ], 200);
    }
}
