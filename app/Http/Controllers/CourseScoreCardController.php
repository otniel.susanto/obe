<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use App\Exports\CourseScoreCardExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Response;
use Session;

class CourseScoreCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $mata_kuliah = Models\MataKuliah::select([
            DB::raw('DISTINCT mata_kuliah.id as id'),
            'mata_kuliah.kode as kode',
            'mata_kuliah.nama as nama',
        ])
        ->join('detail_mata_kuliah', 'mata_kuliah.id', '=', 'detail_mata_kuliah.mata_kuliah_id')
        ->whereIn('detail_mata_kuliah.program_studi_id', $arrayIdProgramStudi)
        ->get();
        $program_studi = $user->programStudi;
        $tahun_ajaran = Models\TahunAjaran::orderBy('id', 'desc')->get();
        $arrayLevelKontribusi = null;
        $cpl = null;
        $kelas = null;
        if(isset($request->program_studi_id) && isset($request->tahun_ajaran_id) && isset($request->mata_kuliah_id)){
            $kelas = Models\Kelas::where('mata_kuliah_id', $request->mata_kuliah_id)
            // ->where('program_studi_id', $request->program_studi_id)
            ->where('tahun_ajaran_id', $request->tahun_ajaran_id);
            $cpl = Models\Cpl::select([
                DB::raw('DISTINCT cpl.id as id'),
                'cpl.kode as kode',
                'cpl.program_studi_id as program_studi_id',
                'level_kontribusi.skor_maks as skor_maks',
            ])
            ->join('level_kontribusi', 'cpl.id', '=', 'level_kontribusi.cpl_id')
            ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
            ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
            ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
            ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
            // ->where('kelas.program_studi_id', $request->program_studi_id)
            ->where('kelas.mata_kuliah_id', $request->mata_kuliah_id)
            ->where('kelas.tahun_ajaran_id', $request->tahun_ajaran_id)
            ->orderBy('cpl.program_studi_id')
            ->orderBy('cpl.id');
            $level_kontribusi = Models\LevelKontribusi::select([
                DB::raw('DISTINCT level_kontribusi.id as id'),
                'cpl.id as cpl_id',
                'cpl.program_studi_id as program_studi_id',
                'skor_mahasiswa.mahasiswa_id as mahasiswa_id',
                'skor_mahasiswa.skor as skor',
            ])
            ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
            ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
            ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
            ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
            ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
            ->join('kelas_mahasiswa', 'kelas.id', '=', 'kelas_mahasiswa.kelas_id')
            ->join('skor_mahasiswa', 'level_kontribusi.id', '=', 'skor_mahasiswa.level_kontribusi_id')
            // ->where('kelas.program_studi_id', $request->program_studi_id)
            ->where('kelas.mata_kuliah_id', $request->mata_kuliah_id)
            ->where('kelas.tahun_ajaran_id', $request->tahun_ajaran_id)
            ->orderBy('cpl.id');
            if(isset($request->kp) && $request->kp != ""){
                $stringKp = str_replace(" ", "", $request->kp);
                $arrayKp = explode(",", $stringKp);
                $cpl = $cpl->whereIn('kelas.kp', $arrayKp);
                $level_kontribusi = $level_kontribusi->whereIn('kelas.kp', $arrayKp);
                $kelas = $kelas->whereIn('kp', $arrayKp);
            }
            $level_kontribusi = $level_kontribusi->get();
            $cpl = $cpl->get();
            $kelas = $kelas->get();
            $arrayLevelKontribusi = [];
            foreach($level_kontribusi as $itemLevelKontribusi){
                $arrayLevelKontribusi[$itemLevelKontribusi->cpl_id][$itemLevelKontribusi->mahasiswa_id] = $itemLevelKontribusi->skor;
            }
        }
        $kelompok_nilai = Models\KelompokNilai::all();
        return view('course_score_card.index',[
            'mata_kuliah' => $mata_kuliah,
            'program_studi' => $program_studi,
            'tahun_ajaran' => $tahun_ajaran,
            'cpl' => $cpl,
            'level_kontribusi' => $arrayLevelKontribusi,
            'kelas' => $kelas,
            'kelompok_nilai' => $kelompok_nilai,
            'request' => $request,
            'user' => $user
        ]);
    }

    public function export(Request $request){
        $program_studi = Models\ProgramStudi::find($request->program_studi_id);
        $mata_kuliah = Models\MataKuliah::find($request->mata_kuliah_id);
        $tahun_ajaran = Models\TahunAjaran::find($request->tahun_ajaran_id);
        $namaFile = $program_studi->nama . '_' . $mata_kuliah->kode . 
        '_' . $mata_kuliah->nama . '_' . str_replace('/', '-', $tahun_ajaran->keterangan) . ' - Course Score Card.xlsx';
        return Excel::download(new CourseScoreCardExport($mata_kuliah->id, $program_studi->id, $tahun_ajaran->id, $request->kp), $namaFile);
    }
}
