<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

class IsiSoalController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'kp' => 'required',
            'mata_kuliah_id' => 'required',
            'tahun_ajaran_id' => 'required'
        ]);
        $isi_soal = new Models\IsiSoal();
        $isi_soal->kp = $request->kp;
        $isi_soal->mata_kuliah_id = $request->mata_kuliah_id;
        $isi_soal->tahun_ajaran_id = $request->tahun_ajaran_id;
        $isi_soal->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data isi soal telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $isi_soal = Models\IsiSoal::find($id);
        return response()->json([ 
            'error' => false, 
            'isi_soal' => $isi_soal, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $isi_soal = Models\IsiSoal::find($id);
        $cpmk = $isi_soal->cpmk->sortBy('cpmk.kode');
        $taksonomi_bloom = $isi_soal->taksonomiBloom;
        return response()->json([ 
            'error' => false, 
            'isi_soal' => $isi_soal,
            'cpmk' => $cpmk,
            'taksonomi_bloom' => $taksonomi_bloom
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kp' => 'required',
            'mata_kuliah_id' => 'required',
            'tahun_ajaran_id' => 'required'
        ]);
        $isi_soal = Models\IsiSoal::find($id);
        $isi_soal->kp = $request->kp;
        $isi_soal->mata_kuliah_id = $request->mata_kuliah_id;
        $isi_soal->tahun_ajaran_id = $request->tahun_ajaran_id;
        $isi_soal->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data isi soal telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $isi_soal = Models\IsiSoal::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data isi soal telah dihapus", 
        ], 200);
    }
}