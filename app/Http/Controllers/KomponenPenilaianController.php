<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;

class KomponenPenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $komponen_penilaian = Models\KomponenPenilaian::orderBy('id')->paginate(8);
        $jenis_penilaian = Models\JenisPenilaian::all();
        return view('komponen_penilaian.index',[
            'komponen_penilaian' => $komponen_penilaian,
            'jenis_penilaian' => $jenis_penilaian,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $jenis_penilaian = Models\JenisPenilaian::all();
        return view('komponen_penilaian.create',[
            'jenis_penilaian' => $jenis_penilaian,
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'jenis_penilaian_id' => 'required',
            'persentase' => 'required',
            'judul' => 'required',
            'keterangan' => 'required',
            'kelompok' => 'required'
        ]);
        $komponen_penilaian = new Models\KomponenPenilaian();
        $komponen_penilaian->jenis_penilaian_id = $request->jenis_penilaian_id;
        $komponen_penilaian->persentase = $request->persentase;
        $komponen_penilaian->judul = $request->judul;
        $komponen_penilaian->keterangan = $request->keterangan;
        $komponen_penilaian->kelompok = $request->kelompok;
        $komponen_penilaian->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data komponen penilaian telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $komponen_penilaian = Models\KomponenPenilaian::find($id);
        return response()->json([ 
            'error' => false, 
            'komponen_penilaian' => $komponen_penilaian, 
        ], 200);
    }
    public function showJenisPenilaian($id)
    {
        $komponen_penilaian = Models\KomponenPenilaian::find($id);
        $jenis_penilaian = Models\JenisPenilaian::find($komponen_penilaian->jenis_penilaian_id);
        return response()->json([ 
            'error' => false, 
            'jenis_penilaian' => $jenis_penilaian, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $komponen_penilaian = Models\KomponenPenilaian::find($id);
        $jenis_penilaian = Models\JenisPenilaian::find($komponen_penilaian->jenis_penilaian_id);
        return response()->json([ 
            'error' => false, 
            'komponen_penilaian' => $komponen_penilaian, 
            'jenis_penilaian' => $jenis_penilaian, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $request->validate([
            'jenis_penilaian_id' => 'required',
            'persentase' => 'required',
            'judul' => 'required',
            'keterangan' => 'required',
            'kelompok' => 'required'
        ]);
        $komponen_penilaian = Models\KomponenPenilaian::find($id);
        $komponen_penilaian->jenis_penilaian_id = $request->jenis_penilaian_id;
        $komponen_penilaian->persentase = $request->persentase;
        $komponen_penilaian->judul = $request->judul;
        $komponen_penilaian->keterangan = $request->keterangan;
        $komponen_penilaian->kelompok = $request->kelompok;
        $komponen_penilaian->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data komponen penilaian telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $komponen_penilaian = Models\KomponenPenilaian::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data komponen penilaian telah dihapus", 
        ], 200);
    }
}
