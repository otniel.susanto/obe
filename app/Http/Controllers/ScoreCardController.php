<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use App\Exports\ScoreCardExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Response;
use Session;

class ScoreCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $mahasiswa = Models\Mahasiswa::whereIn('program_studi_id', $arrayIdProgramStudi)->get();
        $data = null;
        $mata_kuliah = null;
        $level_kontribusi = null;
        $cpl = null;
        $jenis_cpl = null;
        $arrayLevelKontribusi = null;
        if(isset($request->mahasiswa_id)){
            $data = Models\Mahasiswa::find($request->mahasiswa_id);
            $cpl = Models\Cpl::select([
                'cpl.id as id',
                'cpl.kode as kode'
            ])
            ->join('kategori_cpl', 'kategori_cpl.id', 'cpl.kategori_cpl_id')
            ->join('jenis_cpl', 'jenis_cpl.id', 'kategori_cpl.jenis_cpl_id')
            ->where('cpl.program_studi_id', $data->program_studi_id)
            ->orderBy('cpl.id')
            ->get();
            $jenis_cpl = Models\JenisCpl::select([
                'jenis_cpl.nama as nama', 
                DB::raw('COUNT(*) as jumlah')
            ])
            ->join('kategori_cpl', 'kategori_cpl.jenis_cpl_id', 'jenis_cpl.id')
            ->join('cpl', 'cpl.kategori_cpl_id', 'kategori_cpl.id')
            ->where('cpl.program_studi_id', $data->program_studi_id)
            ->groupBy('jenis_cpl.id', 'jenis_cpl.nama')
            ->orderBy('jenis_cpl.id')
            ->get();
            $mata_kuliah = Models\MataKuliah::join('detail_mata_kuliah', 'mata_kuliah.id', '=', 'detail_mata_kuliah.mata_kuliah_id')
            ->join('rps', 'mata_kuliah.id', '=', 'rps.mata_kuliah_id')
            ->join('cpmk', 'rps.id', '=', 'cpmk.rps_id')
            ->join('level_kontribusi_cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
            ->join('level_kontribusi', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
            ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
            ->join('program_studi', 'program_studi.id', '=', 'detail_mata_kuliah.program_studi_id')
            ->join('tahun_ajaran', 'level_kontribusi.tahun_ajaran_id', '=', 'tahun_ajaran.id')
            //Semua mata kuliah yang punya relasi RPSnya dengan CPL Program Studi Tersebut
            ->where('cpl.program_studi_id', '=', $data->program_studi_id);
            // ->where('detail_mata_kuliah.program_studi_id', '=', $data->program_studi_id);
            // ->where('detail_mata_kuliah.jenis_mata_kuliah_id', '=', 1);

            $level_kontribusi = Models\LevelKontribusi::select([
                DB::raw('DISTINCT level_kontribusi.id as id'),
                'cpl.id as cpl_id',
                'level_kontribusi.level_id as level_id',
                'mata_kuliah.id as mata_kuliah_id',
                'level_kontribusi.skor_maks as skor_maks',
            ])
            ->join('cpl', 'cpl.id', '=', 'level_kontribusi.cpl_id')
            ->join('level_kontribusi_cpmk', 'level_kontribusi.id', '=', 'level_kontribusi_cpmk.level_kontribusi_id')
            ->join('cpmk', 'cpmk.id', '=', 'level_kontribusi_cpmk.cpmk_id')
            ->join('rps', 'rps.id', '=', 'cpmk.rps_id')
            ->join('kelas', 'rps.id', '=', 'kelas.rps_id')
            ->join('kelas_mahasiswa', 'kelas_mahasiswa.kelas_id', '=', 'kelas.id')
            ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'level_kontribusi.tahun_ajaran_id')
            ->join('mata_kuliah', 'mata_kuliah.id', '=', 'rps.mata_kuliah_id')
            ->join('detail_mata_kuliah', 'mata_kuliah.id', '=', 'detail_mata_kuliah.mata_kuliah_id')
            ->where('kelas_mahasiswa.mahasiswa_id', $data->id);
            
            #region Filter Checkbox Alur 7 Semester OFF / Null
            if($request->alur_7_semester === null){
                $mata_kuliah = $mata_kuliah->select([
                    DB::raw('DISTINCT mata_kuliah.id'),
                    'mata_kuliah.kode as kode',
                    'mata_kuliah.nama as nama',
                    'mata_kuliah.sks as sks',
                    'detail_mata_kuliah.alur_8_semester as semester'
                    // 'detail_mata_kuliah.program_studi_id as prodi'
                ])
                ->orderBy('detail_mata_kuliah.alur_8_semester');
            }
            #endregion
            #region Filter Checkbox Alur 7 Semester ON
            else if($request->alur_7_semester == "on"){
                $mata_kuliah = $mata_kuliah->select([
                    DB::raw('DISTINCT mata_kuliah.id'),
                    'mata_kuliah.kode as kode',
                    'mata_kuliah.nama as nama',
                    'mata_kuliah.sks as sks',
                    'detail_mata_kuliah.alur_7_semester as semester'
                    // 'detail_mata_kuliah.program_studi_id as prodi'
                ])
                ->orderBy('detail_mata_kuliah.alur_7_semester');
            }
            #endregion
            $mata_kuliah = $mata_kuliah->get();
            $level_kontribusi = $level_kontribusi->get();
            // dd($mata_kuliah);
            // foreach($level_kontribusi as $item){
            //     $arrayLevelKontribusi[$item->mata_kuliah_id][$item->cpl_id] = $level_kontribusi
            //     ->where('mata_kuliah_id', $item->mata_kuliah_id)
            //     ->where('cpl_id', $item->cpl_id)->first();
            // }
        }
        return view('score_card.index',[
            'mata_kuliah' => $mata_kuliah,
            'cpl' => $cpl,
            'jenis_cpl' => $jenis_cpl,
            'level_kontribusi' => $level_kontribusi,
            'mahasiswa' => $mahasiswa,
            'data' => $data,
            'request' => $request,
            'user' => $user
        ]);
    }

    public function export(Request $request){
        $mahasiswa = Models\Mahasiswa::find($request->mahasiswa_id);
        return Excel::download(new ScoreCardExport($request->mahasiswa_id, $request->alur_7_semester), 
        $mahasiswa->nrp . ' - ' . $mahasiswa->nama . ' - Score Card.xlsx');
    }
}
