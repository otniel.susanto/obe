<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Response;
use Carbon;
use PDF;

use App\Models;
use DB;

class VerifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($program_studi = $user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $tahun_ajaran = Models\TahunAjaran::orderBy('id', 'desc')->get();
        $assessment = Models\Assessment::select([
            DB::raw('DISTINCT assessment.id as id'),
            'assessment.komponen_penilaian_id as komponen_penilaian_id',
            'assessment.aktivitas as aktivitas',
            'assessment.persentase as persentase',
            'assessment.kelompok as kelompok',
            'assessment.jenis_penilaian as jenis_penilaian',
            'assessment.jenis_soal as jenis_soal',
            'assessment.tanggal as tanggal',
            'assessment.durasi as durasi',
            'assessment.sifat as sifat',
            'assessment.nama_file_soal as nama_file_soal',
            'assessment.verifikasi_id as verifikasi_id',
            'kelas.program_studi_id as program_studi_id',
            'tahun_ajaran.id as tahun_ajaran_id',
            'mata_kuliah.kode as mata_kuliah_kode'
        ])->where('jenis_penilaian', 'Ujian')
        ->join('assessment_kelas', 'assessment.id', '=', 'assessment_kelas.assessment_id')
        ->join('kelas', 'kelas.id', '=', 'assessment_kelas.kelas_id')
        ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'kelas.tahun_ajaran_id')
        ->join('mata_kuliah', 'mata_kuliah.id', '=', 'kelas.mata_kuliah_id')
        ->whereIn('kelas.program_studi_id', $arrayIdProgramStudi);
        if($request === null){
            $assessment = $assessment
            ->orderByRaw('assessment.verifikasi_id IS NULL DESC')
            ->orderBy('assessment.id', 'desc')
            ->orderBy('assessment.tanggal');
        }
        if($request->aktivitas !== null && $request->aktivitas != ""){
            $assessment = $assessment->where('assessment.aktivitas', 'LIKE', '%' . $request->aktivitas . '%');
        }
        if($request->program_studi_id !== null && $request->program_studi_id != ""){
            $assessment = $assessment->where('kelas.program_studi_id', $request->program_studi_id);
        }
        if($request->tahun_ajaran_id !== null && $request->tahun_ajaran_id != ""){
            $assessment = $assessment->where('kelas.tahun_ajaran_id', $request->tahun_ajaran_id);
        }
        else{
            $assessment = $assessment->where('tahun_ajaran.status', 'Aktif');
        }
        if($request->nama_mata_kuliah !== null && $request->nama_mata_kuliah != ""){
            $assessment = $assessment->where('mata_kuliah.nama', 'LIKE', '%' . $request->nama_mata_kuliah . '%');
        }
        if($request->kode_mata_kuliah !== null && $request->kode_mata_kuliah != ""){
            $assessment = $assessment->where('mata_kuliah.kode', 'LIKE', '%' . $request->kode_mata_kuliah . '%');
        }
        if($request->status !== null && $request->status != ""){
            if($request->status == "Belum"){
                $assessment = $assessment->whereNull('assessment.verifikasi_id');
            }
            if($request->status == "Menunggu"){
                $assessment = $assessment->whereNotNull('assessment.verifikasi_id');
            }
            if($request->status == "Sudah"){
                $assessment = $assessment->join('verifikasi', 'verifikasi.id', '=', 'assessment.verifikasi_id')
                ->whereNotNull('verifikasi.verified_at');
            }
        }
        $assessment = $assessment
        ->orderBy('kelas.program_studi_id','asc')
        ->orderBy('tahun_ajaran.id','desc')
        ->orderBy('mata_kuliah.kode','asc')
        ->orderBy('assessment.komponen_penilaian_id','asc')
        ->orderBy('assessment.aktivitas','asc')
        ->orderBy('assessment.tanggal','desc')
        ->get();
        return view('verifikasi.index',[
            'assessment' => $assessment,
            'program_studi' => $program_studi,
            'tahun_ajaran' => $tahun_ajaran,
            'request' => $request,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('verifikasi.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'assessment_id' => 'required'
        ]);
        $assessment = Models\Assessment::find($request->assessment_id);
        $verifikasi = Models\Verifikasi::find($assessment->verifikasi_id);
        if($verifikasi === null){
            $verifikasi = new Models\Verifikasi();
        }
        $verifikasi->verified_by = $user->id;
        $verifikasi->verified_at = date('Y-m-d H:i:s');
        $verifikasi->save();
        $assessment->verifikasi_id = $verifikasi->id;
        $assessment->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Soal ujian telah terverifikasi",
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $assessment = Models\Assessment::find($id);
        return view('verifikasi.show',[
            'assessment' => $assessment,
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $verifikasi = Models\Verifikasi::find($id);
        return response()->json([ 
            'error' => false, 
            'verifikasi' => $verifikasi,
            'user' => $user
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            
        ]);
        $verifikasi = Models\Verifikasi::find($id);
        $verifikasi->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data verifikasi telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $verifikasi = Models\Verifikasi::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data verifikasi telah dihapus", 
        ], 200);
    }

    public function getSoal($id){ 
        $user = Auth::user();
        $assessment = Models\Assessment::find($id);
        if(isset($assessment->verifikasi_id)){
            $verifikasi = Models\Verifikasi::find($assessment->verifikasi_id);
            $verifikasi->save();
        }
        else{
            $now = date("Y-m-d H:i:s");
            $verifikasi = new Models\Verifikasi();
            $verifikasi->downloaded_at = date('Y-m-d H:i:s');
            $verifikasi->verified_by = $user->id;
            $verifikasi->save();
            $assessment->verifikasi_id = $verifikasi->id;
            $assessment->save();
        }
        $filePath = public_path(). "/file-soal/" . $assessment->nama_file_soal;
        return Response::download($filePath);
        // return Storage::disk('public/storage')->download($fileName);
    }
    
    public function downloadLembarVerifikasi($id){
        $assessment = Models\Assessment::find($id);
        $pdf = PDF::loadview('verifikasi.pdf',[
            'assessment' => $assessment
        ]);
        return $pdf->download();
    }

    public function simpanCatatan(Request $request){
        $request->validate([
            'assessment_id' => 'required',
            'catatan' => 'required'
        ]);
        $user = Auth::user();
        $assessment = Models\Assessment::find($request->assessment_id);
        $verifikasi = Models\Verifikasi::find($assessment->verifikasi_id);
        if(!isset($verifikasi)){
            $verifikasi = new Models\Verifikasi();
        }
        $verifikasi->verified_by = $user->id;
        $verifikasi->catatan = $request->catatan;
        $verifikasi->save();
        $assessment->verifikasi_id = $verifikasi->id;
        $assessment->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Catatan verifikasi telah disimpan", 
        ], 200);
    }
}
