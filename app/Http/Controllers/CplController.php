<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

use App\Imports\CplImport;
use App\ExcelTemplates\CplTemplate;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Response;
use Session;

class CplController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $cpl = Models\Cpl::join('kategori_cpl', 'cpl.kategori_cpl_id', '=' ,'kategori_cpl.id')
        ->join('jenis_cpl', 'kategori_cpl.jenis_cpl_id', '=' ,'jenis_cpl.id');
        if(isset($request->program_studi_id) && $request->program_studi_id != ""){
            $cpl = $cpl->where('cpl.program_studi_id', $request->program_studi_id);
        }
        else{
            $cpl = $cpl->whereIn('cpl.program_studi_id', $arrayIdProgramStudi);
        }
        if(isset($request->kode_cpl) && $request->kode_cpl != ""){
            $cpl = $cpl->where('cpl.kode', 'LIKE', '%' . $request->kode_cpl . '%');
        }
        if(isset($request->keterangan_cpl) && $request->keterangan_cpl != ""){
            $cpl = $cpl->where('cpl.keterangan', 'LIKE', '%' . $request->keterangan_cpl . '%');
        }
        if(isset($request->kategori_cpl_id) && $request->kategori_cpl_id != ""){
            $cpl = $cpl->where('kategori_cpl.id', $request->kategori_cpl_id);
        }
        if(isset($request->jenis_cpl_id) && $request->jenis_cpl_id != ""){
            $cpl = $cpl->where('jenis_cpl.id', $request->jenis_cpl_id);
        }
        $cpl = $cpl->get();
        $kategori_cpl = Models\KategoriCpl::all();
        $jenis_cpl = Models\JenisCpl::all();
        $program_studi = $user->programStudi;
        return view('cpl.index',[
            'cpl' => $cpl,
            'kategori_cpl' => $kategori_cpl,
            'jenis_cpl' => $jenis_cpl,
            'program_studi' => $program_studi,
            'request' => $request,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $kategori_cpl = Models\KategoriCpl::all();
        $program_studi = $user->programStudi;
        return view('cpl.create', [
            'kategori_cpl' => $kategori_cpl,
            'program_studi' => $program_studi,
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'kode' => 'required',
            'keterangan' => 'required',
            'kategori_cpl_id' => 'required',
            'program_studi_id' => 'required'
        ]);
        $cpl = new Models\Cpl();
        $cpl->kode = $request->kode;
        $cpl->keterangan = $request->keterangan;
        $cpl->kategori_cpl_id = $request->kategori_cpl_id;
        $cpl->program_studi_id = $request->program_studi_id;
        $cpl->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data CPL telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $cpl = Models\Cpl::find($id);
        return response()->json([ 
            'error' => false, 
            'cpl' => $cpl, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cpl = Models\Cpl::find($id);
        $program_studi = Models\ProgramStudi::find($cpl->program_studi_id);
        $kategori_cpl = Models\KategoriCpl::find($cpl->kategori_cpl_id);
        $jenis_cpl = Models\JenisCpl::find($kategori_cpl->jenis_cpl_id);
        return response()->json([ 
            'error' => false,
            'cpl' => $cpl,
            'program_studi' => $program_studi,
            'kategori_cpl' => $kategori_cpl,
            'jenis_cpl' => $jenis_cpl
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'keterangan' => 'required',
            'kategori_cpl_id' => 'required',
        ]);
        $cpl = Models\Cpl::find($id);
        $cpl->kode = $request->kode;
        $cpl->keterangan = $request->keterangan;
        $cpl->kategori_cpl_id = $request->kategori_cpl_id;
        $cpl->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data cpl telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cpl = Models\Cpl::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data cpl telah dihapus", 
        ], 200);
    }

    public function template($program_studi_id)
    {
        $program_studi = Models\ProgramStudi::find($program_studi_id);
        if(isset($program_studi)){
            return Excel::download(new CplTemplate($program_studi->id), 'Template Input CPL - ' . $program_studi->nama . '.xlsx');
        }
        throw new Exception('Program Studi tidak ditemukan');
    }

    public function import(Request $request) 
	{
        try{
            // validasi
            $this->validate($request, [
                'file' => 'required|file|mimes:csv,xls,xlsx,txt'
            ]);
            Excel::import(new CplImport, $request->file('file'));
            return redirect('/cpl');
        }catch(\ErrorException $error){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Data tidak ditemukan di dalam database<br>", 
            ], 500);
        }catch(\Illuminate\Database\QueryException $exception){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Data tidak ditemukan di dalam database<br>", 
            ], 500);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
	}
}
