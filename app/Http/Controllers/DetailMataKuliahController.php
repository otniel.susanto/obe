<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

class DetailMataKuliahController extends Controller
{
    public function edit($id)
    {
        $detail_mata_kuliah = Models\DetailMataKuliah::find($id);
        $program_studi = Models\ProgramStudi::find($detail_mata_kuliah->program_studi_id);
        $jenis_mata_kuliah = Models\JenisMataKuliah::find($detail_mata_kuliah->jenis_mata_kuliah_id);
        return response()->json([ 
            'error' => false, 
            'detail_mata_kuliah' => $detail_mata_kuliah,
            'program_studi' => $program_studi,
            'jenis_mata_kuliah' => $jenis_mata_kuliah,
        ], 200);
    }
}
