<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

class JenisPenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $jenis_penilaian = Models\JenisPenilaian::orderBy('id')->paginate(8);
        return view('jenis_penilaian.index',[
            'jenis_penilaian' => $jenis_penilaian,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('jenis_penilaian.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'nama' => 'required'
        ]);
        $jenis_penilaian = new Models\JenisPenilaian();
        $jenis_penilaian->nama = $request->nama;
        $jenis_penilaian->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data jenis penilaian telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $jenis_penilaian = Models\JenisPenilaian::find($id);
        return response()->json([ 
            'error' => false, 
            'jenis_penilaian' => $jenis_penilaian, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis_penilaian = Models\JenisPenilaian::find($id);
        return response()->json([ 
            'error' => false, 
            'jenis_penilaian' => $jenis_penilaian, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == 5){
            return response()->json([ 
                'error' => true, 
                'message' => "Data jenis penilaian tidak dapat diubah karena kebutuhan sistem\nBila harus mengubah data, silakan hubungi admin", 
            ], 200);
        }
        $request->validate([
            'nama' => 'required'
        ]);
        $jenis_penilaian = Models\JenisPenilaian::find($id);
        $jenis_penilaian->nama = $request->nama;
        $jenis_penilaian->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data jenis penilaian telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id == 5){
            return response()->json([ 
                'error' => true, 
                'message' => "Data jenis penilaian tidak dapat dihapus karena kebutuhan sistem\nBila harus menghapus data, silakan hubungi admin", 
            ], 200);
        }
        $jenis_penilaian = Models\JenisPenilaian::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data jenis penilaian telah dihapus", 
        ], 200);
    }
}
