<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;
use App\Models;
use Illuminate\Support\Facades\Auth;

class CpmkController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'kode' => 'required',
            'keterangan' => 'required',
            'mata_kuliah_id' => 'required'
        ]);
        $cpmk = new Cpmk();
        $cpmk->kode = $request->kode;
        $cpmk->keterangan = $request->keterangan;
        $cpmk->mata_kuliah_id = $request->mata_kuliah_id;
        $cpmk->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data cpmk telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $cpmk = Models\Cpmk::find($id);
        return response()->json([ 
            'error' => false, 
            'cpmk' => $cpmk,
            'user' => $user
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cpmk = Models\Cpmk::find($id);
        $mata_kuliah = Models\MataKuliah::find($cpmk->mata_kuliah_id);
        return response()->json([ 
            'error' => false, 
            'cpmk' => $cpmk, 
            'mata_kuliah' => $mata_kuliah
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'keterangan' => 'required',
            'mata_kuliah_id' => 'required'
        ]);
        $cpmk = Models\Cpmk::find($id);
        $cpmk->kode = $request->kode;
        $cpmk->keterangan = $request->keterangan;
        $cpmk->mata_kuliah_id = $request->mata_kuliah_id;
        $cpmk->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data cpmk telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cpmk = Models\Cpmk::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data cpmk telah dihapus", 
        ], 200);
    }
}
