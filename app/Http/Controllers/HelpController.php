<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HelpController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('help.index',['user' => $user]);
    }
}
