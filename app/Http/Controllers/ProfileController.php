<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('profile.index',[
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $program_studi = Models\ProgramStudi::all();
        return view('profile.edit',[
            'user' => $user,
            'program_studi' => $program_studi
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dosen = User::find($id);
        return response()->json([ 
            'error' => false, 
            'dosen' => $dosen, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $user = Auth::user();
        $user->name = $request->name;
        if($request->program_studi_delete !== null){
            foreach(json_decode($request->program_studi_delete) as $item){
                $user->programStudi()->detach($item->program_studi_id);
            }
        }
        if($request->program_studi_create !== null){
            foreach(json_decode($request->program_studi_create) as $item){
                $user->programStudi()->attach($item->program_studi_id);
            }
        }
        //Save File Gambar Tanda Tangan
        if($request->file('file') !== null){
            $this->validate($request, [
                'file' => 'required|mimes:png,jpg,jpeg'
            ]);
            $file = $request->file('file');
            $nama_file = date('dmY-His') . ' - ' . $file->getClientOriginalName();

            $file->move('tanda-tangan', $nama_file );
            $user->nama_file_ttd = $nama_file;
        }
        $user->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data profil telah diubah", 
        ], 200);
    }
}
