<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;
use Exception;

class KelompokNilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $kelompok_nilai = Models\KelompokNilai::orderBy('id')->get();
        return view('kelompok_nilai.index',[
            'kelompok_nilai' => $kelompok_nilai,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('kelompok_nilai.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'nama' => 'required',
            'persentase' => 'required'
        ]);
        $kelompok_nilai = new Models\KelompokNilai();
        $kelompok_nilai->nama = $request->nama;
        $kelompok_nilai->persentase = $request->persentase;
        $kelompok_nilai->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data kelompok nilai telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kelompok_nilai = Models\KelompokNilai::find($id);
        return response()->json([ 
            'error' => false, 
            'kelompok_nilai' => $kelompok_nilai,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelompok_nilai = Models\KelompokNilai::find($id);
        return response()->json([ 
            'error' => false, 
            'kelompok_nilai' => $kelompok_nilai, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'persentase' => 'required'
        ]);
        $kelompok_nilai = Models\KelompokNilai::find($id);
        $kelompok_nilai->nama = $request->nama;
        $kelompok_nilai->persentase = $request->persentase;
        $kelompok_nilai->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data kelompok nilai telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id != 1){
            $kelompok_nilai = Models\KelompokNilai::destroy($id);
        }else{
            throw new Exception("Data kelompok nilai tidak boleh dihapus.<br>Silakan hubungi sistem administrator bila harus menghapus");
        }
        return response()->json([ 
            'error' => false, 
            'message' => "Data kelompok nilai telah dihapus", 
        ], 200);
    }
}

