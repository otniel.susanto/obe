<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;
use App\Models;
use Illuminate\Support\Facades\Auth;

class ProgramStudiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $program_studi = Models\ProgramStudi::all();
        return view('program_studi.index',[
            'program_studi' => $program_studi,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('program_studi.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required|min:4|max:4',
            'nama' => 'required'
        ]);
        $program_studi = new Models\ProgramStudi();
        $program_studi->kode = $request->kode;
        $program_studi->nama = $request->nama;
        $program_studi->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data program studi telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $program_studi = Models\ProgramStudi::find($id);
        return response()->json([ 
            'error' => false, 
            'program_studi' => $program_studi, 
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $program_studi = Models\ProgramStudi::find($id);
        return response()->json([ 
            'error' => false, 
            'program_studi' => $program_studi, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $request->validate([
                'kode' => 'required|min:4|max:4',
                'nama' => 'required'
            ]);
            $program_studi = Models\ProgramStudi::find($id);
            $program_studi->kode = $request->kode;
            $program_studi->nama = $request->nama;
            $program_studi->save();
            return response()->json([ 
                'error' => false, 
                'message' => "Data program studi telah diubah", 
            ], 200);
        }catch(\ValidationException $exception){
            return response()->json([ 
                'error' => false, 
                'message' => $exception->getMessage(), 
            ], 500);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => false, 
                'message' => "Data ada yang salah, periksa kembali data yang dimasukkan", 
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id <= 16){
            return response()->json([ 
                'error' => true, 
                'message' => "Mohon maaf, program studi tidak dapat dihapus\nBila harus menghapus, silakan menghubungi admin", 
            ], 200);
        }
        $program_studi = Models\ProgramStudi::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data program studi telah dihapus", 
        ], 200);
    }
}
