<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;

use App\Imports\MahasiswaImport;
use App\ExcelTemplates\MahasiswaTemplate;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Response;
use Session;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $mahasiswa = Models\Mahasiswa::select([
            'mahasiswa.id as id',
            'mahasiswa.nrp as nrp',
            'mahasiswa.nama as nama',
            'mahasiswa.program_studi_id as program_studi_id',
            'mahasiswa.tahun_masuk as tahun_masuk',
        ])
        ->whereIn('program_studi_id', $arrayIdProgramStudi);
        if(isset($request->nrp_mahasiswa) && $request->nrp_mahasiswa != ""){
            $mahasiswa = $mahasiswa->where('mahasiswa.nrp', 'LIKE', '%'.$request->nrp_mahasiswa . '%');
        }
        if(isset($request->nama_mahasiswa) && $request->nama_mahasiswa != ""){
            $mahasiswa = $mahasiswa->where('mahasiswa.nama', 'LIKE', '%'.$request->nama_mahasiswa . '%');
        }
        if(isset($request->tahun_masuk) && $request->tahun_masuk != ""){
            $mahasiswa = $mahasiswa->where('mahasiswa.tahun_masuk', 'LIKE', '%'.$request->tahun_masuk . '%');
        }
        if(isset($request->program_studi_id) && $request->program_studi_id != ""){
            $mahasiswa = $mahasiswa->where('mahasiswa.program_studi_id', '=', $request->program_studi_id);
        }
        $mahasiswa = $mahasiswa->get();
        $program_studi = $user->programStudi;
        $jenis_mata_kuliah = Models\JenisMataKuliah::all();
        return view('mahasiswa.index',[
            'mahasiswa' => $mahasiswa,
            'program_studi' => $program_studi,
            'jenis_mata_kuliah' => $jenis_mata_kuliah,
            'request' => $request,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('mahasiswa.create', [
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $program_studi = ProgramStudi::all();
        $request->validate([
            'nrp' => 'required',
            'nama' => 'required',
            'program_studi_id' => 'required'
        ]);
        $mahasiswa = new Models\Mahasiswa();
        $mahasiswa->nrp = $request->nrp;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->program_studi_id = $program_studi->firstWhere('kode', substr($request->nrp, 2, 2))->id;
        $mahasiswa->tahun_masuk = '20'. substr($request->nrp, 4, 2);
        $mahasiswa->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data mahasiswa telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mahasiswa = Models\Mahasiswa::find($id);
        return response()->json([ 
            'error' => false, 
            'mahasiswa' => $mahasiswa,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mahasiswa = Models\Mahasiswa::find($id);
        return response()->json([ 
            'error' => false, 
            'mahasiswa' => $mahasiswa,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nrp' => 'required',
            'nama' => 'required'
        ]);
        $mahasiswa = Models\Mahasiswa::find($id);
        $mahasiswa->nrp = $request->nrp;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->program_studi_id = $program_studi->firstWhere('kode', substr($request->nrp, 2, 2))->id;
        $mahasiswa->tahun_masuk = '20'. substr($request->nrp, 4, 2);
        $mahasiswa->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data mahasiswa telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $mahasiswa = Models\Mahasiswa::destroy($id);
            return response()->json([ 
                'error' => false, 
                'message' => "Data mahasiswa telah dihapus", 
            ], 200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }

    public function template()
    {
        return Excel::download(new MahasiswaTemplate, 'Template Input Mahasiswa.xlsx');
    }

    public function import(Request $request) 
	{
        try{
            // validasi
            $this->validate($request, [
                'file' => 'required|file|mimes:csv,xls,xlsx,txt'
            ]);
            Excel::import(new MahasiswaImport, $request->file('file'));

            return redirect('/mahasiswa');
        }catch(\ErrorException $error){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Data tidak ditemukan di dalam database<br>", 
            ], 500);
        }catch(\Illuminate\Database\QueryException $exception){
            return response()->json([ 
                'error' => true, 
                'message' => "Ada kesalahan, periksa kembali file yang dimasukkan.<br>" . 
                "Kesalahan yang mungkin terjadi pada file: <br>" .
                "- Ada baris atau kolom yang belum terisi atau terlewati<br>".
                "- Ada data yang tidak bisa diproses karena salah<br>", 
            ], 500);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }
    
    public function downloadScoreCard($id){
        
    }
}
