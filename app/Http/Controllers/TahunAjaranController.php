<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;
use DB;

class TahunAjaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $tahun_ajaran = Models\TahunAjaran::orderBy('id', 'desc')->paginate(8);
        return view('tahun_ajaran.index',[
            'tahun_ajaran' => $tahun_ajaran,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('tahun_ajaran.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'keterangan' => 'required'
        ]);
        $tahun_ajaran = Models\TahunAjaran::orderBy('id','desc')->first();
        if($tahun_ajaran !== null){
            $tahun_ajaran->status = 'Nonaktif';
            $tahun_ajaran->save();
            $rps = Models\Rps::where('tahun_ajaran_id', $tahun_ajaran->id)->get();
        }
        $tahun_ajaran = new Models\TahunAjaran();
        $tahun_ajaran->keterangan = $request->keterangan;
        $tahun_ajaran->status = 'Aktif';
        $tahun_ajaran->save();
        foreach($rps as $itemRps){
            $itemRps->tahun_ajaran_id = $tahun_ajaran->id;
            $itemRps->save();
        }
        return response()->json([ 
            'error' => false, 
            'message' => "Data tahun ajaran telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $tahun_ajaran = Models\TahunAjaran::find($id);
        return response()->json([ 
            'error' => false, 
            'tahun_ajaran' => $tahun_ajaran,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $tahun_ajaran = Models\TahunAjaran::find($id);
        return response()->json([ 
            'error' => false, 
            'tahun_ajaran' => $tahun_ajaran, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'keterangan' => 'required'
        ]);
        $tahun_ajaran = Models\TahunAjaran::find($id);
        $tahun_ajaran->keterangan = $request->keterangan;
        $tahun_ajaran->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data tahun ajaran telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $tahun_ajaran = Models\TahunAjaran::find($id);
            if($tahun_ajaran->status == 'Aktif'){
                $tahun_ajaran_sebelumnya = Models\TahunAjaran::where('id', '!=', $id)->orderBy('id','desc')->first();
                $tahun_ajaran_sebelumnya->status = 'Aktif';
                $tahun_ajaran_sebelumnya->save();
                $rps = Models\Rps::where('tahun_ajaran_id', $tahun_ajaran->id)->get();
                $kelas = Models\Kelas::where('tahun_ajaran_id', $tahun_ajaran->id)->first();
                $arrayIdRps = [];
                foreach($rps as $itemRps){
                    if($kelas !== null){
                        return response()->json([ 
                            'error' => true, 
                            'message' => "Tahun Ajaran tidak dapat dihapus karena telah memiliki Kelas Paralel", 
                        ], 500);
                    }
                    $arrayIdRps[] = $itemRps->id;
                }
                DB::table('rps')->whereIn('id', $arrayIdRps)->update(array('tahun_ajaran_id' => $tahun_ajaran_sebelumnya->id));
                $hapusTahunAjaran = Models\TahunAjaran::destroy($id);
            }
            else{
                return response()->json([ 
                    'error' => true, 
                    'message' => "Tahun Ajaran tidak dapat dihapus karena berhubungan dengan data lainnya", 
                ], 500);
            }
            return response()->json([ 
                'error' => false, 
                'message' => "Data tahun ajaran telah dihapus", 
            ], 200);
        }catch(Exception $exception){
            return response()->json([ 
                'error' => true, 
                'message' => $exception->getMessage(), 
            ], 500);
        }
    }
}
