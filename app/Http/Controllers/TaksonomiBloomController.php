<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

class TaksonomiBloomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $taksonomi_bloom = Models\TaksonomiBloom::orderBy('id')->paginate(8);
        return view('taksonomi_bloom.index',[
            'taksonomi_bloom' => $taksonomi_bloom,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('taksonomi_bloom.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'level' => 'required',
            'keterangan' => 'required'
        ]);
        $taksonomi_bloom = new Models\TaksonomiBloom();
        $taksonomi_bloom->level = $request->level;
        $taksonomi_bloom->keterangan = $request->keterangan;
        $taksonomi_bloom->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data taksonomi bloom telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $taksonomi_bloom = Models\TaksonomiBloom::find($id);
        return response()->json([ 
            'error' => false, 
            'taksonomi_bloom' => $taksonomi_bloom,
            'user' => $user
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $taksonomi_bloom = Models\TaksonomiBloom::find($id);
        return response()->json([ 
            'error' => false, 
            'taksonomi_bloom' => $taksonomi_bloom,
            'user' => $user
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'level' => 'required',
            'keterangan' => 'required'
        ]);
        $taksonomi_bloom = Models\TaksonomiBloom::find($id);
        $taksonomi_bloom->level = $request->level;
        $taksonomi_bloom->keterangan = $request->keterangan;
        $taksonomi_bloom->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data taksonomi bloom telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $taksonomi_bloom = Models\TaksonomiBloom::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data taksonomi bloom telah dihapus", 
        ], 200);
    }
}
