<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $level = Models\Level::orderBy('id')->paginate(8);
        return view('level.index',[
            'level' => $level,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('level.create',[
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'level' => 'required',
            'keterangan' => 'required'
        ]);
        $level = new Models\Level();
        $level->level = $request->level;
        $level->keterangan = $request->keterangan;
        $level->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data level telah disimpan", 
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $level = Models\Level::find($id);
        return response()->json([ 
            'error' => false, 
            'level' => $level,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $level = Models\Level::find($id);
        return response()->json([ 
            'error' => false, 
            'level' => $level, 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'level' => 'required',
            'keterangan' => 'required'
        ]);
        $level = Models\Level::find($id);
        $level->level = $request->level;
        $level->keterangan = $request->keterangan;
        $level->save();
        return response()->json([ 
            'error' => false, 
            'message' => "Data level telah diubah", 
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level = Models\Level::destroy($id);
        return response()->json([ 
            'error' => false, 
            'message' => "Data level telah dihapus", 
        ], 200);
    }
}

