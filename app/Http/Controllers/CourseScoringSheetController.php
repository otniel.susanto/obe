<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models;
use DB;
use Illuminate\Support\Facades\Auth;

use App\Exports\CourseScoringSheetExport;
use Maatwebsite\Excel\Facades\Excel;

class CourseScoringSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $arrayIdProgramStudi = [];
        foreach($user->programStudi as $item){
            $arrayIdProgramStudi[] = $item->id;
        }
        $kelompok_nilai = Models\KelompokNilai::all();
        $program_studi = $user->programStudi;
        $mata_kuliah = Models\MataKuliah::select([
            DB::raw('DISTINCT mata_kuliah.id as id'),
            'mata_kuliah.kode as kode',
            'mata_kuliah.nama as nama',
        ])
        ->join('detail_mata_kuliah', 'mata_kuliah.id', '=', 'detail_mata_kuliah.mata_kuliah_id')
        ->whereIn('detail_mata_kuliah.program_studi_id', $arrayIdProgramStudi)
        ->get();
        $tahun_ajaran = Models\TahunAjaran::orderBy('id', 'desc')->get();
        $data = null;
        if(isset($request->mata_kuliah_id) && isset($request->tahun_ajaran_id)){
            $data = Models\Kelas::where('mata_kuliah_id', '=', $request->mata_kuliah_id)
            ->where('tahun_ajaran_id', '=', $request->tahun_ajaran_id);
            // ->where('kelas.program_studi_id', $request->program_studi_id);
            if(isset($request->kp) && $request->kp != ""){
                $stringKp = str_replace(" ", "", $request->kp);
                $arrayKp = explode(",", $stringKp);
                $data = $data->whereIn('kelas.kp', $arrayKp);
            }
            $data = $data->get();
        }
        return view('course_scoring_sheet.index',[
            'program_studi' => $program_studi,
            'mata_kuliah' => $mata_kuliah,
            'tahun_ajaran' => $tahun_ajaran,
            'kelompok_nilai' => $kelompok_nilai,
            'data' => $data,
            'request' => $request,
            'user' => $user
        ]);
    }
    
    public function export(Request $request)
    {
        $program_studi = Models\ProgramStudi::find($request->program_studi_id);
        $mata_kuliah = Models\MataKuliah::find($request->mata_kuliah_id);
        $tahun_ajaran = Models\TahunAjaran::find($request->tahun_ajaran_id);
        return Excel::download(new CourseScoringSheetExport($request->program_studi_id, 
        $request->mata_kuliah_id, $request->tahun_ajaran_id, $request->kp), 'Course Scoring Sheet.xlsx');
    }
}
