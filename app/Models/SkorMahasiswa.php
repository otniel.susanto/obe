<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

class SkorMahasiswa extends Model
{
    use InsertOnDuplicateKey;
    protected $table = 'skor_mahasiswa';
    public function mahasiswa(){
        return $this->belongsTo('App\Models\Mahasiswa', 'mahasiswa_id');
    }
    public function levelKontribusi(){
        return $this->belongsTo('App\Models\LevelKontribusi', 'level_kontribusi_id');
    }
}
