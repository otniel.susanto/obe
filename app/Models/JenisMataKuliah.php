<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisMataKuliah extends Model
{
    protected $table = 'jenis_mata_kuliah';
    public function detailMataKuliah(){
        return $this->hasMany('App\Models\DetailMataKuliah', 'jenis_mata_kuliah_id', 'id');
    }
}
