<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    public function mataKuliah(){
        return $this->belongsTo('App\Models\MataKuliah', 'mata_kuliah_id');
    }
    public function tahunAjaran(){
        return $this->belongsTo('App\Models\TahunAjaran', 'tahun_ajaran_id');
    }
    public function rps(){
        return $this->belongsTo('App\Models\Rps', 'rps_id');
    }
    public function programStudi(){
        return $this->belongsTo('App\Models\ProgramStudi', 'program_studi_id');
    }
    public function assessment(){
        return $this->belongsToMany('App\Models\Assessment', 'assessment_kelas', 'kelas_id', 'assessment_id');
    }
    public function mahasiswa(){
        return $this->belongsToMany('App\Models\Mahasiswa', 'kelas_mahasiswa', 'kelas_id', 'mahasiswa_id');
    }
}
