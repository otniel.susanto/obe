<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    protected $table = 'mata_kuliah';
    protected $guarded = [];
    // protected $fillable = ['kode', 'nama', ];
    public function kelas(){
        return $this->hasMany('App\Models\Kelas', 'mata_kuliah_id', 'id');
    }
    public function rps(){
        return $this->hasMany('App\Models\Rps', 'mata_kuliah_id', 'id');
    }
    public function programStudi(){
        return $this->belongsToMany('App\Models\ProgramStudi', 'detail_mata_kuliah', 'mata_kuliah_id', 'program_studi_id')
        ->withPivot('id','alur_7_semester','alur_8_semester','jenis_mata_kuliah_id');
    }
    public function detailMataKuliah(){
        return $this->hasMany('App\Models\DetailMataKuliah', 'mata_kuliah_id', 'id');
    }
}
