<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisPenilaian extends Model
{
    protected $table = 'jenis_penilaian';
    protected $fillable = [
        'nama'
    ];
    public function komponenPenilaian(){
        return $this->hasMany('App\Models\KomponenPenilaian', 'jenis_penilaian_id','id');
    }
}
