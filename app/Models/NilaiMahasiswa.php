<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

class NilaiMahasiswa extends Model
{
    use InsertOnDuplicateKey;
    protected $table = 'nilai_mahasiswa';
    public function isiSoal(){
        return $this->belongsToMany('App\Models\IsiSoal', 'detail_nilai_mahasiswa', 'nilai_mahasiswa_id', 'isi_soal_id')
        ->withPivot('nilai');
    }
    public function assessment(){
        return $this->belongsTo('App\Models\Assessment', 'assessment_id');
    }
    public function mahasiswa(){
        return $this->belongsTo('App\Models\Mahasiswa', 'mahasiswa_id');
    }
}
