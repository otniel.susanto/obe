<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailMataKuliah extends Model
{
    protected $table = 'detail_mata_kuliah';
    public function programStudi(){
        return $this->belongsTo('App\Models\ProgramStudi', 'program_studi_id');
    }
    public function mataKuliah(){
        return $this->belongsTo('App\Models\MataKuliah', 'mata_kuliah_id');
    }
    public function jenisMataKuliah(){
        return $this->belongsTo('App\Models\JenisMataKuliah', 'jenis_mata_kuliah_id');
    }
}
