<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cpl extends Model
{
    protected $table = 'cpl';
    protected $guarded = [];

    public function levelKontribusi(){
        return $this->hasMany('App\Models\LevelKontribusi', 'cpl_id', 'id');
    }
    public function kategoriCpl(){
        return $this->belongsTo('App\Models\KategoriCpl', 'kategori_cpl_id');
    }
    public function programStudi(){
        return $this->belongsTo('App\Models\ProgramStudi', 'program_studi_id');
    }
}
