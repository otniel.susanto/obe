<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verifikasi extends Model
{
    protected $table = 'verifikasi';
    // protected $guarded = [];
    public function dosen(){
        return $this->belongsTo('App\User', 'verified_by');
    }
    public function assessment(){
        return $this->hasOne('App\Models\Assessment', 'verifikasi_id','id');
    }
}
