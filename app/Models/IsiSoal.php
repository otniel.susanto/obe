<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsiSoal extends Model
{
    protected $table = 'isi_soal';
    public function assessment(){
        return $this->belongsTo('App\Models\Assessment', 'assessment_id');
    }
    public function cpmk(){
        return $this->belongsToMany('App\Models\Cpmk', 'cpmk_isi_soal', 'isi_soal_id', 'cpmk_id');
    }
    public function taksonomiBloom(){
        return $this->belongsTo('App\Models\TaksonomiBloom', 'taksonomi_bloom_id');
    }
    public function nilaiMahasiswa(){
        return $this->belongsToMany('App\Models\NilaiMahasiswa', 'detail_nilai_mahasiswa', 'isi_soal_id', 'nilai_mahasiswa_id')
        ->withPivot('nilai');
    }
}
