<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KelompokNilai extends Model
{
    protected $table = 'kelompok_nilai';
    public function komponenPenilaian(){
        return $this->hasMany('App\Models\KomponenPenilaian', 'kelompok_nilai_id', 'id');
    }
}
