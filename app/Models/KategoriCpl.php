<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriCpl extends Model
{
    protected $table = 'kategori_cpl';
    
    public function jenisCpl(){
        return $this->belongsTo('App\Models\JenisCpl', 'jenis_cpl_id');
    }
    public function cpl(){
        return $this->hasMany('App\Models\Cpl', 'kategori_cpl_id', 'id');
    }
}
