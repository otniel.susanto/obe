<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    protected $table = 'tahun_ajaran';
    public function kelas(){
        return $this->hasMany('App\Models\Kelas', 'tahun_ajaran_id','id');
    }
    public function rps(){
        return $this->hasMany('App\Models\Rps', 'tahun_ajaran_id','id');
    }
    public function levelKontribusi(){
        return $this->hasMany('App\Models\LevelKontribusi', 'tahun_ajaran_id','id');
    }
}
