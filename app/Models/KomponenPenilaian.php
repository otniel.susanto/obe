<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KomponenPenilaian extends Model
{
    protected $table = 'komponen_penilaian';
    public function mataKuliah(){
        return $this->belongsTo('App\Models\MataKuliah', 'mata_kuliah_id');
    }
    public function jenisPenilaian(){
        return $this->belongsTo('App\Models\JenisPenilaian', 'jenis_penilaian_id');
    }
    public function kelompokNilai(){
        return $this->belongsTo('App\Models\KelompokNilai', 'kelompok_nilai_id');
    }
    public function assessment(){
        return $this->hasMany('App\Models\Assessment', 'komponen_penilaian_id','id');
    }
}
