<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelKontribusi extends Model
{
    protected $table = 'level_kontribusi';
    public function cpl(){
        return $this->belongsTo('App\Models\Cpl', 'cpl_id');
    }
    public function level(){
        return $this->belongsTo('App\Models\Level', 'level_id');
    }
    public function tahunAjaran(){
        return $this->belongsTo('App\Models\TahunAjaran', 'tahun_ajaran_id');
    }
    public function cpmk(){
        return $this->belongsToMany('App\Models\Cpmk', 'level_kontribusi_cpmk', 'level_kontribusi_id', 'cpmk_id');
    }
    public function skorMahasiswa(){
        return $this->hasMany('App\Models\SkorMahasiswa', 'level_kontribusi_id','id');
    }
    public function mahasiswa(){
        return $this->belongsToMany('App\Models\Mahasiswa', 'skor_mahasiswa', 'level_kontribusi_id', 'mahasiswa_id')
        ->withPivot('skor');
    }
    public function clone($tahun_ajaran_id){
        //copy attributes
        $new = $this->replicate();
        $new->tahun_ajaran_id = $tahun_ajaran_id;
        $new->skor_maks = 0;
        //save model before you recreate relations (so it has an id)
        $new->push();
        //reset relations on EXISTING MODEL (this way you can control which ones will be loaded
        $this->relations = [];
        //load relations on EXISTING MODEL
        $this->load('cpmk');
        //re-sync everything
        foreach ($this->relations as $relationName => $values){
            $new->{$relationName}()->sync($values);
        }
    }
}
