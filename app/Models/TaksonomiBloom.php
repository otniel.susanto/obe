<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaksonomiBloom extends Model
{
    protected $table = 'taksonomi_bloom';
    public function isiSoal(){
        return $this->hasMany('App\Models\IsiSoal', 'taksonomi_bloom_id','id');
    }
}
