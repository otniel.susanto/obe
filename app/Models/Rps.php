<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rps extends Model
{
    protected $table = 'rps';
    protected $guarded = [];
    public function cpmk(){
        return $this->hasMany('App\Models\Cpmk', 'rps_id', 'id');
    }
    public function kelas(){
        return $this->hasMany('App\Models\Kelas', 'rps_id', 'id');
    }
    public function komponenPenilaian(){
        return $this->hasMany('App\Models\KomponenPenilaian', 'rps_id', 'id');
    }
    public function dosenPengampu(){
        return $this->belongsTo('App\User', 'dosen_pengampu_id');
    }
    public function pembuatRevisi(){
        return $this->belongsTo('App\User', 'updated_by');
    }
    public function programStudi(){
        return $this->belongsTo('App\Models\ProgramStudi', 'program_studi_id');
    }
    public function mataKuliah(){
        return $this->belongsTo('App\Models\MataKuliah', 'mata_kuliah_id');
    }
    public function tahunAjaran(){
        return $this->belongsTo('App\Models\TahunAjaran', 'tahun_ajaran_id');
    }
}
