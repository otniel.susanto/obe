<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'level';
    
    public function levelKontribusi(){
        return $this->hasMany('App\Models\LevelKontribusi', 'level_id', 'id');
    }
}
