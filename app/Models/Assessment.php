<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    protected $table = 'assessment';
    public function isiSoal(){
        return $this->hasMany('App\Models\IsiSoal', 'assessment_id','id');
    }
    public function nilaiMahasiswa(){
        return $this->hasMany('App\Models\NilaiMahasiswa', 'assessment_id','id');
    }
    public function dosenPenguji(){
        return $this->belongsToMany('App\User', 'dosen_penguji', 'assessment_id', 'dosen_id');
    }
    public function kelas(){
        return $this->belongsToMany('App\Models\Kelas', 'assessment_kelas', 'assessment_id', 'kelas_id');
    }
    public function verifikasi(){
        return $this->belongsTo('App\Models\Verifikasi', 'verifikasi_id');
    }
    public function komponenPenilaian(){
        return $this->belongsTo('App\Models\KomponenPenilaian', 'komponen_penilaian_id');
    }
}
