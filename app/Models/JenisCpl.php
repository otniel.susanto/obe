<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisCpl extends Model
{
    protected $table = 'jenis_cpl';
    
    public function kategoriCpl(){
        return $this->hasMany('App\Models\KategoriCpl', 'jenis_cpl_id', 'id');
    }
}
