<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramStudi extends Model
{
    protected $table = 'program_studi';
    protected $guarded = [];
    public function mataKuliah(){
        return $this->belongsToMany('App\Models\MataKuliah', 'detail_mata_kuliah', 'program_studi_id', 'mata_kuliah_id')
        ->withPivot('id','alur_7_semester','alur_8_semester','jenis_mata_kuliah_id');
    }
    public function detailMataKuliah(){
        return $this->hasMany('App\Models\DetailMataKuliah', 'program_studi_id', 'id');
    }
    public function cpl(){
        return $this->hasMany('App\Cpl', 'program_studi_id', 'id');
    }
    public function dosen(){
        return $this->belongsToMany('App\User', 'program_studi_dosen', 'dosen_id', 'program_studi_id');
    }
    public function kelas(){
        return $this->hasMany('App\Models\Kelas', 'program_studi_id', 'id');
    }
    public function rps(){
        return $this->hasMany('App\Models\Rps', 'program_studi_id', 'id');
    }
}
