<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cpmk extends Model
{
    protected $table = 'cpmk';
    
    public function rps(){
        return $this->belongsTo('App\Models\Rps', 'rps_id');
    }
    public function isiSoal(){
        return $this->belongsToMany('App\Models\IsiSoal', 'cpmk_isi_soal', 'cpmk_id', 'isi_soal_id');
    }
    public function levelKontribusi(){
        return $this->belongsToMany('App\Models\LevelKontribusi', 'level_kontribusi_cpmk', 'cpmk_id', 'level_kontribusi_id');
    }
    // public function levelKontribusiCpmk(){
    //     return $this->hasMany('App\Models\LevelKontribusiCpmk', 'cpmk_id', 'id');
    // }
}
