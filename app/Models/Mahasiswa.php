<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    // Custom Primary Key
    // protected $primaryKey = 'nrp'; // or null
    // public $incrementing = false;
    // // In Laravel 6.0+ make sure to also set $keyType
    // protected $keyType = 'string';

    protected $table = 'mahasiswa';
    protected $guarded = [];
    public function nilaiMahasiswa(){
        return $this->hasMany('App\Models\NilaiMahasiswa', 'mahasiswa_id','id');
    }
    public function skorMahasiswa(){
        return $this->hasMany('App\Models\SkorMahasiswa', 'mahasiswa_id','id');
    }
    public function kelas(){
        return $this->belongsToMany('App\Models\Kelas', 'kelas_mahasiswa', 'mahasiswa_id', 'kelas_id');
    }
    public function programStudi(){
        return $this->belongsTo('App\Models\ProgramStudi', 'program_studi_id');
    }
    public function levelKontribusi(){
        return $this->belongsToMany('App\Models\LevelKontribusi', 'skor_mahasiswa', 'mahasiswa_id', 'level_kontribusi_id')
        ->withPivot('skor');
    }
}
