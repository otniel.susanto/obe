<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/', 'AssessmentController@index')->middleware('auth');
Route::resource('profile', 'ProfileController')->middleware('auth');
Route::post('profile/{id}', 'ProfileController@update')->middleware('auth');

Route::resource('taksonomi-bloom', 'TaksonomiBloomController')->middleware('auth');

Route::resource('jenis-penilaian', 'JenisPenilaianController')->middleware('auth');

Route::resource('tahun-ajaran', 'TahunAjaranController')->middleware('auth');

Route::resource('mata-kuliah', 'MataKuliahController')->middleware('auth');
Route::post('mata-kuliah-import', 'MataKuliahController@import')->middleware('auth');
Route::get('mata-kuliah-template', 'MataKuliahController@template')->middleware('auth');
Route::get('get-mata_kuliah', 'MataKuliahController@getMataKuliah')->middleware('auth');

Route::resource('cpmk', 'CpmkController')->middleware('auth');

Route::resource('komponen-penilaian', 'KomponenPenilaianController')->middleware('auth');

Route::resource('assessment', 'AssessmentController')->middleware('auth');
Route::get('download-soal/{id}', 'AssessmentController@downloadSoal')->middleware('auth');
Route::get('download-file-nilai/{id}', 'AssessmentController@downloadFileNilai')->middleware('auth');
Route::post('assessment/{id}', 'AssessmentController@update')->middleware('auth');
Route::get('get-dosen', 'AssessmentController@getDosen')->middleware('auth');
Route::get('get-komponen-mk', 'AssessmentController@getKomponenMk')->middleware('auth');
Route::get('get-komponen-penilaian/{id}', 'KomponenPenilaianController@showJenisPenilaian')->middleware('auth');
Route::get('get-cpmk', 'AssessmentController@getCpmk')->middleware('auth');
Route::get('assessment-template', 'AssessmentController@template')->middleware('auth');
Route::post('assessment-nilai-import', 'AssessmentController@importNilai')->middleware('auth');
Route::post('update-url-jawaban/{id}', 'AssessmentController@updateUrlJawaban')->middleware('auth');

Route::resource('isi-soal', 'IsiSoalController')->middleware('auth');

Route::resource('jenis-cpl', 'JenisCplController')->middleware('auth');

Route::resource('kategori-cpl', 'KategoriCplController')->middleware('auth');

Route::resource('level', 'LevelController')->middleware('auth');

Route::resource('level-kontribusi', 'LevelKontribusiController')->middleware('auth');
Route::get('level-kontribusi-template/{id}', 'LevelKontribusiController@template')->middleware('auth');
Route::post('level-kontribusi-import', 'LevelKontribusiController@import');

Route::resource('cpl', 'CplController')->middleware('auth');
Route::post('cpl/import', 'CplController@import')->middleware('auth');
Route::get('cpl-template/{program_studi_id}', 'CplController@template')->middleware('auth');

Route::resource('mahasiswa', 'MahasiswaController')->middleware('auth');
Route::post('mahasiswa/import', 'MahasiswaController@import')->middleware('auth');
Route::get('mahasiswa-template', 'MahasiswaController@template')->middleware('auth');

Route::post('kelas-mahasiswa', 'KelasController@tambahMahasiswa')->middleware('auth');
Route::delete('kelas-mahasiswa', 'KelasController@deleteMahasiswa')->middleware('auth');
Route::delete('delete-all-mahasiswa', 'KelasController@deleteAllMahasiswa')->middleware('auth');
Route::get('kelas-template', 'KelasController@template')->middleware('auth');
Route::get('kelas-rps', 'KelasController@kelasRps')->middleware('auth');
Route::post('kelas-mahasiswa/import', 'KelasController@importMahasiswa')->middleware('auth');
Route::resource('kelas', 'KelasController')->middleware('auth');

Route::resource('verifikasi', 'VerifikasiController')->middleware('auth');
Route::get('get-soal/{id}', 'VerifikasiController@getSoal')->middleware('auth');
Route::get('get-verifikasi/{id}', 'VerifikasiController@downloadLembarVerifikasi')->middleware('auth');
Route::post('verifikasi-catatan', 'VerifikasiController@simpanCatatan')->middleware('auth');

Route::get('get-rps/{fileName}', 'RPSController@downloadRPS')->middleware('auth');
Route::post('upload-rps', 'RPSController@uploadRPS')->middleware('auth');
Route::resource('rps', 'RPSController')->middleware('auth');
Route::post('rps/{id}', 'RPSController@update')->middleware('auth');
Route::post('upload-rps', 'RPSController@uploadRPS')->middleware('auth');
Route::get('get-last-rps', 'RPSController@getLastRps')->middleware('auth');
Route::get('rps-mahasiswa-template/{id}', 'RpsController@template')->middleware('auth');
Route::post('rps-import', 'RpsController@import')->middleware('auth');
Route::post('rps-update-as-new/{id}', 'RpsController@updateAsNew')->middleware('auth');

Route::resource('score-card', 'ScoreCardController')->middleware('auth');
Route::get('score-card-export', 'ScoreCardController@export')->middleware('auth');

Route::resource('course-score-card', 'CourseScoreCardController')->middleware('auth');
Route::get('course-score-card-export', 'CourseScoreCardController@export')->middleware('auth');

Route::resource('course-scoring-sheet', 'CourseScoringSheetController')->middleware('auth');
Route::get('course-scoring-sheet-export', 'CourseScoringSheetController@export')->middleware('auth');

Route::resource('program-studi', 'ProgramStudiController')->middleware('auth');

Route::resource('detail-mata-kuliah', 'DetailMataKuliahController')->middleware('auth');

Route::resource('kelompok-nilai', 'KelompokNilaiController')->middleware('auth');

Route::resource('users', 'UsersController')->middleware('auth');
Route::resource('help', 'HelpController')->middleware('auth');

Route::resource('rekap-cpl', 'RekapCPLController')->middleware('auth');
Route::get('rekap-cpl-export', 'RekapCPLController@export')->middleware('auth');