<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatDetailMataKuliah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_mata_kuliah', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('program_studi_id')->nullable();
            $table->foreign('program_studi_id')->references('id')->on('program_studi');
            $table->unsignedBigInteger('mata_kuliah_id')->nullable();
            $table->foreign('mata_kuliah_id')->references('id')->on('mata_kuliah');
            $table->unsignedBigInteger('jenis_mata_kuliah_id')->nullable();
            $table->foreign('jenis_mata_kuliah_id')->references('id')->on('jenis_mata_kuliah');
            $table->string('alur_7_semester', 45);
            $table->string('alur_8_semester', 45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_mata_kuliah');
    }
}
