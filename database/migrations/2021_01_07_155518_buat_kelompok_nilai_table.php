<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatKelompokNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelompok_nilai', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->float('persentase');
            $table->timestamps();
        });
        Schema::table('komponen_penilaian', function (Blueprint $table) {
            $table->dropColumn('kelompok');
            $table->unsignedBigInteger('kelompok_nilai_id');
            $table->foreign('kelompok_nilai_id')->references('id')->on('kelompok_nilai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komponen_penilaian', function (Blueprint $table) {
            $table->dropForeign(['kelompok_nilai_id']);
            $table->dropColumn('kelompok_nilai_id');
            $table->enum('kelompok',[
                'NTS', 'NAS'
            ]);
        });
        Schema::dropIfExists('kelompok_nilai');
    }
}
