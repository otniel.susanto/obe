<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatKomponenPenilaian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komponen_penilaian', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rps_id');
            $table->foreign('rps_id')->references('id')->on('rps');
            $table->unsignedBigInteger('jenis_penilaian_id');
            $table->foreign('jenis_penilaian_id')->references('id')->on('jenis_penilaian');
            $table->string('aktivitas',200);
            $table->float('persentase');
            $table->enum('kelompok',[
                'NTS', 'NAS'
            ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komponen_penilaian');
    }
}
