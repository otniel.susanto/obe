<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('komponen_penilaian_id')->nullable();
            $table->foreign('komponen_penilaian_id')->references('id')->on('komponen_penilaian');
            $table->string('aktivitas');
            $table->float('persentase');
            $table->enum('kelompok',['NTS', 'NAS']);
            $table->string('jenis_penilaian');
            $table->string('jenis_soal')->nullable();
            $table->datetime('tanggal')->nullable();
            $table->string('durasi')->nullable();
            $table->string('sifat')->nullable();
            $table->string('nama_file_soal', 200)->nullable();
            $table->unsignedBigInteger('verifikasi_id')->nullable();
            $table->foreign('verifikasi_id')->references('id')->on('verifikasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment');
    }
}
