<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatLevelKontribusiCpmk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_kontribusi_cpmk', function (Blueprint $table) {
            $table->unsignedBigInteger('level_kontribusi_id');
            $table->foreign('level_kontribusi_id')->references('id')->on('level_kontribusi');
            $table->unsignedBigInteger('cpmk_id');
            $table->foreign('cpmk_id')->references('id')->on('cpmk');
            $table->primary(['level_kontribusi_id','cpmk_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_kontribusi_cpmk');
    }
}
