<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatKategoriCpl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori_cpl', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('jenis_cpl_id');
            $table->foreign('jenis_cpl_id')->references('id')->on('jenis_cpl');
            $table->string('nama');
            $table->float('bobot');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori_cpl');
    }
}
