<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatIsiSoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isi_soal', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('assessment_id');
            $table->foreign('assessment_id')->references('id')->on('assessment');
            // $table->unsignedBigInteger('subcpmk_id');
            // $table->foreign('subcpmk_id')->references('id')->on('subcpmk');
            $table->string('nomor_soal');
            $table->float('skor_maks');
            $table->string('keterangan', 500)->nullable();
            $table->longText('rubrik')->nullable();
            $table->longText('kompetensi')->nullable();
            $table->unsignedBigInteger('taksonomi_bloom_id')->nullable();
            $table->foreign('taksonomi_bloom_id')->references('id')->on('taksonomi_bloom');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('isi_soal');
    }
}
