<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatCpmkIsiSoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpmk_isi_soal', function (Blueprint $table) {
            $table->unsignedBigInteger('cpmk_id');
            $table->foreign('cpmk_id')->references('id')->on('cpmk');
            $table->unsignedBigInteger('isi_soal_id');
            $table->foreign('isi_soal_id')->references('id')->on('isi_soal');
            $table->primary(['cpmk_id', 'isi_soal_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpmk_isi_soal');
    }
}
