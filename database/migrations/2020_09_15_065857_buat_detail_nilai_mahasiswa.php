<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatDetailNilaiMahasiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_nilai_mahasiswa', function (Blueprint $table) {
            $table->unsignedBigInteger('isi_soal_id');
            $table->foreign('isi_soal_id')->references('id')->on('isi_soal');
            $table->unsignedBigInteger('nilai_mahasiswa_id');
            $table->foreign('nilai_mahasiswa_id')->references('id')->on('nilai_mahasiswa');
            $table->float('nilai');
            $table->primary(['isi_soal_id', 'nilai_mahasiswa_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_nilai_mahasiswa');
    }
}
