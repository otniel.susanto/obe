<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Mahasiswa;
use Faker\Generator as Faker;

$factory->define(Mahasiswa::class, function (Faker $faker) {
    return [
        'nrp' => $faker->unique()->numerify('160417###'),
        'nama' => $faker->name,
        'program_studi_id' => $faker->numberBetween($min= 1, $max = 7)
    ];
});
