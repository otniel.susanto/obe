<?php

use Illuminate\Database\Seeder;

class KomponenPenilaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('komponen_penilaian')->insert([
            'mata_kuliah_id' => 1,
            'jenis_penilaian_id' => 4,
            'keterangan' => 'Ujian Tengah Semester',
            'persentase' => 16,
            'kelompok' => 'NTS'
        ]);
    }
}
