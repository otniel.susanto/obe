<?php

use Illuminate\Database\Seeder;

class AssessmentKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assessment_kelas')->insert([
            'kelas_id' => 1,
            'assessment_id' => 1
        ]);
        DB::table('assessment_kelas')->insert([
            'kelas_id' => 2,
            'assessment_id' => 1
        ]);
        DB::table('assessment_kelas')->insert([
            'kelas_id' => 3,
            'assessment_id' => 1
        ]);
    }
}
