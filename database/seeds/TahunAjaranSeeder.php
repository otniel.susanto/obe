<?php

use Illuminate\Database\Seeder;

class TahunAjaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tahun_ajaran')->insert([
            'status' => 'Aktif',
            'keterangan' => 'Gasal 2020/2021',
        ]);
    }
}
