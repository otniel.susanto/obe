<?php

use Illuminate\Database\Seeder;

class KelasMahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => 1,
            'mahasiswa_id' => 1,
        ]);
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => 1,
            'mahasiswa_id' => 2,
        ]);
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => 2,
            'mahasiswa_id' => 3,
        ]);
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => 2,
            'mahasiswa_id' => 4,
        ]);
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => 3,
            'mahasiswa_id' => 5,
        ]);
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => 3,
            'mahasiswa_id' => 6,
        ]);
    }
}
