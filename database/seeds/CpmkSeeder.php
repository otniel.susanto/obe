<?php

use Illuminate\Database\Seeder;

class CpmkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cpmk')->insert([
            'rps_id' => 1,
            'kode' => 'CPMK-1',
            'keterangan' => 'Mampu memahami penggunaan class',
        ]);
        DB::table('cpmk')->insert([
            'rps_id' => 1,
            'kode' => 'CPMK-2',
            'keterangan' => 'Mampu memahami bahasa pemrograman C#',
        ]);
    }
}
