<?php

use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelas')->insert([
            'program_studi_id' => 1,
            'mata_kuliah_id' => 1,
            'rps_id' => 1,
            'tahun_ajaran_id' => 3,
            'kp' => 'A'
        ]);
        DB::table('kelas')->insert([
            'program_studi_id' => 1,
            'mata_kuliah_id' => 1,
            'rps_id' => 1,
            'tahun_ajaran_id' => 3,
            'kp' => 'B'
        ]);
        DB::table('kelas')->insert([
            'program_studi_id' => 1,
            'mata_kuliah_id' => 1,
            'rps_id' => 1,
            'tahun_ajaran_id' => 3,
            'kp' => 'C'
        ]);
    }
}
