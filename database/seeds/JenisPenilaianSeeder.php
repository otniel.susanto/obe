<?php

use Illuminate\Database\Seeder;

class JenisPenilaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_penilaian')->insert([
            'nama' => 'Tugas',
        ]);
        DB::table('jenis_penilaian')->insert([
            'nama' => 'Keaktifan',
        ]);
        DB::table('jenis_penilaian')->insert([
            'nama' => 'Kuis',
        ]);
        DB::table('jenis_penilaian')->insert([
            'nama' => 'Project',
        ]);
        DB::table('jenis_penilaian')->insert([
            'nama' => 'Ujian',
        ]);
    }
}
