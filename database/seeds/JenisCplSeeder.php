<?php

use Illuminate\Database\Seeder;

class JenisCplSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_cpl')->insert([
            'nama' => 'HARD SKILLS'
        ]);
        DB::table('jenis_cpl')->insert([
            'nama' => 'SOFT SKILLS'
        ]);
    }
}
