<?php

use Illuminate\Database\Seeder;

class RpsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rps')->insert([
            'tahun_ajaran_id' => 3,
        ]);
    }
}
