<?php

use Illuminate\Database\Seeder;
use App\Models\Mahasiswa;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // -------------Data Utama---------------
        $this->call(TaksonomiBloomSeeder::class);
        $this->call(JenisCplSeeder::class);
        $this->call(KategoriCplSeeder::class);
        $this->call(JenisPenilaianSeeder::class);
        $this->call(LevelSeeder::class);
        $this->call(ProgramStudiSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TahunAjaranSeeder::class);
        $this->call(JenisMataKuliahSeeder::class);
        $this->call(KelompokNilaiSeeder::class);
        // --------------------------------------
        // // factory(Mahasiswa::class, 40)->create();
        // $this->call(MataKuliahSeeder::class);
        // $this->call(ProgramStudiMataKuliahSeeder::class);
        // // $this->call(CpmkSeeder::class);
        // $this->call(RpsSeeder::class);
        // $this->call(KelasSeeder::class);
        // $this->call(MahasiswaSeeder::class);
        // $this->call(KelasMahasiswaSeeder::class);
        // $this->call(AssessmentSeeder::class);
        // $this->call(NilaiMahasiswaSeeder::class);
        // $this->call(AssessmentKelasSeeder::class);
        // $this->call(IsiSoalSeeder::class);
    }
}
