<?php

use Illuminate\Database\Seeder;

class JenisAssessmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_assessment')->insert([
            'nama' => 'Tugas',
        ]);
        DB::table('jenis_assessment')->insert([
            'nama' => 'Kuis',
        ]);
        DB::table('jenis_assessment')->insert([
            'nama' => 'Project',
        ]);
        DB::table('jenis_assessment')->insert([
            'nama' => 'Ujian',
        ]);
    }
}
