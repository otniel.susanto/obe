<?php

use Illuminate\Database\Seeder;

class JenisMataKuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_mata_kuliah')->insert([
            'nama' => 'Wajib'
        ]);
        DB::table('jenis_mata_kuliah')->insert([
            'nama' => 'Pilihan'
        ]);
    }
}
