<?php

use Illuminate\Database\Seeder;

class ProgramStudiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('program_studi')->insert([
            'kode' => '1600',
            'nama' => 'Fakultas Teknik',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1601',
            'nama' => 'Teknik Elektro',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1601',
            'nama' => 'Teknik Elektro - Biomedical Electronics',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1602',
            'nama' => 'Teknik Kimia',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1603',
            'nama' => 'Teknik Industri',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1603',
            'nama' => 'Teknik Industri - SES',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1603',
            'nama' => 'Teknik Industri - SSCS',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1604',
            'nama' => 'Teknik Informatika',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1604',
            'nama' => 'Teknik Informatika - DSAI',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1604',
            'nama' => 'Teknik Informatika - IMES',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1604',
            'nama' => 'Teknik Informatika - NCS',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1604',
            'nama' => 'Teknik Informatika - GD',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1605',
            'nama' => 'Teknik Mesin dan Manufaktur',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1607',
            'nama' => 'Sistem Informasi Bisnis',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1608',
            'nama' => 'Multimedia',
        ]);
        DB::table('program_studi')->insert([
            'kode' => '1609',
            'nama' => 'Information Technology Dual Degree (IT-DD)',
        ]);
    }
}
