<?php

use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('level')->insert([
            'level' => 1,
            'keterangan' => 'Low (Rendah)'
        ]);
        DB::table('level')->insert([
            'level' => 2,
            'keterangan' => 'Medium (Sedang)'
        ]);
        DB::table('level')->insert([
            'level' => 3,
            'keterangan' => 'High (Tinggi)'
        ]);
    }
}
