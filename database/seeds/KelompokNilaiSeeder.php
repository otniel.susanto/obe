<?php

use Illuminate\Database\Seeder;

class KelompokNilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelompok_nilai')->insert([
            'nama' => 'NTS',
            'persentase' => 40,
        ]);
        DB::table('kelompok_nilai')->insert([
            'nama' => 'NAS',
            'persentase' => 60,
        ]);
    }
}
