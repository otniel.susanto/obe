<?php

use Illuminate\Database\Seeder;

class MataKuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mata_kuliah')->insert([
            'kode' => '1604B021',
            'nama' => 'Object Oriented Programming',
            'sks' => '6',
        ]);
    }
}
