<?php

use Illuminate\Database\Seeder;

class CplSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cpl')->insert([
            'kategori_cpl_id' => 1,
            'kode' => 'S-1',
            'keterangan' => 'Bertaqwa kepada Tuhan Yang Maha Esa dan mampu menunjukkan sikap religius'
        ]);
        DB::table('cpl')->insert([
            'kategori_cpl_id' => 2,
            'kode' => 'PP-1',
            'keterangan' => 'Menguasai metodologi rekayasa perangkat lunak, pemrograman komputer, teknik desain dan manajemen basis data, teknik implementasi dan manajemen jaringan komputer, teknik pengamanan informasi dan jaringan komputer.'
        ]);
        DB::table('cpl')->insert([
            'kategori_cpl_id' => 3,
            'kode' => 'KK-1',
            'keterangan' => 'Mampu menganalisa masalah dan merumuskan solusinya melalui penggunaan teknologi informasi dan komunikasi'
        ]);
        DB::table('cpl')->insert([
            'kategori_cpl_id' => 4,
            'kode' => 'KU-1',
            'keterangan' => 'Mampu menerapkan pemikiran logis, kritis, sistematis, dan inovatif dalam konteks pengembangan atau implementasi ilmu pengetahuan dan teknologi yang memperhatikan dan menerapkan nilai humaniora yang sesuai dengan bidang keahliannya'
        ]);
    }
}
