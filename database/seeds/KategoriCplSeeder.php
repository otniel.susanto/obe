<?php

use Illuminate\Database\Seeder;

class KategoriCplSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori_cpl')->insert([
            'jenis_cpl_id' => 1 ,
            'nama' => 'Penguasaan Pengetahuan (PP)',
            'bobot' => 1.00
        ]);
        DB::table('kategori_cpl')->insert([
            'jenis_cpl_id' => 1 ,
            'nama' => 'Keterampilan Khusus (KK)',
            'bobot' => 1.00
        ]);
        DB::table('kategori_cpl')->insert([
            'jenis_cpl_id' => 2 ,
            'nama' => 'Sikap (S)',
            'bobot' => 1.00
        ]);
        DB::table('kategori_cpl')->insert([
            'jenis_cpl_id' => 2 ,
            'nama' => 'Keterampilan Umum (KU)',
            'bobot' => 1.00
        ]);
    }
}
