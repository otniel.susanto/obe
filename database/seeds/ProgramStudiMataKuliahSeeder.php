<?php

use Illuminate\Database\Seeder;

class ProgramStudiMataKuliahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_mata_kuliah')->insert([
            'program_studi_id' => 8,
            'mata_kuliah_id' => 1,
            'jenis_mata_kuliah_id' => 1,
            'alur_7_semester' => 2,
            'alur_8_semester' => 2
        ]);
    }
}
