<?php

use Illuminate\Database\Seeder;

class AssessmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assessment')->insert([
            'aktivitas' => 'UTS',
            'jenis_penilaian' => 'Ujian',
            'tanggal' => '2020/10/09 13:30:00',
            'persentase' => 60,
            'keterangan' => 'Ujian Tengah Semester Gasal 2020/2021',
            'kelompok' => 'NTS',
            'durasi' => '120 Menit',
            'sifat' => 'Terbuka Hard Copy',
            'jenis_soal' => 'Essay',
            'nama_file_soal' => 'soal UTS PBO.pdf'
        ]);
    }
}
