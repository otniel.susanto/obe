<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Otniel',
            'npk' => '000001',
            'email' => 'otniel@staff.ubaya.ac.id',
            'email_verified_at' => null,
            'password' => Hash::make('password'),
            'jabatan' => 'Administrator',
            'status' => 'Dosen Tetap',
            'remember_token' => null,
        ]);
        DB::table('users')->insert([
            'name' => 'Administrator Sistem',
            'npk' => '224001',
            'email' => 'admin@staff.ubaya.ac.id',
            'email_verified_at' => null,
            'password' => Hash::make('4dm1n0be'),
            'jabatan' => 'Administrator',
            'status' => 'Dosen Tetap',
            'remember_token' => null,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 8,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 9,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 10,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 11,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 12,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 14,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 15,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 16,
            'dosen_id' => 1,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 8,
            'dosen_id' => 2,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 9,
            'dosen_id' => 2,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 10,
            'dosen_id' => 2,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 11,
            'dosen_id' => 2,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 12,
            'dosen_id' => 2,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 14,
            'dosen_id' => 2,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 15,
            'dosen_id' => 2,
        ]);
        DB::table('program_studi_dosen')->insert([
            'program_studi_id' => 16,
            'dosen_id' => 2,
        ]);
    }
}
