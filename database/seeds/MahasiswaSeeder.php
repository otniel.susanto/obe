<?php

use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mahasiswa')->insert([
            'nrp' => '160418006',
            'nama' => 'FELIKS ADHITAMA',
        ]);
        DB::table('mahasiswa')->insert([
            'nrp' => '160418008',
            'nama' => 'SESILIA SHANIA',
        ]);
        DB::table('mahasiswa')->insert([
            'nrp' => '160418037',
            'nama' => 'FASYEKH KHOIRUL MUBARROK',
        ]);
        DB::table('mahasiswa')->insert([
            'nrp' => '160418039',
            'nama' => 'ANDREAS TANDIONO',
        ]);
        DB::table('mahasiswa')->insert([
            'nrp' => '160418070',
            'nama' => 'JOHANES REXY MAYDERIO SARAGIH',
        ]);
        DB::table('mahasiswa')->insert([
            'nrp' => '160418071',
            'nama' => 'MATTHEW NOTO SISWO',
        ]);
    }
}
