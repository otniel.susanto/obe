<?php

use Illuminate\Database\Seeder;

class IsiSoalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('isi_soal')->insert([
        //     'assessment_id' => 1,
        //     'nomor_soal' => 'Pengurangan',
        //     'skor_maks' => -5,
        //     'keterangan' => 'Nama Folder paling luar tidak sesuai',
        //     'rubrik' => '-',
        //     'taksonomi_bloom_id' => 3
        // ]);
        // DB::table('isi_soal')->insert([
        //     'assessment_id' => 1,
        //     'nomor_soal' => 'Pengurangan',
        //     'skor_maks' => -5,
        //     'keterangan' => 'Nama class tidak sesuai ketentuan',
        //     'rubrik' => '-',
        //     'taksonomi_bloom_id' => 3
        // ]);
        // DB::table('isi_soal')->insert([
        //     'assessment_id' => 1,
        //     'nomor_soal' => 'Pengurangan',
        //     'skor_maks' => -5,
        //     'keterangan' => 'Penggunaan huruf besar kecil tidak sesuai ketentuan',
        //     'rubrik' => '-',
        //     'taksonomi_bloom_id' => 3
        // ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '1',
            'skor_maks' => 10,
            'keterangan' => 'class, data member, property, constructor',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '2',
            'skor_maks' => 10,
            'keterangan' => 'Exception',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '3',
            'skor_maks' => 5,
            'keterangan' => 'List children/kids',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '4a',
            'skor_maks' => 10,
            'keterangan' => 'Method add data to list',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '4b',
            'skor_maks' => 10,
            'keterangan' => 'Method count data in list',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '4c',
            'skor_maks' => 10,
            'keterangan' => 'Method get data at specific index',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '4d',
            'skor_maks' => 10,
            'keterangan' => 'Method delete data at specific index',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '5',
            'skor_maks' => 5,
            'keterangan' => 'List staff/employee',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '6',
            'skor_maks' => 10,
            'keterangan' => 'Form Menu',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '7',
            'skor_maks' => 10,
            'keterangan' => 'Form Add Data Staff/Employee',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
        DB::table('isi_soal')->insert([
            'assessment_id' => 1,
            'nomor_soal' => '8',
            'skor_maks' => 10,
            'keterangan' => 'Form Add Data Child/Kid',
            'rubrik' => '-',
            'taksonomi_bloom_id' => 3
        ]);
    }
}
