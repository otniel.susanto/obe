<?php

use Illuminate\Database\Seeder;

class NilaiMahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nilai_mahasiswa')->insert([
            'assessment_id' => 1,
            'mahasiswa_id' => 1,
            'nilai' => 0
        ]);
        DB::table('nilai_mahasiswa')->insert([
            'assessment_id' => 1,
            'mahasiswa_id' => 2,
            'nilai' => 0
        ]);
        DB::table('nilai_mahasiswa')->insert([
            'assessment_id' => 1,
            'mahasiswa_id' => 3,
            'nilai' => 0
        ]);
        DB::table('nilai_mahasiswa')->insert([
            'assessment_id' => 1,
            'mahasiswa_id' => 4,
            'nilai' => 0
        ]);
        DB::table('nilai_mahasiswa')->insert([
            'assessment_id' => 1,
            'mahasiswa_id' => 5,
            'nilai' => 0
        ]);
        DB::table('nilai_mahasiswa')->insert([
            'assessment_id' => 1,
            'mahasiswa_id' => 6,
            'nilai' => 0
        ]);
    }
}
