@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Edit Data RPS</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('rps')}}">RPS</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('rps')}}/{{$rps->id}}">Show</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('rps')}}/{{$rps->id}}/edit">Edit</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formRps" enctype="multipart/form-data">
            <div class="form-group row">
                <div class="col-lg-4">
                    <label for="program_studi_id">Program Studi</label>
                    <select id="program_studi_id" class="select2 form-control">
                        <option disabled></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}"{{($item->id == $rps->program_studi_id ? 'selected' : '')}}>
                            {{$item->nama}}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-5">
                    <label for="mata_kuliah_id">Mata Kuliah</label>
                    <select id="mata_kuliah_id" class="select2 form-control">
                        <option disabled></option>
                        @foreach($mata_kuliah as $item)
                        <option value="{{$item->id}}"{{($item->id == $rps->mata_kuliah_id ? 'selected' : '')}}>
                        {{$item->kode}} - {{$item->nama}}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <label for="dosen_pengampu_id">Dosen Pengampu</label>
                    <select id="dosen_pengampu_id" class="select2 form-control">
                        @foreach($dosen as $item)
                        <option value="{{$item->id}}"
                            {{($rps->dosen_pengampu_id == $item->id ? 'selected' : '')}}>
                            {{$item->name}}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-5">
                    <label for="keterangan">CPMK</label>
                    <textarea rows="4" class="form-control" id="keterangan" placeholder="Isi CPMK">{{$rps->keterangan}}</textarea>
                </div>
                <div class="col-md-2">
                    <label for="file">Upload File RPS (akan menghapus yang lama)</label>
                    <input type="file" name="file" id="file">
                </div>
            </div>
        </form>

        <div class="row mb-1">
            <div class="col-4">
                <h5>Sub-CPMK / Kompetensi Dasar (KD)</h5>
            </div>
            <div class="col-5 float-right">
                <button onclick="event.preventDefault();formAddCpmk();" class="btn btn-primary float-right open-modal"
                    data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Tambah Sub-CPMK / KD
                </button>
            </div>
        </div>
        <!-- Table CPMK -->
        <div class="row">
            <div class="col-md-9">
                <div class='table-responsive'>
                    <table width="100% data-toggle=" class="table myTable table-hover" id="tableCpmk">
                        <tr>
                            <th width="15%">Kode</th>
                            <th width="80%">Keterangan</th>
                            <th width="10%">Edit</th>
                            <th width="10%">Delete</th>
                        </tr>
                        @foreach($rps->cpmk->sortBy('kode') as $item)
                        <tr id="cpmk{{$item->id}}">
                            <td>{{$item->kode}}</td>
                            <td>{{$item->keterangan}}</td>
                            <td>
                                <a onclick="event.preventDefault();formEditCpmk({{$item->id}});" class="edit open-modal"
                                    data-toggle="modal" value="{{$item->id}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            <td>
                                <a onclick="event.preventDefault();formDeleteCpmk({{$item->id}});" class="delete"
                                    data-toggle="modal">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="row mb-1">
            <div class="col-3">
                <h5>Komponen Penilaian</h5>
            </div>
            <div class="col-9">
                <button onclick="event.preventDefault();formCreateKomponenPenilaian();"
                    class="btn btn-primary float-right open-modal" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Tambah Komponen Penilaian
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-auto">
                <p class="bg-warning">NA = 40% NTS + 60% NAS</p>
            </div>
        </div>

        <div class='table-responsive'>
            <table class="table myTable table-hover" id="tableKomponenPenilaian">
                <tr>
                    <th class="text-center align-middle" width="10%">Jenis Penilaian</th>
                    <th class="text-center align-middle" width="50%">Tugas / Aktivitas</th>
                    <th class="text-center align-middle" width="10%">Persentase</th>
                    <th class="text-center align-middle" width="10%">Kelompok Nilai</th>
                    <th class="text-center align-middle" width='10%'>Edit</th>
                    <th class="text-center align-middle" width='10%'>Delete</th>
                </tr>
                @foreach($rps->komponenPenilaian as $item)
                <tr id="komponen{{$item->id}}">
                    <td>{{$item->jenisPenilaian->nama}}</td>
                    <td>{{$item->aktivitas}}</td>
                    <td>{{$item->persentase}}%</td>
                    <td>{{$item->kelompokNilai->nama}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditKomponenPenilaian({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteKomponenPenilaian({{$item->id}});"
                            class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>

        <div class="form-group row">
            <div class="col-12">
                <button type="button" class="btn btn-primary float-right m-1" id="btnUpdate">
                    <i class="fa fa-save"></i>
                    Simpan
                </button>
                <button disabled type="button" class="btn btn-success float-right m-1" id="btnCreate">
                    <i class="fa fa-save"></i>
                    Simpan Sebagai Data Baru
                </button>
            </div>
        </div>
    </div>
    @include('kelas.modal_edit')
    @include('kelas.modal_delete')
    @include('cpmk.modal_add')
    @include('cpmk.modal_edit')
    @include('cpmk.modal_delete')
    @include('komponen_penilaian.create')
    @include('komponen_penilaian.edit')
    @include('komponen_penilaian.delete')
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(window).bind('beforeunload', function(){
        return 'Data belum disimpan, apakah anda yakin untuk meninggalkan halaman ini?';
    });
    var iKomponen = 0;
    var iCpmk = 0;
    var iKelas = 0;
    var cpmk_create = [];
    var cpmk_update = [];
    var cpmk_delete = [];
    var komponen_penilaian_create = [];
    var komponen_penilaian_update = [];
    var komponen_penilaian_delete = [];
    $(document).ready(function () {
        $('.select2').select2();
        $('#add-jenis_penilaian_id').select2({
            placeholder: 'Pilih Jenis Penilaian'
        });
        $('#edit-jenis_penilaian_id').select2({
            placeholder: 'Pilih Jenis Penilaian'
        });
        $("#btnUpdate").click(function () {
            submitData("Update");
        });
        $("#btnCreate").click(function () {
            submitData("Create");
        });

        $("#btnTambahCpmk").click(function () {
            cpmk_create.push({
                index: ++iCpmk,
                kode: $('#add-kodeCpmk').val(),
                keterangan: $('#add-keteranganCpmk').val()
            });
            $('#tableCpmk tr:last').after('<tr id="cpmkBaru' + iCpmk + '">' +
                '<td>' + $('#add-kodeCpmk').val() + '</td>' +
                '<td>' + $('#add-keteranganCpmk').val() + '</td>' +
                '<td>-</td>' +
                '<td class="actionCpmk"><a onclick="event.preventDefault();deleteCpmk(' +
                iCpmk +
                ');" data-toggle="modal">' +
                '<i class="fa fa-trash"></i></a></td>' +
                '</tr>'
            );
            $('#kodeCpmk').val('');
            $('#keteranganCpmk').val('');
            $('#modalAddCpmk').modal('hide');
        });
        $("#btnUpdateCpmk").click(function () {
            for (i = 0; i < cpmk_update.length; i++) {
                if (cpmk_update[i].id == $('#id-edit-cpmk').val()) {
                    cpmk_update.splice(i, 1);
                }
            }
            cpmk_update.push({
                id: $('#id-edit-cpmk').val(),
                kode: $('#edit-kodeCpmk').val(),
                keterangan: $('#edit-keteranganCpmk').val()
            });
            $('tr#cpmk' + $('#id-edit-cpmk').val()).html('');
            $('tr#cpmk' + $('#id-edit-cpmk').val()).append('<td>' + $('#edit-kodeCpmk').val() +
                '</td>' +
                '<td>' + $('#edit-keteranganCpmk').val() + '</td>' +
                '<td><a onclick="event.preventDefault();formEditCpmk(' + $('#id-edit-cpmk').val() +
                ');"' +
                'class="edit open-modal" data-toggle="modal" value="' + $('#id-edit-cpmk').val() +
                '">' +
                '<i class="fa fa-edit"></i></a></td>' +
                '<td><a onclick="event.preventDefault();formDeleteCpmk(' + $('#id-edit-cpmk')
            .val() + ');"' +
                'class="delete" data-toggle="modal"><i class="fa fa-trash"></i></a></td>'
            );
            $('#edit-kodeCpmk').val('');
            $('#edit-keteranganCpmk').val('');
            $('#modalEditCpmk').modal('hide');
        });
        $("#btnDeleteCpmk").click(function () {
            cpmk_delete.push({
                id: $("#formDeleteCpmk #id-delete-cpmk").val()
            });
            $("tr#cpmk" + $("#formDeleteCpmk #id-delete-cpmk").val()).remove();
            $('#modalDeleteCpmk').modal('hide');
        });

        $("#btnTambahKomponenPenilaian").click(function () {
            komponen_penilaian_create.push({
                index: ++iKomponen,
                jenis_penilaian_id: $('#add-jenis_penilaian_id').val(),
                aktivitas: $('#add-aktivitas').val(),
                persentase: $('#add-persentase').val(),
                kelompok: $("input[name='add-kelompok']:checked").val()
            });
            $('#tableKomponenPenilaian tr:last').after('<tr id="komponenBaru' + iKomponen + '">' +
                '<td>' + $('#add-jenis_penilaian_id option:selected').text() + '</td>' +
                '<td>' + $('#add-aktivitas').val() + '</td>' +
                '<td>' + $('#add-persentase').val() + '%</td>' +
                '<td>' + $("input[name='add-kelompok']:checked").data("name") + '</td>' +
                '<td>-</td>' +
                '<td><a onclick="event.preventDefault();deleteKomponenPenilaian(' +
                iKomponen +
                ');" data-toggle="modal">' +
                '<i class="fa fa-trash"></i></a></td>' +
                '</tr>'
            );
            $('#add-aktivitas').val('');
            $('#add-persentase').val('');
            $('#modalCreateKomponenPenilaian').modal('hide');
            console.log(komponen_penilaian_create);
        });
        $("#btnUpdateKomponenPenilaian").click(function () {
            for (i = 0; i < komponen_penilaian_update.length; i++) {
                if (komponen_penilaian_update[i].id == $('#id-edit-komponen-penilaian').val()) {
                    komponen_penilaian_update.splice(i, 1);
                }
            }
            komponen_penilaian_update.push({
                id: $('#id-edit-komponen-penilaian').val(),
                jenis_penilaian_id: $('#edit-jenis_penilaian_id').val(),
                aktivitas: $('#edit-aktivitas').val(),
                persentase: $('#edit-persentase').val(),
                kelompok: $("input[name='edit-kelompok']:checked").val()
            });
            $('tr#komponen' + $('#id-edit-komponen-penilaian').val()).html('');
            $('tr#komponen' + $('#id-edit-komponen-penilaian').val()).append('<td>' + 
                $('#edit-jenis_penilaian_id option:selected').text() + '</td>' +
                '<td>' + $('#edit-aktivitas').val() + '</td>' +
                '<td>' + $('#edit-persentase').val() + '%</td>' +
                '<td>' + $("input[name='edit-kelompok']:checked").data("name") + '</td>' +
                '<td><a onclick="event.preventDefault();formEditKomponenPenilaian(' + 
                $('#id-edit-komponen-penilaian').val() + ');"'+
                'class="edit open-modal" data-toggle="modal" value="'+ 
                $('#id-edit-komponen-penilaian').val() + '"><i class="fa fa-edit"></i></a></td>'+
                '<td><a onclick="event.preventDefault();formDeleteKomponenPenilaian('+ 
                $('#id-edit-komponen-penilaian').val() + 
                ');"class="delete" data-toggle="modal"><i class="fa fa-trash"></i></a></td>'
            );
            console.log(komponen_penilaian_update);
            $('#modalEditKomponenPenilaian').modal('hide');
        });
        $("#btnDeleteKomponenPenilaian").click(function () {
            komponen_penilaian_delete.push({
                id: $("#id-delete-komponen-penilaian").val()
            });
            $("tr#komponen" + $("#id-delete-komponen-penilaian").val()).remove();
            $('#modalDeleteKomponenPenilaian').modal('hide');
        });

    });

    function deleteCpmk(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#cpmkBaru' + id).remove();
            var index = cpmk_create.findIndex(function (item) {
                return item.index == id;
            });
            cpmk_create.splice(index, 1);
        }
    }
    function formAddCpmk() {
        $("#create-error-bag").hide();
        $('#modalAddCpmk').modal('show');
    }
    function formEditCpmk(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("cpmk")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-cpmk-error-bag").hide();
                $("#formEditCpmk #edit-kodeCpmk").val(data.cpmk.kode);
                $("#formEditCpmk #edit-keteranganCpmk").val(data.cpmk.keterangan);
                $("#formEditCpmk #id-edit-cpmk").val(data.cpmk.id);
                $('#modalEditCpmk').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    function formDeleteCpmk(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("cpmk")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteCpmk #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data-cpmk").html("<p>Kode: " + data.cpmk.kode + "</p>" +
                    "<p>" + data.cpmk.keterangan + "</p>")
                $("#formDeleteCpmk #id-delete-cpmk").val(data.cpmk.id);
                $('#modalDeleteCpmk').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function deleteKomponenPenilaian(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#komponenBaru' + id).remove();
            var index = komponen_penilaian_create.findIndex(function (item) {
                return item.index == id;
            });
            komponen_penilaian_create.splice(index, 1);
            console.log(komponen_penilaian_create);
        }
    }
    function formCreateKomponenPenilaian() {
        $("#create-komponen-penilaian-error-bag").hide();
        $('#modalCreateKomponenPenilaian').modal('show');
    }
    function formEditKomponenPenilaian(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("komponen-penilaian")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-komponen-penilaian-error-bag").hide();
                $("#edit-jenis_penilaian_id").val(data.komponen_penilaian
                    .jenis_penilaian_id).change();
                $("#edit-aktivitas").val(data.komponen_penilaian.aktivitas);
                $("#edit-persentase").val(data.komponen_penilaian.persentase);
                data.komponen_penilaian.kelompok == "NTS" ? $("#edit-kelompok-nts").prop("checked", true) :
                    $("#edit-kelompok-nas").prop("checked", true);
                // $("#kelompok").val(data.komponen_penilaian.kelompok);
                $("#id-edit-komponen-penilaian").val(data.komponen_penilaian.id);
                $('#modalEditKomponenPenilaian').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    function formDeleteKomponenPenilaian(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("komponen-penilaian")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteKomponenPenilaian #delete-title").html(
                    "Apakah anda yakin menghapus data ini?");
                $("#delete-data-komponen-penilaian").html("<p>Komponen Penilaian</p><p>" + data.komponen_penilaian.aktivitas +
                    "</p>" +
                    "<p>Jenis Penilaian: " + data.jenis_penilaian.nama + "</p>" +
                    "<p>Persentase: " + data.komponen_penilaian.persentase + "%</p>" +
                    "<p>Kelompok: " + data.komponen_penilaian.kelompok + "</p>")
                $("#formDeleteKomponenPenilaian #id-delete-komponen-penilaian").val(data.komponen_penilaian
                    .id);
                $('#modalDeleteKomponenPenilaian').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function submitData(perintah){
        var url = "";
        if(perintah == "Update"){
            url = '{{url("rps")}}/{{$rps->id}}'
        }
        else{
            if(!confirm("Anda harus memasukkan kembali data Matriks CPL vs CPMK bila memilih menyimpan sebagai data baru.\n" + 
            "Apakah anda yakin untuk melanjutkan?")){
                return false;
            }
            url = '{{url("rps-update-as-new")}}/{{$rps->id}}'
        }
        var formData = new FormData();
        var files = $('#file')[0].files[0];
        formData.append('file', files);
        formData.append('keterangan', $('#keterangan').val());
        formData.append('program_studi_id', $('#program_studi_id').val());
        formData.append('mata_kuliah_id', $('#mata_kuliah_id').val());
        formData.append('dosen_pengampu_id', $('#dosen_pengampu_id').val());
        formData.append('cpmk_create', JSON.stringify(cpmk_create));
        formData.append('cpmk_update', JSON.stringify(cpmk_update));
        formData.append('cpmk_delete', JSON.stringify(cpmk_delete));
        formData.append('komponen_penilaian_create', JSON.stringify(komponen_penilaian_create));
        formData.append('komponen_penilaian_update', JSON.stringify(komponen_penilaian_update));
        formData.append('komponen_penilaian_delete', JSON.stringify(komponen_penilaian_delete));

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            method: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                alert(data.message);
                window.location.replace("{{url('rps')}}/{{$rps->id}}");
            },
            error: function (data) {
                console.log(data.message);
                console.log(data.request);
            }
        });
    }
</script>
@endpush
