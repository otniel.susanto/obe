<!-- Konfirmasi Rps Modal Form HTML -->
<div class="modal fade" id="modalKonfirmasiRps">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formKonfirmasiRps">
                <div class="modal-header">
                    <h4 class="modal-title" id="konfirmasi-rps-title" name="title">Apakah anda yakin menghapus data RPS dan Kelas ini?</h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="konfirmasi-error-bag"></div>
                    <div id="konfirmasi-update">
                        <p>Perubahan data RPS akan dilakukan. Apakah anda yakin untuk melanjutkan?</p>
                    </div>
                    <div id="konfirmasi-update-as-new">
                        <h6>Hal yang perlu diperhatikan sebelum mengkonfirmasi perubahan ini:</h6>
                        <ul class="alert alert-warning">
                            <li>Data RPS akan dinonaktifkan (Tahun Ajaran dimundurkan pada data terakhir yang nonaktif)</li>
                            <li>Data RPS yang telah dinonaktifkan tidak dapat diaktifkan kembali</li>
                            <li>Data Matriks CPL vs CPMK harus diinput kembali, khusus untuk Mata Kuliah pada RPS ini karena data tidak tersebut tidak dibuat ulang</li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-primary" value="" id="btnKonfirmasiUpdate" type="button">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
