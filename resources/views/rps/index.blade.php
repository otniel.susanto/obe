@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Rencana Pembelajaran Semester (RPS)</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('rps')}}">RPS</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Filter Section -->
        <form action="{{url('rps')}}" method="get">
            <div class="row">
                <div class="col-md-4">
                    <select name="program_studi_id" id="program_studi_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->program_studi_id) selected @endif
                        >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <input name="kode_mata_kuliah" type="search" class="form-control" placeholder="Kode Mata Kuliah" value="{{$request->kode_mata_kuliah}}">
                </div>
                <div class="col-md-3">
                    <input name="nama_mata_kuliah" type="search" class="form-control" placeholder="Nama Mata Kuliah" value="{{$request->nama_mata_kuliah}}">
                </div>
                <div class="col-md-2">
                    <select id="tahun_ajaran_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}">{{$item->keterangan}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="m-1">
                <a href="{{url('rps/create')}}" type="button" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </a>
                <button class="btn btn-secondary float-right" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                <button class="btn btn-primary float-right mr-1" type="submit" id="btnFilter"><i class="fa fa-search"></i> Cari</button>
            </div>
        </form>
        <!-- Filter Section End -->
        <table width="100%" data-toggle="table" data-pagination="true" class="table myTable table-striped table-hover" id="tableAssessment">
            <thead>
                <tr>
                    <th >No.</th>
                    <th>Program Studi</th>
                    <th>Kode Mata Kuliah</th>
                    <th>Nama Mata Kuliah</th>
                    <th>Update Terakhir</th>
                    <th>File RPS</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rps as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->programStudi->nama}}</td>
                    <td><a href="{{url('rps')}}/{{$item->id}}">{{$item->mataKuliah->kode}}</a></td>
                    <td><a href="{{url('rps')}}/{{$item->id}}">{{$item->mataKuliah->nama}}</a></td>
                    <td>
                        {{\Carbon\Carbon::parse($item->updated_at)->isoFormat('D-MMMM-YYYY / HH:mm')}} WIB <br>
                        Oleh: {{$item->pembuatRevisi->name}}
                    </td>
                    <td>
                    @if(isset($item->nama_file_rps))
                        <a href="{{url('get-rps')}}/{{$item->nama_file_rps}}" 
                        class="" target="_blank">
                        <i class="fa fa-download"></i>
                        Download File RPS</a>
                    @else
                        Belum ada file RPS <br> 
                        <a href="{{url('rps')}}/{{$item->id}}"><u>Segera upload!</u></a>
                    @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        // Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi"
        });
        $('#tahun_ajaran_id').select2({
            allowClear: true,
            placeholder: "Pilih Tahun Ajaran"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('rps')}}");
        });
        //Filter Section End
    });
</script>
@endpush