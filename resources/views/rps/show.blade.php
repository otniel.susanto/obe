@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <h1>Rincian Data RPS</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('rps')}}">RPS</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('rps')}}/{{$rps->id}}">Rincian</a></li>
                </ol>
            </div>
            <div class="col-md-8">
                <a class="btn btn-warning float-sm-right mr-1" href="{{url('rps')}}/{{$rps->id}}/edit">
                    <i class="fa fa-edit"></i>
                    Edit RPS
                </a>
                <button onclick="event.preventDefault();formDeleteRPS();" data-toggle="modal" type="button"
                    class="btn btn-danger float-sm-right mr-1">
                    <i class="fa fa-trash"></i>
                    Delete RPS
                </button>
                <a href="{{url('get-rps')}}/{{$rps->nama_file_rps}}" class="btn btn-info float-sm-right mr-1"
                    target="_blank">
                    <i class="fa fa-download"></i>
                    Download File RPS
                </a>
                <button onclick="event.preventDefault();formUploadRPS();" data-toggle="modal" type="button"
                    class="btn btn-primary float-sm-right mr-1">
                    <i class="fa fa-upload"></i>
                    Upload File RPS
                </button>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Detail RPS -->
                <div class="row">
                    <div class="col-md-3">
                        <h6 class="">Program Studi</h6>
                    </div>
                    <div class="col-auto">
                        <h6><u>{{$rps->programStudi->nama}}</u></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h6 class="">Nama Mata Kuliah</h6>
                    </div>
                    <div class="col-auto">
                        <h6><u>{{$rps->mataKuliah->nama}}</u></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h6 class="">Kode Mata Kuliah</h6>
                    </div>
                    <div class="col-auto">
                        <h6><u>{{$rps->mataKuliah->kode}}</u></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h6 class="">SKS</h6>
                    </div>
                    <div class="col-auto">
                        <h6><u>{{$rps->mataKuliah->sks}} SKS</u></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h6 class="">Semester</h6>
                    </div>
                    <div class="col">
                        <h6><u>
                            @foreach($rps->mataKuliah->programStudi as $item) 
                                {{($item->id == $rps->program_studi_id ? $item->pivot->alur_7_semester . ' (Alur 7 Semester) | ' . $item->pivot->alur_8_semester . ' (Alur 8 Semester)' : '')}}
                            @endforeach
                            </u>
                        </h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h6 class="">Dosen Pengampu</h6>
                    </div>
                    <div class="col-auto">
                        <h6><u>{{$rps->dosenPengampu->name}}</u></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h6 class="">Terakhir Diupdate/ Digunakan</h6>
                    </div>
                    <div class="col-auto">
                        <h6><u>{{$rps->tahunAjaran->keterangan}} - {{$rps->tahunAjaran->status}}</u></h6>
                    </div>
                </div>
                <!-- Detail RPS End -->
            </div>
        </div>

        <br>
        <h6>CPMK</h6>
        <div class="row">
            <div class="col-md-6">
                <h6>{{$rps->keterangan}}</h6>
            </div>
        </div>
        <br>

        <!-- Tabel Sub-CPMK dan Komponen Penilaian -->
        <div class="row">
            <!-- Tabel CPMK -->
            <div class="col-md-6">
                <h6>Sub-CPMK / Kompetensi Dasar (KD)</h6>
                <div class="table-responsive">
                    <table class="table textWrap table-bordered table-hover" id="tableCpmk">
                        <tr>
                            <th class="text-center align-middle" width="5%">No.</th>
                            <th class="text-center align-middle" width="10%">Kode</th>
                            <th class="text-center align-middle" width="50%">Keterangan</th>
                        </tr>
                        @foreach($rps->cpmk as $key=>$item)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-left">{{$item->kode}}</td>
                            <td class="text-left">{{$item->keterangan}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <!-- Table CPMK End -->
            <!-- Table Komponen Penilaian -->
            <div class="col-md-6">
                <h6>Tugas / Aktivitas dan Penilaian (Assessment)
                <br>
                <span>NA = 
                    @foreach($kelompok_nilai as $key=>$item)
                        @if($key>0) + @endif
                        {{$item->persentase}}% {{$item->nama}}
                    @endforeach
                </span>
                </h6>
                <div class="table-responsive">
                    <table class="table textWrap table-bordered table-hover" id="tableKomponenPenilaian">
                        <tr>
                            <th class="text-center align-middle" width="5%">No.</th>
                            <th class="text-center align-middle" width="20%">Tugas / Aktivitas</th>
                            <th class="text-center align-middle" width="20%">Kelompok Nilai</th>
                            <th class="text-center align-middle" width="5%">Persentase <br> dari Kelompok Nilai</th>
                        </tr>
                        @foreach($rps->komponenPenilaian as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->aktivitas}}</td>
                            <td>{{$item->kelompokNilai->nama}}</td>
                            <td>{{$item->persentase}}%</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <!-- Table Komponen Penilaian End -->
        </div>
        <!-- Tabel Histori Dokumen RPS -->
        <div class="row">
            <div class="col-md-10">
                <h6>Histori Dokumen</h6>
                <div class="table-responsive">
                    <table class="table textWrap table-bordered table-hover" id="tableCpmk">
                        <tr>
                            <th class="text-center align-middle" width="10%">Rev.</th>
                            <th class="text-center align-middle" width="25%">Tanggal</th>
                            <th class="text-center align-middle" width="20%">Direvisi oleh</th>
                            <th class="text-center align-middle" width="20%">Terakhir digunakan pada Tahun Ajaran</th>
                        </tr>
                        @foreach($histori_rps as $key=>$item)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">
                                <a href="{{url('rps')}}/{{$item->id}}">
                                    {{\Carbon\Carbon::parse($item->updated_at)->isoFormat('D-MMMM-YYYY / HH:mm')}} WIB
                                </a>
                            </td>
                            <td class="text-center">{{$item->pembuatRevisi->name}}</td>
                            <td class="text-center">{{$item->tahunAjaran->keterangan}} - {{$item->tahunAjaran->status}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    @include('rps.upload')
    @include('rps.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpload").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);
            formData.append('rps_id', {{$rps->id}});

            $.ajax({
                url: '{{url("upload-rps")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    $('#formUploadRPS').trigger("reset");
                    $("#formUploadRPS .close").click();
                    window.location.reload();
                },
                error: function (data) {

                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("rps")}}/' + {{$rps->id}},
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteRps .close").click();
                    window.location.replace("{{url('rps')}}");
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formUploadRPS() {
        $("#upload-error-bag").hide();
        $('#modalUploadRPS').modal('show');
    }
    function formDeleteRPS() {
        $("#delete-error-bag").hide();
        $('#modalDeleteRps').modal('show');
    }
</script>
@endpush
