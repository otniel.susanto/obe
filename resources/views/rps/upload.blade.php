<!-- Upload Modal HTML -->
<div class="modal fade" id="modalUploadRPS">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formUploadRPS" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Upload RPS</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="upload-error-bag">
                        <ul id="upload-rps-errors"> </ul>
                    </div>
                    
                    <label for="file">Pilih File RPS</label><br>
                    <input type="file" name="file" id="file" required="required">
                    <p class="text-danger">File RPS lama akan terhapus dari database</p>
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpload" type="button">
                        <i class="fa fa-upload"></i> 
                        Upload
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

