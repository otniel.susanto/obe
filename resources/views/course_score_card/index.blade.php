@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1 class="mb-1">Course Competencies Score Card</h1>
        <!-- Filter Section -->
        <form action="{{url('course-score-card')}}">
            <div class="form-group row">
                <div class="col-md-3">
                    <select id="program_studi_id" name="program_studi_id" class="form-control" placeholder="Pilih Program Studi" required>
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->program_studi_id)
                                    selected
                                @endif
                            >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <select id="mata_kuliah_id" name="mata_kuliah_id" class="form-control" placeholder="Pilih Mata Kuliah" required>
                        <option disabled selected></option>
                        @foreach($mata_kuliah as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->mata_kuliah_id)
                                    selected
                                @endif
                            >{{$item->kode}} - {{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select id="tahun_ajaran_id" name="tahun_ajaran_id" class="form-control" placeholder="Pilih Tahun Ajaran" required>
                        <option disabled selected></option>
                        @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}"
                                @if($item->id == $request->tahun_ajaran_id)
                                    selected
                                @endif
                            >{{$item->keterangan}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <input name="kp" type="search" class="form-control" placeholder="KP" value="{{$request->kp}}">
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" type="submit" id="btnCari"><i class="fa fa-search"></i> Cari</button>
                    <button class="btn btn-secondary" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @if($kelas === null || $kelas->count() < 0)
        <h5>Belum ada data, silakan mencari data lainnya</h5>
        @else
        <!-- Keterangan CSCS -->
        <div>
            <!-- Keterangan Mata Kuliah -->
            <h5>Program Studi: <br> {{$kelas[0]->programStudi->nama}}</h5>
            <h5>Mata Kuliah: <br> {{$kelas[0]->mataKuliah->kode}} - {{$kelas[0]->mataKuliah->nama}}</h5>
            <h5>Tahun Ajaran: <br> {{$kelas[0]->tahunAjaran->keterangan}}</h5>
            <span>
                <form action="{{url('course-score-card-export')}}" method="get">
                    <input type="hidden" name="program_studi_id" value="{{$request->program_studi_id}}">
                    <input type="hidden" name="mata_kuliah_id" value="{{$request->mata_kuliah_id}}">
                    <input type="hidden" name="tahun_ajaran_id" value="{{$request->tahun_ajaran_id}}">
                    <input type="hidden" name="kp" value="{{$request->kp}}">
                    <button type="submit" class="btn btn-success float-sm-right mb-1">
                    <i class="fa fa-file-excel"></i>
                    Export ke File Excel</button>
                </form>
            </span>
        </div>
        <!-- Tabel CSCS -->
        <div class='table-responsive'>
            <table data-toggle="table" data-pagination="true" class="table myTable table-bordered table-hover" id="tableCourseScoreCard">
                <thead>
                    <!-- ROW Pertama / SEMESTER, Capaian Pembelajaran Lulusan (CPL) -->
                    <tr>
                        <th data-sortable="true" rowspan='4' class='text-center align-middle'>No.</th>
                        <th rowspan='3' colspan='7' class='text-center align-middle'>Mahasiswa</th>
                        <th colspan="{{($cpl->count() + $cpl->groupBy('program_studi_id')->count())*2}}" class="text-center">Capaian Pembelajaran Lulusan (CPL)</th>
                        <!-- <th rowspan='2' class='text-center align-middle'>Total Skor</th> -->
                    </tr>
                    <!-- ROW KEDUA / Program Studi CPL -->
                    <tr>
                        @foreach($cpl->groupBy('program_studi_id') as $itemCpl)
                            <th colspan="{{($itemCpl->count() + 1)*2}}" class="text-center align-middle">{{$itemCpl[0]->programStudi->nama}}</th>
                        @endforeach
                    </tr>
                    <!-- ROW KETIGA / KODE CPL -->
                    <tr>
                        @foreach($cpl as $key=>$itemCpl)
                            <th colspan="2" class='text-center align-middle'>{{$itemCpl->kode}}</th>
                            @if(isset($cpl[$key+1]))
                                @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                                    <th colspan="2" class='text-center align-middle'>Total</th>
                                    @php 
                                        $totalSkorMaks = 0;
                                    @endphp
                                @endif
                            @else
                                <th colspan="2" class='text-center align-middle'>Total</th>
                            @endif
                        @endforeach
                    </tr>
                    <!-- ROW KEEMPAT / ROW SKOR MAKS -->
                    <tr>
                        <th data-sortable="true" class='text-center align-middle'>NRP</th>
                        <th data-sortable="true" class='text-center align-middle'>Nama</th>
                        <th data-sortable="true" class='text-center align-middle'>KP</th>
                        <th data-sortable="true" class='text-center align-middle'>NTS</th>
                        <th data-sortable="true" class='text-center align-middle'>NAS</th>
                        <th data-sortable="true" class='text-center align-middle'>NA</th>
                        <th data-sortable="true" class='text-center align-middle'>Nisbi</th>
                        @php 
                            $totalSkorMaks = 0;
                        @endphp
                        @foreach($cpl as $key=>$itemCpl)
                            @php 
                                $totalSkorMaks += $itemCpl->skor_maks ?? 0;
                            @endphp
                            <th colspan="2" data-sortable="true" class='text-center align-middle'>{{$itemCpl->skor_maks ?? '-'}}</th>
                            @if(isset($cpl[$key+1]))
                                @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                                    <th colspan="2" data-sortable="true" class='text-center align-middle'>{{$totalSkorMaks}}</th>
                                    @php 
                                        $totalSkorMaks = 0;
                                    @endphp
                                @endif
                            @else
                                <th colspan="2" data-sortable="true" class='text-center align-middle'>{{$totalSkorMaks}}</th>
                            @endif
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @php $countMahasiswa = 0; $arrayIdAssessment = []; @endphp
                    @foreach($kelas[0]->assessment as $itemAssessment)
                        @php $arrayIdAssessment[] = $itemAssessment->id; @endphp
                    @endforeach
                    @foreach($kelas as $itemKelas)
                        @foreach($itemKelas->mahasiswa as $key=>$itemMahasiswa)
                            <tr>
                                <td>{{++$countMahasiswa}}</td>
                                <td>{{$itemMahasiswa->nrp}}</td>
                                <td>{{$itemMahasiswa->nama}}</td>
                                <td>{{$itemKelas->kp}}</td>
                                @php $totalNTS = 0; @endphp
                                @php $totalNAS = 0; @endphp
                                @php $totalNA = 0; @endphp
                                @foreach($itemMahasiswa->nilaiMahasiswa->whereIn('assessment_id', $arrayIdAssessment) as $itemNilaiMahasiswa)
                                    @if($itemNilaiMahasiswa->assessment->kelompok == 'NTS')
                                        @php $totalNTS += $itemNilaiMahasiswa->nilai * $itemNilaiMahasiswa->assessment->persentase / 100; @endphp
                                    @elseif($itemNilaiMahasiswa->assessment->kelompok == 'NAS')
                                        @php $totalNAS += $itemNilaiMahasiswa->nilai * $itemNilaiMahasiswa->assessment->persentase / 100; @endphp
                                    @endif
                                @endforeach

                                @php
                                    $totalNTS = ceil($totalNTS);
                                    $totalNAS = ceil($totalNAS);
                                    $totalNA = number_format((float) round($totalNTS * $kelompok_nilai->firstWhere('nama', 'NTS')->persentase / 100 
                                    + $totalNAS * $kelompok_nilai->firstWhere('nama', 'NAS')->persentase /100,2), 2, '.', '');
                                @endphp
                                <td>{{$totalNTS}}</td>
                                <td>{{$totalNAS}}</td>
                                <td>{{$totalNA}}</td>
                                @php $totalNA = (float) $totalNA; @endphp
                                @if($totalNA >= 81)
                                    <td>A</td>
                                @elseif($totalNA < 81 && $totalNA >= 73)
                                    <td>AB</td>
                                @elseif($totalNA < 73 && $totalNA >= 66)
                                    <td>B</td>
                                @elseif($totalNA < 66 && $totalNA >= 60)
                                    <td>BC</td>
                                @elseif($totalNA < 60 && $totalNA >= 55)
                                    <td>C</td>
                                @elseif($totalNA < 55 && $totalNA >= 40)
                                    <td>D</td>
                                @elseif($totalNA < 40)
                                    <td>E</td>
                                @endif
                                @php
                                    $totalSkorMahasiswa = 0;
                                    $totalSkorMaksCpl = 0;
                                @endphp
                                @foreach($cpl as $keyCpl=>$itemCpl)
                                    @if(isset($cpl[$keyCpl+1]))
                                        @php
                                            $skorMahasiswa = $level_kontribusi[$itemCpl->id][$itemMahasiswa->id] ?? '-';
                                            $totalSkorMahasiswa += is_numeric($skorMahasiswa) ? $skorMahasiswa : 0 ;
                                            $skorMaksCpl = $itemCpl->skor_maks;
                                            $totalSkorMaksCpl += $skorMaksCpl;
                                            $persentase = is_numeric($skorMahasiswa) ? (
                                                $skorMahasiswa > 0 ? (
                                                    $skorMaksCpl > 0 ? (
                                                        round($skorMahasiswa / $skorMaksCpl * 100,2) . '%') : '-'
                                                ): '-'
                                            ): '-';
                                        @endphp
                                        <td>{{$skorMahasiswa}}</td>
                                        <td>{{$persentase}}</td>
                                        @if($itemCpl->program_studi_id != $cpl[$keyCpl+1]->program_studi_id)
                                            <td>{{$totalSkorMahasiswa}}</td>
                                            <td>{{$totalSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                                                round($totalSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : 'N/A' ) : 'N/A'}}</td>
                                            @php
                                                $totalSkorMahasiswa = 0;
                                                $totalSkorMaksCpl = 0;
                                            @endphp
                                        @endif
                                    @else
                                        @php
                                            $skorMahasiswa = $level_kontribusi[$itemCpl->id][$itemMahasiswa->id] ?? '-';
                                            $totalSkorMahasiswa += is_numeric($skorMahasiswa) ? $skorMahasiswa : 0 ;
                                            $skorMaksCpl = $itemCpl->skor_maks;
                                            $totalSkorMaksCpl += $skorMaksCpl;
                                            $persentase = is_numeric($skorMahasiswa) ? (
                                                $skorMahasiswa > 0 ? (
                                                    $skorMaksCpl > 0 ? (
                                                        round($skorMahasiswa / $skorMaksCpl * 100,2) . '%') : '-'
                                                ): '-'
                                            ): '-';
                                        @endphp
                                        <td>{{$skorMahasiswa ?? '-'}}</td>
                                        <td>{{$persentase}}</td>
                                    @endif
                                @endforeach
                                <td>{{$totalSkorMahasiswa}}</td>
                                <td>{{$totalSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                                    round($totalSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : 'N/A' ) : 'N/A'}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</section>
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        //Filter Section
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi",
        });
        $('#mata_kuliah_id').select2({
            placeholder: "Pilih Mata Kuliah",
        });
        $('#tahun_ajaran_id').select2({
            placeholder: "Pilih Tahun Ajaran",
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('course-score-card')}}");
        });
        //Filter Section End
    });
</script>
@endpush
