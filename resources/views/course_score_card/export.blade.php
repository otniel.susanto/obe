<table id="tableCourseScoreCard">
    <thead>
        <!-- KETERANGAN -->
        <tr>
            <th colspan="3"><b>Program Studi: {{$kelas[0]->programStudi->nama}}</b></th>
        </tr>
        <tr>
            <th colspan="3"><b>Mata Kuliah: {{$kelas[0]->mataKuliah->kode}} - {{$kelas[0]->mataKuliah->nama}}</b></th>
        </tr>
        <tr>
            <th colspan="3"><b>Tahun Ajaran: {{$kelas[0]->tahunAjaran->keterangan}}</b></th>
        </tr>
        <tr></tr>
        <!-- ROW PERTAMA -->
        <tr>
            <th rowspan='4' style="text-align:center; vertical-align:center;"><b>No.</b></th>
            <th rowspan='3' colspan='7' style="text-align:center; vertical-align:center;"><b>Mahasiswa</b></th>
            <th colspan="{{($cpl->count() + $cpl->groupBy('program_studi_id')->count())*2}}" style="text-align:center;"><b>Capaian Pembelajaran Lulusan</b></th>
        </tr>
        <!-- ROW KEDUA (KODE CPL) -->
        <tr>
            @foreach($cpl->groupBy('program_studi_id') as $itemCpl)
            <th colspan="{{($itemCpl->count() + 1)*2}}" style="text-align:center; vertical-align:center;"><b>{{$itemCpl[0]->programStudi->kode}} - {{$itemCpl[0]->programStudi->nama}}</b></th>
            @endforeach
        </tr>
        <!-- ROW KETIGA / Program Studi CPL -->
        <tr>
            @foreach($cpl as $key=>$itemCpl)
                <th colspan="2" style="text-align: center; vertical-align:center;"><b>{{$itemCpl->kode}}</b></th>
                @if(isset($cpl[$key+1]))
                    @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                        <th colspan="2" style="text-align: center; vertical-align:center;"><b>Total</b></th>
                        @php 
                            $totalSkorMaks = 0;
                        @endphp
                    @endif
                @else
                    <th colspan="2" style="text-align: center; vertical-align:center;"><b>Total</b></th>
                @endif
            @endforeach
        </tr>
        <!-- ROW KEEMPAT / ROW SKOR MAKS -->
        <tr>
            <th style="text-align: center; vertical-align:center;"><b>NRP</b></th>
            <th style="text-align: center; vertical-align:center;"><b>Nama</b></th>
            <th style="text-align: center; vertical-align:center;"><b>KP</b></th>
            <th style="text-align: center; vertical-align:center;"><b>NTS</b></th>
            <th style="text-align: center; vertical-align:center;"><b>NAS</b></th>
            <th style="text-align: center; vertical-align:center;"><b>NA</b></th>
            <th style="text-align: center; vertical-align:center;"><b>Nisbi</b></th>
            @php 
                $totalSkorMaks = 0;
            @endphp
            @foreach($cpl as $key=>$itemCpl)
                @php 
                    $totalSkorMaks += $itemCpl->skor_maks ?? 0;
                @endphp
                <th colspan="2" style="text-align: center; vertical-align:center;"><b>{{$itemCpl->skor_maks ?? '-'}}</b></th>
                @if(isset($cpl[$key+1]))
                    @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                        <th colspan="2" style="text-align: center; vertical-align:center;"><b>{{$totalSkorMaks}}</b></th>
                        @php 
                            $totalSkorMaks = 0;
                        @endphp
                    @endif
                @else
                    <th colspan="2" style="text-align: center; vertical-align:center;"><b>{{$totalSkorMaks}}</b></th>
                @endif
            @endforeach
        </tr>
    </thead>
    <tbody>
        @php $countMahasiswa = 0; $arrayIdAssessment = []; @endphp
        @foreach($kelas[0]->assessment as $itemAssessment)
            @php $arrayIdAssessment[] = $itemAssessment->id; @endphp
        @endforeach
        @foreach($kelas as $itemKelas)
            @foreach($itemKelas->mahasiswa as $key=>$itemMahasiswa)
                <tr>
                    <td style="text-align: center; vertical-align:center;">{{++$countMahasiswa}}</td>
                    <td style="text-align: center; vertical-align:center;">{{$itemMahasiswa->nrp}}</td>
                    <td style="text-align: center; vertical-align:center;">{{$itemMahasiswa->nama}}</td>
                    <td style="text-align: center; vertical-align:center;">{{$itemKelas->kp}}</td>
                    
                    @php 
                        $totalNTS = 0;
                        $totalNAS = 0;
                        $totalNA = 0;
                    @endphp
                    @foreach($itemMahasiswa->nilaiMahasiswa->whereIn('assessment_id', $arrayIdAssessment) as $itemNilaiMahasiswa)
                        @if($itemNilaiMahasiswa->assessment->kelompok == 'NTS')
                            @php $totalNTS += $itemNilaiMahasiswa->nilai * $itemNilaiMahasiswa->assessment->persentase / 100; @endphp
                        @elseif($itemNilaiMahasiswa->assessment->kelompok == 'NAS')
                            @php $totalNAS += $itemNilaiMahasiswa->nilai * $itemNilaiMahasiswa->assessment->persentase / 100; @endphp
                        @endif
                    @endforeach
                    @php $totalNA = $totalNTS * 40/100 + $totalNAS * 60/100; @endphp
                    
                    @php
                        $totalNTS = ceil($totalNTS);
                        $totalNAS = ceil($totalNAS);
                        $totalNA = (float) number_format((float) round($totalNTS * $kelompok_nilai->firstWhere('nama', 'NTS')->persentase / 100 
                        + $totalNAS * $kelompok_nilai->firstWhere('nama', 'NAS')->persentase /100,2), 2, '.', ''); 
                    @endphp
                    <td style="text-align: center; vertical-align:center;">{{$totalNTS}}</td>
                    <td style="text-align: center; vertical-align:center;">{{$totalNAS}}</td>
                    <td style="text-align: center; vertical-align:center;">{{$totalNA}}</td>
                    @php $totalNA = (float) $totalNA; @endphp
                    @if($totalNA >= 81)
                        <td style="text-align: center; vertical-align:center;">A</td>
                    @elseif($totalNA < 81 && $totalNA >= 73)
                        <td style="text-align: center; vertical-align:center;">AB</td>
                    @elseif($totalNA < 73 && $totalNA >= 66)
                        <td style="text-align: center; vertical-align:center;">B</td>
                    @elseif($totalNA < 66 && $totalNA >= 60)
                        <td style="text-align: center; vertical-align:center;">BC</td>
                    @elseif($totalNA < 60 && $totalNA >= 55)
                        <td style="text-align: center; vertical-align:center;">C</td>
                    @elseif($totalNA < 55 && $totalNA >= 40)
                        <td style="text-align: center; vertical-align:center;">D</td>
                    @elseif($totalNA < 40)
                        <td style="text-align: center; vertical-align:center;">E</td>
                    @endif
                    @php
                        $totalSkorMahasiswa = 0;
                        $totalSkorMaksCpl = 0;
                    @endphp
                    @foreach($cpl as $keyCpl=>$itemCpl)
                        @if(isset($cpl[$keyCpl+1]))
                            @php
                                $skorMahasiswa = $level_kontribusi[$itemCpl->id][$itemMahasiswa->id] ?? '-';
                                $totalSkorMahasiswa += is_numeric($skorMahasiswa) ? $skorMahasiswa : 0 ;
                                $skorMaksCpl = $itemCpl->skor_maks;
                                $totalSkorMaksCpl += $skorMaksCpl;
                                $persentase = is_numeric($skorMahasiswa) ? (
                                    $skorMahasiswa > 0 ? (
                                        $skorMaksCpl > 0 ? (
                                            round($skorMahasiswa / $skorMaksCpl * 100,2) . '%') : '-'
                                    ): '-'
                                ): '-';
                            @endphp
                            <td style="text-align: center; vertical-align:center;">{{$skorMahasiswa}}</td>
                            <td style="text-align: center; vertical-align:center;">{{$persentase}}</td>
                            @if($itemCpl->program_studi_id != $cpl[$keyCpl+1]->program_studi_id)
                                <td style="text-align: center; vertical-align:center;">{{$totalSkorMahasiswa}}</td>
                                <td style="text-align: center; vertical-align:center;">{{$totalSkorMahasiswa > 0 ? round($totalSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%' : '-'}}</td>
                                @php
                                    $totalSkorMahasiswa = 0;
                                    $totalSkorMaksCpl = 0;
                                @endphp
                            @endif
                        @else
                            @php
                                $skorMahasiswa = $level_kontribusi[$itemCpl->id][$itemMahasiswa->id] ?? '-';
                                $totalSkorMahasiswa += is_numeric($skorMahasiswa) ? $skorMahasiswa : 0 ;
                                $skorMaksCpl = $itemCpl->skor_maks;
                                $totalSkorMaksCpl += $skorMaksCpl;
                                $persentase = is_numeric($skorMahasiswa) ? (
                                    $skorMahasiswa > 0 ? (
                                        $skorMaksCpl > 0 ? (
                                            round($skorMahasiswa / $skorMaksCpl * 100,2) . '%') : '-'
                                    ): '-'
                                ): '-';
                            @endphp
                            <td style="text-align: center; vertical-align:center;">{{$skorMahasiswa ?? '-'}}</td>
                            <td style="text-align: center; vertical-align:center;">{{$persentase}}</td>
                        @endif
                    @endforeach
                    <td style="text-align: center; vertical-align:center;">{{$totalSkorMahasiswa}}</td>
                    <td style="text-align: center; vertical-align:center;">{{$totalSkorMahasiswa > 0 ? round($totalSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%' : '-'}}</td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>