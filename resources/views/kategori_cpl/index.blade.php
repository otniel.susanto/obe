@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Kategori CPL</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('kategori-cpl')}}">Kategori CPL</a></li>
        </ol>
        <a href="{{url('kategori-cpl/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Nama</th>
                    <th width="">Bobot</th>
                    <th width="">Kategori</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kategori_cpl as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->bobot}}</td>
                    <td>{{$item->jenisCpl->nama}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditKategoriCpl({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteKategoriCpl({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$kategori_cpl->count()}}</b> out of <b>{{$kategori_cpl->total()}}</b> entries
            </div> {{ $kategori_cpl->links() }}
        </div>
    </div>
    @include('kategori_cpl.edit')
    @include('kategori_cpl.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("kategori-cpl")}}/' + $("#formEditKategoriCpl #id_edit").val(),
                data: {
                    jenis_cpl_id: $("#edit-jenis_cpl_id").val(),
                    nama: $("#edit-nama").val(),
                    bobot: $("#edit-bobot").val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditKategoriCpl').trigger("reset");
                    $("#formEditKategoriCpl .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-kategori-cpl-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-kategori-cpl-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("kategori-cpl")}}/' + $("#formDeleteKategoriCpl #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteKategoriCpl .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditKategoriCpl(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kategori-cpl")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#edit-jenis_cpl_id").val(data.kategori_cpl.jenis_cpl_id).change();
                $("#edit-nama").val(data.kategori_cpl.nama);
                $("#edit-bobot").val(data.kategori_cpl.bobot);
                $("#id_edit").val(data.kategori_cpl.id);
                $('#modalEditKategoriCpl').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteKategoriCpl(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kategori-cpl")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteKategoriCpl #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Kategori cpl: " + data.kategori_cpl.nama + "</p>")
                $("#formDeleteKategoriCpl #id_delete").val(data.kategori_cpl.id);
                $('#modalDeleteKategoriCpl').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
