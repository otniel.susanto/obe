<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditKategoriCpl">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditKategoriCpl">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Kategori Cpl </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-kategori-cpl-errors"> </ul>
                    </div>

                    <label for="edit-jenis_cpl_id">Jenis CPL</label>
                    <select id="edit-jenis_cpl_id" class="select2 form-control">
                        @foreach($jenis_cpl as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                    <label for="edit-nama">Nama</label>
                    <input required type="text" class="form-control" name="edit-nama" id="edit-nama"
                        placeholder="Masukkan nama Kategori Cpl">

                    <label for="edit-bobot">Bobot</label>
                    <input required type="number" class="form-control" name="edit-bobot" id="edit-bobot"
                        placeholder="Masukkan bobot">

                </div>
                <div class="modal-footer">
                    <input id="id_edit" type="hidden" value="0">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
