@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Kategori Cpl</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('kategori-cpl')}}">Kategori CPL</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('kategori-cpl/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAddKategoriCpl">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="jenis_cpl_id">Jenis CPL</label>
                    <select id="jenis_cpl_id" class="select2 form-control">
                        @foreach($jenis_cpl as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6">
                    <label for="nama">Nama</label>
                    <input required type="text" class="form-control" name="nama" id="nama"
                        placeholder="Masukkan nama Kategori Cpl">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6">
                    <label for="bobot">Bobot</label>
                    <input required type="number" class="form-control" name="bobot" id="bobot"
                        placeholder="Masukkan bobot">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $("#btnAdd").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("kategori-cpl")}}',
                data: {
                    jenis_cpl_id: $("#jenis_cpl_id").val(),
                    nama: $("#nama").val(),
                    bobot: $("#bobot").val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('kategori-cpl')}}");
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#add-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#add-errors').append('<li>' + value + '</li>');
                    });
                    $("#add-error-bag").show();
                }
            });
        });
    });

</script>
@endpush
