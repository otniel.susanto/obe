<!-- Create Modal HTML -->
@push('head-script')
    <!-- Summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-lite.min.css')}}">
@endpush
<div class="modal fade" id="modalCreateIsiSoal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formCreateIsiSoal">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Isi Soal </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="create-error-bag">
                        <ul id="create-isi_soal-errors"> </ul>
                    </div>

                    <div><label for="add-nomor_soal">Nomor Soal</label></div>
                    <div class="text-secondary mt-n2">Catatan: gunakan , (koma) untuk banyak nomor sekaligus</div>
                    <input required type="string" class="form-control" name="nomor_soal" id="add-nomor_soal" placeholder="Contoh: 1, 2a, 2b">

                    <label for="add-skor_maks">Skor Maksimum</label>
                    <input required type="number" class="form-control" name="skor_maks" id="add-skor_maks" placeholder="Contoh: 10, 20, 35">
                    
                    <label >CPMK</label>
                    <div id="add-cpmk-wrapper">
                        
                    </div>

                    <label for="add-taksonomi_bloom_id">Taksonomi Bloom</label>
                    <select id="add-taksonomi_bloom_id" class="form-control">
                        <option value="" selected> - </option>
                        @foreach($taksonomi_bloom as $item)
                            <option value="{{$item->id}}">{{$item->level}} - {{$item->keterangan}}</option>
                        @endforeach
                    </select>

                    @if($assessment !== null)
                        @if($assessment->jenis_penilaian == "Ujian")
                            <label for="add-kompetensi">Kompetensi/Kemampuan Akhir yang Diharapkan</label>
                            <textarea rows="2" class="form-control" id="add-kompetensi" 
                            placeholder="Contoh: mampu menerapkan ilmu dalam kehidupan sehari-hari"></textarea>
                        @endif
                    @else
                        <div id="detailUjian" class="d-none">
                            <label for="add-kompetensi">Kompetensi/Kemampuan Akhir yang Diharapkan</label>
                            <textarea rows="2" class="form-control" id="add-kompetensi" 
                            placeholder="Contoh: mampu menerapkan ilmu dalam kehidupan sehari-hari"></textarea>
                        </div>
                    @endif

                    <label for="add-keterangan-isi_soal">Keterangan</label>
                    <textarea rows="2" class="form-control" id="add-keterangan-isi_soal" 
                     placeholder="Keterangan soal"></textarea>

                    <label for="add-rubrik">Rubrik</label>                    
                    <textarea rows="5" class="form-control" id="add-rubrik" 
                     placeholder="Masukkan cara penilaian terhadap soal ini"></textarea>

                </div>
                <div class="modal-footer">
                    <button onclick="event.preventDefault();" class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i>
                        Cancel
                    </button>
                    <button onclick="event.preventDefault();" class="btn btn-info" id="btnTambahIsiSoal">
                        <i class="fa fa-plus"></i>
                        Tambah
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('child-script')
    <script src="{{asset('js/summernote-lite.min.js')}}"></script>
<script>
    $(document).ready(function(){
        // $('#add-rubrik').summernote({
        //     dialogsInBody: true
        //     airMode: true
        // });
    });
</script>
@endpush
