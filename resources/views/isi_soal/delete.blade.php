<!-- Delete Isi Soal Modal Form HTML -->
<div class="modal fade" id="modalDeleteIsiSoal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteIsiSoal">
                <div class="modal-header">
                    <h4 class="modal-title" name="title">Hapus Isi Soal</h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="delete-data-isi_soal"></div>
                    <p class="text-warning"><small>Data isi soal akan terhapus dari Penilaian (Assessment) ini.</small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="id-delete-isi_soal" type="hidden" value="">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDeleteIsiSoal" type="button">
                        <i class="fa fa-trash"></i>
                        Hapus 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
