<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditIsiSoal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditIsiSoal">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Isi Soal </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-isi_soal-errors"> </ul>
                    </div>

                    <label for="edit-nomor_soal">Nomor Soal</label>
                    <input required type="string" class="form-control" name="nomor_soal" id="edit-nomor_soal" placeholder="Contoh: 1, 2a, 2b">

                    <label for="edit-skor_maks">Skor Maksimum</label>
                    <input required type="string" class="form-control" name="skor_maks" id="edit-skor_maks" placeholder="Contoh: 10, 20, 35">
                    
                    <label >CPMK</label>
                    <div id="edit-cpmk-wrapper">
                        
                    </div>                    
                    
                    <label for="edit-taksonomi_bloom_id">Taksonomi Bloom</label>
                    <select id="edit-taksonomi_bloom_id" class="select2 form-control">
                        <option value="" selected> - </option>
                        @foreach($taksonomi_bloom as $item)
                            <option value="{{$item->id}}">{{$item->level}} - {{$item->keterangan}}</option>
                        @endforeach
                    </select>

                    @if($assessment->jenis_penilaian == "Ujian")
                        <label for="edit-kompetensi">Kompetensi/Kemampuan Akhir yang Diharapkan</label>
                        <textarea rows="2" class="form-control" id="edit-kompetensi" 
                        placeholder="Contoh: mampu menerapkan ilmu dalam kehidupan sehari-hari"></textarea>
                    @endif

                    <label for="edit-keterangan-isi_soal">Keterangan</label>                    
                    <textarea rows="2" class="form-control" id="edit-keterangan-isi_soal" 
                     placeholder="Keterangan soal"></textarea>

                    <label for="edit-rubrik">Rubrik</label>                    
                    <textarea rows="5" class="form-control" id="edit-rubrik" 
                     placeholder="Masukkan cara penilaian terhadap soal ini"></textarea>

                </div>
                <div class="modal-footer">
                    <input id="id-edit-isi_soal" type="hidden" value="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        Cancel
                    </button>
                    <button type="button" class="btn btn-info" id="btnEditIsiSoal">
                        <i class="fa fa-marker"></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
