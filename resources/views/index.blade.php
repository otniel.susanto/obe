<!DOCTYPE html>
<html lang="{{ \App::getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Penilaian OBE</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{asset('img/iconUbaya.png')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Select2 CSS -->
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <!-- Select2 CSS Bootstrap4 Theme -->
    <link href="{{asset('css/select2-bootstrap4.min.css')}}" rel="stylesheet" />
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
    <!-- Bootstrap-table -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-table.min.css')}}">
    <!-- My Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/my-custom.css')}}">
    @stack('head-script')
</head>

<body class="hold-transition sidebar-mini layout-fixed text-sm">
    <!-- Site wrapper -->
    <div class="wrapper">
        @include('parts.navbar')
        @include('parts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->

        <!-- Footer -->
        @include('parts.footer')

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('/js/demo.js')}}"></script>
    <!-- Select2 JS -->
    <script src="{{asset('js/select2.min.js')}}"></script>
    <!-- InputMask -->
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/jquery.inputmask.bundle.min.js')}}"></script>
    <!-- date-range-picker -->
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <!-- Bootstrap-table JS -->
    <script src="{{asset('js/bootstrap-table.min.js')}}"></script>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap4");
        });

    </script>

    @stack('child-scripts')
</body>

</html>
