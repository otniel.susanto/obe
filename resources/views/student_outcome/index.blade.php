@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Capaian Pembelajaran Lulusan (CPL)</h1>
        <a href="{{url('student-outcome/create')}}" type="button" class="btn btn-primary float-sm-right">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Keterangan</th>
                    <th width="">Bobot</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($student_outcome as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->bobot->nilai}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditStudentOutcome({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteStudentOutcome({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$student_outcome->count()}}</b> out of <b>{{$student_outcome->total()}}</b> entries
            </div> {{ $student_outcome->links() }}
        </div>
    </div>
    @include('student_outcome.edit')
    @include('student_outcome.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("student-outcome")}}/' + $("#formEditStudentOutcome #id_edit").val(),
                data: {
                    keterangan: $("#formEditStudentOutcome #keterangan").val(),
                    bobot_id: $("#formEditStudentOutcome #bobot_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditStudentOutcome').trigger("reset");
                    $("#formEditStudentOutcome .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-student-outcome-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-student-outcome-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("student-outcome")}}/' + $("#formDeleteStudentOutcome #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteStudentOutcome .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditStudentOutcome(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("student-outcome")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditStudentOutcome #keterangan").val(data.student_outcome.keterangan);
                $("#formEditStudentOutcome #id_edit").val(data.student_outcome.id);
                $('#modalEditStudentOutcome').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteStudentOutcome(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("student-outcome")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteStudentOutcome #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Capaian Pembelajaran Lulusan (CPL): " + data.student_outcome.keterangan + "</p>")
                $("#formDeleteStudentOutcome #id_delete").val(data.student_outcome.id);
                $('#modalDeleteStudentOutcome').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
