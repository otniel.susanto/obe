<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditStudentOutcome">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditStudentOutcome">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Capaian Pembelajaran Lulusan (CPL) </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> 
                        × 
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-student-outcome-errors"> </ul>
                    </div>
                    <div class="form-group"> 
                        <label> Keterangan </label> 
                        <input class="form-control" id="keterangan" required="" type="text">
                    </div>
                </div>
                <div class="modal-footer"> 
                    <input id="id_edit" type="hidden" value="0"> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button> 
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add"> 
                        Update 
                    </button> 
                </div>
            </form>
        </div>
    </div>
</div>
