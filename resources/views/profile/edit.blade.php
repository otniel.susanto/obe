@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1>Edit Profil</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="{{url('profile')}}">Profil Saya</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('profile')}}/1/edit">Edit</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formEditProfile">
            <div class="form-group row">
                <div class="col-auto">
                    <label for="name">Nama Lengkap</label>
                    <input required type="text" class="form-control" id="name" 
                    placeholder="Nama Lengkap" value="{{$user->name}}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-2">
                    <img class="img-thumbnail" src="{{asset('tanda-tangan')}}/{{$user->nama_file_ttd}}" alt="Gambar Tanda Tangan">
                </div>
                <div class="col-auto">
                    <label for="file">Ganti Tanda Tangan (akan menghapus yang lama)</label>
                    <br>
                    <input type="file" name="file" id="file">
                </div>
            </div>
        </form>

        <h5>Program Studi yang diajar</h5>
        <div class="form-group row">
            <div class="col-md-4">
                <select id="program_studi_id" class="select2 form-control">
                    <option disabled selected></option>
                    @foreach($program_studi as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary float-right" id="btnTambahTabel">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </button>
            </div>
        </div>

        <div class="row mb-1">
            <div class="col-md-6">
                <table width="100% data-toggle=" class="table myTable table-hover" id="tabelProgramStudi">
                    <thead>
                        <tr>
                            <th width="30%">Program Studi</th>
                            <th width="10%">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user->programStudi as $item)
                            <tr id="programStudi{{$item->id}}">
                                <td>{{$item->nama}}</td>
                                <td>
                                    <a onclick="event.preventDefault();formDeleteProgramStudi({{$item->id}});" class="delete"
                                        data-toggle="modal">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                <button type="button" class="btn btn-primary float-right mb-1" id="btnEdit">
                    <i class="fa fa-marker"></i>
                    Update
                </button>
            </div>
        </div>
    </div>
    @include('program_studi.delete')
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    var program_studi_create = [];
    var program_studi_delete = [];
    var iBaru = 0;
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $("#btnEdit").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);
            formData.append('name', $('#name').val());
            formData.append('program_studi_create', JSON.stringify(program_studi_create));
            formData.append('program_studi_delete', JSON.stringify(program_studi_delete));
            $.ajax({
                method: 'POST',
                url:'{{url("profile")}}/1',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('profile')}}");
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                }
            });
        });

        $("#btnTambahTabel").click(function () {
            if($('#program_studi_id').val()){
                var duplicate = false;
                $.each(program_studi_create, function (key, value) {
                    if(value.program_studi_id == $('#program_studi_id').val()){
                        duplicate = true;
                    }
                });
                if(!duplicate){
                    program_studi_create.push({
                        index: ++iBaru,
                        program_studi_id: $('#program_studi_id').val(),
                    });
                    $('#tabelProgramStudi tr:last').after('<tr id="baru' + iBaru + '">' +
                        '<td>' + $('#program_studi_id option:selected').text() + '</td>' +
                        '<td><a onclick="event.preventDefault();btnDeleteTabel(' +
                        iBaru +
                        ');" data-toggle="modal">' +
                        '<i class="fa fa-trash"></i></a></td>' +
                        '</tr>'
                    );
                }
                else{
                    alert('Program studi sudah ada');
                }
            }
            else{
                alert("Pilih program studi terlebih dahulu");
            }
        });

        $("#btnDelete").click(function(){
            program_studi_delete.push({
                program_studi_id: $("#id_delete").val()
            })
            $("tr#programStudi" + $("#id_delete").val()).remove();
            $('#modalDeleteProgramStudi').modal('hide');
        });
    });

    function formDeleteProgramStudi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("program-studi")}}/' + id + '/edit',
            success: function (data) {
                $("#delete-data").html("<p>Hapus Program Studi dari Daftar Mengajar</p>" + 
                "<p>Program Studi: " + data.program_studi.nama + "</p>")
                $("#id_delete").val(data.program_studi.id);
                $('#modalDeleteProgramStudi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function btnDeleteTabel(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#baru' + id).remove();
            var index = program_studi_create.findIndex(function (item) {
                return item.index == id;
            });
            program_studi_create.splice(index, 1);
        }
    }

</script>
@endpush