@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-1">
            <div class="col-sm-6">
                <h1>Profil Saya</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="{{url('profile')}}">Profil Saya</a></li>
                </ol>
                <a href="{{url('profile')}}/1/edit" type="button" class="btn btn-warning float-sm-right">
                    <i class="fa fa-edit"></i>
                    Edit Profil
                </a>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <img class="img-fluid" src="https://my.ubaya.ac.id/img/krywn/{{$user->npk}}_m.jpg" alt="User Image">
            </div>
            <div class="col-md-10">
                <!-- NPK -->
                <div class="row">
                    <div class="col-md-1">
                        <h6>NPK</h6>
                    </div>
                    <div class="col-md-auto">
                        <h6>: {{$user->npk}}</h6>
                    </div>
                </div>
                <!-- Nama -->
                <div class="row">
                    <div class="col-md-1">
                        <h6>Nama</h6>
                    </div>
                    <div class="col-md-auto">
                        <h6>: {{$user->name}}</h6>
                    </div>
                </div>
                <!-- Jabatan -->
                <div class="row">
                    <div class="col-md-1">
                        <h6>Jabatan</h6>
                    </div>
                    <div class="col-md-auto">
                        <h6>: {{$user->jabatan}}</h6>
                    </div>
                </div>
                <!-- Status -->
                <div class="row">
                    <div class="col-md-1">
                        <h6>Status</h6>
                    </div>
                    <div class="col-md-auto">
                        <h6>: {{$user->status}}</h6>
                    </div>
                </div>
            </div>
        </div>
        <h6>Program Studi yang diajar:</h6>
        <div class="row">
            <div class="col-md-8">
                <ul class="list-group list-group-flush">
                <div class="row">
                    @foreach($user->programStudi as $key=>$item)
                    <div class="col-md-4">
                        <li class="list-group-item">
                            {{$item->nama}}
                        </li>
                    </div>
                    @endforeach
                </div>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
