<!-- Delete Rps Modal Form HTML -->
<div class="modal fade" id="modalDeleteRps">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteRps">
                <div class="modal-header">
                    <h4 class="modal-title" id="delete-rps-title" name="title">Apakah anda yakin menghapus data RPS dan Kelas ini?</h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="delete-error-bag"></div>
                    <div id="delete-data-rps">
                        <p>Penghapusan dapat dilakukan jika kelas masih belum memiliki assessment dan nilai mahasiswa.</p>
                    </div>
                    <p class="text-warning"> <small> Data akan terhapus selamanya. </small> </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDelete" type="button">
                        <i class="fa fa-trash"></i>
                        Hapus 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
