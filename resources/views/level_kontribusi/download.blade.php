<!-- Download Modal HTML -->
<div class="modal fade" id="modalDownloadLevelKontribusi">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDownloadLevelKontribusi" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Download Template Matriks CPL vs CPMK</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="download-error-bag">
                    </div>
                    
                    <label for="download-program_studi_id">Program Studi</label>
                    <select id="download-program_studi_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button>
                    <a href="{{url('level-kontribusi-template')}}" id="btnDownload" type="button" class="btn btn-info float-right mr-1">    
                        <i class="fa fa-download"></i>
                        Download
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

