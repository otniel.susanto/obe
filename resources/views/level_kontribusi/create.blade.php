@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Matriks CPL vs CPMK</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('rps')}}">Matriks CPL vs CPMK</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('rps/create')}}">Tambah Data</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formMatriks" enctype="multipart/form-data">
            <div class="form-group row">
                <div class="col-md-4">
                    <label for="program_studi_id">Program Studi</label>
                    <select id="program_studi_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="mata_kuliah_id">Mata Kuliah</label>
                    <select id="mata_kuliah_id" class="form-control">
                        <option disabled selected></option>
                    </select>
                </div>
            </div>
            <div class="form-group row">

            </div>
        </form>

        <!-- CPMK -->
        <div class="row mb-1">
            <div class="col-1">
                <h5>CPMK</h5>
            </div>
            <div class="col-8 float-right">
                <button onclick="event.preventDefault();showAddCpmk();"
                    class="btn btn-primary float-right open-modal" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Tambah CPMK
                </button>
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-9">
                <table width="100% data-toggle=" class="table myTable table-hover table-bordered" id="tableCpmk">
                    <tr class="thead"> 
                        <th width="20%">Kode</th>
                        <th width="60%">Keterangan</th>
                        <th width="20%">Edit</th>
                        <th width="20%">Delete</th>
                    </tr>
                </table>
            </div>
        </div>

        <!-- Komponen Penilaian -->
        <div class="row mb-1">
            <div class="col-3">
                <h5 class="inline">Komponen Penilaian</h5>
                <span class="bg-warning">NA = 40% NTS + 60% NAS</span>
            </div>
            <div class="col-9">
                <button onclick="event.preventDefault();showCreateKomponenPenilaian();"
                    class="btn btn-primary float-right open-modal" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Tambah Komponen Penilaian
                </button><br><br>
                <span class="bg-warning float-right mr-1" id="warning-total_persentase_nas">Total persentase NAS saat ini: 0%</span>
                <span class="bg-warning float-right mr-1" id="warning-total_persentase_nts">Total persentase NTS saat ini: 0%</span>
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-md-12">
                <table class="table myTable table-hover table-bordered" id="tableKomponenPenilaian">
                    <tr class="thead">
                        <th class="text-center align-middle" width="15%">CPL</th>
                        <th class="text-center align-middle" width="45%">CPMK</th>
                        <th class="text-center align-middle" width='10%'>Edit</th>
                        <th class="text-center align-middle" width='10%'>Delete</th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12">
                <button type="button" class="btn btn-primary float-right btn-lg w-100" id="btnAdd">
                    <i class="fa fa-save fa-lg"></i>
                    <span class="text-lg">Simpan</span>
                </button>
            </div>
        </div>
    @include('cpmk.modal_add')
    @include('cpmk.modal_edit')
    @include('komponen_penilaian.create')
    @include('komponen_penilaian.edit')
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(window).bind('beforeunload', function(){
        return 'Data belum disimpan, apakah anda yakin untuk meninggalkan halaman ini?';
    });
    var kelas_create = [];
    var cpmk_create = [];
    var komponen_penilaian_create = [];
    var iKomponen = 0;
    var iCpmk = 0;
    var iKelas = 0;
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: 'Pilih Program Studi'
        });
        $("#program_studi_id").change(function () {
            var id = $("#mata_kuliah_id").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("get-mata_kuliah")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    $("#mata_kuliah_id").html('');
                    var stringAppend = '<option disabled selected></option>';
                    $.each(data.mata_kuliah, function (key, value) {
                        stringAppend += '<option value="' + value.id + '">' +
                            value.kode + ' - ' + value.nama + '</option>';
                    });
                    $("#mata_kuliah_id").append(stringAppend);
                    $("#mata_kuliah_id").val(id).change();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $('#mata_kuliah_id').select2({
            placeholder: 'Pilih Mata Kuliah'
        });
        $('#dosen_pengampu_id').select2({
            placeholder: 'Pilih Dosen Pengampu'
        });
        $('#add-jenis_penilaian_id').select2({
            placeholder: 'Pilih Jenis Penilaian'
        });
        $('#edit-jenis_penilaian_id').select2({
            placeholder: 'Pilih Jenis Penilaian'
        });
        $("#btnAdd").click(function () {
            if(cekTotalPersentase() == 200){
                var formData = new FormData();
                var files = $('#file')[0].files[0];
                formData.append('file', files);
                formData.append('program_studi_id', $('#program_studi_id').val());
                formData.append('nama_file_rps', $('#nama_file_rps').val());
                formData.append('mata_kuliah_id', $('#mata_kuliah_id').val());
                formData.append('keterangan', $('#keterangan').val());
                formData.append('dosen_pengampu_id', $('#dosen_pengampu_id').val());
                formData.append('cpmk_create', JSON.stringify(cpmk_create));
                formData.append('komponen_penilaian_create', JSON.stringify(komponen_penilaian_create));
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:'{{url("rps")}}',
                    type: 'post',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        alert(data.message);
                        $(window).bind('beforeunload', function(){
                            return false;
                        });
                        window.location.replace("{{url('rps')}}/" + data.rps_id);
                    },
                    error: function (data) {
                        console.log(data);
                        var errors = $.parseJSON(data.responseText);
                        var stringException = "";
                        if(data.status == 422){
                            console.log(errors);
                            $.each(errors.errors, function (key, value) {
                                stringException += value + "\n";
                            });
                        }
                        else{
                            stringException = data.message;
                        }
                        alert(stringException);
                    }
                });
            }else{
                alert("Total persentase NTS dan NAS masing-masing harus 100%.")
            }
        });
        //Add CPMK
        $("#btnTambahCpmk").click(function () {
            pushCpmk($('#add-kodeCpmk').val(), $('#add-keteranganCpmk').val())
            $('#add-kodeCpmk').val('');
            $('#add-keteranganCpmk').val('');
            closeModalAddCpmk();
        });
        //Update CPMK
        $("#btnUpdateCpmk").click(function () {
            var id = $("#id-edit-cpmk").val();
            var index = cpmk_create.findIndex(function (item) {
                return item.index == id;
            });
            cpmk_create[index].kode = $('#edit-kodeCpmk').val();
            cpmk_create[index].keterangan = $('#edit-keteranganCpmk').val();
            $('tr#cpmk' + id).html('');
            $('tr#cpmk' + id).append('<td>' + $('#edit-kodeCpmk').val() + '</td>' +
            '<td>' + $('#edit-keteranganCpmk').val() + '</td>' +
            '<td><a onclick="event.preventDefault();formEditCpmk(' + id + ');"' +
            'class="edit open-modal"data-toggle="modal" value="'+ id + '">'+
            '<i class="fa fa-edit"></i></a></td>' + 
            '<td class="actionCpmk"><a onclick="event.preventDefault();deleteCpmk(' +
            id +
            ');" data-toggle="modal">' +
            '<i class="fa fa-trash"></i></a></td>');
            console.log(id);
            $('#modalEditCpmk').modal('hide');
        });
        //Add Komponen Penilaian
        $("#btnTambahKomponenPenilaian").click(function () {
            pushKomponenPenilaian(
                $('#add-jenis_penilaian_id').val(),
                $('#add-aktivitas').val(),
                $('#add-persentase').val(),
                $("input[name='add-kelompok']:checked").val()
            )
            $('#aktivitas').val('');
            $('#persentase').val('');
            closeModalCreateKomponenPenilaian();
        });
        //Update Komponen Penilaian
        $("#btnUpdateKomponenPenilaian").click(function () {
            var id = $("#id-edit-komponen-penilaian").val();
            var index = komponen_penilaian_create.findIndex(function (item) {
                return item.index == id;
            });
            komponen_penilaian_create[index].jenis_penilaian_id = $('#edit-jenis_penilaian_id').val();
            komponen_penilaian_create[index].aktivitas = $('#edit-aktivitas').val();
            komponen_penilaian_create[index].persentase = $('#edit-persentase').val();
            komponen_penilaian_create[index].kelompok = $("input[name='edit-kelompok']:checked").val();
            $('tr#komponen' + id).html('');
            $('tr#komponen' + id).append('<td>' + $('#edit-jenis_penilaian_id option:selected').text() + '</td>' +
            '<td>' + $('#edit-aktivitas').val() + '</td>' +
            '<td>' + $('#edit-persentase').val() + '%</td>' +
            '<td>' + $("input[name='edit-kelompok']:checked").data("name") + '</td>' +
            '<td><a onclick="event.preventDefault();formEditKomponenPenilaian(' + id + ');"' +
            'class="edit open-modal"data-toggle="modal" value="'+ id + '">'+
            '<i class="fa fa-edit"></i></a></td>' + 
            '<td><a onclick="event.preventDefault();deleteKomponenPenilaian(' + id +
            ');" data-toggle="modal"><i class="fa fa-trash"></i></a></td>');
            console.log(id);
            $('#modalEditKomponenPenilaian').modal('hide');
        });
        //Button Copy
        //Show or Hide btnCopy
        $("#mata_kuliah_id").change(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("get-last-rps")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val(),
                    mata_kuliah_id: $("#mata_kuliah_id").val(),
                    rps_id: $("#idCopy").val(),
                },
                dataType: 'json',
                success: function (data) {
                    if(data.rps_id !== null){
                        $("#btnCopy").removeClass("d-none");
                        $("#btnCopy").show();
                        $("#idCopy").val(data.rps_id.id);
                    }
                    else{
                        $("#btnCopy").addClass("d-none");
                        $("#btnCopy").hide();
                        $("#idCopy").val('');
                        $("#nama_file_rps").val('');
                        kelas_create = [];
                        cpmk_create = [];
                        komponen_penilaian_create = [];
                        $("#tableCpmk tr.tbody").html("");
                        $("#tableKomponenPenilaian tr.tbody").remove();
                    }
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });
        //btnCopy Click Trigger
        $("#btnCopy").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("get-last-rps")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val(),
                    mata_kuliah_id: $("#mata_kuliah_id").val(),
                    rps_id: $('#idCopy').val()
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data.cpmk);
                    console.log(data.komponen_penilaian);
                    $('#dosen_pengampu_id').val(data.rps.dosen_pengampu_id).change();
                    $("#nama_file_rps").val(data.rps.nama_file_rps);
                    $.each(data.cpmk, function(key,value){
                        pushCpmk(value.kode, value.keterangan);
                    });
                    console.log(data.komponen_penilaian);
                    $.each(data.komponen_penilaian, function(key,value){
                        pushKomponenPenilaian(value.jenis_penilaian_id, value.aktivitas, value.persentase, value.kelompok_nilai_id);
                    });
                    $('#kp').focus();
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });
    });
    
    //CPMK
    function deleteCpmk(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#cpmk' + id).remove();
            var index = cpmk_create.findIndex(function (item) {
                return item.index == id;
            });
            cpmk_create.splice(index, 1);
        }
    }
    function closeModalAddCpmk() {
        $('#modalAddCpmk').modal('hide');
    }
    function showAddCpmk() {
        $("#create-error-bag").hide();
        $('#modalAddCpmk').modal('show');
    }
    function formEditCpmk(id) {
        var index = cpmk_create.findIndex(function (item) {
            return item.index == id;
        });
        $("#edit-cpmk-error-bag").hide();
        $("#id-edit-cpmk").val(id);
        $("#edit-kodeCpmk").val(cpmk_create[index].kode);
        $("#edit-keteranganCpmk").val(cpmk_create[index].keterangan);
        $('#modalEditCpmk').modal('show');
    }

    //Komponen Penilaian
    function deleteKomponenPenilaian(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#komponen' + id).remove();
            var index = komponen_penilaian_create.findIndex(function (item) {
                return item.index == id;
            });
            komponen_penilaian_create.splice(index, 1);
            cekTotalPersentase();
        }
    }
    function closeModalCreateKomponenPenilaian() {
        $('#modalCreateKomponenPenilaian').modal('hide');
    }
    function showCreateKomponenPenilaian() {
        $("#create-komponen-penilaian-error-bag").hide();
        $('#modalCreateKomponenPenilaian').modal('show');
    }
    function formEditKomponenPenilaian(id) {
        var index = komponen_penilaian_create.findIndex(function (item) {
            return item.index == id;
        });
        $("#edit-komponen_penilaian-error-bag").hide();
        $("#id-edit-komponen-penilaian").val(id);
        $("#edit-jenis_penilaian_id").val(komponen_penilaian_create[index].jenis_penilaian_id).change();
        $("#edit-aktivitas").val(komponen_penilaian_create[index].aktivitas);
        $("#edit-persentase").val(komponen_penilaian_create[index].persentase);
        $.each($('input[name="edit-kelompok"]'), function(){
            if(this.value == komponen_penilaian_create[index].kelompok){
                $("input[name='edit-kelompok'][value='" + this.value + "']").prop("checked", true);
            }
        });
        $("#edit-komponen-penilaian-error-bag").hide();
        $("#edit-komponen-penilaian-errors").html('');
        $('#modalEditKomponenPenilaian').modal('show');
    }

    function pushCpmk(kodeCpmk, keteranganCpmk){
        cpmk_create.push({
            index: ++iCpmk,
            kode: kodeCpmk,
            keterangan: keteranganCpmk
        });
        $('#tableCpmk tr:last').after('<tr class="tbody" id="cpmk' + iCpmk + '">' +
            '<td>' + kodeCpmk + '</td>' +
            '<td>' + keteranganCpmk + '</td>' +
            '<td><a onclick="event.preventDefault();formEditCpmk(' + iCpmk + ');"' +
            'class="edit open-modal"data-toggle="modal" value="'+ iCpmk + '">'+
            '<i class="fa fa-edit"></i></a></td>' + 
            '<td class="actionCpmk"><a onclick="event.preventDefault();deleteCpmk(' +
            iCpmk +
            ');" data-toggle="modal">' +
            '<i class="fa fa-trash"></i></a></td>' +
            '</tr>'
        );
    }

    function pushKomponenPenilaian(idJenisPenilaian, aktivitas, persentase, kelompok){
        komponen_penilaian_create.push({
            index: ++iKomponen,
            jenis_penilaian_id: idJenisPenilaian,
            aktivitas: aktivitas,
            persentase: persentase,
            kelompok: kelompok
        });
        $('#add-jenis_penilaian_id').val(idJenisPenilaian);
        $('#tableKomponenPenilaian tr:last').after('<tr class="tbody" id="komponen' + iKomponen + '">' +
            '<td>' + $('#add-jenis_penilaian_id option:selected').text() + '</td>' +
            '<td>' + aktivitas + '</td>' +
            '<td>' + persentase + '%</td>' +
            '<td>' + $("input[name='add-kelompok']:checked").data("name") + '</td>' +
            '<td><a onclick="event.preventDefault();formEditKomponenPenilaian(' + iKomponen + ');"' +
            'class="edit open-modal"data-toggle="modal" value="'+ iKomponen + '">'+
            '<i class="fa fa-edit"></i></a></td>' + 
            '<td><a onclick="event.preventDefault();deleteKomponenPenilaian(' +
            iKomponen +
            ');" data-toggle="modal">' +
            '<i class="fa fa-trash"></i></a></td>' +
            '</tr>'
        );
        cekTotalPersentase();
    }
    
    function cekTotalPersentase (){
        var totalPersentaseNTS = 0;
        var totalPersentaseNAS = 0;
        $.each(komponen_penilaian_create, function(key, value){
            if(value.kelompok == 1){
                totalPersentaseNTS += parseInt(value.persentase);
            }
            else{
                totalPersentaseNAS += parseInt(value.persentase);
            }
        });
        $('#warning-total_persentase_nts').html('');
        $('#warning-total_persentase_nas').html('');
        if(totalPersentaseNTS < 100){
            $('#warning-total_persentase_nts').html('Total persentase NTS saat ini: ' + totalPersentaseNTS + '%');
            $('#warning-total_persentase_nts').removeClass('bg-success');
            $('#warning-total_persentase_nts').addClass('bg-warning');
        }
        else if(totalPersentaseNTS == 100){
            $('#warning-total_persentase_nts').html('Total persentase NTS sudah 100%');
            $('#warning-total_persentase_nts').removeClass('bg-warning');
            $('#warning-total_persentase_nts').addClass('bg-success');
        }
        else{
            $('#warning-total_persentase_nts').html('Total persentase NTS melebihi 100%. Persentase saat ini: ' + totalPersentaseNTS + '%');
            $('#warning-total_persentase_nts').removeClass('bg-success');
            $('#warning-total_persentase_nts').addClass('bg-warning');
        }
        if(totalPersentaseNAS < 100){
            $('#warning-total_persentase_nas').html('Total persentase NAS saat ini: ' + totalPersentaseNAS + '%');
            $('#warning-total_persentase_nas').removeClass('bg-success');
            $('#warning-total_persentase_nas').addClass('bg-warning');
        }
        else if(totalPersentaseNAS == 100){
            $('#warning-total_persentase_nas').html('Total persentase sudah 100%');
            $('#warning-total_persentase_nas').removeClass('bg-warning');
            $('#warning-total_persentase_nas').addClass('bg-success');
        }
        else{
            $('#warning-total_persentase_nas').html('Total persentase NAS melebihi 100%. Persentase saat ini: ' + totalPersentaseNAS + '%');
            $('#warning-total_persentase_nas').removeClass('bg-success');
            $('#warning-total_persentase_nas').addClass('bg-warning');
        }
        return totalPersentaseNTS + totalPersentaseNAS;
    }
</script>
@endpush
