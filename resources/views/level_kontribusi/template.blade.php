<table>
    <thead>
        <tr>
            <td style="width:3px;"></td>
            <td colspan="4"><b>MATRIKS CPL VS CPMK</b></td>
            <td colspan="14"><b>LEVEL/TINGKAT KETERKAITAN/KONTRIBUSI MK TERHADAP CPL:</b></td>
        </tr>
        @foreach($level as $key=>$item)
        <tr>
            <td></td>
            <td></td>
            @if($key==0)
            <td style="text-align:right;"><b>Program Studi:</b></td>
            <td>{{$program_studi->nama}}</td>
            @elseif($key == 1)
            <td style="text-align:right;"><b>Tahun Ajaran:</b></td>
            <td>{{$tahun_ajaran->keterangan}}</td>
            @else
            <td></td>
            <td></td>
            @endif
            <td></td>
            <td style="text-align:center;">{{$item->level}}</td>
            <td colspan="2">{{$item->keterangan}}</td>
        </tr>
        @endforeach
        <tr style="height:1px;"></tr>
        <tr>
            <td></td>
            <th rowspan='3' style="text-align:center; vertical-align:center;"><b>Semester</b></th>
            <th rowspan='3' style="text-align:center; vertical-align:center;"><b>No</b></th>
            <th colspan='3' rowspan='2' style="text-align:center; vertical-align:center;"><b>Mata Kuliah</b></th>
            <th rowspan='3' style="text-align:center; vertical-align:center;"><b>Jumlah CPL</b></th>
            @foreach($jenis_cpl as $item)
            <th colspan='{{$item->jumlah*2}}' style="text-align:center; 
            vertical-align:center;"><b>Capaian Pembelajaran Lulusan (CPL)</b></th>
            @endforeach
        </tr>
        <tr>
            <td></td>
            @foreach($jenis_cpl as $item)
            <th colspan='{{$item->jumlah * 2 - 8}}' style="text-align:center; vertical-align:center;">
                <b>{{$item->nama}}</b></th>
            <th colspan='6' style="text-align:right;"><b>Weight(%):</b></th>
            <th colspan='2'></th>
            @endforeach
        </tr>
        <tr>
            <td></td>
            <th style="text-align:center;"><b>Kode</b></th>
            <th style="text-align:center;"><b>Nama</b></th>
            <th style="text-align:center;"><b>SKS</b></th>
            @foreach($cpl as $item)
            <th colspan="2" style="text-align:center;"><b>{{$item->kode}}</b></th>
            @endforeach
        </tr>
    </thead>
</table>
