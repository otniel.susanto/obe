@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Matriks CPL vs CPMK</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('level-kontribusi')}}">Matriks CPL vs CPMK</a></li>
        </ol>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Filter Section -->
        <form action="{{url('level-kontribusi')}}" method="get">
                <div class="row">
                    <div class="col-md-2">
                        <select name="tahun_ajaran_id" id="tahun_ajaran_id" class="select2 form-control">
                            <option disabled selected></option>
                            @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}"
                                @if($item->id == $request->tahun_ajaran_id) selected @endif
                            >{{$item->keterangan}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="program_studi_id" id="program_studi_id" class="form-control">
                            <option disabled selected></option>
                            @foreach($program_studi as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->program_studi_id) selected @endif
                            >{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-6">
                                <input name="kode_mata_kuliah" type="search" class="form-control" placeholder="Kode Mata Kuliah" value="{{$request->kode_mata_kuliah}}">
                            </div>
                            <div class="col-md-6">
                                <input name="nama_mata_kuliah" type="search" class="form-control" placeholder="Nama Mata Kuliah" value="{{$request->nama_mata_kuliah}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <input name="semester" type="search" class="form-control" placeholder="Semester" value="{{$request->semester}}">
                    </div>
                </div>
                <div class="row mt-1 mb-3">
                    <div class="col-md-6">
                        <button onclick="event.preventDefault();formDownloadLevelKontribusi();" data-toggle="modal" type="button"
                            class="btn btn-info">
                            <i class="fa fa-download"></i>
                            Download Template
                        </button>
                        <button onclick="event.preventDefault();formImportLevelKontribusi();" data-toggle="modal" type="button"
                            class="btn btn-success">
                            <i class="fa fa-upload"></i>
                            Import Data
                        </button>
                        <a href="{{url('level-kontribusi/create')}}" type="button" class="btn btn-primary float-sm-right">
                            <i class="fa fa-plus"></i>
                            Tambah Data
                        </a>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-secondary float-right" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                        <button class="btn btn-primary float-right mr-1" type="submit" id="btnFilter"><i class="fa fa-search"></i> Cari</button>
                        <div class="custom-control custom-switch d-inline float-right mr-1">
                            <input type="checkbox" class="custom-control-input" id="alur_7_semester" name="alur_7_semester"
                                @if($request->alur_7_semester == "on")
                                    checked
                                @endif
                            >
                            <label class="custom-control-label" for="alur_7_semester">Alur 7 Semester</label>
                        </div>
                    </div>
                </div>
        </form>
        <!-- Filter Section End -->
        <!-- Keterangan -->
        <h5>Program Studi: <u>
        @if(isset($request->program_studi_id))
            {{$program_studi->firstWhere('id', $request->program_studi_id)->nama}} 
        @else 
            {{$user->programStudi[0]->nama}}
        @endif</u></h5>
        <h5>Tahun Ajaran: <u>
        @if(isset($request->tahun_ajaran_id))
            {{$tahun_ajaran->firstWhere('id', $request->tahun_ajaran_id)->keterangan}} 
        @else 
            {{$tahun_ajaran->firstWhere('status', 'Aktif')->keterangan}} 
        @endif</u></h5>
        <p>Matriks CPL vs CPMK rendah artinya suatu mata kuliah tidak terlalu kuat kontribusinya dalam penilaian CPL Program Studi</p>
        @if($cpl->count() <= 0)
            <p class="bg-danger text-center text-lg">Tambahkan data CPL terlebih dahulu untuk program studi ini</p>
        <!-- Keterangan End -->
        @else
        <!-- Table -->
        <div class='table-responsive'>
            <table class="table myTable table-bordered table-hover table-striped" 
            id="tableLevelKontribusi">
                <thead>
                    <tr>
                        <th rowspan='3' width='' class='align-middle'>Semester</th>
                        <th rowspan='3' width='' class='align-middle'>No.</th>
                        <th colspan='3' rowspan='2' width='' class='text-center'>Mata Kuliah</th>
                    </tr>
                    <tr class='header-jenis-wrapper'>
                        @foreach($jenis_cpl as $item)
                        <th colspan='{{($item->jumlah*3)-12}}' class='text-center'>{{$item->nama}}</th>
                        <th colspan='9'>Weight(%):</th>
                        <th colspan="3"></th>
                        @endforeach
                        <th rowspan='3' width='' class='align-middle'>Jumlah CPL</th>
                        <th rowspan='3' width='' class='align-middle'>Total Skor Maks</th>
                    </tr>
                    <tr>
                        <th class='text-center'>Kode</th>
                        <th class='text-center'>Nama</th>
                        <th class='text-center'>SKS</th>
                        @foreach($cpl as $item)
                        <th colspan="3" class='text-center' width="30%">{{$item->kode}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($mata_kuliah as $key=>$itemMk)
                    <tr>
                        <td rowspan="2">{{$itemMk->semester}}</td>
                        <td rowspan="2">{{$key+1}}</td>
                        <td rowspan="2">{{$itemMk->kode}}</td>
                        <td rowspan="2">{{$itemMk->nama}}</td>
                        <td rowspan="2">{{$itemMk->sks}}</td>
                        @php
                            $countLevelKontribusi = 0;
                            $totalSkorMaks = 0;
                        @endphp
                        @foreach($cpl as $itemCpl)
                            @if($itemLevelKontribusi = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first())
                                @php
                                    $countLevelKontribusi++;
                                    $cpmkString = "";
                                    $level = 0;
                                @endphp
                                @foreach($itemLevelKontribusi->cpmk as $keyCpmk=>$itemCpmk)
                                    @if($keyCpmk == 0)
                                        @php
                                            $level = $itemLevelKontribusi->level->level;
                                            $totalSkorMaks += $itemLevelKontribusi->skor_maks;
                                            $cpmkString .= $itemCpmk->kode
                                        @endphp
                                    @else
                                        @php
                                            $cpmkString .= ', ' . $itemCpmk->kode
                                        @endphp
                                    @endif
                                @endforeach
                                <td rowspan="2">{{$level ?? ''}}</td>
                                <td colspan="2">{{$cpmkString}}</td>
                            @else
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                        @endforeach
                        <td rowspan="2">{{$countLevelKontribusi}}</td>
                        <td rowspan="2">{{$totalSkorMaks}}</td>
                    </tr>
                    <tr>
                    @foreach($cpl as $itemCpl)
                            @if($itemLevelKontribusi = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first())
                                <td>Max</td>
                                <td>{{$itemLevelKontribusi->skor_maks ?? '-'}}</td>
                            @else
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
    @include('level_kontribusi.import')
    @include('level_kontribusi.download')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        //Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi"
        });
        $('#tahun_ajaran_id').select2({
            allowClear: true,
            placeholder: "Pilih Tahun Ajaran"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('level-kontribusi')}}");
        });
        //Filter Section End
        $('#download-program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $("#btnImport").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);

            $.ajax({
                url: '{{url("level-kontribusi-import")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    if(data.error){
                        var stringException = "";
                        $("#import-error-bag").html('');
                        $.each(data.arrayMessage, function(key, value){
                            stringException += value;
                        });
                        stringException += "*Data yang benar tetap terinput ke dalam database <br> Silakan reload halaman ini";
                        $("#import-error-bag").append(stringException);
                        $("#import-error-bag").show();
                    }
                    // $('#formImportLevelKontribusi').trigger("reset");
                    // $("#formImportLevelKontribusi .close").click();
                    // window.location.replace("{{url('level-kontribusi')}}?program_studi_id=" + data.program_studi_id);
                    // window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    $("#import-error-bag").html('');
                    if(data.responseText){
                        var errors = $.parseJSON(data.responseText) ?? '';
                    }
                    var stringException = "";
                    stringException = "File tidak dapat diimport karena ada perubahan.<br>Silakan reload halaman ini terlebih dahulu dan ulangi proses ini dengan file yang sama.";
                    if(data.status == 422){
                        stringException = "Import gagal. Format file yang diterima: .csv, .xls, atau .xlsx";
                    }
                    else if(data.status == 500){
                        if(errors.message != ""){
                            stringException = errors.message;
                        }
                    }
                    $("#import-error-bag").append(stringException);
                    $("#import-error-bag").show();
                }
            });
        });
        $("#btnDownload").click(function () {
            if($("#download-program_studi_id").val() !== null){
                $("#btnDownload").attr("href", "{{url('level-kontribusi-template')}}/" + $("#download-program_studi_id").val());
                $("#formDownloadLevelKontribusi .close").click();
            }
            else{
                $("#download-error-bag").html('');
                $("#download-error-bag").append("<p>Pilih Program Studi terlebih dahulu</p>");
                $("#download-error-bag").show();
                return false;
            }
        });
    });

    function formImportLevelKontribusi() {
        $("#import-error-bag").hide();
        $('#modalImportLevelKontribusi').modal('show');
    }

    function formDownloadLevelKontribusi() {
        $("#download-error-bag").hide();
        $('#modalDownloadLevelKontribusi').modal('show');
    }

</script>
@endpush
