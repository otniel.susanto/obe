
<!-- Keterangan SCS -->
<table>
    <tr>
        <td colspan="5">Student Competencies Scorecard (SCS) Sheet</td>
    </tr>
    <tr></tr>
    <!-- Keterangan Mahasiswa -->
    <tr>
        <td>NRP</td>
        <td>: {{$data->nrp}}</td>
        <td></td><td></td>
        <td>Program Studi</td>
        <td colspan="2">: {{$data->programStudi->nama}}</td>
    </tr>
    <tr>
        <td>Nama</td>
        <td colspan="3">: {{$data->nama}}</td>
        <td>Admission Year</td>
        <td>: {{$data->tahun_masuk}}</td>
    </tr>
    <tr></tr>
    <tr></tr>
    <!-- ROW SEMESTER, Capaian Pembelajaran Lulusan (CPL) -->
    <tr>
        <th rowspan='3' style="text-align:center; word-wrap: break-word; vertical-align:center;">Semester</th>
        <th rowspan='3' style="text-align:center; word-wrap: break-word; vertical-align:center;">No.</th>
        <th colspan='3' rowspan='2' style="text-align:center; word-wrap: break-word; vertical-align:center;">Mata Kuliah</th>
        @foreach($jenis_cpl as $item)
        <th colspan='{{($item->jumlah*4)}}' style="text-align:center; word-wrap: break-word; vertical-align:center;">Capaian Pembelajaran Lulusan (CPL)</th>
        @endforeach
        <th rowspan='3' style="text-align:center; word-wrap: break-word; vertical-align:center;">Total Skor Maksimal</th>
        <th rowspan='3' style="text-align:center; word-wrap: break-word; vertical-align:center;">Total Skor Mahasiswa</th>
        <th rowspan='3' style="text-align:center; word-wrap: break-word; vertical-align:center;">Persentase Total Capaian</th>
        <th rowspan='3' style="text-align:center; word-wrap: break-word; vertical-align:center;">Jumlah CPL</th>
    </tr>
    <!-- ROW HARD SKILLS, SOFT SKILLS -->
    <tr>
        @foreach($jenis_cpl as $item)
        <th colspan='{{($item->jumlah*4)-16}}' style="text-align:center; word-wrap: break-word; vertical-align:center;">{{$item->nama}}</th>
        <th colspan='12'>Weight(%):</th>
        <th colspan='4'></th>
        @endforeach
    </tr>
    <!-- ROW KODE CPL -->
    <tr>
        <th style="text-align:center; word-wrap: break-word; vertical-align:center;">Kode</th>
        <th style="text-align:center; word-wrap: break-word; vertical-align:center;">Nama</th>
        <th style="text-align:center; word-wrap: break-word; vertical-align:center;">SKS</th>
        @foreach($cpl as $item)
        <th colspan="3" style="text-align:center; word-wrap: break-word; vertical-align:center;">{{$item->kode}}</th>
        @endforeach
    </tr>
    <!-- ROW Data Mata Kuliah -->
    @foreach($mata_kuliah as $key=>$itemMk)
    <tr>
        <td rowspan="3" style="text-align:center; vertical-align:center;">{{$itemMk->semester}}</td>
        <td rowspan="3" style="text-align:center; vertical-align:center;">{{$key+1}}</td>
        <td rowspan="3" style="text-align:center; vertical-align:center;">{{$itemMk->kode}}</td>
        <td rowspan="3" style="text-align:center; vertical-align:center;">{{$itemMk->nama}}</td>
        <td rowspan="3" style="text-align:center; vertical-align:center;">{{$itemMk->sks}}</td>
        @php
            $countLevelKontribusi = 0;
            $countTotalSkorMaks = 0;
            $countTotalSkorMahasiswa = 0;
        @endphp
        @foreach($cpl as $itemCpl)
            @php
                $cpmkString = "";
                $itemLevel = "";
                $dataExist = false;
            @endphp
            @if($itemLevelKontribusi = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first())
                @php
                    $itemLevel = $itemLevelKontribusi->level->level;
                    $countLevelKontribusi++;
                    $countTotalSkorMaks += $itemLevelKontribusi->skor_maks;
                    $countTotalSkorMahasiswa += $itemLevelKontribusi->skorMahasiswa->firstWhere('mahasiswa_id', $data->id)->skor ?? 0;
                    $dataExist = true;
                @endphp
                @foreach($itemLevelKontribusi->cpmk as $keyCpmk=>$itemCpmk)
                    @if($keyCpmk == 0)
                        @php
                            $cpmkString .= $itemCpmk->kode
                        @endphp
                    @else
                        @php
                            $cpmkString .= ', ' . $itemCpmk->kode
                        @endphp
                    @endif
                @endforeach
            @endif
            @if($dataExist === true)
            <td rowspan="3" style="text-align:center; vertical-align:center;">{{$itemLevel}}</td>
            <td colspan="3" style="text-align:center; vertical-align:center;">{{$cpmkString}}</td>
            @else
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            @endif
        @endforeach
        @php
            $persentaseTotalCapaian = $countTotalSkorMahasiswa > 0 ? round($countTotalSkorMahasiswa / $countTotalSkorMaks * 100,2) . '%': 'N/A' ;
        @endphp
        <td rowspan="3" style="text-align:center; word-wrap: break-word; vertical-align:center;">{{$countTotalSkorMaks}}</td>
        <td rowspan="3" style="text-align:center; word-wrap: break-word; vertical-align:center;">{{$countTotalSkorMahasiswa}}</td>
        <td rowspan="3" style="text-align:center; word-wrap: break-word; vertical-align:center;">{{$persentaseTotalCapaian}}</td>
        <td rowspan="3" style="text-align:center; word-wrap: break-word; vertical-align:center;">{{$countLevelKontribusi}}</td>
    </tr>
    <!-- Row Tulisan Max dan Score -->
    <tr>
        @foreach($cpl as $itemCpl)
            @php
                $cpmkString = "";
                $itemLevel = "";
                $dataExist = false;
            @endphp
            @foreach($level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id) as $itemLevelKontribusi)
                @php
                    $dataExist = true;
                @endphp
            @endforeach
            @if($dataExist === true)
                <td>Max</td>
                <td>Score</td>
                <td>%</td>
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
        @endforeach
    </tr>
    <!-- Row Angka Max dan Score Mahasiswa -->
    <tr>
        @foreach($cpl as $itemCpl)
            @if($level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first())
                @php 
                    $skorMaks = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first()->skor_maks ?? 0;
                    $skorMahasiswa = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first()->skorMahasiswa->firstWhere('mahasiswa_id',$data->id)->skor ?? 0;
                    $persentase = $skorMahasiswa > 0 ? round($skorMahasiswa / $skorMaks * 100,2) . '%' : 'N/A';
                @endphp
                <td style="text-align:center; vertical-align:center;">{{$skorMaks}}</td>
                <td style="text-align:center; vertical-align:center;">{{$skorMahasiswa}}</td>
                <td style="text-align:center; vertical-align:center;">{{$persentase}}</td>
            @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            @endif
        @endforeach
    </tr>
    @endforeach
</table>