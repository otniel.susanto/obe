@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1 class="mb-1">Student Competencies Score Card</h1>
        <!-- Filter Section -->
        <form action="{{url('score-card')}}" method="get" class="mt-2">
            <div class="form-group row">
                <div class="col-4">
                    <select id="mahasiswa_id" name="mahasiswa_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($mahasiswa as $item)
                            <option value="{{$item->id}}"
                                @if($item->id == ($data->id ?? ''))
                                selected
                                @endif
                            >{{$item->nrp}} - {{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-auto">
                    <div class="custom-control custom-switch mt-2">
                        <input type="checkbox" class="custom-control-input" id="alur_7_semester" name="alur_7_semester"
                            @if($request->alur_7_semester == "on")
                                checked
                            @endif
                        >
                        <label class="custom-control-label" for="alur_7_semester">Alur 7 Semester</label>
                    </div>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary" id="btnCari"><i class="fa fa-search"></i>Cari</button>
                    <button class="btn btn-secondary" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                </div>
            </div>
            <div class="form-group row">
            </div>
        </form>
        <!-- Filter Section End -->
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @if(!isset($data) || $data == null)
        <h5>No Data</h5>
        @elseif($data != null)
        <!-- Keterangan SCS -->
        <div>
            <!-- Keterangan Mahasiswa -->
            <div class="row">
                <div class="col-1">
                    <h5>NRP</h5>
                </div>
                <div class="col-4">
                    <h5>: {{$data->nrp}}</h5>
                </div>
                <div class="col-2">
                    <h5>Program Studi</h5>
                </div>
                <div class="col-4">
                    <h5>: {{$data->programStudi->nama}} </h5>
                </div>
            </div>
            <div class="row">
                <div class="col-1">
                    <h5>Nama</h5>
                </div>
                <div class="col-4">
                    <h5>: {{$data->nama}}</h5>
                </div>
                <div class="col-2">
                    <h5>Admission Year</h5>
                </div>
                <div class="col-4">
                    <h5>: {{$data->tahun_masuk}}</h5>
                </div>
            </div>
            <span>
                <form action="{{url('score-card-export')}}" method="get">
                    <input type="hidden" name="mahasiswa_id" value="{{$data->id}}">
                    <input type="hidden" name="alur_7_semester" value="{{$request->alur_7_semester}}">
                    <button type="submit" class="btn btn-success float-sm-right mb-1">
                    <i class="fa fa-file-excel"></i>
                    Export ke File Excel</button>
                </form>
            </span>
        </div>
        
        <div class='table-responsive'>
            <table class="table myTable table-bordered table-hover" id="tableScoreCard">
                <thead>
                    <!-- ROW SEMESTER, Capaian Pembelajaran Lulusan (CPL) -->
                    <tr>
                        <th rowspan='3' width='' class='align-middle'>Semester</th>
                        <th rowspan='3' width='' class='align-middle'>No.</th>
                        <th colspan='3' rowspan='2' width='' class='text-center align-middle'>Mata Kuliah</th>
                        @foreach($jenis_cpl as $item)
                        <th colspan='{{($item->jumlah*4)}}' class='text-center'>Capaian Pembelajaran Lulusan (CPL)</th>
                        @endforeach
                        <th rowspan='3' class='text-center align-middle'>Total Skor Maks</th>
                        <th rowspan='3' class='text-center align-middle'>Total Skor Mahasiswa</th>
                        <th rowspan='3' class='text-center align-middle'>Persentase Total Capaian</th>
                        <th rowspan='3' class='text-center align-middle'>Jumlah CPL</th>
                    </tr>
                    <!-- ROW HARD SKILLS, SOFT SKILLS -->
                    <tr class='header-jenis-wrapper'>
                        @foreach($jenis_cpl as $item)
                        <th colspan='{{($item->jumlah*4)-16}}' class='text-center'>{{$item->nama}}</th>
                        <th colspan='12' class="text-right">Weight(%):</th>
                        <th colspan='4'></th>
                        @endforeach
                    </tr>
                    <!-- ROW KODE CPL -->
                    <tr>
                        <th class='text-center align-middle'>Kode</th>
                        <th class='text-center align-middle'>Nama</th>
                        <th class='text-center align-middle'>SKS</th>
                        @foreach($cpl as $item)
                        <th colspan="4" class='text-center' width="30%">{{$item->kode}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    <!-- ROW Data Mata Kuliah -->
                    @foreach($mata_kuliah as $key=>$itemMk)
                    <tr>
                        <td rowspan="3" class="text-center align-middle">{{$itemMk->semester}}</td>
                        <td rowspan="3" class="text-center align-middle">{{$key+1}}</td>
                        <td rowspan="3" class="text-center align-middle">{{$itemMk->kode}}</td>
                        <td rowspan="3" class="text-center align-middle">{{$itemMk->nama}}</td>
                        <td rowspan="3" class="text-center align-middle">{{$itemMk->sks}}</td>
                        @php
                            $countLevelKontribusi = 0;
                            $countTotalSkorMaks = 0;
                            $countTotalSkorMahasiswa = 0;
                        @endphp
                        @foreach($cpl as $itemCpl)
                            @php
                                $cpmkString = "";
                                $itemLevel = "";
                                $dataExist = false;
                            @endphp
                            @if($itemLevelKontribusi = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first())
                                @php
                                    $itemLevel = $itemLevelKontribusi->level->level;
                                    $countLevelKontribusi++;
                                    $countTotalSkorMaks += $itemLevelKontribusi->skor_maks;
                                    $countTotalSkorMahasiswa += $itemLevelKontribusi->skorMahasiswa->firstWhere('mahasiswa_id', $data->id)->skor ?? 0;
                                    $dataExist = true;
                                @endphp
                                @foreach($itemLevelKontribusi->cpmk as $keyCpmk=>$itemCpmk)
                                    @if($keyCpmk == 0)
                                        @php
                                            $cpmkString .= $itemCpmk->kode
                                        @endphp
                                    @else
                                        @php
                                            $cpmkString .= ', ' . $itemCpmk->kode
                                        @endphp
                                    @endif
                                @endforeach
                            @endif
                            @if($dataExist === true)
                            <td rowspan="3" class="text-center align-middle">{{$itemLevel}}</td>
                            <td colspan="3" class="text-center align-middle">{{$cpmkString}}</td>
                            @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @endif
                        @endforeach
                        @php
                            $persentaseTotalCapaian = $countTotalSkorMahasiswa > 0 ? round($countTotalSkorMahasiswa / $countTotalSkorMaks * 100,2) . '%': 'N/A' ;
                        @endphp
                        <td rowspan="3" class="text-center align-middle">{{$countTotalSkorMaks}}</td>
                        <td rowspan="3" class="text-center align-middle">{{$countTotalSkorMahasiswa}}</td>
                        <td rowspan="3" class="text-center align-middle">{{$persentaseTotalCapaian}}</td>
                        <td rowspan="3" class="text-center align-middle">{{$countLevelKontribusi}}</td>
                    </tr>
                    <!-- Row Tulisan Max dan Score -->
                    <tr>
                        @foreach($cpl as $itemCpl)
                            @php
                                $cpmkString = "";
                                $itemLevel = "";
                                $dataExist = false;
                            @endphp
                            @foreach($level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id) as $itemLevelKontribusi)
                                @php
                                    $dataExist = true;
                                @endphp
                            @endforeach
                            @if($dataExist === true)
                                <td class="text-center">Max</td>
                                <td class="text-center">Score</td>
                                <td class="text-center">%</td>
                            @else
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                        @endforeach
                    </tr>
                    <!-- Row Angka Max dan Score Mahasiswa -->
                    <tr>
                        @foreach($cpl as $itemCpl)
                            @if($level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first())
                                @php 
                                    $skorMaks = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first()->skor_maks ?? 0;
                                    $skorMahasiswa = $level_kontribusi->where('mata_kuliah_id', $itemMk->id)->where('cpl_id', $itemCpl->id)->first()->skorMahasiswa->firstWhere('mahasiswa_id',$data->id)->skor ?? 0;
                                    $persentase = $skorMahasiswa > 0 ? round($skorMahasiswa / $skorMaks * 100,2) . '%' : 'N/A';
                                @endphp
                                <td class="text-center">{{$skorMaks}}</td>
                                <td class="text-center">{{$skorMahasiswa}}</td>
                                <td class="text-center">{{$persentase}}</td>
                            @else
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</section>
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        //Filter Section
        $('#mahasiswa_id').select2({
            placeholder: "Pilih Mahasiswa",
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('score-card')}}");
        });
        //Filter Section End
    });
</script>
@endpush
