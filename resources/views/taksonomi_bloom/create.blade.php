@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Taksonomi Bloom</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('taksonomi-bloom')}}">Taksonomi Bloom</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('taksonomi-bloom/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAddTaksonomiBloom">
            <div class="form-group row">
                <div class="col-3">
                    <label for="level">Level</label>
                    <input required type="text" class="form-control" name="level" id="level" placeholder="Masukkan level">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6">
                    <label for="keterangan">Keterangan</label>
                    <input required type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Masukkan keterangan Taksonomi Bloom">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
$(document).ready(function () {
    $("#btnAdd").click(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url:'{{url("taksonomi-bloom")}}',
            data: {
                level: $("#level").val(),
                keterangan: $("#keterangan").val()
            },
            dataType: 'json',
            success: function (data) {
                alert(data.message);
                window.location.replace("{{url('taksonomi-bloom')}}");
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-errors').html('');
                $.each(errors.message, function (key, value) {
                    $('#add-errors').append('<li>' + value + '</li>');
                });
                $("#add-error-bag").show();
            }
        });
    });
});
</script>
@endpush