@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Taksonomi Bloom</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('taksonomi-bloom')}}">Taksonomi Bloom</a></li>
        </ol>
        <a href="{{url('taksonomi-bloom/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Level</th>
                    <th width="">Keterangan</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($taksonomi_bloom as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->level}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditTaksonomiBloom({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteTaksonomiBloom({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$taksonomi_bloom->count()}}</b> out of <b>{{$taksonomi_bloom->total()}}</b> entries
            </div> {{ $taksonomi_bloom->links() }}
        </div>
    </div>
    @include('taksonomi_bloom.edit')
    @include('taksonomi_bloom.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("taksonomi-bloom")}}/' + $("#formEditTaksonomiBloom #id_edit").val(),
                data: {
                    level: $("#formEditTaksonomiBloom #level").val(),
                    keterangan: $("#formEditTaksonomiBloom #keterangan").val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditTaksonomiBloom').trigger("reset");
                    $("#formEditTaksonomiBloom .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-taksonomi-bloom-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-taksonomi-bloom-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("taksonomi-bloom")}}/' + $("#formDeleteTaksonomiBloom #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteTaksonomiBloom .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditTaksonomiBloom(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("taksonomi-bloom")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditTaksonomiBloom #level").val(data.taksonomi_bloom.level);
                $("#formEditTaksonomiBloom #keterangan").val(data.taksonomi_bloom.keterangan);
                $("#formEditTaksonomiBloom #id_edit").val(data.taksonomi_bloom.id);
                $('#modalEditTaksonomiBloom').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteTaksonomiBloom(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("taksonomi-bloom")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteTaksonomiBloom #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Taksonomi Bloom level: " + data.taksonomi_bloom.level + "</p>" + 
                "<p>Keterangan: " + data.taksonomi_bloom.keterangan + "</p>")
                $("#formDeleteTaksonomiBloom #id_delete").val(data.taksonomi_bloom.id);
                $('#modalDeleteTaksonomiBloom').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
