@extends('auth.layout')
@section('content')
<!-- .login-box -->
<div class="login-box">
    <div class="login-logo">
        <img src="{{asset('img/logoUbaya.png')}}" alt="Logo UBAYA" style="width:170px; height:200px;">
        <h3>Sistem Penilaian Outcome Based Education</h3>
        <h5>Universitas Surabaya</h5>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <form action="{{url('login')}}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input id="username" type="string" class="form-control @error('email') is-invalid @enderror" name="username"
                    value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="Email Ubaya">
                    <input type="hidden" id="email" name="email" value="{{ old('email') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    <span class="mt-2 text-sm ml-1">@staff.ubaya.ac.id</span>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>Email tidak terdaftar</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <!-- <input required type="password" class="form-control" placeholder="Password"> -->
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="current-password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- /.social-auth-links -->

            <p class="mb-1">
                <a href="{{url('password/reset')}}">Lupa password?</a>
            </p>
            <p class="mb-0">
                <a href="{{url('register')}}" class="text-center">Daftar akun baru di sini!</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#username").change(function(){
            $("#email").val($("#username").val() + "@staff.ubaya.ac.id");
        });
    });
</script>
@endpush