<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Penilaian OBE</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icon kecil web tab -->
    <link rel="icon" href="{{asset('img/iconUbaya.png')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Select2 CSS -->
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <!-- Select2 CSS Bootstrap4 Theme -->
    <link href="{{asset('css/select2-bootstrap4.min.css')}}" rel="stylesheet" />
</head>

<body class="hold-transition login-page">

    @yield('content')
    
    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.min.js')}}"></script>
    <!-- Select2 JS -->
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap4");
        });
    </script>

    @stack('child-scripts')
</body>

</html>

