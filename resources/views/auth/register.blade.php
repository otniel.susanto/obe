@extends('auth.layout')
@section('content')
<!-- .register-box -->
<div class="register-box">
    <div class="register-logo">
        <img src="{{asset('img/logoUbaya.png')}}" alt="Logo UBAYA" style="width:70px; height:80px;">
        <h5>Sistem Penilaian Outcome Based Education</h5>
        <h6>Universitas Surabaya</h6>
    </div>
    <!-- /.register-logo -->
    <div class="card">
        <div class="card-body register-card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="input-group mb-3">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                        value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nama Lengkap">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="input-group mb-3">
                    <input id="npk" type="text" class="form-control @error('npk') is-invalid @enderror" name="npk"
                        value="{{ old('npk') }}" required autocomplete="npk" placeholder="NPK">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    @error('npk')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <!-- Email -->
                <div class="input-group mb-3">
                    <input id="email" type="text"  name="email" class="form-control @error('email') is-invalid @enderror"
                        value="{{ old('email') }}" required autocomplete="email" placeholder="nama@staff.ubaya.ac.id">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-at"></span>
                        </div>
                    </div>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                
                <!-- Program Studi -->
                <span class="d-block">Program studi yang diajar saat ini: <br> (Boleh pilih lebih dari 1)</span>
                @foreach($program_studi->unique('kode') as $item)
                <div class="input-group">
                    <div class="form-check d-block">
                        <input name="program_studi[]" class="form-check-input" type="checkbox" value="{{$item->id}}" id="program_studi_{{$item->id}}">
                        <label class="form-check-label" for="program_studi_{{$item->id}}">
                        {{$item->nama}}
                        </label>
                    </div>
                </div>
                @endforeach
                @error('program_studi')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <!-- Jabatan -->
                <!-- <div class="input-group mb-3">
                    <select id="jabatan" class="form-control @error('jabatan') is-invalid @enderror"
                            name="jabatan" value="{{ old('jabatan') }}" required autocomplete="jabatan"
                            placeholder="Jabatan">
                        <option disabled selected></option>
                        <option value="Dosen">Dosen</option>
                        <option value="Wakil Dekan">Wakil Dekan</option>
                        <option value="Kepala Jurusan">Kepala Jurusan</option>
                        <option value="Kepala Laboratotium">Kepala Laboratorium</option>
                        <option value="Kepala Departemen">Kepala Departemen</option>
                        <option value="Koordinator Mata Kuliah">Koordinator Mata Kuliah</option>
                    </select>
                    @error('jabatan')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div> -->
                <!-- Status -->
                <div class="input-group mb-3 mt-3">
                    <div class="row">
                        <label for="status" class="col col-form-label text-md-right">{{ __('Status') }}</label>

                        <div class="col">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="status-dosen-tetap"
                                    value="Dosen Tetap" checked>
                                <label class="form-check-label" for="status-dosen-tetap">
                                    Dosen Tetap
                                </label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-check">
                                <input class="form-check-input @error('status') is-invalid @enderror" type="radio"
                                    name="status" id="status-dosen-lb" value="Dosen LB">
                                <label class="form-check-label" for="status-dosen-lb">
                                    Dosen LB
                                </label>
                            </div>
                            <!-- <input id="npk" type="text" class="form-control @error('npk') is-invalid @enderror"
                                    name="npk" value="{{ old('npk') }}" required autocomplete="npk"> -->

                        </div>
                    </div>
                    @error('status')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="new-password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>


                <div class="input-group mb-3">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                        required autocomplete="new-password" placeholder="Konfirmasi Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" id="btnSubmit" class="btn btn-primary btn-block">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
            <img src="" class="d-none" style="display:none;" id="imgUser">
            <!-- /.social-auth-links -->
            <p class="mb-0">
                <a href="{{url('login')}}" class="text-center">Kembali ke halaman Login</a>
            </p>
        </div>
        <!-- /.register-card-body -->
    </div>
</div>
<!-- /.register-box -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        // Filter Section
        $('#jabatan').select2({
            placeholder: "Pilih Jabatan"
        });
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi yang Diajar"
        });
        //Filter Section End
        $("#btnSubmit").click(function(){
            $("#imgUser")
            .on('error', function() { 
                $("form").submit(function(e){
                    // e.preventDefault();
                    return false;
                });
                alert("Anda tidak diijinkan mendaftar, data NPK tidak ditemukan!\nSilakan hubungi admin sistem untuk mendaftar");
            })
            .attr("src", "https://my.ubaya.ac.id/img/krywn/" + $("#npk").val() + "_m.jpg");
        });
    });
</script>
@endpush
