@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1 class="mb-2">Cara Penggunaan Aplikasi</h1>
        <!-- Navbar -->
        <div class="sticky-top">
            <ul class="list-group list-group-horizontal">
                <li class="list-group-item"><a href="#fitur-utama" class="text-secondary"><u>Fitur Utama</u></a></li>
                <li class="list-group-item"><a href="#pergantian-tahun-ajaran" class="text-secondary"><u>Pergantian Tahun Ajaran</u></a></li>
                <!-- <li class="list-group-item"><a href="#" class="text-secondary"><u></u></a></li> -->
            </ul>
        </div>
        <!-- /.navbar -->
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div id="">
            <h4>Selamat datang di Aplikasi Sistem Penilaian Berbasis Outcome Based Education!</h4>
            <div class="row">
                <p class=" col-md-10">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aplikasi Sistem Penilaian Berbasis Outcome Based Education ini dibuat untuk Universitas Surabaya. 
                    Melalui aplikasi ini, harapannya pengguna merasakan manfaat dari laporan yang dihasilkan oleh aplikasi 
                    dalam mengetahui penilaian terhadap Capaian Pembelajaran Lulusan (CPL). Untuk mengetahui cara menggunakan aplikasi 
                    silakan membaca keterangan berikut ini.
                </p>
            </div>
        </div>
        <div id="fitur-utama">
            <h5><u>Fitur Utama</u></h5>
            <div class="row">
                <div class="col-md-8">
                    <p class="">Fitur utama dari apllikasi ini adalah membuat penilaian terhadap CPL pada setiap Mahasiswa. <br>
                        Langkah-langkah yang perlu dilakukan untuk membuat laporan penilaian tersebut adalah:</p>
                    <ol>
                        <li class="m-1">Menambah data master <a href="{{url('mata-kuliah')}}"><u>Mata Kuliah</u></a></li>
                        <li class="m-1">Menambah data master <a href="{{url('cpl')}}"><u>Capaian Pembelajaran Lulusan (CPL)</u></a> untuk setiap Program Studi</li>
                        <li class="m-1">Menambah data <a href="{{url('rps')}}"><u>Rencana Pembelajaran Semester (RPS)</u></a> Mata Kuliah<br>
                        Setiap kode Mata Kuliah hanya memiliki 1 data RPS saja dan pilihlah Program Studi yang sesuai <br>
                        *Perhatikan Mata Kuliah yang diikuti oleh Mahasiswa dari beberapa Program Studi</li>
                        <li class="m-1">Menambah data <a href="{{url('level-kontribusi')}}"><u>Matriks CPL vs CPMK</u></a>untuk setiap Program Studi</li>
                        <li class="m-1">Menambah data <a href="{{url('kelas')}}"><u>Kelas Paralel</u></a> beserta data Mahasiswa <br>untuk Mata Kuliah pada Tahun Ajaran yang sedang aktif<br>
                        *Data Mahasiswa dapat dimasukkan menggunakan file dari <a href="https://my.ubaya.ac.id" target="blank"><u>MyUbaya</u></a></li>
                        <li class="m-1">Menambah data <a href="{{url('assessment')}}"><u>Penilaian (Assessment)</u></a> untuk Kelas Paralel yang telah dibuat berdasarkan data RPS</li>
                        <li class="m-1">Memasukkan <a href="{{url('assessment')}}"><u>Nilai Mahasiswa</u></a> untuk setiap penilaian yang telah dibuat <br>
                        *Sistem menyediakan file template penilaian yang bisa didownload pada halaman detail penilaian <br>
                        *Ubah dan gunakan file yang sama untuk memasukkan penilaian</li>
                        <li class="m-1">Laporan sudah dapat dilihat pada menu Laporan 
                            <ul>
                                <li><a href="{{url('score-card')}}"><u>Student Competencies Score Card</u></a></li>
                                <li><a href="{{url('course-score-card')}}"><u>Course Competencies Score Card</u></a></li>
                                <li><a href="{{url('course-scoring-sheet')}}"><u>Course Scoring Sheet</u></a></li>
                            </ul>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div id="pergantian-tahun-ajaran">
            <div class="row">
                <div class="col-md-8">
                    <h5><u>Pergantian Tahun Ajaran</u></h5>
                    <p class="">Saat melakukan pergantian Tahun Ajaran, perlu dipastikan bahwa data dari semua Program Studi sudah siap untuk dikunci (tidak bisa diubah lagi). <br>
                    Beberapa data yang akan terkunci adalah:</p>
                    <ul>
                        <li class="m-1">Data RPS akan memiliki perubahan data Tahun Ajaran</li>
                        <li class="m-1">Hubungan antara CPL dengan CPMK <br>
                        *Data ini otomatis ditambahkan saat Kelas Paralel baru dibuat setelah Tahun Ajaran baru ditambahkan. 
                        User tidak perlu memasukkan data Matriks CPL vs CPMK lagi</li>
                    </ul>
                    <p class="">Pengguna perlu menambahkan data Tahun Ajaran dan mengisi keterangan sesuai dengan <br>
                    nama Tahun Ajaran (Contoh: Gasal 2020/2021) <br>
                    Klik <a href="{{url('tahun-ajaran')}}"><u>di sini</u></a> untuk melakukan pergantian Tahun Ajaran</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection