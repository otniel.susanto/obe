<!-- Delete Dosen Penguji Modal Form HTML -->
<div class="modal fade" id="modalDeleteDosenPenguji">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteDosenPenguji">
                <div class="modal-header">
                    <h4 class="modal-title" name="title">Hapus Dosen Penguji </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="delete-data-dosen_penguji"></div>
                    <p class="text-warning"><small>Data Dosen penguji akan terhapus dari Penilaian (Assessment) ini.</small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="id-delete-dosen_penguji" type="hidden" value=""> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDeleteDosenPenguji" type="button">
                    <i class="fa fa-trash"></i>
                    Hapus </button>
                </div>
            </form>
        </div>
    </div>
</div>
