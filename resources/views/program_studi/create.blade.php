@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Program Studi</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('program-studi')}}">Program Studi</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('program-studi/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formProgramStudi">
            <div class="alert alert-danger d-none" id="add-error-bag"></div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="kode">Kode pada NRP Mahasiswa (4 digit pertama kode MK)</label>
                    <input required type="text" class="form-control" name="kode" id="kode" placeholder="4 digit pertama NRP Mahasiswa">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="nama">Nama</label>
                    <input required type="text" class="form-control" name="nama" id="nama" placeholder="Nama">
                </div>
            </div>
            <div class="form-group">
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
$(document).ready(function () {
    $("#btnAdd").click(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url:'{{url("program-studi")}}',
            data: {
                kode: $("#kode").val(),
                nama: $("#nama").val()
            },
            dataType: 'json',
            success: function (data) {
                alert(data.message);
                window.location.replace("{{url('program-studi')}}");
            },
            error: function (data) {
                $("#add-error-bag").removeClass("d-none");
                $("#add-error-bag").html('');
                var errors = $.parseJSON(data.responseText);
                var stringException = '';
                if(data.status == 422){
                    $.each(errors.errors, function (key, value) {
                        stringException += value + "<br>";
                    });
                }else{
                    stringException = errors.message;
                }
                $("#add-error-bag").html(stringException);
                $("#add-error-bag").show();
            }
        });
    });
});
</script>
@endpush