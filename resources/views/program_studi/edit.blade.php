<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditProgramStudi">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditProgramStudi">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Program Studi </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> 
                        × 
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                    </div>
                    <div class="form-group"> 
                        <label>Kode pada NRP Mahasiswa (4 digit pertama)</label> 
                        <input class="form-control" id="edit-kode" required="" type="text" placeholder="4 digit pertama NRP Mahasiswa">
                    </div>
                    <div class="form-group"> 
                        <label>Nama </label> 
                        <input class="form-control" id="edit-nama" required="" type="text">
                    </div>
                </div>
                <div class="modal-footer"> 
                    <input id="id_edit" type="hidden" value="0"> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button> 
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add">
                        <i class="fa fa-marker"></i>
                        Update 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
