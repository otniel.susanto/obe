@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Program Studi</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('program-studi')}}">Program Studi</a></li>
        </ol>
        <a href="{{url('program-studi/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100%" data-toggle="table" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th data-sortable="true">Kode pada NRP Mahasiswa<br> (4 digit pertama)</th>
                    <th width="10%">Nama</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($program_studi as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->kode}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditProgramStudi({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteProgramStudi({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('program_studi.edit')
    @include('program_studi.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("program-studi")}}/' + $("#id_edit").val(),
                data: {
                    kode: $("#edit-kode").val(),
                    nama: $("#edit-nama").val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $('#formEditProgramStudi').trigger("reset");
                    $("#formEditProgramStudi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    $("#edit-error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    $("#edit-error-bag").append(stringException);
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("program-studi")}}/' + $("#id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formDeleteProgramStudi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    $("#delete-error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    $("#delete-error-bag").append(stringException);
                    $("#delete-error-bag").show();
                }
            });
        });
    });

    function formEditProgramStudi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("program-studi")}}/' + id  + '/edit',
            success: function (data) {
                if(data.error == true){
                    alert(data.message);
                }
                else{
                    $("#edit-error-bag").hide();
                    $("#edit-kode").val(data.program_studi.kode);
                    $("#edit-nama").val(data.program_studi.nama);
                    $("#formEditProgramStudi #id_edit").val(data.program_studi.id);
                    $('#modalEditProgramStudi').modal('show');
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteProgramStudi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("program-studi")}}/' + id + '/edit',
            success: function (data) {
                $("#delete-error-bag").hide();
                if(data.error == true){
                    alert(data.message);
                }
                else{
                    $("#delete-data").html("<p>Program Studi: " + data.program_studi.nama + "</p>")
                    $("#id_delete").val(data.program_studi.id);
                    $('#modalDeleteProgramStudi').modal('show');
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
