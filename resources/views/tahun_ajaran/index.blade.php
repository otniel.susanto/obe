@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Tahun Ajaran</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('tahun-ajaran')}}">Tahun Ajaran</a></li>
        </ol>
        <a href="{{url('tahun-ajaran/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle="  class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Keterangan</th>
                    <th width="">Status</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tahun_ajaran as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        @if($item->status == 'Aktif')
                        <span class="bg-success">{{$item->status}}</span>
                        @else
                        <span class="bg-danger">{{$item->status}}</span>
                        @endif
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formEditTahunAjaran({{$item->id}});" class="edit open-modal"
                            data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteTahunAjaran({{$item->id}});" class="delete"
                            data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$tahun_ajaran->count()}}</b> out of <b>{{$tahun_ajaran->total()}}</b>
                entries
            </div> {{ $tahun_ajaran->links() }}
        </div>
    </div>
    @include('tahun_ajaran.edit')
    @include('tahun_ajaran.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("tahun-ajaran")}}/' + $("#formEditTahunAjaran #id_edit").val(),
                data: {
                    keterangan: $("#formEditTahunAjaran #keterangan").val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditTahunAjaran').trigger("reset");
                    $("#formEditTahunAjaran .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-tahun-ajaran-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-tahun-ajaran-errors').append('<li>' + value +
                            '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("tahun-ajaran")}}/' + $("#formDeleteTahunAjaran #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteTahunAjaran .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var stringException = '';
                    $("#delete-error-bag").html('');
                    if(data.responseText){
                        var errors = $.parseJSON(data.responseText) ?? '';
                    }
                    if(data.status == 422){
                        $.each(errors.message, function (key, value) {
                            stringException += value + "<br>";
                        });
                    }else{
                        stringException = errors.message;
                    }
                    console.log(data);
                    $("#delete-error-bag").html(stringException);
                    $("#delete-error-bag").show();
                }
            });
        });
    });

    function formEditTahunAjaran(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("tahun-ajaran")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditTahunAjaran #keterangan").val(data.tahun_ajaran.keterangan);
                $("#formEditTahunAjaran #id_edit").val(data.tahun_ajaran.id);
                $('#modalEditTahunAjaran').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteTahunAjaran(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("tahun-ajaran")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteTahunAjaran #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Tahun Ajaran: " + data.tahun_ajaran.keterangan + "</p>")
                $("#formDeleteTahunAjaran #id_delete").val(data.tahun_ajaran.id);
                $("#delete-error-bag").hide();
                $('#modalDeleteTahunAjaran').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
