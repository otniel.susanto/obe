@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Tahun Ajaran</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('tahun-ajaran')}}">Tahun Ajaran</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('tahun-ajaran/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAddTahunAjaran">
            <div class="form-group row">
                <div class="col-6">
                    <label for="keterangan">Keterangan</label>
                    <input required type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Masukkan keterangan Tahun Ajaran">
                </div>
            </div>
            <div class="row">
                <div class="col-auto">
                <p class="bg-warning">Menambah data Tahun Ajaran akan otomatis menonaktifkan Tahun Ajaran yang masih aktif</p>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
$(document).ready(function () {
    $("#btnAdd").click(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url:'{{url("tahun-ajaran")}}',
            data: {
                level: $("#level").val(),
                keterangan: $("#keterangan").val()
            },
            dataType: 'json',
            success: function (data) {
                alert(data.message);
                window.location.replace("{{url('tahun-ajaran')}}");
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-errors').html('');
                $.each(errors.message, function (key, value) {
                    $('#add-errors').append('<li>' + value + '</li>');
                });
                $("#add-error-bag").show();
            }
        });
    });
});
</script>
@endpush