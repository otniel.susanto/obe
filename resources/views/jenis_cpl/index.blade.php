@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Jenis CPL</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('jenis-cpl')}}">Jenis CPL</a></li>
        </ol>
        <a href="{{url('jenis-cpl/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Nama</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($jenis_cpl as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditJenisCpl({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteJenisCpl({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$jenis_cpl->count()}}</b> out of <b>{{$jenis_cpl->total()}}</b> entries
            </div> {{ $jenis_cpl->links() }}
        </div>
    </div>
    @include('jenis_cpl.edit')
    @include('jenis_cpl.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("jenis-cpl")}}/' + $("#formEditJenisCpl #id_edit").val(),
                data: {
                    nama: $("#formEditJenisCpl #nama").val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditJenisCpl').trigger("reset");
                    $("#formEditJenisCpl .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-jenis-cpl-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-jenis-cpl-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("jenis-cpl")}}/' + $("#formDeleteJenisCpl #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteJenisCpl .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditJenisCpl(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("jenis-cpl")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditJenisCpl #nama").val(data.jenis_cpl.nama);
                $("#formEditJenisCpl #id_edit").val(data.jenis_cpl.id);
                $('#modalEditJenisCpl').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteJenisCpl(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("jenis-cpl")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteJenisCpl #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Jenis cpl: " + data.jenis_cpl.nama + "</p>")
                $("#formDeleteJenisCpl #id_delete").val(data.jenis_cpl.id);
                $('#modalDeleteJenisCpl').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
