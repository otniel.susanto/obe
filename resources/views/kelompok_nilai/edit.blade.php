<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditKelompokNilai">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditKelompokNilai">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Kelompok Nilai</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-kelompok_nilai-errors"> </ul>
                    </div>

                    <label for="nama">Nama</label>
                    <input type="text" id="nama" class="form-control">
                    
                    <label for="persentase">Persentase (%)</label>
                    <input type="number" id="persentase" class="form-control">

                </div>
                <div class="modal-footer">
                    <input id="id_edit" type="hidden" value="0">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i>
                        Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add">
                        <i class="fa fa-marker"></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
