@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Tambah Data Kelompok Nilai Penilaian (Assessment)</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('kelompok-nilai')}}">Kelompok Nilai Penilaian (Assessment)</a></li>
            <li class="breadcrumb-item active"><a href="{{url('kelompok-nilai/create')}}">Tambah Data</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formKelompokNilai">

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="nama">Nama</label>
                    <input type="text" id="nama" class="form-control">
                </div>
                <div class="col-md-2">
                    <label for="persentase">Persentase (%)</label>
                    <input type="number" id="persentase" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-5">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $("#btnAdd").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("kelompok-nilai")}}',
                data: {
                    nama: $('#nama').val(),
                    persentase: $('#persentase').val(),
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('kelompok-nilai')}}");
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });
    });

</script>
@endpush
