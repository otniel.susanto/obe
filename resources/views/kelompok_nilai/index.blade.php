@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Kelompok Nilai Penilaian (Assessment)</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('kelompok-nilai')}}">Kelompok Nilai Penilaian (Assessment)</a></li>
        </ol>
        <a href="{{url('kelompok-nilai/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Nama</th>
                    <th width="">Persentase</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kelompok_nilai as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->persentase}}%</td>
                    <td>
                        <a onclick="event.preventDefault();formEditKelompokNilai({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteKelompokNilai({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('kelompok_nilai.edit')
    @include('kelompok_nilai.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("kelompok-nilai")}}/' + $("#id_edit").val(),
                data: {
                    nama: $('#nama').val(),
                    persentase: $('#persentase').val()
                },
                dataType: 'json',
                success: function (data) {
                    alert("Data berhasil diubah");
                    $('#formEditKelompokNilai').trigger("reset");
                    $("#formEditKelompokNilai .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("kelompok-nilai")}}/' + $("#formDeleteKelompokNilai #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    alert("Data berhasil dihapus");
                    $("#formDeleteKelompokNilai .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = errors.message;
                    }
                    $("#delete-error-bag").html('');
                    $("#delete-error-bag").html(stringException);
                    $("#delete-error-bag").show();
                }
            });
        });
    });

    function formEditKelompokNilai(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kelompok-nilai")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#nama").val(data.kelompok_nilai.nama);
                $("#persentase").val(data.kelompok_nilai.persentase);
                $("#id_edit").val(data.kelompok_nilai.id);
                $('#modalEditKelompokNilai').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteKelompokNilai(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kelompok-nilai")}}/' + id + '/edit',
            success: function (data) {
                $("#delete-error-bag").hide();
                $("#delete-data").html("<p>Kelompok Nilai: " + data.kelompok_nilai.nama + "</p>" + 
                "<p>Persentase: " + data.kelompok_nilai.persentase + "%</p>")
                $("#id_delete").val(data.kelompok_nilai.id);
                $('#modalDeleteKelompokNilai').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
