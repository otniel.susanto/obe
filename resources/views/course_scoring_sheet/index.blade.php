@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1 class="mb-1">Course Scoring Sheet</h1>
        <!-- Filter Section -->
        <form action="{{url('course-scoring-sheet')}}">
            <div class="form-group row">
                <!-- Drop Down Program Studi -->
                <!-- <div class="col-md-3">
                    <select id="program_studi_id" name="program_studi_id" class="form-control" placeholder="Pilih Program Studi" required>
                        <option disabled selected></option>
                        //@foreach($program_studi as $item)
                            //<option value="{{$item->id}}" 
                                //@if($item->id == $request->program_studi_id)
                                    //selected
                                //@endif
                            //>{{$item->nama}}</option>
                        //@endforeach
                    </select>
                </div> -->
                <!-- Drop Down Mata Kuliah -->
                <div class="col-md-4">
                    <select id="mata_kuliah_id" name="mata_kuliah_id" class="form-control" placeholder="Pilih Mata Kuliah" required>
                        <option disabled selected></option>
                        @foreach($mata_kuliah as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->mata_kuliah_id)
                                    selected
                                @endif
                            >{{$item->kode}} - {{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- Drop Down Tahun Ajaran -->
                <div class="col-md-3">
                    <select id="tahun_ajaran_id" name="tahun_ajaran_id" class="form-control" placeholder="Pilih Tahun Ajaran" required>
                        <option disabled selected></option>
                        @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}"
                                @if($item->id == $request->tahun_ajaran_id)
                                    selected
                                @endif
                            >{{$item->keterangan}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <input name="kp" type="search" class="form-control" placeholder="Kelas Paralel (KP)" value="{{$request->kp}}">
                    <span class="text-secondary">Gunakan koma (,) untuk mencari lebih dari 1 Kelas Paralel</span>
                </div>
                <div class="col-md-2">
                <button class="btn btn-primary" type="submit" id="btnCari"><i class="fa fa-search"></i> Cari</button>
                    <button class="btn btn-secondary" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @if(!isset($data) || $data->count() <=0)
        <h5>Belum ada data, silakan mencari data lainnya</h5>
        @else
        <!-- Keterangan SCS -->
        <div>
            <!-- Keterangan Mata Kuliah -->
            <h5>Kode: {{$data[0]->mataKuliah->kode}} - {{$data[0]->mataKuliah->nama}}</h5>
            <h5>SKS: {{$data[0]->mataKuliah->sks}}</span></h5>
            <h5>Tahun Ajaran: {{$data[0]->tahunAjaran->keterangan}}</h5>
            <br>
            <!-- CPMK -->
            @foreach($data[0]->rps->cpmk as $itemCpmk)
            <h6>{{$itemCpmk->kode}} : {{$itemCpmk->keterangan}}</h6>
            @endforeach
            <br>
            <!-- Penilaian (Assessment) -->
            <h6>Penilaian (Assessment):</h6>
            @foreach($kelompok_nilai as $key=>$item)
                <h6>
                    {{$item->nama}} =
                    @php $i = 0 @endphp
                    @foreach($data[0]->assessment->where('kelompok', $item->nama) as $itemAssessment)
                        @if($i > 0) + @endif
                        {{$itemAssessment->persentase}}% {{$itemAssessment->aktivitas}}
                        @php $i++ @endphp
                    @endforeach
                </h6>
            @endforeach
            <span>
                <form action="{{url('course-scoring-sheet-export')}}" method="get">
                    <input type="hidden" name="program_studi_id" value="{{$request->program_studi_id}}">
                    <input type="hidden" name="mata_kuliah_id" value="{{$request->mata_kuliah_id}}">
                    <input type="hidden" name="tahun_ajaran_id" value="{{$request->tahun_ajaran_id}}">
                    <input type="hidden" name="kp" value="{{$request->kp}}">
                    <button type="submit" class="btn btn-success float-sm-right mb-1">
                    <i class="fa fa-file-excel"></i>
                    Export ke File Excel</button>
                </form>
            </span>
        </div>
        <div class='table-responsive'>
            <table class="table myTable table-bordered table-hover" id="tableCourseScoringSheet">
                <thead>
                    <!-- ROW PERTAMA - ASSESSMENT (aktivitas) -->
                    <tr>
                        <th rowspan='6' class='align-middle'>No.</th>
                        <th colspan='3' class='text-center'>Penilaian (Assessment)</th>
                        @foreach($data[0]->assessment as $item)
                            <th colspan='{{$item->isiSoal->count()+1}}' class='text-center'>{{$item->aktivitas}}</th>
                        @endforeach
                    </tr>
                    <!-- ROW KEDUA - WEIGHT (persentase) -->
                    <tr>
                        <th colspan='3' class='text-center'>Weight</th>
                        @foreach($data[0]->assessment as $item)
                            <th colspan='{{$item->isiSoal->count()+1}}' class='text-center'>{{$item->persentase}}%</th>
                        @endforeach
                    </tr>
                    <!-- ROW KETIGA - Question Number (nomor soal) -->
                    <tr>
                        <th colspan='3' class='text-center'>Nomor Soal</th>
                        @foreach($data[0]->assessment as $item)
                            @foreach($item->isiSoal as $itemSoal)
                                <th class='text-center'>{{$itemSoal->nomor_soal}}</th>
                            @endforeach
                            <th rowspan="3" class="text-center align-middle">Total</th>
                        @endforeach
                    </tr>
                    <!-- ROW KEEMPAT - LO (CPMK) -->
                    <tr>
                        <th colspan='3' class='text-center align-middle'>CPMK</th>
                        @foreach($data[0]->assessment as $item)
                            @foreach($item->isiSoal as $itemSoal)
                                @php
                                    $cpmkString = "";
                                @endphp
                                @foreach($itemSoal->cpmk as $key=>$itemCpmk)
                                    @if($key==0)
                                        @php
                                            $cpmkString .= $itemCpmk->kode;
                                        @endphp
                                    @else
                                        @php
                                            $cpmkString .= ', ' . $itemCpmk->kode;
                                        @endphp
                                    @endif
                                @endforeach
                                <th class="text-center align-middle">{{$cpmkString}}</th>
                            @endforeach
                        @endforeach
                    </tr>
                    <!-- ROW KELIMA - Num of LO (jumlah cpmk) -->
                    <tr>
                        <th colspan='3' class='text-center'>Jumlah CPMK</th>
                        @foreach($data[0]->assessment as $item)
                            @foreach($item->isiSoal as $itemSoal)
                                <th class="text-center">{{$itemSoal->cpmk->count()}}</th>
                            @endforeach
                        @endforeach
                    </tr>
                    <!-- ROW KEENAM - Max Point (Skor maksimal per nomor) -->
                    <tr>
                        <th colspan='3' class='text-center'>Skor Maksimal</th>
                        @foreach($data[0]->assessment as $item)
                            @php
                                $totalSkorAssessment = 0;
                            @endphp
                            @foreach($item->isiSoal as $itemSoal)
                                <th class="text-center">{{$itemSoal->skor_maks}}</th>
                                @php
                                    $totalSkorAssessment += $itemSoal->skor_maks;
                                @endphp
                            @endforeach
                            <th class="text-center">{{$totalSkorAssessment}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $itemKelas)
                        @foreach($itemKelas->mahasiswa as $key=>$itemMahasiswa)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{$itemMahasiswa->nrp}}</td>
                            <td class="text-center">{{$itemMahasiswa->nama}}</td>
                            <td class="text-center">{{$itemKelas->kp}}</td>
                            @foreach($itemKelas->assessment as $itemAssessment)
                                @foreach($itemAssessment->isiSoal as $itemIsiSoal)
                                    <td class="text-center">{{$itemIsiSoal->nilaiMahasiswa->firstWhere('mahasiswa_id', $itemMahasiswa->id) ?
                                        $itemIsiSoal->nilaiMahasiswa->firstWhere('mahasiswa_id', $itemMahasiswa->id)->pivot->nilai : 'N/A'}}</td>
                                @endforeach
                                <td class="text-center">
                                    {{$itemIsiSoal->nilaiMahasiswa->firstWhere('mahasiswa_id', $itemMahasiswa->id)->nilai ?? 'N/A'}}
                                </td>
                            @endforeach
                        </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</section>
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        //Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi",
        });
        $('#mata_kuliah_id').select2({
            placeholder: "Pilih Mata Kuliah",
        });
        $('#tahun_ajaran_id').select2({
            placeholder: "Pilih Tahun Ajaran",
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('course-scoring-sheet')}}");
        });
        //Filter Section End
    });
</script>
@endpush
