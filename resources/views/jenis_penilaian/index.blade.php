@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Jenis penilaian</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('jenis-penilaian')}}">Jenis Penilaian</a></li>
        </ol>
        <a href="{{url('jenis-penilaian/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Nama</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($jenis_penilaian as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditJenisPenilaian({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteJenisPenilaian({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$jenis_penilaian->count()}}</b> out of <b>{{$jenis_penilaian->total()}}</b> entries
            </div> {{ $jenis_penilaian->links() }}
        </div>
    </div>
    @include('jenis_penilaian.edit')
    @include('jenis_penilaian.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("jenis-penilaian")}}/' + $("#formEditJenisPenilaian #id_edit").val(),
                data: {
                    nama: $("#formEditJenisPenilaian #nama").val(),
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $('#formEditJenisPenilaian').trigger("reset");
                    $("#formEditJenisPenilaian .close").click();
                    if(data.error == false){
                        window.location.reload();
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-jenis-penilaian-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-jenis-penilaian-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("jenis-penilaian")}}/' + $("#formDeleteJenisPenilaian #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formDeleteJenisPenilaian .close").click();
                    if(data.error == false){
                        window.location.reload();
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditJenisPenilaian(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("jenis-penilaian")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditJenisPenilaian #nama").val(data.jenis_penilaian.nama);
                $("#formEditJenisPenilaian #id_edit").val(data.jenis_penilaian.id);
                $('#modalEditJenisPenilaian').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteJenisPenilaian(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("jenis-penilaian")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteJenisPenilaian #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Jenis penilaian: " + data.jenis_penilaian.nama + "</p>")
                $("#formDeleteJenisPenilaian #id_delete").val(data.jenis_penilaian.id);
                $('#modalDeleteJenisPenilaian').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
