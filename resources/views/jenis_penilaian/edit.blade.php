<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditJenisPenilaian">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditJenisPenilaian">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Jenis Penilaian </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> 
                        × 
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-jenis-penilaian-errors"> </ul>
                    </div>
                    <div class="form-group"> 
                        <label> Nama </label> 
                        <input class="form-control" id="nama" required="" type="text">
                    </div>
                </div>
                <div class="modal-footer"> 
                    <input id="id_edit" type="hidden" value="0"> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button> 
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add"> 
                        Update 
                    </button> 
                </div>
            </form>
        </div>
    </div>
</div>
