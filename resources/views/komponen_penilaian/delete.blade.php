<!-- Delete Modal Form HTML -->
<div class="modal fade" id="modalDeleteKomponenPenilaian">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteKomponenPenilaian">
                <div class="modal-header">
                    <h4 class="modal-title" id="delete-title-komponen-penilaian" name="title"> Delete Komponen Penilaian </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="delete-data-komponen-penilaian"></div>
                    <p class="text-warning"> <small> Data akan terhapus selamanya. </small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="id-delete-komponen-penilaian" type="hidden" value=""> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDeleteKomponenPenilaian" type="button">
                        <i class="fa fa-trash"></i>
                        Hapus
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
