@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
                <h1>Komponen Penilaian</h1>
            </div>
            <div class="col-sm-6">
                <a href="{{url('komponen-penilaian/create')}}" type="button" class="btn btn-primary float-sm-right">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Jenis Penilaian</th>
                    <th width="">Judul</th>
                    <th width="">Persentase</th>
                    <th width="">Kelompok</th>
                    <th width="">Keterangan</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($komponen_penilaian as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->jenisPenilaian->nama}}</td>
                    <td>{{$item->judul}}</td>
                    <td>{{$item->persentase}}%</td>
                    <td>{{$item->kelompok}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditKomponenPenilaian({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteKomponenPenilaian({{$item->id}});" class="delete"
                            data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$komponen_penilaian->count()}}</b> out of
                <b>{{$komponen_penilaian->total()}}</b> entries
            </div> {{ $komponen_penilaian->links() }}
        </div>
    </div>
    @include('komponen_penilaian.edit')
    @include('komponen_penilaian.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("komponen-penilaian")}}/' + $("#formEditKomponenPenilaian #id_edit").val(),
                data: {
                    jenis_penilaian_id: $('#jenis_penilaian_id').val(),
                    persentase: $('#persentase').val(),
                    judul: $('#judul').val(),
                    keterangan: $('#keterangan').val(),
                    kelompok: ($('#kelompok-nts').is(':checked') ? 'NTS' : 'NAS')
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditKomponenPenilaian').trigger("reset");
                    $("#formEditKomponenPenilaian .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-komponen-penilaian-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-komponen-penilaian-errors').append('<li>' + value +
                            '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("komponen-penilaian")}}/' + $("#formDeleteKomponenPenilaian #id_delete")
                .val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteKomponenPenilaian .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditKomponenPenilaian(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("komponen-penilaian")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditKomponenPenilaian #jenis_penilaian_id").val(data.komponen_penilaian
                    .jenis_penilaian_id);
                $("#formEditKomponenPenilaian #persentase").val(data.komponen_penilaian.persentase);
                $("#formEditKomponenPenilaian #judul").val(data.komponen_penilaian.judul);
                $("#formEditKomponenPenilaian #keterangan").val(data.komponen_penilaian.keterangan);
                $("#formEditKomponenPenilaian #kelompok").val(data.komponen_penilaian.kelompok);
                $("#formEditKomponenPenilaian #id_edit").val(data.komponen_penilaian.id);
                $('#modalEditKomponenPenilaian').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteKomponenPenilaian(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("komponen-penilaian")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteKomponenPenilaian #delete-title").html(
                    "Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Komponen Penilaian: " + data.komponen_penilaian.judul + "</p>" +
                    "<p>Persentase: " + data.komponen_penilaian.persentase + "%</p>" +
                    "<p>Kelompok: " + data.komponen_penilaian.kelompok + "</p>" +
                    "<p>Keterangan: " + data.komponen_penilaian.keterangan + "</p>" )
                $("#formDeleteKomponenPenilaian #id_delete").val(data.komponen_penilaian.id);
                $('#modalDeleteKomponenPenilaian').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
