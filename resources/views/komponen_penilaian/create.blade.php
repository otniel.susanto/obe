<!-- Create Modal HTML -->
<div class="modal fade" id="modalCreateKomponenPenilaian">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formCreateKomponenPenilaian">
                <div class="modal-header">
                    <h4 class="modal-title">Komponen Penilaian</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="create-komponen-penilaian-error-bag">
                        <ul id="create-komponen-penilaian-errors"> </ul>
                    </div>
                    <label for="add-jenis_penilaian_id">Jenis Penilaian</label>
                    <select id="add-jenis_penilaian_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($jenis_penilaian as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                    <label for="add-aktivitas">Nama Komponen Penilaian</label>
                    <input required type="text" class="form-control" id="add-aktivitas" placeholder="Tugas 1, Kuis 1">

                    <div><label for="add-persentase">Persentase dari NTS/NAS (%)</label></div>
                    <div class="text-secondary mt-n2">
                    Contoh pengisian: persentase Tugas pada NTS adalah 20%<br>
                    Maka, masukkan angka 20 pada kotak di bawah ini</div>
                    <input required type="number" min="0" max="100" class="form-control" id="add-persentase" placeholder="0-100">

                    <label for="add-kelompok">Kelompok Nilai</label>
                    @foreach($kelompok_nilai as $key=>$item)
                        <div class="form-check">
                            <input class="form-check-input" type="radio" data-name="{{$item->nama}}" name="add-kelompok" id="add-kelompok-{{$item->nama}}" value="{{$item->id}}"
                                @if($key==0) checked @endif>
                            <label class="form-check-label" for="add-kelompok-{{$item->nama}}">
                                {{$item->nama}} - {{$item->persentase}}%
                            </label>
                        </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button>
                    <button class="btn btn-info" id="btnTambahKomponenPenilaian" type="button">
                        <i class="fa fa-plus"></i>
                        Tambah
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
