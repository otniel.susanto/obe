@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Kelas Paralel</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('kelas')}}">Kelas Paralel</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('kelas/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formKelas">
            <div class="form-group border p-2">
                <h6>RPS yang akan digunakan</h6>
                <div class="alert alert-danger d-none" id="rps-error-bag">

                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="program_studi_id">Program Studi</label>
                        <select id="program_studi_id" class="select2 form-control">
                            <option disabled selected></option>
                            @foreach($program_studi as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="mata_kuliah_id">Mata Kuliah</label>
                        <select id="mata_kuliah_id" class="select2 form-control">
                            <option disabled selected></option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="data_rps">Preview</label><br>
                        <input type="hidden" id="rps_id" value="">
                        <a target="_blank" rel="noopener noreferrer" class="btn btn-secondary" id="btnPreview" disabled>
                            <i class="fa fa-search"></i>
                            Lihat Data RPS
                        </a>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="tahun_ajaran_id">Tahun Ajaran</label>
                    <input type="hidden" name="tahun_ajaran_id" id="tahun_ajaran_id" value="{{$tahun_ajaran->id}}">
                    <input type="text" class="form-control" readonly value="{{$tahun_ajaran->keterangan}}">
                </div>
                <div class="col-md-5">
                    <label for="file">Upload File Mahasiswa Peserta Kelas Paralel (Optional)</label>
                    <input type="file" name="file" id="file">
                </div>
            </div>

            <div class="row mb-1">
                <div class="col-auto">
                    <h5>Kelas Paralel (KP)</h5>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-2">
                    <input required type="string" class="form-control" name="kp" id="kp"
                        placeholder="Masukkan kode KP">
                </div>
                <div class="col-auto float-right">
                    <button type="button" id="btnTambahKelasParalel" class="btn btn-primary float-right">
                        <i class="fa fa-plus"></i>
                        Tambah Kelas Paralel
                    </button>
                </div>
            </div>

            <div class="row mb-1">
                <div class="col-4">
                    <table width="100% data-toggle="  class="table myTable table-hover" id="tableKelasParalel">
                        <tr>
                            <th width="20%">KP</th>
                            <th width="20%">Delete</th>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-4">
                    <button type="button" class="btn btn-secondary float-right" id="btnAdd">
                        <i class="fa fa-save"></i> 
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    var kelas = [];
    var i = 0;
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $('#mata_kuliah_id').select2({
            placeholder: "Pilih Mata Kuliah"
        });
        $("#program_studi_id").change(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("get-mata_kuliah")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    $("#mata_kuliah_id").html('');
                    var stringAppend = '<option disabled selected></option>';
                    $.each(data.mata_kuliah, function (key, value) {
                        stringAppend += '<option value="' + value.id + '">' +
                            value.kode + ' - ' + value.nama + '</option>';
                    });
                    $("#mata_kuliah_id").append(stringAppend);
                    $("#mata_kuliah_id").change();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#mata_kuliah_id").change(function () {
            $.ajax({
                method: 'GET',
                url:'{{url("kelas-rps")}}',
                data: {
                    program_studi_id: $('#program_studi_id').val(),
                    mata_kuliah_id: $('#mata_kuliah_id').val()
                },
                dataType: 'json',
                success: function (data) {
                    if($('#mata_kuliah_id').val() != null){
                        $("#rps-error-bag").html('');
                        if(data.error == false){
                            $("#btnPreview").removeClass("btn-secondary");
                            $("#btnPreview").addClass("btn-primary");
                            $("#btnPreview").attr("href", "{{url('rps')}}" + '/' + data.rps.id);
                            $("#btnAdd").prop("disabled", false);
                            $("#btnAdd").removeClass("btn-secondary");
                            $("#btnAdd").addClass("btn-primary");
                            $("#rps_id").val(data.rps.id);
                            $("#rps-error-bag").hide();
                        }else{
                            $("#btnPreview").removeClass("btn-primary");
                            $("#btnPreview").addClass("btn-secondary");
                            $("#btnAdd").removeClass("btn-primary");
                            $("#btnAdd").addClass("btn-secondary");
                            $("#btnPreview").removeAttr("href");
                            $("#btnAdd").prop("disabled", true);
                            $("#rps_id").val('');
                            $("#rps-error-bag").html('Data RPS tidak ditemukan');
                            $("#rps-error-bag").show();
                        }
                        $("#rps-error-bag").removeClass("d-none");
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#btnAdd").click(function () {
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            if(files == null){
                if(!confirm("File Excel / CSV untuk memasukkan Mahasiswa peserta Kelas Paralel belum ada\n" + 
                "Bila tidak diupload, anda harus memasukkan data peserta Kelas Paralel satu per satu.\n" + 
                "Apakah anda yakin untuk melanjutkan?")){
                    return false;
                }
            }
            if(kelas.length <= 0){
                alert("Masukkan kode Kelas Paralel terlebih dahulu!");
                return false;
            }
            formData.append('file', files);
            formData.append('rps_id', $('#rps_id').val());
            formData.append('tahun_ajaran_id', $('#tahun_ajaran_id').val());
            formData.append('kp', JSON.stringify(kelas));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("kelas")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('kelas')}}");
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });
        
        $("#btnTambahKelasParalel").click(function () {
            var duplicate = false;
            if($('#kp').val() != ""){
                $.each(kelas, function (key, value) {
                    if(value.kp == $('#kp').val()){
                        duplicate = true;
                    }
                });
                if(duplicate){
                    alert("Kode sudah ada");
                }
                else{
                    kelas.push({
                        index: ++i,
                        kp: $('#kp').val()
                    });
                    $('#tableKelasParalel tr:last').after('<tr id="' + i + '">' +
                        '<td>' + $('#kp').val() + '</td>' +
                        '<td><a onclick="event.preventDefault();deleteKelasParalel(' +
                        i +
                        ');" data-toggle="modal">' +
                        '<i class="fa fa-trash"></i></a></td>' +
                        '</tr>'
                    );
                    $('#kp').val('');
                }
            }
            else{
                alert("Masukkan kode terlebih dahulu")
            }
            $('#kp').focus();
        });
        $('#kp').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                $('#btnTambahKelasParalel').click();
                return false;  
            }
        }); 
    });

    function deleteKelasParalel(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#' + id).remove();
            var index = kelas.findIndex(function (item) {
                return item.index == id;
            });
            kelas.splice(index, 1);
        }
    }
</script>
@endpush
