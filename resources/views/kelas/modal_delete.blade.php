<!-- Delete Task Modal Form HTML -->
<div class="modal fade" id="modalDeleteKelas">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteKelas">
                <div class="modal-header">
                    <h4 class="modal-title" name="title">Hapus Kelas Paralel </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="delete-data-kelas"></div>
                    <p class="text-warning"> <small> Data akan terhapus selamanya. </small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="id-delete-kelas" type="hidden" value=""> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDeleteKelas" type="button">
                        <i class="fa fa-trash"></i>
                        Hapus 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
