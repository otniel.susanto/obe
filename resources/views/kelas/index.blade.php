@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Kelas Paralel</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('kelas')}}">Kelas Paralel</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Filter Section -->
        <form action="{{url('kelas')}}" method="get">
            <div class="row">
                <div class="col-md-2">
                    <select name="tahun_ajaran_id" id="tahun_ajaran_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($tahun_ajaran as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->tahun_ajaran_id) selected @endif
                        >{{$item->keterangan}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="program_studi_id" id="program_studi_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->program_studi_id) selected @endif
                        >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <input name="kode_mata_kuliah" type="search" class="form-control" placeholder="Kode MK" value="{{$request->kode_mata_kuliah}}">
                </div>
                <div class="col-md-3">
                    <input name="nama_mata_kuliah" type="search" class="form-control" placeholder="Nama MK" value="{{$request->nama_mata_kuliah}}">
                </div>
                <div class="col-md-2">
                    <input type="search" name="kp" class="form-control" placeholder="KP" value="{{$request->kp}}">
                </div>
            </div>
            <div class="row mt-1 mb-1">
                <div class="col-md-8">
                    <a href="{{url('kelas/create')}}" type="button" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        Tambah Data
                    </a>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-secondary float-right" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                    <button class="btn btn-primary float-right mr-1" type="submit" id="btnFilter"><i class="fa fa-search"></i> Cari</button>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
        <table width="100%" data-toggle="table" data-pagination="true" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th >No.</th>
                    <th >Tahun Ajaran</th>
                    <th >Program Studi</th>
                    <th >Mata Kuliah</th>
                    <th >KP</th>
                    <th >Info</th>
                    <th >Edit</th>
                    <th >Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kelas as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->tahunAjaran->keterangan}}</a></td>
                    <td>{{$item->programStudi->nama}}</a></td>
                    <td><a href="{{url('kelas')}}/{{$item->id}}">{{$item->mataKuliah->kode}} - {{$item->mataKuliah->nama}}</a></td>
                    <td>{{$item->kp}}</td>
                    <td>
                        @if(!isset($item->mahasiswa[0]->pivot))
                            <span class="bg-warning">
                            Tambahkan data mahasiswa peserta kelas paralel</span><br>
                            <a href="{{url('kelas')}}/{{$item->id}}">di sini</a>
                        @else
                            <span class='bg-success'>Data mahasiswa kelas paralel telah terisi</span>
                        @endif
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formEditKelas({{$item->id}});" class="edit open-modal"
                            data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteKelas({{$item->id}});" class="delete"
                            data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('kelas.edit')
    @include('kelas.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        // Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi"
        });
        $('#tahun_ajaran_id').select2({
            allowClear: true,
            placeholder: "Tahun Ajaran"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('kelas')}}");
        });
        //Filter Section End

        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("kelas")}}/' + $("#formEditKelas #id_edit").val(),
                data: {
                    kp: $("#kp").val(),
                    mata_kuliah_id: $('#edit-mata_kuliah_id').val(),
                    tahun_ajaran_id: $('#edit-tahun_ajaran_id').val()
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditKelas').trigger("reset");
                    $("#formEditKelas .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-kelas-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-kelas-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("kelas")}}/' + $("#id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formDeleteKelas .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditKelas(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kelas")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#edit-kp").val(data.kelas.kp);
                $("#edit-program_studi_id").val(data.kelas.program_studi_id);
                $("#edit-tahun_ajaran_id").val(data.kelas.tahun_ajaran_id);
                $("#edit-mata_kuliah_id").val(data.kelas.mata_kuliah_id);
                $("#edit-program_studi_text").val(data.program_studi.nama);
                $("#edit-mata_kuliah_text").val(data.mata_kuliah.kode + ' - ' + data.mata_kuliah.nama);
                $("#edit-tahun_ajaran_text").val(data.tahun_ajaran.keterangan);
                $("#id_edit").val(data.kelas.id);
                $('#modalEditKelas').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteKelas(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("kelas")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteKelas #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Kelas Paralel: " + data.kelas.kp + "</p>" +
                    "<p>Program Studi: " + data.program_studi.nama + "</p>" +
                    "<p>Mata kuliah: " + data.mata_kuliah.nama + "</p>" +
                    "<p>Tahun ajaran: " + data.tahun_ajaran.keterangan + "</p>")
                $("#formDeleteKelas #id_delete").val(data.kelas.id);
                $('#modalDeleteKelas').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
