<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditKelas">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditKelas">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Kelas Paralel</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-kelas-errors"> </ul>
                    </div>
                    <label for="edit-program_studi_id">Program Studi</label>
                    <input type="hidden" id="edit-program_studi_id" value="">
                    <input class="form-control" type="text" id="edit-program_studi_text" value="" disabled>

                    <label for="edit-mata_kuliah_id">Mata Kuliah</label>
                    <input type="hidden" id="edit-mata_kuliah_id" value="">
                    <input class="form-control" type="text" id="edit-mata_kuliah_text" value="" disabled>
                    
                    <label for="edit-tahun_ajaran_id">Tahun Ajaran</label>
                    <input type="hidden" id="edit-tahun_ajaran_id" value="">
                    <input class="form-control" type="text" id="edit-tahun_ajaran_text" value="" disabled>

                    <label for="edit-kp">Kelas Paralel</label>
                    <input required type="string" class="form-control" name="edit-kp" id="edit-kp" placeholder="Masukkan Kelas Paralel">

                </div>
                <div class="modal-footer">
                    <input id="id-edit-kelas" type="hidden" value="0">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdate" type="button">
                        <i class="fa fa-marker"></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
