<!-- Delete Task Modal Form HTML -->
<div class="modal fade" id="modalDeleteKelas">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteKelas">
                <div class="modal-header">
                    <h4 class="modal-title" id="delete-title" name="title">Apakah anda yakin menghapus data kelas ini?</h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="delete-data"></div>
                    <p class="text-warning"> <small> Data akan terhapus selamanya. </small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="id_delete" type="hidden" value=""> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDelete" type="button">
                        <i class="fa fa-trash"></i>
                        Hapus
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
