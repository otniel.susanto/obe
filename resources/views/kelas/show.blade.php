@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-sm-6">
                <h1>Data Kelas</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('kelas')}}">Kelas</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('kelas')}}/{{$kelas->id}}">Show</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <h4 class="">Kode Mata Kuliah</h4>
            </div>
            <div class="col">
                <h4>:{{$kelas->mataKuliah->kode}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h4 class="">Nama Mata Kuliah</h4>
            </div>
            <div class="col">
                <h4>:{{$kelas->mataKuliah->nama}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h4 class="">Kelas Paralel</h4>
            </div>
            <div class="col-md-3">
                <h4>:{{$kelas->kp}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h4 class="">Tahun Ajaran</h4>
            </div>
            <div class="col">
                <h4>:{{$kelas->tahunAjaran->keterangan}}</h4>
            </div>
        </div>

        <div class="row mb-1 mt-5">
            <div class="col-1">
                <h3>Mahasiswa</h3>
            </div>
            <div class="col-9">
                <button onclick="event.preventDefault();formAddMahasiswa();"
                    class="btn btn-primary float-right open-modal mr-1" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Tambah Mahasiswa
                </button>
                <button onclick="event.preventDefault();formDeleteAllMahasiswa();"
                    class="btn btn-danger float-right open-modal mr-1" data-toggle="modal">
                    <i class="fa fa-trash"></i>
                    Delete Semua Mahasiswa
                </button>
                <button onclick="event.preventDefault();formImportMahasiswa();" data-toggle="modal" type="button"
                    class="btn btn-success float-right mr-1">
                    <i class="fa fa-upload"></i>
                    Import Excel
                </button>
                <!-- <a href="{{url('mahasiswa-template')}}" type="button" class="btn btn-info float-right mr-1">
                    <i class="fa fa-download"></i>
                    Download Template
                </a> -->
            </div>
        </div>

        <div class="row mb-1">
            <div class="col-10">
                <table width="100% data-toggle=" class="table myTable table-hover" id="tableMahasiswa">
                    <tr>
                        <th width="10%">No</th>
                        <th width="15%">NRP</th>
                        <th width="40%">Nama</th>
                        <th width="15%">Delete</th>
                    </tr>
                        @foreach($kelas->mahasiswa as $key=>$itemDetail)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$itemDetail->nrp}}</td>
                                <td>{{$itemDetail->nama}}</td>
                                <td>
                                    <a onclick="event.preventDefault();formDeleteMahasiswa({{$itemDetail->id}});" class="delete"
                                        data-toggle="modal">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
    @include('mahasiswa.import')
    @include('mahasiswa.modal_add')
    @include('mahasiswa.delete')
    @include('kelas.delete_all')
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(document).ready(function () {
        // alert(jQuery.fn.jquery);
        $('.select2').select2();

        $('#btnTambahMahasiswa').click(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("kelas-mahasiswa")}}',
                data: {
                    mahasiswa_id: $('#add-mahasiswa_id').val(),
                    kelas_id: {{$kelas->id}}
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formAddMahasiswa .close").click();
                    window.location.reload('{{url("kelas")}}/{{$kelas->id}}');
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#add-errors').html('');
                    console.log('errors.message');
                    $.each(errors.message, function (key, value) {
                        $('#add-errors').append('<li>' + value + '</li>');
                    });
                    $("#add-error-bag").show();
                }
            });
        });

        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("kelas-mahasiswa")}}',
                data:{
                    mahasiswa_id:  $("#formDeleteMahasiswa #id_delete").val(),
                    kelas_id: {{$kelas->id}},
                },
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteMahasiswa .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        
        $("#btnImport").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);
            formData.append('kelas_id', {{$kelas->id}});

            $.ajax({
                url:'{{url("kelas-mahasiswa/import")}}',
                method: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#formImportMahasiswa').trigger("reset");
                    $("#formImportMahasiswa .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    $("#import-error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        stringException = "Import gagal. Format file yang diterima: .csv, .xls, atau .xlsx";
                    }
                    else{
                        stringException = errors.message;
                    }
                    $("#import-error-bag").append(stringException);
                    $("#import-error-bag").show();
                }                
            });
        });
        
        $("#btnDeleteAllMahasiswa").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("delete-all-mahasiswa")}}',
                data:{
                    kelas_id: {{$kelas->id}},
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formDeleteAllMahasiswa .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });

    });

    function formAddMahasiswa(id) {
        $("#create-error-bag").hide();
        $('#modalAddMahasiswa').modal('show');
    }

    function formDeleteMahasiswa(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("mahasiswa")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteMahasiswa #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p> Mahasiswa </p>" + 
                "<p>NRP: " + data.mahasiswa.nrp + "</p>" +
                "<p>Nama: " + data.mahasiswa.nama + "</p>")
                $("#formDeleteMahasiswa #id_delete").val(data.mahasiswa.id);
                $('#modalDeleteMahasiswa').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteAllMahasiswa(id) {
        $('#modalDeleteAllMahasiswa').modal('show');
    }

    function formImportMahasiswa() {
        $("#import-error-bag").hide();
        $('#modalImportMahasiswa').modal('show');
    }

</script>
@endpush
