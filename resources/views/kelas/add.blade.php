<!-- Add Modal HTML -->
<div class="modal fade" id="modalAddKelasParalel">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formAddKelasParalel">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah CPMK</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="create-error-bag">
                        <ul id="create-isi_soal-errors"> </ul>
                    </div>

                    <label for="kp">Kode KP</label>
                    <input required type="string" class="form-control" name="kp" id="kp" placeholder="-, A, B, E1">

                </div>
                <div class="modal-footer">
                    <input onclick="event.preventDefault();" class="btn btn-default" data-dismiss="modal" type="button" value="Cancel">
                    <button onclick="event.preventDefault();closeModalAddKelasParalel()" class="btn btn-info" id="btnTambahKelasParalel">
                        Tambah
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
