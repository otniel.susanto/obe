<!-- Edit Modal HTML -->
<div class="modal fade" id="modalAddMahasiswa">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formAddMahasiswa">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Mahasiswa</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="create-error-bag">
                        <ul id="create-isi_soal-errors"> </ul>
                    </div>

                    <label for="add-nrpMahasiswa">Pilih Mahasiswa</label>
                    <select id="add-mahasiswa_id" class="select2 form-control">
                        @foreach($mahasiswa as $item)
                        <option value="{{$item->id}}">{{$item->nrp}} - {{$item->nama}}</option>
                        @endforeach
                    </select>

                </div>
                <div class="modal-footer">
                    <button onclick="event.preventDefault();" class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i>
                        Cancel
                    </button>
                    <button class="btn btn-info" id="btnTambahMahasiswa">
                        <i class="fa fa-plus"></i>
                        Tambah
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
