<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditMahasiswa">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditMahasiswa">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Mahasiswa </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-mahasiswa-errors"> </ul>
                    </div>
                    
                    <label for="edit-nrp">NRP</label>
                    <input required type="text" class="form-control" id="edit-nrp">

                    <label for="edit-nama">Nama</label>
                    <input type="text" class="form-control" id="edit-nama">
                    
                    <label for="edit-program_studi_id">Program Studi</label>
                    <select id="edit-program_studi_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                </div>
                <div class="modal-footer">
                    <input id="id_edit-mahasiswa" type="hidden" value="0">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
