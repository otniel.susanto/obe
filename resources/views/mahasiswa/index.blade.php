@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Data Mahasiswa</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('mahasiswa')}}">Mahasiswa</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Filter Section -->
        <form action="{{url('mahasiswa')}}" method="get">
            <div class="row">
                <div class="col-md-2">
                    <input name="nrp_mahasiswa" type="search" class="form-control" placeholder="NRP" value="{{$request->nrp_mahasiswa}}">
                </div>
                <div class="col-md-4">
                    <input name="nama_mahasiswa" type="search" class="form-control" placeholder="Nama Mahasiswa" value="{{$request->nama_mahasiswa}}">
                </div>
                <div class="col-md-4">
                    <select name="program_studi_id" id="program_studi_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->program_studi_id) selected @endif
                        >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <input name="tahun_masuk" type="search" class="form-control" placeholder="Tahun Masuk" value="{{$request->tahun_masuk}}">
                </div>
            </div>
            <div class="row mt-1 mb-1">
                <div class="col-md-8">
                    <a href="{{url('mahasiswa/create')}}" type="button" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        Tambah Manual Mahasiswa
                    </a>
                    <button onclick="event.preventDefault();formImportMahasiswa();" data-toggle="modal" type="button"
                        class="btn btn-success mr-1">
                        <i class="fa fa-upload"></i>
                        Import Excel
                    </button>
                    <!-- <a href="{{url('mahasiswa-template')}}" type="button" class="btn btn-info mr-1">
                        <i class="fa fa-download"></i>
                        Download Template
                    </a> -->
                </div>
                <div class="col-md-4">
                    <button class="btn btn-secondary float-right" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                    <button class="btn btn-primary float-right mr-1" type="submit" id="btnFilter"><i class="fa fa-search"></i> Cari</button>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
        <table width="100%" data-toggle="table" data-pagination="true" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th data-sortable="true" width="10%">NRP</th>
                    <th data-sortable="true" width="15%">Nama</th>
                    <th data-sortable="true" width="15%">Program Studi</th>
                    <th data-sortable="true" width="15%">Tahun Masuk</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($mahasiswa as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nrp}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->programStudi->nama}}</td>
                    <td>{{$item->tahun_masuk}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditMahasiswa({{$item->id}});" class="edit open-modal"
                            data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteMahasiswa({{$item->id}});" class="delete"
                            data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('mahasiswa.edit')
    @include('mahasiswa.delete')
    @include('mahasiswa.import')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        //Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('mahasiswa')}}");
        });
        //Filter Section End
        $('#edit-program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("mahasiswa")}}/' + $("#id_edit-mahasiswa").val(),
                data: {
                    nrp: $("#edit-nrp").val(),
                    nama: $("#edit-nama").val(),
                    program_studi_id: $('#edit-program_studi_id').val()
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditMahasiswa').trigger("reset");
                    $("#formEditMahasiswa .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-mahasiswa-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-mahasiswa-errors').append('<li>' + value +
                            '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("mahasiswa")}}/' + $("#id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteMahasiswa .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#btnImport").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);

            $.ajax({
                url: '{{url("mahasiswa/import")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#formImportMahasiswa').trigger("reset");
                    $("#formImportMahasiswa .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#import-mahasiswa-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#import-mahasiswa-errors').append('<li>' + value +
                            '</li>');
                    });
                    $("#import-error-bag").show();
                }                
            });
        });
    });

    function formEditMahasiswa(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("mahasiswa")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#edit-nrp").val(data.mahasiswa.nrp);
                $("#edit-nama").val(data.mahasiswa.nama);
                $("#edit-program_studi_id").val(data.mahasiswa.program_studi_id).change();
                $("#id_edit-mahasiswa").val(data.mahasiswa.id);
                $('#modalEditMahasiswa').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteMahasiswa(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("mahasiswa")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteMahasiswa #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>NRP: " + data.mahasiswa.nrp + "</p>" +
                "<p>Nama: " + data.mahasiswa.nama + "</p>" + 
                "<p>Program Studi: " + data.program_studi.nama + "</p>" + 
                "<p>Tahun masuk: " + data.mahasiswa.tahun_masuk + "</p>")
                $("#formDeleteMahasiswa #id_delete").val(data.mahasiswa.id);
                $('#modalDeleteMahasiswa').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formImportMahasiswa() {
        $("#import-error-bag").hide();
        $('#modalImportMahasiswa').modal('show');
    }

</script>
@endpush
