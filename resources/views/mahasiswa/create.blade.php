@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-sm-6">
                <h1>Tambah Data Mahasiswa</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('mahasiswa')}}">Mahasiswa</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('mahasiswa/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAddMahasiswa">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="nrp">NRP</label>
                    <input required type="text" class="form-control" id="nrp" placeholder="Masukkan NRP">
                </div>
                <div class="col-md-3">
                    <label for="program_studi_id">Program Studi</label>
                    <select id="program_studi_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="nama">Nama</label>
                    <input rows="5" required type="text" class="form-control" id="nama" placeholder="Masukkan nama">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd">
                    <i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: "Pilih program studi"
        });
        $("#btnAdd").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("mahasiswa")}}',
                data: {
                    nrp: $("#nrp").val(),
                    nama: $("#nama").val(),
                    program_studi_id: $("#program_studi_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('mahasiswa')}}");
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#add-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#add-errors').append('<li>' + value + '</li>');
                    });
                    $("#add-error-bag").show();
                }
            });
        });
    });

</script>
@endpush
