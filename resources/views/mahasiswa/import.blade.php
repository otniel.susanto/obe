<!-- Import Modal HTML -->
<div class="modal fade" id="modalImportMahasiswa">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formImportMahasiswa" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title"> Import Data Mahasiswa </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="import-error-bag">
                        <ul id="import-mahasiswa-errors"> </ul>
                    </div>
                    
                    <label for="file">Pilih File Excel</label><br>
                    <input type="file" name="file" id="file" required="required">
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button>
                    <button class="btn btn-success" id="btnImport" type="button">
                        <i class="fa fa-upload"></i>
                        Import
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

