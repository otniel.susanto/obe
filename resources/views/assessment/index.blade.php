@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Penilaian (Assessment)</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('assessment')}}">Penilaian (Assessment)</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Filter Section -->
        <form action="{{url('assessment')}}" method="get">
            <div class="row">
                <div class="col-md-3">
                    <select name="program_studi_id" id="program_studi_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->program_studi_id) selected @endif
                        >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="tahun_ajaran_id" id="tahun_ajaran_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($tahun_ajaran as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->tahun_ajaran_id) selected @endif
                        >{{$item->keterangan}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <input name="kode_mata_kuliah" type="search" class="form-control" placeholder="Kode MK" value="{{$request->kode_mata_kuliah}}">
                </div>
                <div class="col-md-3">
                    <input name="nama_mata_kuliah" type="search" class="form-control" placeholder="Nama MK" value="{{$request->nama_mata_kuliah}}">
                </div>
                <div class="col-md-2">
                    <input name="aktivitas" type="search" class="form-control" placeholder="Aktivitas" value="{{$request->aktivitas}}">
                </div>
            </div>
            <div class="row mt-1 mb-1">
                <div class="col-md-8">
                    <a href="{{url('assessment/create')}}" type="button" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        Tambah Data
                    </a>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-secondary float-right " type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                    <button class="btn btn-primary float-right mr-1" type="submit" id="btnFilter"><i class="fa fa-search"></i> Filter</button>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
        <table width="100%" data-toggle="table" data-pagination="true" class="table myTable table-striped table-hover" id="tableAssessment">
            <thead>
                <tr>
                    <th>No.</th>
                    <th data-sortable="true">Program Studi</th>
                    <th data-sortable="true">Tahun Ajaran</th>
                    <th data-sortable="true">Kode MK</th>
                    <th data-sortable="true">Nama MK</th>
                    <th data-sortable="true">KP</th>
                    <th data-sortable="true">Aktivitas</th>
                    <th data-sortable="true">Tanggal</th>
                </tr>
            </thead>
            <tbody>
                @foreach($assessment as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->kelas[0]->programStudi->nama}}</td>
                    <td>{{$item->kelas[0]->tahunAjaran->keterangan}}</td>
                    <td>{{$item->kelas[0]->mataKuliah->kode}}</td>
                    <td><a href="{{url('assessment')}}/{{$item->id}}">{{$item->kelas[0]->mataKuliah->nama}}</a></td>
                    <td>
                        @foreach($item->kelas as $keyDetail=>$item_detail)
                            @if($keyDetail>0)
                                , 
                            @endif
                            {{$item_detail->kp}}
                        @endforeach
                    </td>
                    <td><a href="{{url('assessment')}}/{{$item->id}}">{{$item->aktivitas}}</a></td>
                    <td>{{\Carbon\Carbon::parse($item->tanggal)->isoFormat('D-MMMM-YYYY / HH:mm')}} WIB</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        // Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi"
        });
        $('#tahun_ajaran_id').select2({
            allowClear: true,
            placeholder: "Pilih Tahun Ajaran"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('mata-kuliah')}}");
        });
        //Filter Section End
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("assessment")}}/' + $("#formEditAssessment #id_edit").val(),
                data: {
                    kp: $("#kp").val(),
                    mata_kuliah_id: $('#mata_kuliah_id').val(),
                    tahun_ajaran_id: $('#tahun_ajaran_id').val()
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditAssessment').trigger("reset");
                    $("#formEditAssessment .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-assessment-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-assessment-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("assessment")}}/' + $("#formDeleteAssessment #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteAssessment .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });
</script>
@endpush
