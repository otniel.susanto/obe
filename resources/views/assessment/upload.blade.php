<!-- Upload Modal HTML -->
<div class="modal fade" id="modalUploadSoal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formUploadSoal" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Upload Soal</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="upload-error-bag">
                        <ul id="upload-soal-errors"> </ul>
                    </div>
                    
                    <label for="file">Pilih File Soal</label><br>
                    <input type="file" name="file" id="file" required="required">
                    <p class="text-danger">File Soal lama akan terhapus dari database</p>
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpload" type="button">
                        Upload
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

