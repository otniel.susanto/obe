@extends('index')
@push('head-script')
<link href="https://unpkg.com/bootstrap-table@1.18.1/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.min.css" rel="stylesheet">
<link rel="stylesheet" href="extensions/sticky-header/bootstrap-table-sticky-header.css">
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h1>Data Assessment</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('assessment')}}">Assessment</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('assessment')}}/{{$assessment->id}}">Show</a></li>
                </ol>
            </div>
            <div class="col-md-8">
            <button onclick="event.preventDefault();formImportNilai();" data-toggle="modal" type="button"
                class="btn btn-success float-sm-right m-1">
                <i class="fa fa-upload"></i>
                Import Nilai
            </button>
            <form action="{{url('assessment-template')}}" method="get">
                <input type="hidden" name="id" id="id" value="{{$assessment->id}}">
                <button type="submit" class="btn btn-info float-sm-right m-1">
                <i class="fa fa-download"></i>
                Download Template Penilaian</button>
            </form>
            <a href="{{url('assessment')}}/{{$assessment->id}}/edit" class="btn btn-warning float-sm-right m-1">
                <i class="fa fa-edit"></i>
                Edit Assessment
            </a>
            <button onclick="event.preventDefault();formDeleteAssessment();" data-toggle="modal" type="button"
                class="btn btn-danger float-sm-right m-1">
                <i class="fa fa-trash"></i>
                Hapus Assessment
            </button>
            @if($assessment->nama_file_soal !== null && $assessment->nama_file_soal != "")
                <a href="{{url('download-soal')}}/{{$assessment->id}}" class="btn btn-dark float-sm-right m-1" target="_blank">
                    <i class="fa fa-download"></i>
                    Download Soal
                </a>
            @endif
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Keterangan Assessment -->
        <div class="row">
            <div class="col-md-6">
                <!-- Program Studi -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Program Studi</h6>
                    </div>
                    <div class="col">
                        <h6><u>{{$assessment->kelas[0]->programStudi->nama}}</u></h6>
                    </div>
                </div>
                <!-- Kode Mata Kuliah -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Kode Mata Kuliah</h6>
                    </div>
                    <div class="col">
                        <h6><u>{{$assessment->kelas[0]->mataKuliah->kode}}</u></h6>
                    </div>
                </div>
                <!-- Nama Mata Kuliah -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Nama Mata Kuliah</h6>
                    </div>
                    <div class="col">
                        <h6><u>{{$assessment->kelas[0]->mataKuliah->nama}}</u></h6>
                    </div>
                </div>
                <!-- Kelas Paralel -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Kelas Paralel</h6>
                    </div>
                    <div class="col-md-6">
                        <h6><u>
                            @foreach($assessment->kelas as $key=>$item)
                                @if($key > 0 )
                                    ,
                                @endif
                                {{$item->kp}}
                            @endforeach
                        </u></h6>
                    </div>
                </div>
                <!-- Tahun Ajaran -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Tahun Ajaran</h6>
                    </div>
                    <div class="col">
                        <h6><u>{{$assessment->kelas[0]->tahunAjaran->keterangan}}</u></h6>
                    </div>
                </div>
                <!-- Dosen Penguji -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Dosen Penguji</h6>
                    </div>
                    <div class="col">
                        <h6><u>
                        @foreach($assessment->dosenPenguji as $key=>$itemDosen) 
                            @if($key==0)
                                {{$itemDosen->npk}} - {{$itemDosen->name}}
                            @else
                                , <br> {{$itemDosen->npk}} - {{$itemDosen->name}}
                            @endif
                        @endforeach
                        </u></h6>
                    </div>
                </div>
            </div>
            <!-- Kolom kedua -->
            <div class="col-md-6">
                <!-- Keterangan Assessment Lainnya -->
                <!-- Aktivitas -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Aktivitas</h6>
                    </div>
                    <div class="col-md-8">
                        <h6><u>{{$assessment->aktivitas}}</u></h6>
                    </div>
                </div>
                <!-- Jenis Penilaian -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Jenis Penilaian</h6>
                    </div>
                    <div class="col-md-8">
                        <h6><u>{{$assessment->jenis_penilaian}}</u></h6>
                    </div>
                </div>
                <!-- Persentase dari NA -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Persentase dari NA</h6>
                    </div>
                    <div class="col-md-8 ">
                        <h6><u>{{$assessment->persentase}}%</u></h6>
                    </div>
                </div>
                <!-- Kelompok Nilai -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Kelompok Nilai</h6>
                    </div>
                    <div class="col-md-8 ">
                        <h6><u>{{$assessment->kelompok}}</u></h6>
                    </div>
                </div>
                <!-- Tanggal -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Tanggal / Jam</h6>
                    </div>
                    <div class="col-md-8">
                        <h6><u>{{\Carbon\Carbon::parse($assessment->tanggal)
                        ->isoFormat('D-MMMM-YYYY / HH:mm')}} WIB</u></h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Durasi</h6>
                    </div>
                    <div class="col-md-8">
                        <h6><u>{{$assessment->durasi}}</u></h6>
                    </div>
                </div>
                <!-- Sifat -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Sifat</h6>
                    </div>
                    <div class="col-md-8">
                        <h6><u>{{$assessment->sifat}}</u></h6>
                    </div>
                </div>
                <!-- Jenis Soal -->
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="">Jenis Soal</h6>
                    </div>
                    <div class="col-md-8">
                        <h6><u>{{$assessment->jenis_soal}}</u></h6>
                    </div>
                </div>
            </div>
            <!-- Kolom ketiga -->
            <div class="col-md-4">
            </div>
        </div>
        
        <br>
        <!-- Keterangan Isi Soal -->
        
        <div class="m-1">
        @if($assessment->nama_file_penilaian !== null && $assessment->nama_file_penilaian != "")
            <a href="{{url('download-file-nilai')}}/{{$assessment->id}}" class="btn btn-success" target="_blank">
                <i class="fa fa-download"></i>
                Download File Nilai
            </a>
        @endif
        @if($assessment->url_jawaban_mahasiswa !== null && $assessment->url_jawaban_mahasiswa != "")
            <a href="{{$assessment->url_jawaban_mahasiswa}}" class="btn btn-primary" target="_blank">
                <i class="fa fa-link"></i>
                Link Jawaban Mahasiswa
            </a>
        @endif
        <button onclick="event.preventDefault();formUpdateUrlJawaban();" data-toggle="modal" type="button"
            class="btn btn-warning">
            <i class="fa fa-edit"></i>
            Ubah URL Jawaban
        </button>
        </div>

        <!-- Isi Soal -->
        <h5>Detail Assessment / Isi Soal</h5>
        <!-- Tabel Isi Soal -->
        <div class='table-responsive'>
            <table class="table myTable table-hover table-bordered" id="tableIsiSoal">
                <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">Skor Maks</th>
                    <th class="text-center">CPMK</th>
                    <th class="text-center">Taksonomi Bloom</th>
                    @if($assessment->jenis_penilaian == "Ujian") 
                        <th class="text-center">Kompetensi</th> 
                    @endif
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Rubrik</th>
                </tr>
                @php
                    $totalSkorMaks = 0;
                @endphp
                @foreach($assessment->isiSoal->sortBy('nomor_soal') as $item)
                <tr id="isiSoal{{$item->id}}">
                    <td class="text-center">{{$item->nomor_soal}}</td>
                    <td class="text-right">{{$item->skor_maks}}</td>
                    @php
                        $totalSkorMaks += $item->skor_maks;
                    @endphp
                    <td>
                        @foreach($item->cpmk as $key=>$itemCpmk) 
                            @if($key>0)
                                , {{$itemCpmk->kode}}
                            @else
                                {{$itemCpmk->kode}}
                            @endif
                        @endforeach
                    </td>
                    <td>
                    @if($item->taksonomiBloom) 
                    {{$item->taksonomiBloom->level}}: {{$item->taksonomiBloom->keterangan}}
                    @else - @endif
                    </td>
                    @if($assessment->jenis_penilaian == "Ujian") <td>{{$item->kompetensi}}</td> @endif
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->rubrik}}</td>
                </tr>
                @endforeach
                <tr>
                    <td class="text-right">Total Skor:</td>
                    <td class="text-right">{{$totalSkorMaks}}</td>
                </tr>
            </table>
        </div>

        <!-- Nilai -->
        <div class="row">
            <div class="col-md-auto">
                <h5>Nilai</h5>
            </div>
            <div class="col-md-auto">
            </div>                
        </div>
        <div class="row">
            <div class="col-md-6">
                <h6><u>Keterangan</u></h6>
                <ul>
                    <li>Pastikan kolom Total Nilai Hasil Kalkulasi Sistem <b>SAMA DENGAN</b> Total Hasil Import File Nilai Excel yang Bapak/Ibu upload</li>
                    <li>Total Hasil Kalkulasi Sistem diperoleh dari penjumlahan semua nilai dari tiap soal yg berhasil tersimpan ke sistem</li>
                    <li>Jika terdapat perbedaan, harap dibetulkan di excel lalu diupload kembali. Jika tidak ada pembetulan, maka yg digunakan sebagai dasar perhitungan di sistem ini adalah Total Hasil Kalkulasi Sistem</li>
                </ul>
            </div>
        </div>
        <div class="table-responsive item">
            <table data-toggle="table" 
            data-pagination="true" 
            data-fixed-columns="true"
            data-fixed-number="4"
            class="table textWrap table-bordered table-hover" id="tableIsiSoal">
                <thead>
                    <tr>
                        <th data-sortable="true" rowspan="2" class="text-center align-middle">No.</th>
                        <th data-sortable="true" rowspan="2" class="text-center align-middle">NRP</th>
                        <th data-sortable="true" rowspan="2" class="text-center align-middle">Nama</th>
                        <th data-sortable="true" rowspan="2" class="text-center align-middle">KP</th>
                        <th class="text-center align-middle" width="15%">Total <br> (Hasil Kalkulasi Sistem) </th>
                        <th class="text-center align-middle" width="15%">Total <br> (Hasil Import Excel) </th>
                        <th colspan="{{count($assessment->isiSoal)}}" class="text-center">Nomor Soal</th>
                        <th rowspan="2" class="text-center align-middle min-vw-100">Feedback</th>
                    </tr>
                    <tr>
                        @php $total =0; @endphp
                        @foreach($assessment->isiSoal as $item)
                            @php $total += $item->skor_maks; @endphp
                        @endforeach
                        <th data-sortable="true" class="text-center align-middle" >{{$total}}</th>
                        <th data-sortable="true" class="text-center align-middle" >{{$total}}</th>
                        @foreach($assessment->isiSoal as $item)
                        <th data-sortable="true" class="text-center" width="5%">{{$item->nomor_soal}}<br>
                            ({{$item->skor_maks}})</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                @php
                $i=0;
                @endphp
                @foreach($assessment->kelas as $item)
                    @foreach($item->mahasiswa as $itemMahasiswa)
                    <tr>
                        <td>{{$i+=1}}</td>
                        <td class="text-left">{{$itemMahasiswa->nrp}}</td>
                        <td class="text-left">{{$itemMahasiswa->nama}}</td>
                        <td>{{$item->kp}}</td>
                        @php 
                            $totalKalkulasi = 0; 
                            $itemNilaiMahasiswa = $itemMahasiswa->nilaiMahasiswa->firstWhere('assessment_id', $assessment->id);
                        @endphp
                        <!-- Total Kalkulasi -->
                        @if($itemNilaiMahasiswa)
                        @foreach($itemNilaiMahasiswa->isiSoal as $itemDetailNilaiMahasiswa)
                            @php 
                                $totalKalkulasi += 
                                $itemDetailNilaiMahasiswa->pivot ? $itemDetailNilaiMahasiswa->pivot->nilai : 0;
                                $totalKalkulasi = round($totalKalkulasi,2);
                            @endphp
                        @endforeach
                        @endif
                        <td class="@if($itemNilaiMahasiswa) @if($totalKalkulasi != $itemNilaiMahasiswa->nilai) bg-danger @endif @endif">
                        <b>{{$totalKalkulasi}}</b></td>
                        <!-- Total nilai -->
                        <td>{{$itemNilaiMahasiswa->nilai ?? '0'}}</td>
                        <!-- Nilai per Soal -->
                        @if($itemNilaiMahasiswa)
                        @foreach($itemNilaiMahasiswa->isiSoal as $itemDetailNilaiMahasiswa)
                            <td>{{$itemDetailNilaiMahasiswa->pivot->nilai}}</td>
                        @endforeach
                        @else
                            <td>-</td>
                        @endif
                        <td class="text-left">{{$itemNilaiMahasiswa->feedback ?? ''}}</td>
                    </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('assessment.import-nilai')
    @include('assessment.delete')
    @include('assessment.update-url-jawaban')
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script src="https://unpkg.com/bootstrap-table@1.18.1/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.min.js"></script>
<script src="extensions/sticky-header/bootstrap-table-sticky-header.js"></script>
<script>
    $(document).ready(function () {
        $("#downloadTemplate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("assessment-template")}}',
                type: 'get',
                data: {
                    id: $("#id").val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    // window.location.reload("{{url('assessment')}}/{{$assessment->id}}");
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#add-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#add-errors').append('<li>' + value + '</li>');
                    });
                    $("#add-error-bag").show();
                }
            });
        });

        $("#btnImport").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);
            formData.append('assessment_id', {{$assessment->id}});
            formData.append('url', $("#url").val());

            $.ajax({
                url: '{{url("assessment-nilai-import")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    $('#formImportNilai').trigger("reset");
                    $("#formImportNilai .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    $("#import-error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        stringException = "Import gagal. Format file yang diterima: .csv, .xls, atau .xlsx";
                    }
                    else{
                        stringException = errors.message;
                    }
                    $("#import-error-bag").append(stringException);
                    $("#import-error-bag").show();
                }
            });
        });

        $("#btnDeleteAssessment").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("assessment")}}/' + $("#formDeleteAssessment #delete-assessment_id").val(),
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formDeleteAssessment .close").click();
                    if(data.error == false){
                        window.location.replace("{{url('assessment')}}");
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#btnUpdateUrlJawaban").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("update-url-jawaban")}}/' + {{$assessment->id}},
                data: {
                    url_jawaban_mahasiswa: $("#update-url_jawaban_mahasiswa").val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formUpdateUrlJawaban .close").click();
                    window.location.replace("{{url('assessment')}}/{{$assessment->id}}");
                },
                error: function (data) {
                    $("#update-url-error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = errors.message;
                    }
                    $("#update-url-error-bag").html(stringException);
                    $("#update-url-error-bag").show();
                }
            });
        });
    });

    function formImportNilai() {
        $("#import-error-bag").hide();
        $('#modalImportNilai').modal('show');
    }
    
    function formDeleteAssessment() {
        $('#modalDeleteAssessment').modal('show');
    }
    function formUpdateUrlJawaban() {
        $("#update-url-error-bag").hide();
        $('#modalUpdateUrlJawaban').modal('show');
    }

</script>
@endpush
