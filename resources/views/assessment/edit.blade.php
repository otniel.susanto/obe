@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Edit Data Penilaian (Assessment)</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('assessment')}}">Assessment</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('assessment')}}/{{$assessment->id}}">Show</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('assessment')}}/{{$assessment->id}}/edit">Edit</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAssessment" enctype="multipart/form-data">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="program_studi_id">Program Studi</label>
                    <input type="hidden" id="program_studi_id" value="{{$assessment->kelas[0]->program_studi_id}}">
                    <input type="text" class="form-control" id="program_studi_text" disabled value="{{$assessment->kelas[0]->programStudi->nama}}">
                </div>
                <div class="col-md-4">
                    <label for="mata_kuliah_id">Mata Kuliah</label>
                    <input type="hidden" id="mata_kuliah_id" value="{{$assessment->kelas[0]->mata_kuliah_id}}">
                    <input type="text" class="form-control" id="mata_kuliah_text" disabled value="{{$assessment->kelas[0]->mataKuliah->kode}} - {{$assessment->kelas[0]->mataKuliah->nama}}">
                </div>
                <div class="col-md-4">
                    <label for="komponen_penilaian_id">Komponen Penilaian</label>
                    <input type="hidden" id="komponen_penilaian_id" value="{{$assessment->komponen_penilaian_id}}">
                    <input type="text" class="form-control" id="komponen_penilaian_text" disabled value="{{$assessment->kelompok}} - {{$assessment->persentase}}% - {{$assessment->aktivitas}}">
                </div>
            </div>
            <div class="form-group row">
                <!-- Tahun Ajaran -->
                <div class="col-md-3">
                    <label for="tahun_ajaran_id">Tahun Ajaran</label>
                    <input type="hidden" id="tahun_ajaran_id" value="{{$tahun_ajaran->id}}">
                    <input type="text" class="form-control" id="tahun_ajaran_text" disabled value="{{$tahun_ajaran->keterangan}}">
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <!-- Tanggal -->
                        <div class="col-md-7">
                            <label>Tanggal</label>
                            <div class="input-group">
                                <input required type="date" class="form-control" name="tanggal" id="tanggal"
                                    format="dd-mm-yyyy" value="{{date('Y-m-d',strtotime($assessment->tanggal))}}">
                            </div>
                        </div>
                        <!-- Jam -->
                        <div class="col-md-5">
                            <label>Jam</label>
                            <div class="input-group">
                                <input required type="time" class="form-control" name="jam" id="jam" format="" value="{{date('H:i',strtotime($assessment->tanggal))}}">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Jenis Soal -->
                <div class="col-md-4">
                    <label for="jenis_soal">Jenis Soal</label>
                    <input required type="string" class="form-control" name="jenis_soal" id="jenis_soal"
                        placeholder="Essay, Multiple Choice, dll" value="{{$assessment->jenis_soal}}">
                </div>
            </div>
            
            <div class="form-group row">
                <!-- Durasi -->
                <div class="col-md-3">
                    <label for="durasi">Durasi</label>
                    <input required type="string" class="form-control" name="durasi" id="durasi"
                        placeholder="30 menit, 1 jam" value="{{$assessment->durasi}}">
                </div>
                <!-- Sifat -->
                <div class="col-md-4">
                    <label for="sifat">Sifat</label>
                    <input required type="string" class="form-control" name="sifat" id="sifat"
                        placeholder="Terbuka, Tertutup" value="{{$assessment->sifat}}">
                </div>
                <!-- Upload -->
                <div class="col-md-4">
                    <label for="file">Upload File Soal (akan menghapus data yang lama)</label>
                    <input type="file" name="file" id="file">
                </div>
            </div>
        </form>
        
        <!-- Dosen Penguji -->
        <div class="row mb-1">
            <div class="col-md-6 border">
                <label for="dosen_pengujji_id">Dosen Penguji</label>
                <div class="form-group row">
                    <div class="col-md-9">
                        <select id="dosen_penguji_id" class="select2 form-control">
                            @foreach($dosen as $item)
                            <option value="{{$item->id}}">{{$item->npk}} - {{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button type="button" id="btnTambahDosenPenguji" class="btn btn-primary">
                            <i class="fa fa-plus"></i>
                            Tambah
                        </button>
                    </div>
                </div>
                <div class='table-responsive'>
                    <table class="table table-bordered" id="tableDosenPenguji">
                        <tr>
                            <th>NPK</th>
                            <th>Nama</th>
                            <th>Delete</th>
                        </tr>
                        @foreach($assessment->dosenPenguji as $item)
                        <tr id="dosen{{$item->id}}">
                            <td>{{$item->npk}}</td>
                            <td>{{$item->name}}</td>
                            <td>
                                <a onclick="event.preventDefault();formDeleteDosenPenguji({{$item->id}});"
                                    class="delete" data-toggle="modal">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- Isi Soal -->
        <div class="row mb-1">
            <div class="col-auto">
                <h4>Detail Penilaian (Assessment) / Isi Soal</h4>
            </div>
            <div class="col-auto">
                <button onclick="event.preventDefault();showCreateIsiSoal();"
                    class="btn btn-primary open-modal" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </button>
            </div>
        </div>
        <!-- Tabel Isi Soal -->
        <div class='table-responsive'>
            <table class="table myTable table-hover table-bordered" id="tableIsiSoal">
                <tr>
                    <th>No.</th>
                    <th>Skor Maks</th>
                    <th>CPMK</th>
                    <th>Taksonomi Bloom</th>
                    @if($assessment->jenis_penilaian == "Ujian") 
                        <th>Kompetensi</th> 
                    @endif
                    <th>Keterangan</th>
                    <th>Rubrik</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                @foreach($assessment->isiSoal as $item)
                @php $totalSkor =0.0; $totalSkor += $item->skor_maks @endphp
                <tr id="isiSoal{{$item->id}}">
                    <td>{{$item->nomor_soal}}</td>
                    <td>{{$item->skor_maks}}</td>
                    <td>
                        @foreach($item->cpmk as $key=>$itemCpmk) 
                            @if($key>0)
                                , {{$itemCpmk->kode}}
                            @else
                                {{$itemCpmk->kode}}
                            @endif
                        @endforeach
                    </td>
                    <td>{{$item->taksonomiBloom->level}}: {{$item->taksonomiBloom->keterangan}}</td>
                    @if($assessment->jenis_penilaian == "Ujian") <td>{{$item->kompetensi}}</td> @endif
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->rubrik}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditIsiSoal({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteIsiSoal({{$item->id}});"
                            class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>

        <div class="form-group row">
            <div class="col-12">
                <button type="button" class="btn btn-primary float-right" id="btnUpdate"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
    </div>
    @include('isi_soal.create')
    @include('isi_soal.edit')
    @include('isi_soal.delete')
    @include('dosen_penguji.delete')
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    var totalSkor = 0.0;
    var isi_soal_create = [];
    var isi_soal_update = [];
    var isi_soal_delete = [];
    var iIsiSoal = 0;
    var iDosen = 0;
    var dosen_penguji_create = [];
    var dosen_penguji_delete = [];
    $("#kelas").ready(function () {
        $("#kelas").hide();
    });
    $("#detailUjian").ready(function () {
        $("#detailUjian").hide();
    });
    // $('#modalCreateIsiSoal').on('shown.bs.modal', function() {
    //     $('#add-rubrik').summernote();
    // })
    $(document).ready(function () {
        totalSkor = parseFloat($("#totalSkor").val());
        $("#warning-total-skor").html('Total skor saat ini: ' + totalSkor);
        $('#dosen_penguji_id').select2({
            placeholder: "Pilih Dosen Penguji"
        });
        $('#add-taksonomi_bloom_id').select2({
            allowClear: true,
            placeholder: "Pilih Taksonomi Bloom"
        });
        $('#edit-taksonomi_bloom_id').select2({
            allowClear: true,
            placeholder: "Pilih Taksonomi Bloom"
        });
        // Button Update
        $("#btnUpdate").click(function () {
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);
            formData.append('program_studi_id', $('#program_studi_id').val());
            formData.append('mata_kuliah_id', $("#mata_kuliah_id").val());
            formData.append('tahun_ajaran_id', $('#tahun_ajaran_id').val());
            formData.append('komponen_penilaian_id', $('#komponen_penilaian_id').val());
            formData.append('tanggal', $('#tanggal').val());
            formData.append('jam', $('#jam').val());
            formData.append('durasi', $('#durasi').val());
            formData.append('jenis_soal', $('#jenis_soal').val());
            formData.append('sifat', $('#sifat').val());
            formData.append('isi_soal_create', JSON.stringify(isi_soal_create));
            formData.append('isi_soal_update', JSON.stringify(isi_soal_update));
            formData.append('isi_soal_delete', JSON.stringify(isi_soal_delete));
            formData.append('dosen_penguji_create', JSON.stringify(dosen_penguji_create));
            formData.append('dosen_penguji_delete', JSON.stringify(dosen_penguji_delete));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("assessment")}}/{{$assessment->id}}',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('assessment')}}/{{$assessment->id}}");
                },
                error: function (data) {
                    console.log(data);
                    alert(data.message);
                }
            });
        });
        //Tambah Isi Soal Baru
        $("#btnTambahIsiSoal").click(function () {
            if($('#add-nomor_soal').val() == ""){
                alert("Masukkan nomor soal terlebih dahulu");
                return false;
            }
            if($('#add-skor_maks').val() == ""){
                alert("Masukkan skor maksimal soal terlebih dahulu");
                return false;
            }
            var arrCpmk = $('input[name="cpmkTambah[]"]input:checkbox:checked').map(function(){
                return $(this).val();
            }).get();
            $.ajax({
                url:'{{url("get-cpmk")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val(),
                    mata_kuliah_id: $("#mata_kuliah_id").val(),
                    tahun_ajaran_id: $("#tahun_ajaran_id").val(),
                    cpmk: arrCpmk
                },
                dataType: 'json',
                success: function (data) {
                    var cpmkString = "";
                    $.each(data.cpmk, function(key, value){
                        if(key <= 0){
                            cpmkString += value.kode;
                        }
                        else{
                            cpmkString += ', ' + value.kode;
                        }
                    });
                    var nomor = $('#add-nomor_soal').val().replace(/\s/g,'');
                    $.each(nomor.split(","),function(key, value){
                        isi_soal_create.push({
                            index: ++iIsiSoal,
                            cpmk: arrCpmk,
                            nomor_soal: value,
                            skor_maks: $('#add-skor_maks').val(),
                            kompetensi: $('#add-kompetensi').val(),
                            keterangan: $('#add-keterangan-isi_soal').val(),
                            rubrik: $('#add-rubrik').val(),
                            taksonomi_bloom_id: $('#add-taksonomi_bloom_id').val()
                        });
                        if("{{$assessment->jenis_penilaian}}" != "Ujian"){
                            $('#tableIsiSoal tr:last').after('<tr id="' + iIsiSoal + '">' +
                                '<td>' + value + '</td>' +
                                '<td>' + $('#add-skor_maks').val() + '</td>' +
                                '<td>' + cpmkString + '</td>' +
                                '<td>' + $('#add-taksonomi_bloom_id option:selected').text() + '</td>' +
                                '<td>' + $('#add-keterangan-isi_soal').val() + '</td>' +
                                '<td>' + $('#add-rubrik').val() + '</td>' +
                                '<td>-</td>' + 
                                '<td><a onclick="event.preventDefault();deleteIsiSoal(' +
                                iIsiSoal +
                                ');" data-toggle="modal">' +
                                '<i class="fa fa-trash"></i></a></td>' +
                                '</tr>'
                            );
                        }
                        else{
                            $('#tableIsiSoal tr:last').after('<tr id="' + iIsiSoal + '">' +
                                '<td>' + value + '</td>' +
                                '<td>' + $('#add-skor_maks').val() + '</td>' +
                                '<td>' + cpmkString + '</td>' +
                                '<td>' + $('#add-taksonomi_bloom_id option:selected').text() + '</td>' +
                                '<td>' + $('#add-kompetensi').val() + '</td>' +
                                '<td>' + $('#add-keterangan-isi_soal').val() + '</td>' +
                                '<td>' + $('#add-rubrik').val() + '</td>' +
                                '<td>-</td>' + 
                                '<td><a onclick="event.preventDefault();deleteIsiSoal(' +
                                iIsiSoal +
                                ');" data-toggle="modal">' +
                                '<i class="fa fa-trash"></i></a></td>' +
                                '</tr>'
                            );
                        }
                    });
                    $("#add-nomor_soal").val('');
                    $('#modalCreateIsiSoal').modal('hide');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        //Edit Isi Soal
        $("#btnEditIsiSoal").click(function () {
            if($('#edit-nomor_soal').val() == ""){
                alert("Masukkan nomor soal terlebih dahulu");
                return false;
            }
            if($('#edit-skor_maks').val() == ""){
                alert("Masukkan skor maksimal soal terlebih dahulu");
                return false;
            }
            var arrCpmk = $('input[name="cpmkEdit[]"]input:checkbox:checked').map(function(){
                return $(this).val();
            }).get();
            $.ajax({
                url:'{{url("get-cpmk")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val(),
                    mata_kuliah_id: $("#mata_kuliah_id").val(),
                    tahun_ajaran_id: $("#tahun_ajaran_id").val(),
                    cpmk: arrCpmk
                },
                dataType: 'json',
                success: function (data) {
                    var cpmkString = "";
                    $.each(data.cpmk, function(key, value){
                        if(key <= 0){
                            cpmkString += value.kode;
                        }
                        else{
                            cpmkString += ', ' + value.kode;
                        }
                    });
                    //Hapus data di isi_soal_update kalo sudah pernah diupdate
                    $.each(isi_soal_update, function(key,value){
                        if(value.id == $("#id-edit-isi_soal").val()){
                            isi_soal_update.splice(value.id, 1);
                        }
                    });
                    //Push ke array data yang diubah ke isi_soal_update
                    isi_soal_update.push({
                        id: $("#id-edit-isi_soal").val(),
                        cpmk: arrCpmk,
                        nomor_soal: $("#edit-nomor_soal").val(),
                        skor_maks: $('#edit-skor_maks').val(),
                        kompetensi: $('#edit-kompetensi').val(),
                        keterangan: $('#edit-keterangan-isi_soal').val(),
                        rubrik: $('#edit-rubrik').val(),
                        taksonomi_bloom_id: $('#edit-taksonomi_bloom_id').val()
                    });
                    //Edit tabel
                    $('#tableIsiSoal tr#isiSoal' + $("#id-edit-isi_soal").val()).html('');
                    //Kalau jenis penilaiannya bukan ujian, ga ada kolom kompetensi yang ditambahkan
                    if("{{$assessment->jenis_penilaian}}" != "Ujian"){
                        $('#tableIsiSoal tr#isiSoal' + $("#id-edit-isi_soal").val()).html('<td>' + $("#edit-nomor_soal").val() + '</td>' +
                            '<td>' + $('#edit-skor_maks').val() + '</td>' +
                            '<td>' + cpmkString + '</td>' +
                            '<td>' + $('#edit-taksonomi_bloom_id option:selected').text() + '</td>' +
                            '<td>' + $('#edit-keterangan-isi_soal').val() + '</td>' +
                            '<td>' + $('#edit-rubrik').val() + '</td>' +
                            '<td><a onclick="event.preventDefault();formEditIsiSoal(' + $("#id-edit-isi_soal").val() + ');"' + 
                            'class="edit open-modal" data-toggle="modal" value="' + $("#id-edit-isi_soal").val() + '">' + 
                            '<i class="fa fa-edit"></i></a></td>' + 
                            '<td><a onclick="event.preventDefault();formDeleteIsiSoal(' + $("#id-edit-isi_soal").val() + ');"' +
                            'class="delete" data-toggle="modal"><i class="fa fa-trash"></i></a></td>'
                        );
                    }
                    //Kalau jenis penilaiannya Ujian, ada kolom kompetensi
                    else{
                        $('#tableIsiSoal tr#isiSoal' + $("#id-edit-isi_soal").val()).html('<td>' + $("#edit-nomor_soal").val() + '</td>' +
                            '<td>' + $('#edit-skor_maks').val() + '</td>' +
                            '<td>' + cpmkString + '</td>' +
                            '<td>' + $('#edit-taksonomi_bloom_id option:selected').text() + '</td>' +
                            '<td>' + $('#edit-kompetensi').val() + '</td>' +
                            '<td>' + $('#edit-keterangan-isi_soal').val() + '</td>' +
                            '<td>' + $('#edit-rubrik').val() + '</td>' +
                            '<td><a onclick="event.preventDefault();formEditIsiSoal(' + $("#id-edit-isi_soal").val() + ');"' + 
                            'class="edit open-modal" data-toggle="modal" value="' + $("#id-edit-isi_soal").val() + '">' + 
                            '<i class="fa fa-edit"></i></a></td>' + 
                            '<td><a onclick="event.preventDefault();formDeleteIsiSoal(' + $("#id-edit-isi_soal").val() + ');"' +
                            'class="delete" data-toggle="modal"><i class="fa fa-trash"></i></a></td>'
                        );
                    }
                    $('#formEditIsiSoal').trigger("reset");
                    $('#modalEditIsiSoal').modal('hide');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        //Hapus Isi Soal
        $("#btnDeleteIsiSoal").click(function () {
            isi_soal_delete.push({
                id: $("#id-delete-isi_soal").val()
            });
            $("tr#isiSoal" + $("#id-delete-isi_soal").val()).remove();
            $('#modalDeleteIsiSoal').modal('hide');
        });
        //Tambah Dosen Penguji Baru
        $("#btnTambahDosenPenguji").click(function () {
            var execute = true;
            $.each(dosen_penguji_create, function(key, value){
                if(value.id == $('#dosen_penguji_id').val()){
                    alert("Dosen sudah ditambahkan");
                    execute = false;
                    return false;
                }
            });
            if(execute){
                $.ajax({
                    url:'{{url("get-dosen")}}',
                    method: 'get',
                    data: {
                        dosen_penguji_id: $("#dosen_penguji_id").val(),
                    },
                    dataType: 'json',
                    success: function (data) {
                        dosen_penguji_create.push({
                            index: ++iDosen,
                            id: $('#dosen_penguji_id').val()
                        });
                        $('#tableDosenPenguji tr:last').after('<tr id="dosenBaru' + iDosen + '">' +
                            '<td>' + data.dosen.npk + '</td>' +
                            '<td>' + data.dosen.name + '</td>' +
                            '<td><a onclick="event.preventDefault();deleteDosenPenguji(' +
                            iDosen +
                            ');" data-toggle="modal">' +
                            '<i class="fa fa-trash"></i></a></td>' +
                            '</tr>'
                        );
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        }); 
        //Hapus Dosen Penguji
        $("#btnDeleteDosenPenguji").click(function () {
            dosen_penguji_delete.push({
                id: $("#id-delete-dosen_penguji").val()
            });
            $("tr#dosen" + $("#id-delete-dosen_penguji").val()).remove();
            $('#modalDeleteDosenPenguji').modal('hide');
        });
        //Ambil CPMK Isi Soal dan ditaruh di Create dan Edit Isi Soal
        $.ajax({
            url:'{{url("get-cpmk")}}',
            type: 'get',
            data:{
                program_studi_id: $("#program_studi_id").val(),
                mata_kuliah_id: $("#mata_kuliah_id").val(),
                tahun_ajaran_id: $("#tahun_ajaran_id").val()
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data);
                var htmlElement = '';
                var stringCpl = '';
                var i = 0;
                $("#add-cpmk-wrapper").html('');
                $("#edit-cpmk-wrapper").html('');
                // console.log(data.cpmk);
                $.each(data.cpmk, function (key, value){
                    $.each(data.cpl,function (idx, val){
                        if(val.kodeCpmk == value.kode){
                            if(i>0){
                                stringCpl += ', ';
                            }
                            stringCpl += val.kodeCpl + '->' + val.keterangan;
                            i += 1;
                        }
                    });
                    htmlElement += '<div class="form-check"><input class="form-check-input" type="checkbox" value="' + 
                    value.id + '" name="cpmkTambah[]" id="cpmkTambah' + value.id + '"><label class="form-check-label" for="cpmkTambah' + 
                    value.id + '">' + value.kode + ' | Mapping CPL: ' + stringCpl + '</label></div>';
                    stringCpl = '';
                    i = 0;
                });
                $("#add-cpmk-wrapper").html(htmlElement);
                editElement = htmlElement.replace(/\Tambah/g, "Edit");
                $("#edit-cpmk-wrapper").html(editElement);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    function formEditIsiSoal(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("isi-soal")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-isi_soal-error-bag").hide();
                $("#edit-nomor_soal").val(data.isi_soal.nomor_soal);
                $("#edit-skor_maks").val(data.isi_soal.skor_maks);
                $("#edit-taksonomi_bloom_id").val(data.isi_soal.taksonomi_bloom_id).change();
                $("#edit-kompetensi").val(data.isi_soal.kompetensi);
                $("#edit-keterangan-isi_soal").val(data.isi_soal.keterangan);
                $("#edit-rubrik").val(data.isi_soal.rubrik);
                $("#id-edit-isi_soal").val(data.isi_soal.id);
                $.each(data.cpmk, function(key, value){
                    $.each($('input[name="cpmkEdit[]"]'), function(){
                        if(this.value == value.id){
                            $("#cpmkEdit"+value.id).prop("checked", true);
                        }
                    });
                });
                $("#edit-error-bag").hide();
                $('#modalEditIsiSoal').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    function formDeleteIsiSoal(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("isi-soal")}}/' + id + '/edit',
            success: function (data) {
                $("#delete-data-isi_soal").html("<p>Nomor soal: " + data.isi_soal.nomor_soal + "</p>" +
                "<p>Skor maks: " + data.isi_soal.nomor_soal + "</p>" +
                "<p>Taksonomi Bloom: " + data.taksonomi_bloom.level + " - " + data.taksonomi_bloom.keterangan + "</p>" +
                "<p>Kompetensi: " + data.isi_soal.kompetensi + "</p>" +
                "<p>Keterangan: " + data.isi_soal.keterangan + "</p>" +
                "<p>Rubrik: " + data.isi_soal.rubrik + "</p>");
                $("#id-delete-isi_soal").val(data.isi_soal.id);
                $('#modalDeleteIsiSoal').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    function deleteIsiSoal(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#isiSoalBaru' + id).remove();
            var index = isi_soal_create.findIndex(function (item) {
                return item.index == id;
            });
            isi_soal_create.splice(index, 1);
            console.log(isi_soal_create);
            cekTotalSkor();
        }
    }
    function showCreateIsiSoal() {
        $("#create-error-bag").hide();
        $('#modalCreateIsiSoal').modal('show');
    }

    function deleteDosenPenguji(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#dosenBaru' + id).remove();
            var index = dosen_penguji_create.findIndex(function (item) {
                return item.index == id;
            });
            dosen_penguji_create.splice(index, 1);
            console.log(dosen_penguji_create);
        }
    }
    function formDeleteDosenPenguji(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("profile")}}/' + id,
            success: function (data) {
                $("#delete-data-dosen_penguji").html("<p>NPK: " + data.dosen.npk + "</p>" +
                "<p>Nama: " + data.dosen.name + "</p>")
                $("#id-delete-dosen_penguji").val(data.dosen.id);
                $('#modalDeleteDosenPenguji').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
</script>
@endpush
