<!-- Import Modal HTML -->
<div class="modal fade" id="modalImportNilai">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formImportNilai" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title"> Import Nilai Penilaian (Assessment) </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="import-error-bag">
                        
                    </div>
                    
                    <label for="file">Pilih File Excel</label><br>
                    <input type="file" name="file" id="file" required="required">

                    <label for="url">Masukkan Link (URL) Google Drive Jawaban Mahasiswa</label><br>
                    <input type="text" class="form-control" name="url" id="url" required="required"
                    value="{{$assessment->url_jawaban_mahasiswa !== null ? ($assessment->url_jawaban_mahasiswa != '' ? $assessment->url_jawaban_mahasiswa : '') : ''}}"
                    >
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button>
                    <button class="btn btn-success" id="btnImport" type="button">
                        <i class="fa fa-upload"></i>
                        Import
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

