@extends('index')
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Penilaian (Assessment)</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('assessment')}}">Penilaian (Assessment)</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('assessment/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="alert alert-danger d-none" id="error-bag">
        </div>
        <form id="formAssessment" enctype="multipart/form-data">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="program_studi_id">Program Studi</label>
                    <select id="program_studi_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="mata_kuliah_id">Mata Kuliah</label>
                    <select id="mata_kuliah_id" class="form-control">
                        <option disabled selected></option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="komponen_penilaian_id">Komponen Penilaian</label>
                    <select id="komponen_penilaian_id" class="select2 form-control" required>
                        <option disabled selected></option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <!-- Tahun Ajaran -->
                <div class="col-md-3">
                    <label for="tahun_ajaran_id">Tahun Ajaran</label>
                    <input type="hidden" id="tahun_ajaran_id" value="{{$tahun_ajaran->id}}">
                    <input type="text" class="form-control" id="tahun_ajaran_text" disabled value="{{$tahun_ajaran->keterangan}}">
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <!-- Tanggal -->
                        <div class="col-md-7">
                            <label>Tanggal</label>
                            <div class="input-group">
                                <input required type="date" class="form-control" name="tanggal" id="tanggal"
                                    format="dd-mm-yyyy">
                            </div>
                        </div>
                        <!-- Jam -->
                        <div class="col-md-5">
                            <label>Jam</label>
                            <div class="input-group">
                                <input required type="time" class="form-control" name="jam" id="jam" format="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Jenis Soal -->
                <div class="col-md-4">
                    <label for="jenis_soal">Jenis Soal</label>
                    <input required type="string" class="form-control" name="jenis_soal" id="jenis_soal"
                        placeholder="Essay, Multiple Choice, dll">
                </div>
            </div>
            
            <div class="form-group row">
                <!-- Durasi -->
                <div class="col-md-3">
                    <label for="durasi">Durasi</label>
                    <input required type="string" class="form-control" name="durasi" id="durasi"
                        placeholder="30 menit, 1 jam">
                </div>
                <!-- Sifat -->
                <div class="col-md-4">
                    <label for="sifat">Sifat</label>
                    <input required type="string" class="form-control" name="sifat" id="sifat"
                        placeholder="Terbuka, Tertutup">
                </div>
                <!-- Upload -->
                <div class="col-md-3">
                    <label for="file">Upload File Soal</label>
                    <input type="file" name="file" id="file">
                </div>
            </div>
        </form>
        
        <!-- Dosen Penguji -->
        <div class="row mb-1">
            <div class="col-md-7 border">
                <label for="dosen_pengujji_id">Dosen Penguji</label>
                <div class="form-group row">
                    <div class="col-auto">
                        <select id="dosen_penguji_id" class="select2 form-control">
                            @foreach($dosen as $item)
                            <option value="{{$item->id}}">{{$item->npk}} - {{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-auto">
                        <button type="button" id="btnTambahDosenPenguji" class="btn btn-primary">
                            <i class="fa fa-plus"></i>
                            Tambah
                        </button>
                    </div>
                </div>
                <div class='table-responsive'>
                    <table class="table table-bordered" id="tableDosenPenguji">
                        <tr>
                            <th>NPK</th>
                            <th>Nama</th>
                            <th>Delete</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="row mb-1">
            <div class="col-auto">
                <h4>Detail Penilaian (Assessment) / Isi Soal</h4>
            </div>
            <div class="col-auto">
                <p class="bg-warning" id="warning-total-skor">Total skor saat ini: 0</p>
            </div>
            <div class="col-auto">
                <button onclick="event.preventDefault();showCreateIsiSoal();"
                    class="btn btn-primary open-modal" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </button>
            </div>
        </div>

        <div class='table-responsive'>
            <table class="table myTable table-hover table-bordered" id="tableIsiSoal">
                <tr>
                    <th>No.</th>
                    <th>Skor Maks</th>
                    <th>CPMK</th>
                    <th>Taksonomi Bloom</th>
                    @if($assessment !== null)
                        @if($assessment->jenis_penilaian == "Ujian")
                            <th id="col-kompetensi">Kompetensi</th>
                        @endif
                    @else
                        <th id="col-kompetensi" class="d-none">Kompetensi</th>
                    @endif
                    <th>Keterangan</th>
                    <th>Rubrik</th>
                    <th>Delete</th>
                </tr>
            </table>
        </div>

        <div class="form-group row">
            <div class="col-12">
                <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
    </div>
    @include('isi_soal.create')
</section>
@endsection


@push('child-scripts')
<script>
    var jenis_penilaian = "";
    var isi_soal_assessment = [];
    var i = 0;
    var iDosen = 0;
    var cpmk_array = [];
    var dosen_penguji = [];
    $("#kelas").ready(function () {
        $("#kelas").hide();
    });
    $("#detailUjian").ready(function () {
        $("#detailUjian").hide();
    });
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $('#dosen_penguji_id').select2({
            placeholder: "Pilih Dosen Penguji"
        });
        $('#mata_kuliah_id').select2({
            placeholder: "Pilih Mata Kuliah"
        });
        $("#program_studi_id").change(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("get-mata_kuliah")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    $("#mata_kuliah_id").html('');
                    var stringAppend = '<option disabled selected></option>';
                    $.each(data.mata_kuliah, function (key, value) {
                        stringAppend += '<option value="' + value.id + '">' +
                            value.kode + ' - ' + value.nama + '</option>';
                    });
                    $("#mata_kuliah_id").append(stringAppend);
                    $("#komponen_penilaian_id").change();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $('#komponen_penilaian_id').select2({
            placeholder: "Pilih Komponen Penilaian",
        });
        $('#add-taksonomi_bloom_id').select2({
            allowClear: true,
            placeholder: "Pilih Taksonomi Bloom"
        });
        // Button Simpan
        $("#btnAdd").click(function () {
            if(isi_soal_assessment.length <= 0){
                alert("Tambahkan soal pada penilaian ini terlebih dahulu");
                return false;
            }
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);
            formData.append('program_studi_id', $('#program_studi_id').val());
            formData.append('mata_kuliah_id', $("#mata_kuliah_id").val());
            formData.append('tahun_ajaran_id', $("#tahun_ajaran_id").val());
            formData.append('komponen_penilaian_id', $('#komponen_penilaian_id').val());
            formData.append('tanggal', $('#tanggal').val());
            formData.append('jam', $('#jam').val());
            formData.append('durasi', $('#durasi').val());
            formData.append('jenis_soal', $('#jenis_soal').val());
            formData.append('sifat', $('#sifat').val());
            formData.append('detail', JSON.stringify(isi_soal_assessment));
            formData.append('dosen_penguji', JSON.stringify(dosen_penguji));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("assessment")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    if(data.error == false){
                        window.location.replace("{{url('assessment')}}");
                    }
                },
                error: function (data) {
                    console.log(data);
                    $("#error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = errors.message;
                    }
                    $("#error-bag").html(stringException);
                    $("#error-bag").show();
                    alert(stringException);
                }
            });
        });

        $("#btnTambahIsiSoal").click(function () {
            if($('#add-nomor_soal').val() == ""){
                alert("Masukkan nomor soal terlebih dahulu");
                return false;
            }
            if($('#add-skor_maks').val() == ""){
                alert("Masukkan skor maksimal soal terlebih dahulu");
                return false;
            }
            var arrCpmk = $('input[name="cpmk[]"]input:checkbox:checked').map(function(){
                return $(this).val();
            }).get();
            if(arrCpmk.length <= 0){
                if (!confirm('Soal no. ' + $('#add-nomor_soal').val() + ' tidak memiliki CPMK, \n' + 
                    'berarti skor soal ini tidak diperhitungkan dalam skor CPL.\n' + 
                    'Yakin melanjutkan?')){
                    return false;
                }
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("get-cpmk")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val(),
                    mata_kuliah_id: $("#mata_kuliah_id").val(),
                    tahun_ajaran_id: $("#tahun_ajaran_id").val(),
                    cpmk: arrCpmk
                },
                dataType: 'json',
                success: function (data) {
                    var cpmkString = "";
                    $.each(data.cpmk, function(key, value){
                        if(key <= 0){
                            cpmkString += value.kode;
                        }
                        else{
                            cpmkString += ', ' + value.kode;
                        }
                    });
                    
                    var nomor = $('#add-nomor_soal').val().replace(/\s/g,'');
                    $.each(nomor.split(","),function(key, value){
                        isi_soal_assessment.push({
                            index: ++i,
                            cpmk: arrCpmk,
                            nomor_soal: value,
                            skor_maks: $('#add-skor_maks').val(),
                            kompetensi: $('#add-kompetensi').val(),
                            keterangan: $('#add-keterangan-isi_soal').val(),
                            rubrik: $('#add-rubrik').val(),
                            taksonomi_bloom_id: $('#add-taksonomi_bloom_id').val()
                        });
                        cekTotalSkor();
                        if(jenis_penilaian != "Ujian"){
                            $('#tableIsiSoal tr:last').after('<tr id="' + i + '">' +
                                '<td>' + value + '</td>' +
                                '<td>' + $('#add-skor_maks').val() + '</td>' +
                                '<td>' + cpmkString + '</td>' +
                                '<td>' + $('#add-taksonomi_bloom_id option:selected').text() + '</td>' +
                                '<td>' + $('#add-keterangan-isi_soal').val() + '</td>' +
                                '<td>' + $('#add-rubrik').val() + '</td>' +
                                '<td><a onclick="event.preventDefault();deleteIsiSoal(' +
                                i +
                                ');" data-toggle="modal">' +
                                '<i class="fa fa-trash"></i></a></td>' +
                                '</tr>'
                            );
                        }
                        else{
                            $('#tableIsiSoal tr:last').after('<tr id="' + i + '">' +
                                '<td>' + value + '</td>' +
                                '<td>' + $('#add-skor_maks').val() + '</td>' +
                                '<td>' + cpmkString + '</td>' +
                                '<td>' + $('#add-taksonomi_bloom_id option:selected').text() + '</td>' +
                                '<td>' + $('#add-kompetensi').val() + '</td>' +
                                '<td>' + $('#add-keterangan-isi_soal').val() + '</td>' +
                                '<td>' + $('#add-rubrik').val() + '</td>' +
                                '<td><a onclick="event.preventDefault();deleteIsiSoal(' +
                                i +
                                ');" data-toggle="modal">' +
                                '<i class="fa fa-trash"></i></a></td>' +
                                '</tr>'
                            );
                        }
                    });
                    $("#add-nomor_soal").val('');
                    $('#modalCreateIsiSoal').modal('hide');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#mata_kuliah_id").change(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //Get Komponen MK (Komponen Penilaian dan Kelas)
            $.ajax({
                url:'{{url("get-komponen-mk")}}',
                method: 'get',
                data: {
                    program_studi_id: $("#program_studi_id").val(),
                    mata_kuliah_id: $("#mata_kuliah_id").val(),
                    tahun_ajaran_id: $("#tahun_ajaran_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    if(!data.mahasiswa){
                        alert("Tambahkan data mahasiswa terlebih dahulu melalui menu Kelas Paralel");
                        window.location.replace("{{url('kelas')}}");
                    }
                    var stringAppend = '';
                    if(data.komponen_penilaian.length > 0){
                        $.each(data.komponen_penilaian, function (key, value) {
                            stringAppend += '<option value="' + value.id + '">' +
                                value.kelompok_nilai_nama + ' - ' + 
                                value.persentase + '% - ' + 
                                value.aktivitas + '</option>';
                        });
                        $('#komponen_penilaian_id').select2({
                            placeholder: "Pilih Komponen Penilaian",
                        });
                    }
                    else{
                        stringAppend = '<option disabled selected></option>';
                        $('#komponen_penilaian_id').select2({
                            placeholder: "Selamat, penilaian sudah habis dibuat!",
                        });
                        alert("Komponen Penilaian untuk Mata Kuliah ini telah habis dibuat");
                    }
                    $("#komponen_penilaian_id").html('');
                    $("#komponen_penilaian_id").append(stringAppend);
                    $("#komponen_penilaian_id").change();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#add-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#add-errors').append('<li>' + value + '</li>');
                    });
                    $("#add-error-bag").show();
                }
            });
            //Get CPMK
            $.ajax({
                url:'{{url("get-cpmk")}}',
                type: 'get',
                data:{
                    mata_kuliah_id: $("#mata_kuliah_id").val(),
                    tahun_ajaran_id: $("#tahun_ajaran_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    var htmlElement = '';
                    var stringCpl = '';
                    var i = 0;
                    $("#add-cpmk-wrapper").html('');
                    console.log(data.cpl);
                    console.log(data.cpmk);
                    $.each(data.cpmk, function (key, value){
                        $.each(data.cpl,function (idx, val){
                            if(val.kodeCpmk == value.kode){
                                if(i>0){
                                    stringCpl += ', ';
                                }
                                stringCpl += val.kodeCpl + '->' + val.keterangan;
                                i += 1;
                            }
                        });
                        if(stringCpl == ''){
                            alert("Lengkapi data Matriks CPL vs CPMK pada Mata Kuliah ini terlebih dahulu \n" + 
                            value.kode + " pada mata kuliah ini belum memiliki hubungan pada Mapping CPL");
                            // window.location.replace("{{url('level-kontribusi')}}");
                            // return false;
                        }
                        else{
                            htmlElement += '<div class="form-check"><input class="form-check-input" type="checkbox" value="' + 
                            value.id + '" name="cpmk[]" id="cpmk' + value.id + '"><label class="form-check-label" for="cpmk' + 
                            value.id + '">' + value.kode + ' | Mapping CPL: ' + stringCpl + '</label></div>';
                            stringCpl = '';
                        }
                        i = 0;
                    });
                    $("#add-cpmk-wrapper").html(htmlElement);
                },
                error: function (data) {
                    console.log(data.status);
                    var errors = $.parseJSON(data.responseText);
                    $('#add-errors').html('');
                    if(data.status != 500){
                        $.each(errors.message, function (key, value) {
                            $('#add-errors').append('<li>' + value + '</li>');
                        });
                    }
                    else{
                        
                    }
                    $("#add-error-bag").show();
                }
            });
            //Reset
        });
        $('#komponen_penilaian_id').change(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{url("get-komponen-penilaian")}}/' + $("#komponen_penilaian_id").val(),
                method: 'get',
                success: function (data) {
                    jenis_penilaian = data.jenis_penilaian.nama;
                    if (jenis_penilaian != 'Ujian') {
                        $("#detailUjian").fadeOut();
                        $("#col-kompetensi").fadeOut();
                    } else {
                        $("#detailUjian").fadeIn();
                        $("#detailUjian").removeClass("d-none");
                        $("#col-kompetensi").fadeIn();
                        $("#col-kompetensi").removeClass("d-none");
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#btnTambahDosenPenguji").click(function () {
            var execute = true;
            $.each(dosen_penguji, function(key, value){
                if(value.id == $('#dosen_penguji_id').val()){
                    alert("Dosen sudah ditambahkan");
                    execute = false;
                    return false;
                }
            });
            if(execute){
                $.ajax({
                    url:'{{url("get-dosen")}}',
                    method: 'get',
                    data: {
                        dosen_penguji_id: $("#dosen_penguji_id").val(),
                    },
                    dataType: 'json',
                    success: function (data) {
                        dosen_penguji.push({
                            index: ++iDosen,
                            id: $('#dosen_penguji_id').val()
                        });
                        $('#tableDosenPenguji tr:last').after('<tr id="dosen' + iDosen + '">' +
                            '<td>' + data.dosen.npk + '</td>' +
                            '<td>' + data.dosen.name + '</td>' +
                            '<td><a onclick="event.preventDefault();deleteDosenPenguji(' +
                            iDosen +
                            ');" data-toggle="modal">' +
                            '<i class="fa fa-trash"></i></a></td>' +
                            '</tr>'
                        );
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        });
    });

    function cekTotalSkor (){
        var totalSkor = 0.0;
        $.each(isi_soal_assessment, function(key, value){
            totalSkor += parseFloat(value.skor_maks);
        });
        $('#warning-total-skor').html('');
        if(totalSkor<100){
            $('#warning-total-skor').html('Total skor saat ini: ' + totalSkor);
            $('#warning-total-skor').removeClass('bg-success');
            $('#warning-total-skor').addClass('bg-warning');
        }
        else if(totalSkor == 100){
            $('#warning-total-skor').html('Total skor sudah 100');
            $('#warning-total-skor').removeClass('bg-warning');
            $('#warning-total-skor').addClass('bg-success');
        }
        else{
            $('#warning-total-skor').html('Total skor melebihi 100. Skor saat ini: ' + totalSkor);
            $('#warning-total-skor').removeClass('bg-success');
            $('#warning-total-skor').addClass('bg-warning');
        }
    }

    function deleteIsiSoal(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#' + id).remove();
            var index = isi_soal_assessment.findIndex(function (item) {
                return item.index == id;
            });
            isi_soal_assessment.splice(index, 1);
            console.log(isi_soal_assessment);
            cekTotalSkor();
        }
    }

    function showCreateIsiSoal() {
        $("#create-error-bag").hide();
        $('#modalCreateIsiSoal').modal('show');
    }
    function deleteDosenPenguji(id) {
        if (confirm('Apakah anda yakin akan menghapus data ini?')) {
            $('tr#dosen' + id).remove();
            var index = dosen_penguji.findIndex(function (item) {
                return item.index == id;
            });
            dosen_penguji.splice(index, 1);
            console.log(dosen_penguji);
        }
    }
</script>
@endpush
