<table>
    <tr>
        <td style="text-align:right;">Program Studi:</td>
        <td>{{$assessment->kelas[0]->programStudi->nama}}</td>
    </tr>
    <tr>
        <td style="text-align:right;">Mata Kuliah:</td>
        <td>{{$assessment->kelas[0]->mataKuliah->nama}}</td>
    </tr>
    <tr>
        <td style="text-align:right;">Tahun Ajaran:</td>
        <td>{{$assessment->kelas[0]->tahunAjaran->keterangan}}</td></tr>
    <tr>
        <td style="text-align:right;">Penilaian (Assessment):</td>
        <td>{{$assessment->aktivitas}}</td>
    </tr>
    <tr>
        <td style="text-align:right;">Tanggal:</td>
        <td>{{\Carbon\Carbon::parse($assessment->tanggal)->isoFormat('D-MMMM-YYYY')}}</td>
    </tr>
    <tr>
        <td style="text-align:right;">Kelas Paralel:</td>
        <td>
            @foreach($assessment->kelas as $key=>$item)
                @if($key > 0 )
                    ,
                @endif
                {{$item->kp}}
            @endforeach
        </td>
    </tr>
    <tr></tr>
    <tr>
        <th rowspan="3" style="text-align:center; vertical-align:center;"><b>No.</b></th>
        <th rowspan="3" style="text-align:center; vertical-align:center;"><b>NRP</b></th>
        <th rowspan="3" style="text-align:center; vertical-align:center;"><b>Mahasiswa</b></th>
        <th rowspan="3" style="text-align:center; vertical-align:center;"><b>KP</b></th>
        <th rowspan="3" style="text-align:center; vertical-align:center;"><b>Total</b></th>
        @foreach($isi_soal as $item)
            <th style="text-align:center;">
                <b>No. {{$item->nomor_soal}}</b>
            </th>
        @endforeach
        <!-- <th colspan="{{count($isi_soal)}}" style="text-align:center;"><b>Nomor Soal (Persentase)</b></th> -->
        <th rowspan="3" style="text-align:center; vertical-align:center;"><b>Feedback</b></th>
    </tr>
    <tr>
        @foreach($isi_soal as $item)
            <th style="text-align:center; word-wrap: break-word;">{{$item->keterangan}}</th>
        @endforeach
    </tr>
    <tr>
        @foreach($isi_soal as $item)
            <th style="text-align:center;">
                <b>{{$item->skor_maks}}</b>
            </th>
        @endforeach
    </tr>
    @php
    $i=0;
    @endphp
    @foreach($assessment->kelas->sortBy('kp') as $item)
        @foreach($item->mahasiswa->sortBy('nrp') as $itemDetail)
        <tr>
            <td>{{$i+=1}}</td>
            <td>{{$itemDetail->nrp}}</td>
            <td>{{$itemDetail->nama}}</td>
            <td>{{$item->kp}}</td>
        </tr>
        @endforeach
    @endforeach
</table>
