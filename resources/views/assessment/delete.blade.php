<!-- Delete Task Modal Form HTML -->
<div class="modal fade" id="modalDeleteAssessment">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteAssessment">
                <div class="modal-header">
                    <h4 class="modal-title" id="delete-title" name="title">Hapus Data Penilaian (Assessment)</h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="delete-data">
                        <p>Mata Kuliah: {{$assessment->kelas[0]->mataKuliah->kode}} - {{$assessment->kelas[0]->mataKuliah->nama}}</p> 
                        <p>Penilaian (Assessment): {{$assessment->aktivitas}}</p>
                        <p>Kelompok Penilaian: {{$assessment->kelompok}}</p>
                        <p>Persentase: {{$assessment->persentase}}%</p>
                    </div>
                    <p><b><u>Data Nilai yang sudah dimasukkan akan terhapus dari penilaian. <br>
                    Apakah anda yakin untuk melanjutkan?</u></b></p>
                    <p class="text-warning"> <small> Data akan terhapus selamanya. </small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="delete-assessment_id" type="hidden" value="{{$assessment->id}}"> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDeleteAssessment" type="button">
                    <i class="fa fa-trash"></i>
                    Hapus </button>
                </div>
            </form>
        </div>
    </div>
</div>
