<!-- Update Url Task Modal Form HTML -->
<div class="modal fade" id="modalUpdateUrlJawaban">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formUpdateUrlJawaban">
                <div class="modal-header">
                    <h4 class="modal-title" id="update-url-title" name="title">Ubah URL Jawaban Mahasiswa</h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="update-url-error-bag"></div>
                    <label for="update-url_jawaban_mahasiswa">Masukkan URL Google Drive Jawaban Mahasiswa</label>
                    <input type="text" class="form-control" id="update-url_jawaban_mahasiswa" value="{{$assessment->url_jawaban_mahasiswa}}"
                    placeholder="Masukkan link / URL">
                </div>
                <div class="modal-footer"> 
                    <input id="update-url-assessment_id" type="hidden" value="{{$assessment->id}}"> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-info" id="btnUpdateUrlJawaban" type="button">
                        <i class="fa fa-pen"></i>
                        Ubah 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
