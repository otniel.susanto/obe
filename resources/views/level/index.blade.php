@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Level</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('level')}}">Level</a></li>
        </ol>
        <a href="{{url('level/create')}}" type="button" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Tambah Data
        </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="10%">No.</th>
                    <th width="15%">Level</th>
                    <th width="50%">Keterangan</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($level as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->level}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditLevel({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteLevel({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$level->count()}}</b> out of <b>{{$level->total()}}</b> entries
            </div> {{ $level->links() }}
        </div>
    </div>
    @include('level.edit')
    @include('level.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("level")}}/' + $("#formEditLevel #id_edit").val(),
                data: {
                    level: $("#formEditLevel #level").val(),
                    keterangan: $("#formEditLevel #keterangan").val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditLevel').trigger("reset");
                    $("#formEditLevel .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-level-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-level-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("level")}}/' + $("#formDeleteLevel #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteLevel .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditLevel(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("level")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditLevel #level").val(data.level.level);
                $("#formEditLevel #keterangan").val(data.level.keterangan);
                $("#formEditLevel #id_edit").val(data.level.id);
                $('#modalEditLevel').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteLevel(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("level")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteLevel #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Level: " + data.level.level + "</p>" + 
                "<p>Keterangan: " + data.level.keterangan + "</p>")
                $("#formDeleteLevel #id_delete").val(data.level.id);
                $('#modalDeleteLevel').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
