<!-- Edit Modal HTML -->
<div class="modal fade" id="modalAddCpmk">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formAddCpmk">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Sub-CPMK / Kompetensi Dasar (KD)</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="create-error-bag">
                        <ul id="create-isi_soal-errors"> </ul>
                    </div>

                    <label for="add-kodeCpmk">Kode (Gunakan prefix CPMK-)</label>
                    <input required type="string" class="form-control" name="add-kodeCpmk" id="add-kodeCpmk" placeholder="CPMK-1, CPMK-2">

                    <label for="add-keteranganCpmk">Keterangan</label>
                    <textarea rows="5" required type="string" class="form-control" name="add-keteranganCpmk" id="add-keteranganCpmk" placeholder="Isi CPMK"> </textarea>

                </div>
                <div class="modal-footer">
                    <button onclick="event.preventDefault();" class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button>
                    <button class="btn btn-info" type="button" id="btnTambahCpmk">
                        <i class="fa fa-plus"></i> 
                        Tambah
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
