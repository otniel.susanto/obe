@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
                <h1>Cpmk</h1>
            </div>
            <div class="col-sm-6">
                <a href="{{url('cpmk/create')}}" type="button" class="btn btn-primary float-sm-right">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </a>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <table width="100% data-toggle=" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="">Keterangan</th>
                    <th width='10%'>Edit</th>
                    <th width='10%'>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cpmk as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditCpmk({{$item->id}});"
                            class="edit open-modal" data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteCpmk({{$item->id}});" 
                        class="delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix">
            <div class="hint-text">Showing <b>{{$cpmk->count()}}</b> out of <b>{{$cpmk->total()}}</b> entries
            </div> {{ $cpmk->links() }}
        </div>
    </div>
    @include('cpmk.edit')
    @include('cpmk.delete')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("cpmk")}}/' + $("#formEditCpmk #id_edit").val(),
                data: {
                    kode: $('#kode').val(),
                    keterangan: $('#keterangan').val(),
                    mata_kuliah_id: $('#mata_kuliah_id').val(),
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditCpmk').trigger("reset");
                    $("#formEditCpmk .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-cpmk-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-cpmk-errors').append('<li>' + value + '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("cpmk")}}/' + $("#formDeleteCpmk #id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteCpmk .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formEditCpmk(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("cpmk")}}/' + id  + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#formEditCpmk #keterangan").val(data.cpmk.keterangan);
                $("#formEditCpmk #student_outcome_id").val(data.cpmk.student_outcome_id);
                $("#formEditCpmk #mata_kuliah_id").val(data.cpmk.mata_kuliah_id);
                $("#formEditCpmk #bobot_so_id").val(data.cpmk.bobot_so_id);
                $("#formEditCpmk #id_edit").val(data.cpmk.id);
                $('#modalEditCpmk').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteCpmk(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("cpmk")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteCpmk #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>Cpmk: " + data.cpmk.keterangan + "</p>" + 
                "<p>Mata Kuliah: " + data.mata_kuliah.nama + "</p>" +
                "<p>Capaian Pembelajaran Lulusan (CPL): " + data.student_outcome.keterangan + "</p>" +
                "<p>Bobot: " + data.bobot.nilai + " - " + data.bobot.keterangan + "</p>")
                $("#formDeleteCpmk #id_delete").val(data.cpmk.id);
                $('#modalDeleteCpmk').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
