<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditCpmk">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditCpmk">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Sub-CPMK / Kompetensi Dasar (KD)</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-cpmk-error-bag">
                        <ul id="edit-cpmk-errors"> </ul>
                    </div>

                    <label for="edit-kodeCpmk">Kode (Gunakan prefix CPMK-)</label>
                    <input required type="string" class="form-control" name="edit-kodeCpmk" 
                    id="edit-kodeCpmk" placeholder="CPMK-1, CPMK-2">

                    <label for="edit-keteranganCpmk">Keterangan</label>
                    <textarea rows="5" required type="string" class="form-control" name="edit-keteranganCpmk" id="edit-keteranganCpmk" placeholder="Isi CPMK"> </textarea>

                </div>
                <div class="modal-footer">
                    <input id="id-edit-cpmk" type="hidden" value="">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdateCpmk" type="button">
                        <i class="fa fa-marker"></i>
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
