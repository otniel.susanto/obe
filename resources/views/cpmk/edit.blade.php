<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditCpmk">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditCpmk">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Cpmk </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-cpmk-errors"> </ul>
                    </div>

                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea rows="5" class="form-control" id="keterangan" 
                         placeholder="Masukkan isi CPMK"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="id-edit-cpmk" type="hidden" value="0">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
