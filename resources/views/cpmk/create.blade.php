@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Tambah Data Sub-CPMK</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formCpmk">
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="mata_kuliah_id">Mata Kuliah</label>
                    <select id="mata_kuliah_id" class="select2 form-control">
                        @foreach($mata_kuliah as $item)
                            <option value="{{$item->id}}">{{$item->kode}} - {{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="kode">Kode</label>
                    <input type="text" class="form-control" id="kode"  placeholder="LO-1, LO 2">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label for="keterangan">Keterangan</label>
                    <textarea rows="5" class="form-control" id="keterangan"  placeholder="Masukkan isi CPMK"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(document).ready(function () {
        $('.select2').select2();
        $("#btnAdd").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("cpmk")}}',
                data: {
                    kode: $('#kode').val(),
                    keterangan: $('#keterangan').val(),
                    mata_kuliah_id: $('#mata_kuliah_id').val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('cpmk')}}");
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#add-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#add-errors').append('<li>' + value + '</li>');
                    });
                    $("#add-error-bag").show();
                }
            });
        });
    });

</script>
@endpush
