<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditCpl">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditCpl">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Cpl </h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-cpl-errors"> </ul>
                    </div>

                    <label for="edit-program_studi_id">Prorgam Studi</label>
                    <select id="edit-program_studi_id" class="select2 form-control">
                        @foreach($program_studi as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    
                    <label for="edit-kode">Kode</label>
                    <input required type="text" class="form-control" id="edit-kode" placeholder="Masukkan kode Cpl">

                    <label for="edit-keterangan">Keterangan</label>
                    <textarea required type="text" rows="5" class="form-control" id="edit-keterangan" placeholder="Masukkan keterangan"></textarea>

                    <label for="edit-kategori_cpl_id">Kategori CPL</label>
                    <select id="edit-kategori_cpl_id" class="select2 form-control">
                        @foreach($kategori_cpl as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                </div>
                <div class="modal-footer">
                    <input id="id_edit" type="hidden" value="0">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdate" type="button" value="add">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
