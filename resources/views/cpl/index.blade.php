@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Capaian Pembelajaran Lulusan (CPL)</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('cpl')}}">CPL</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Filter Section -->
        <form action="{{url('cpl')}}" method="get">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <select id="program_studi_id" name="program_studi_id" class="select2 form-control">
                            <option disabled selected></option>
                            @foreach($program_studi as $item)
                            <option value="{{$item->id}}"
                            @if($item->id == $request->program_studi_id)
                            selected
                            @endif
                            >{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="jenis_cpl_id" id="jenis_cpl_id" class="select2 form-control">
                            <option disabled selected></option>
                            @foreach($jenis_cpl as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->jenis_cpl_id) selected @endif
                            >{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="kategori_cpl_id" id="kategori_cpl_id" class="select2 form-control">
                            <option disabled selected></option>
                            @foreach($kategori_cpl as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->kategori_cpl_id) selected @endif
                            >{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <input name="kode_cpl" type="search" class="form-control" placeholder="Kode CPL" value="{{$request->kode_cpl}}">
                    </div>
                    <div class="col-md-3">
                        <input name="keterangan_cpl" type="search" class="form-control" placeholder="Keterangan CPL" value="{{$request->keterangan_cpl}}">
                    </div>
                </div>
                <div class="row mt-1">
                    <div class="col-md-8">
                        <a href="{{url('cpl/create')}}" type="button" class="btn btn-primary">
                            <i class="fa fa-plus"></i>
                            Tambah Data
                        </a>
                        <button onclick="event.preventDefault();formImportCpl();" data-toggle="modal" type="button"
                            class="btn btn-success mr-1">
                            <i class="fa fa-upload"></i>
                            Import Excel
                        </button>
                        <button onclick="event.preventDefault();formDownloadCpl();" data-toggle="modal" type="button"
                            class="btn btn-info">
                            <i class="fa fa-download"></i>
                            Download Template
                        </button>
                    </div>
                    <div class="col-md-4">
                        <div class="float-right">
                            <button class="btn btn-primary" type="submit" id="btnFilter"><i class="fa fa-search"></i> Cari</button>
                            <button class="btn btn-secondary" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
        <table width="100%" data-toggle="table" data-pagination="true" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th >No.</th>
                    <th data-sortable="true">Program Studi</th>
                    <th data-sortable="true">Jenis</th>
                    <th data-sortable="true">Kategori</th>
                    <th data-sortable="true">Kode</th>
                    <th width="50%">Keterangan</th>
                    <th >Edit</th>
                    <th >Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cpl as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->programStudi->nama}}</td>
                    <td>{{$item->kategoriCpl->jenisCpl->nama}}</td>
                    <td>{{$item->kategoriCpl->nama}}</td>
                    <td>{{$item->kode}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <a onclick="event.preventDefault();formEditCpl({{$item->id}});" class="edit open-modal"
                            data-toggle="modal" value="{{$item->id}}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a onclick="event.preventDefault();formDeleteCpl({{$item->id}});" class="delete"
                            data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('cpl.edit')
    @include('cpl.delete')
    @include('cpl.import')
    @include('cpl.download')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $(".select2").select2();
        // Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Program Studi"
        });
        $('#jenis_cpl_id').select2({
            allowClear: true,
            placeholder: "Jenis CPL"
        });
        $('#kategori_cpl_id').select2({
            allowClear: true,
            placeholder: "Kategori CPL"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('cpl')}}");
        });
        //Filter Section End
        $('#download-program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $("#btnUpdate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("cpl")}}/' + $("#id_edit").val(),
                data: {
                    kode: $("#edit-kode").val(),
                    keterangan: $("#edit-keterangan").val(),
                    kategori_cpl_id: $("#edit-kategori_cpl_id").val(),
                    program_studi_id: $("#edit-program_studi_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    $('#formEditCpl').trigger("reset");
                    $("#formEditCpl .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $('#edit-cpl-errors').html('');
                    $.each(errors.message, function (key, value) {
                        $('#edit-cpl-errors').append('<li>' + value +
                            '</li>');
                    });
                    $("#edit-error-bag").show();
                }
            });
        });
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("cpl")}}/' + $("#id_delete").val(),
                dataType: 'json',
                success: function (data) {
                    $("#formDeleteCpl .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#btnImport").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);

            $.ajax({
                url: '{{url("cpl/import")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#formImportCpl').trigger("reset");
                    $("#formImportCpl .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    $("#import-error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        stringException = "Import gagal. Format file yang diterima: .csv, .xls, atau .xlsx";
                    }
                    else{
                        stringException = errors.message;
                    }
                    $("#import-error-bag").append(stringException);
                    $("#import-error-bag").show();
                }                
            });
        });
        $("#btnDownload").click(function () {
            if($("#download-program_studi_id").val() !== null && $("#download-program_studi_id").val() != ""){
                $("#btnDownload").attr("href", "{{url('cpl-template')}}/" + $("#download-program_studi_id").val());
                $("#formDownloadCpl .close").click();
            }
            else{
                $("#download-error-bag").html('');
                $("#download-error-bag").append("<p>Pilih Program Studi terlebih dahulu</p>");
                $("#download-error-bag").show();
                return false;
            }
        });
    });

    function formEditCpl(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("cpl")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-error-bag").hide();
                $("#edit-kode").val(data.cpl.kode);
                $("#edit-keterangan").val(data.cpl.keterangan);
                $("#edit-kategori_cpl_id").val(data.cpl.kategori_cpl_id).change();
                $("#edit-program_studi_id").val(data.cpl.program_studi_id).change();
                $("#id_edit").val(data.cpl.id);
                $('#modalEditCpl').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteCpl(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("cpl")}}/' + id + '/edit',
            success: function (data) {
                $("#formDeleteCpl #delete-title").html("Apakah anda yakin menghapus data ini?");
                $("#delete-data").html("<p>CPL - " + data.program_studi.nama + "</p>" +
                "<p>Jenis CPL: " + data.jenis_cpl.nama + "</p>" +
                "<p>Kategori: " + data.kategori_cpl.nama + "</p>" +
                "<p>Kode: " + data.cpl.kode + "</p>" +
                "<p>Keterangan</p><p>" + data.cpl.keterangan + "</p>")
                $("#id_delete").val(data.cpl.id);
                $('#modalDeleteCpl').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formImportCpl() {
        $("#import-error-bag").hide();
        $('#modalImportCpl').modal('show');
    }
    function formDownloadCpl() {
        $("#download-error-bag").hide();
        $('#modalDownloadCpl').modal('show');
    }

</script>
@endpush
