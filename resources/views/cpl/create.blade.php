@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-sm-6">
                <h1>Tambah Data Cpl</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('cpl')}}">CPL</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('cpl/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAddCpl">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="kode">Kode</label>
                    <input required type="text" class="form-control" id="kode" placeholder="Masukkan kode CPL">
                </div>
                <div class="col-md-3">
                    <label for="program_studi_id">Pilihan Program Studi</label>
                    <select id="program_studi_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="keterangan">Keterangan</label>
                    <textarea rows="5" required type="text" class="form-control" id="keterangan"
                        placeholder="Masukkan keterangan"> </textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="kategori_cpl_id">Kategori CPL</label>
                    <select id="kategori_cpl_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($kategori_cpl as $item)
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <button type="button" class="btn btn-primary float-right" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $('#kategori_cpl_id').select2({
            placeholder: "Pilih Kategori CPL"
        });
        $("#btnAdd").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("cpl")}}',
                data: {
                    kode: $("#kode").val(),
                    keterangan: $("#keterangan").val(),
                    kategori_cpl_id: $("#kategori_cpl_id").val(),
                    program_studi_id: $("#program_studi_id").val()
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('cpl')}}");
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });
    });

</script>
@endpush
