<table>
    <thead>
        <tr>
            <th width="">Kode</th>
            <th width="">Keterangan</th>
            <th width="">No. Kategori CPL</th>
            <th width=""></th>
            <th width=""><b>Petunjuk pengisian:</b></th>
        </tr>
    </thead>
    <tbody>
        <!-- Petunjuk pengisian -->
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>Masukkan ID kategori pada kolom ID Kategori CPL</b></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>No.</b></td>
            <td><b>Nama</b></td>
        </tr>
        <!-- Petunjuk pengisian end -->
        @foreach($kategori_cpl as $item)
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $item->id }}</td>
            <td>{{ $item->nama }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
