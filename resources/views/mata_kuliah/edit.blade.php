@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-sm-6">
                <h1>Edit Data Mata Kuliah</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('mata-kuliah')}}">Mata Kuliah</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('mata-kuliah')}}/{{$detail_mata_kuliah->id}}">Show</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('mata-kuliah')}}/{{$detail_mata_kuliah->id}}/edit">Edit</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formEditMataKuliah">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="kode">Kode</label>
                    <input required type="text" class="form-control" id="kode" 
                    placeholder="Masukkan kode Mata Kuliah" value="{{$detail_mata_kuliah->mataKuliah->kode}}">
                </div>
                <div class="col-md-3">
                    <label for="nama">Nama</label>
                    <input required type="text" class="form-control" id="nama" 
                    placeholder="Masukkan nama Mata Kuliah" value="{{$detail_mata_kuliah->mataKuliah->nama}}">
                </div>
                <div class="col-md-2">
                    <label for="sks">SKS</label>
                    <input required type="number" class="form-control align-right" id="sks" 
                    placeholder="Jumlah SKS"  value="{{$detail_mata_kuliah->mataKuliah->sks}}">
                </div>
            </div>
        </form>

        <h5>Program Studi</h5>
        <div class="form-group row">
            <div class="col-md-4">
                <label for="program_studi_id">Pilihan Program Studi</label>
                <select id="program_studi_id" class="select2 form-control">
                    <option disabled selected></option>
                    @foreach($program_studi as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="jenis_mata_kuliah_id">Jenis Mata Kuliah</label>
                <select id="jenis_mata_kuliah_id" class="select2 form-control">
                    <option disabled selected></option>
                    @foreach($jenis_mata_kuliah as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="alur_7_semester">Alur 7 Semester</label>
                <input required type="text" class="form-control" id="alur_7_semester" placeholder="Alur 7 Semester">
            </div>
            <div class="col-md-2">
                <label for="alur_7_semester">Alur 8 Semester</label>
                <input required type="text" class="form-control" id="alur_8_semester" placeholder="Alur 8 Semester">
            </div>
            <div class="col-md-auto">
                <br>
                <button class="btn btn-primary mt-2" id="btnTambahTabel">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </button>
            </div>
        </div>

        <div class="row mb-1">
            <div class="col-9">
                <table width="100% data-toggle=" class="table myTable table-hover" id="tabelProgramStudi">
                    <thead>
                        <tr>
                            <th width="30%">Program Studi</th>
                            <th width="20%">Jenis Mata Kuliah</th>
                            <th width="15%">Alur 7 Semester</th>
                            <th width="15%">Alur 8 Semester</th>
                            <th width="10%">Edit</th>
                            <th width="10%">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($detail_mata_kuliah->mataKuliah->programStudi as $item)
                            <tr id="detail{{$item->pivot->id}}">
                                <td>{{$item->nama}}</td>
                                <td>{{$jenis_mata_kuliah->find($item->pivot->jenis_mata_kuliah_id)->nama}}</td>
                                <td>{{$item->pivot->alur_7_semester}}</td>
                                <td>{{$item->pivot->alur_8_semester}}</td>
                                <td>
                                    <a onclick="event.preventDefault();formEditDetailMataKuliah({{$item->pivot->id}});" class="edit open-modal"
                                        data-toggle="modal" value="{{$item->id}}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                <td>
                                    <a onclick="event.preventDefault();formDeleteDetailMataKuliah({{$item->pivot->id}});" class="delete"
                                        data-toggle="modal">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-9">
                <button type="button" class="btn btn-primary float-right mb-1" id="btnEdit"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
    </div>
    @include('mata_kuliah.modal_detail_edit')
    @include('mata_kuliah.modal_detail_delete')
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    var detail_mata_kuliah_create = [];
    var detail_mata_kuliah_update = [];
    var detail_mata_kuliah_delete = [];
    var iBaru = 0;
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $('#jenis_mata_kuliah_id').select2({
            placeholder: "Pilih Jenis Mata Kuliah"
        });
        $('#edit-program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $('#edit-jenis_mata_kuliah_id').select2({
            placeholder: "Pilih Jenis Mata Kuliah"
        });
        $("#btnEdit").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'PUT',
                url:'{{url("mata-kuliah")}}/' + {{$detail_mata_kuliah->mata_kuliah_id}},
                data: {
                    kode: $("#kode").val(),
                    nama: $("#nama").val(),
                    sks: $("#sks").val(),
                    detail_mata_kuliah_create: detail_mata_kuliah_create,
                    detail_mata_kuliah_update: detail_mata_kuliah_update,
                    detail_mata_kuliah_delete: detail_mata_kuliah_delete,
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('mata-kuliah')}}");
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                }
            });
        });

        $("#btnTambahTabel").click(function () {
            if($('#program_studi_id').val() == null){
                alert("Pilih Program Studi terlebih dahulu!");
                return false;
            }if($('#jenis_mata_kuliah_id').val() == null){
                alert("Pilih Jenis Mata Kuliah terlebih dahulu!");
                return false;
            }if($('#alur_7_semester').val() == "" || $('#alur_8_semester').val() == ""){
                alert("Masukkan data alur 7 dan 8 semester terlebih dahulu!");
                return false;
            }
            var duplicate = false;
            $.each(detail_mata_kuliah_create, function (key, value) {
                if(value.program_studi_id == $('#program_studi_id').val()){
                    duplicate = true;
                }
            });
            if(!duplicate){
                detail_mata_kuliah_create.push({
                    index: ++iBaru,
                    program_studi_id: $('#program_studi_id').val(),
                    jenis_mata_kuliah_id: $('#jenis_mata_kuliah_id').val(),
                    alur_7_semester: $('#alur_7_semester').val(),
                    alur_8_semester: $('#alur_8_semester').val(),
                });
                $('#tabelProgramStudi tr:last').after('<tr id="baru' + iBaru + '">' +
                    '<td>' + $('#program_studi_id option:selected').text() + '</td>' +
                    '<td>' + $('#jenis_mata_kuliah_id option:selected').text() + '</td>' +
                    '<td>' + $('#alur_7_semester').val() + '</td>' +
                    '<td>' + $('#alur_8_semester').val() + '</td>' +
                    '<td>-</td>' +
                    '<td><a onclick="event.preventDefault();btnDeleteTabel(' +
                    iBaru +
                    ');" data-toggle="modal">' +
                    '<i class="fa fa-trash"></i></a></td>' +
                    '</tr>'
                );
            }
            else{
                alert('Program studi sudah ada');
            }
        });

        $("#btnDeleteDetailMataKuliah").click(function(){
            detail_mata_kuliah_delete.push({
                id: $("#id-delete-detail_mata_kuliah").val()
            })
            $("tr#detail" + $("#id-delete-detail_mata_kuliah").val()).remove();
            $('#modalDeleteDetailMataKuliah').modal('hide');
        });
        $("#btnUpdateDetailMataKuliah").click(function(){
            for (i = 0; i < detail_mata_kuliah_update.length; i++) {
                if (detail_mata_kuliah_update[i].id == $('#id-edit-detail_mata_kuliah').val()) {
                    detail_mata_kuliah_update.splice(i, 1);
                }
            }
            detail_mata_kuliah_update.push({
                id: $('#id-edit-detail_mata_kuliah').val(),
                program_studi_id: $("#edit-program_studi_id").val(),
                mata_kuliah_id: {{$detail_mata_kuliah->mata_kuliah_id}},
                jenis_mata_kuliah_id: $("#edit-jenis_mata_kuliah_id").val(),
                alur_7_semester: $("#edit-alur_7_semester").val(),
                alur_8_semester: $("#edit-alur_8_semester").val(),
            });
            console.log(detail_mata_kuliah_create);
            console.log(detail_mata_kuliah_update);
            console.log(detail_mata_kuliah_delete);
            $('tr#detail' + $('#id-edit-detail_mata_kuliah').val()).html('');
            $('tr#detail' + $('#id-edit-detail_mata_kuliah').val()).append('<td>' + 
                $('#edit-program_studi_id option:selected').text() + '</td>' +
                '<td>' + $('#edit-jenis_mata_kuliah_id option:selected').text() + '</td>' +
                '<td>' + $('#edit-alur_7_semester').val() + '</td>' +
                '<td>' + $('#edit-alur_8_semester').val() + '</td>' +
                '<td><a onclick="event.preventDefault();formEditDetailMataKuliah(' + $('#id-edit-detail_mata_kuliah').val() +
                ');"' +
                'class="edit open-modal" data-toggle="modal">' +
                '<i class="fa fa-edit"></i></a></td>' +
                '<td><a onclick="event.preventDefault();formDeleteDetailMataKuliah(' + $('#id-edit-detail_mata_kuliah').val() + ');"' +
                'class="delete" data-toggle="modal"><i class="fa fa-trash"></i></a></td>'
            );
            $('#modalEditDetailMataKuliah').modal('hide');
        });
    });

    function formEditDetailMataKuliah(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("detail-mata-kuliah")}}/' + id + '/edit',
            success: function (data) {
                $("#edit-detail-error-bag").hide();
                $("#edit-program_studi_id").val(data.detail_mata_kuliah.program_studi_id).change();
                $("#edit-jenis_mata_kuliah_id").val(data.detail_mata_kuliah.jenis_mata_kuliah_id).change();
                $("#edit-alur_7_semester").val(data.detail_mata_kuliah.alur_7_semester);
                $("#edit-alur_8_semester").val(data.detail_mata_kuliah.alur_8_semester);
                $("#id-edit-detail_mata_kuliah").val(data.detail_mata_kuliah.id);
                $('#modalEditDetailMataKuliah').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function formDeleteDetailMataKuliah(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("detail-mata-kuliah")}}/' + id + '/edit',
            success: function (data) {
                $("#delete-data-detail_mata_kuliah").html("<p>Hapus program studi dari mata kuliah ini</p>" + 
                "<p>Program Studi: " + data.program_studi.nama + "</p>" +
                "<p>Jenis Mata Kuliah: " + data.jenis_mata_kuliah.nama + "</p>" +
                "<p>Alur 7 semester: " + data.detail_mata_kuliah.alur_7_semester + "</p>" +
                "<p>Alur 8 semester: " + data.detail_mata_kuliah.alur_8_semester + "</p>")
                $("#id-delete-detail_mata_kuliah").val(data.detail_mata_kuliah.id);
                $('#modalDeleteDetailMataKuliah').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function btnDeleteTabel(id) {
        $('tr#baru' + id).remove();
        var index = detail_mata_kuliah_create.findIndex(function (item) {
            return item.index == id;
        });
        detail_mata_kuliah_create.splice(index, 1);
    }

</script>
@endpush
