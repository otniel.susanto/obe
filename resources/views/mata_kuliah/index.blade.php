@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Mata kuliah</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('mata-kuliah')}}">Mata Kuliah</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @if(isset($error))
        <div class="alert alert-danger">
            <ul>
                @foreach($message as $item)
                <li>{{$item}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <!-- Filter Section -->
        <form action="{{url('mata-kuliah')}}" method="get">
            <div class="row">
                <div class="col-md-3">
                    <select name="program_studi_id" id="program_studi_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->program_studi_id) selected @endif
                        >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="jenis_mata_kuliah_id" id="jenis_mata_kuliah_id" class="select2 form-control">
                        <option disabled selected></option>
                        @foreach($jenis_mata_kuliah as $item)
                        <option value="{{$item->id}}" 
                            @if($item->id == $request->jenis_mata_kuliah_id) selected @endif
                        >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <input name="kode_mata_kuliah" type="search" class="form-control" placeholder="Kode MK" value="{{$request->kode_mata_kuliah}}">
                </div>
                <div class="col-md-3">
                    <input name="nama_mata_kuliah" type="search" class="form-control" placeholder="Nama MK" value="{{$request->nama_mata_kuliah}}">
                </div>
                <div class="col-md-2">
                    <input name="semester" type="search" class="form-control" placeholder="Semester" value="{{$request->semester}}">
                </div>
            </div>
            <div class="row mt-1 mb-1">
                <div class="col-md-8">
                    <a href="{{url('mata-kuliah/create')}}" type="button" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        Tambah Data
                    </a>
                    <button onclick="event.preventDefault();formImportMataKuliah();" data-toggle="modal" type="button"
                        class="btn btn-success mr-1">
                        <i class="fa fa-upload"></i>
                        Import Excel
                    </button>
                    <a href="{{url('mata-kuliah-template')}}" type="button" class="btn btn-info mr-1">
                        <i class="fa fa-download"></i>
                        Download Template
                    </a>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-secondary float-right" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                    <button class="btn btn-primary float-right mr-1" type="submit" id="btnFilter"><i class="fa fa-search"></i> Cari</button>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->

        <!-- Content -->
        <table width="100%" data-toggle="table" data-pagination="true" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center align-middle" data-sortable="true">No.</th>
                    <th rowspan="2" class="text-center align-middle" data-sortable="true">Program Studi</th>
                    <th rowspan="2" class="text-center align-middle" data-sortable="true">Jenis Mata Kuliah</th>
                    <th rowspan="2" class="text-center align-middle" data-sortable="true">Kode</th>
                    <th rowspan="2" class="text-center align-middle">Nama</th>
                    <th rowspan="2" class="text-center align-middle" data-sortable="true">SKS</th>
                    <th colspan="2" class="text-center align-middle" >Semester</th>
                </tr>
                <tr>
                    <th class="text-center align-middle" data-sortable="true">Alur 7 Semester</th>
                    <th class="text-center align-middle" data-sortable="true">Alur 8 Semester</th>
                </tr>
            </thead>
            <tbody>
            @foreach($detail_mata_kuliah as $key=>$itemDetailMataKuliah)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$itemDetailMataKuliah->programStudi->nama}}</td>
                    <td>{{$itemDetailMataKuliah->jenisMataKuliah->nama}}</td>
                    <td>
                        <a href="{{url('mata-kuliah')}}/{{$itemDetailMataKuliah->id}}">
                        {{$itemDetailMataKuliah->mataKuliah->kode}}
                        </a>
                    </td>
                    <td>
                        <a href="{{url('mata-kuliah')}}/{{$itemDetailMataKuliah->id}}">
                            {{$itemDetailMataKuliah->mataKuliah->nama}}
                        </a>
                    </td>
                    <td>{{$itemDetailMataKuliah->mataKuliah->sks}}</td>
                    <td>{{$itemDetailMataKuliah->alur_7_semester}}</td>
                    <td>{{$itemDetailMataKuliah->alur_8_semester}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('mata_kuliah.import')
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        // Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi"
        });
        $('#jenis_mata_kuliah_id').select2({
            allowClear: true,
            placeholder: "Jenis"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('mata-kuliah')}}");
        });
        //Filter Section End
        
        $("#btnImport").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            var files = $('#file')[0].files[0];
            formData.append('file', files);

            $.ajax({
                url: '{{url("mata-kuliah-import")}}',
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    alert(data.message);
                    $('#formImportMataKuliah').trigger("reset");
                    $("#formImportMataKuliah .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                    $("#import-error-bag").html('');
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        stringException = "Import gagal. Format file yang diterima: .csv, .xls, atau .xlsx";
                    }
                    else{
                        stringException = errors.message;
                    }
                    $("#import-error-bag").append(stringException);
                    $("#import-error-bag").show();
                }
            });
        });
    });

    function formImportMataKuliah() {
        $("#import-error-bag").hide();
        $('#modalImportMataKuliah').modal('show');
    }

</script>
@endpush
