<table style="border: solid 1px black;">
    <thead>
        <tr>
            <th colspan="3"><b>Petunjuk pengisian:</b></th>
        </tr>
        <tr>
            <th colspan="3">- Jangan menggunakan merge cell</th>
        </tr>
        <tr>
            <th colspan="3">- Pastikan kode Mata Kuliah tidak ada yang kembar</th>
        </tr>
        <tr>
            <th colspan="3">- Jangan hapus petunjuk ini</th>
        </tr>
        <tr style="height:3px;"></tr>
        <tr>
            <th rowspan="2" style="text-align:center; vertical-align:center;"><b>No.</b></th>
            <th rowspan="2" style="text-align:center; vertical-align:center;"><b>Program Studi</b></th>
            <th rowspan="2" style="text-align:center; vertical-align:center;"><b>Jenis Mata Kuliah</b></th>
            <th rowspan="2" style="text-align:center; vertical-align:center;"><b>Kode Mata Kuliah</b></th>
            <th rowspan="2" style="text-align:center; vertical-align:center;"><b>Nama Mata Kuliah</b></th>
            <th rowspan="2" style="text-align:center; vertical-align:center;"><b>SKS</b></th>
            <th colspan="2" style="text-align:center; vertical-align:center;"><b>Alur Semester</b></th>
            <th></th>
            <th rowspan="2" style="text-align:center; vertical-align:center;"><b>Pilihan Program Studi:</b></th>
        </tr>
        <tr>
            <th style="text-align:center; vertical-align:center;"><b>7 Semester</b></th>
            <th style="text-align:center; vertical-align:center;"><b>8 Semester</b></th>
        </tr>
        @foreach($program_studi as $itemProgramStudi)
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>{{$itemProgramStudi->nama}}</th>
            </tr>
        @endforeach
    </thead>
    <tbody>
    </tbody>
</table>
