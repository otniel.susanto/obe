@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-sm-6">
                <h1>Tambah Data Mata Kuliah</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('mata-kuliah')}}">Mata Kuliah</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('mata-kuliah/create')}}">Create</a></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form id="formAddMataKuliah">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="kode">Kode</label>
                    <input required type="text" class="form-control" id="kode" placeholder="Masukkan kode Mata Kuliah">
                </div>
                <div class="col-md-3">
                    <label for="nama">Nama</label>
                    <input required type="text" class="form-control" id="nama" placeholder="Masukkan nama Mata Kuliah">
                </div>
                <div class="col-md-2">
                    <label for="sks">SKS</label>
                    <input required type="number" class="form-control align-right" id="sks" placeholder="Jumlah SKS">
                </div>
            </div>
        </form>

        <h5>Program Studi</h5>
        <div class="form-group row">
            <div class="col-md-3">
                <label for="program_studi_id">Pilihan Program Studi</label>
                <select id="program_studi_id" class="select2 form-control">
                    <option disabled selected></option>
                    @foreach($program_studi as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="jenis_mata_kuliah_id">Jenis Mata Kuliah</label>
                <select id="jenis_mata_kuliah_id" class="select2 form-control">
                    <option disabled selected></option>
                    @foreach($jenis_mata_kuliah as $item)
                    {{$item->nama}}
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="alur_7_semester">Alur 7 Semester</label>
                <input required type="text" class="form-control" id="alur_7_semester" placeholder="Alur 7 Semester">
            </div>
            <div class="col-md-2">
                <label for="alur_7_semester">Alur 8 Semester</label>
                <input required type="text" class="form-control" id="alur_8_semester" placeholder="Alur 8 Semester">
            </div>
            <div class="col-md-auto">
            <br>
                <button class="btn btn-primary mt-2" id="btnTambahTabel">
                    <i class="fa fa-plus"></i>
                    Tambah Data
                </button>
            </div>
        </div>

        <div class="row mb-1">
            <div class="col-9">
                <table width="100% data-toggle=" class="table myTable table-hover" id="tabelProgramStudi">
                    <tr>
                        <th width="20%">Program Studi</th>
                        <th width="20%">Jenis Mata kuliah</th>
                        <th width="20%">Alur 7 Semester</th>
                        <th width="20%">Alur 8 Semester</th>
                        <th width="20%">Delete</th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-9">
                <button type="button" class="btn btn-primary float-right mb-1" id="btnAdd"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection


@push('child-scripts')
<script>
    var program_studi_create = [];
    var i = 0;
    $(document).ready(function () {
        $('#program_studi_id').select2({
            placeholder: "Pilih Program Studi"
        });
        $('#jenis_mata_kuliah_id').select2({
            placeholder: "Pilih Jenis Mata Kuliah"
        });
        $("#btnAdd").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("mata-kuliah")}}',
                data: {
                    kode: $("#kode").val(),
                    nama: $("#nama").val(),
                    sks: $("#sks").val(),
                    program_studi_create: program_studi_create
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    window.location.replace("{{url('mata-kuliah')}}");
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });

        $("#btnTambahTabel").click(function () {
            var duplicate = false;
            $.each(program_studi_create, function (key, value) {
                if(value.program_studi_id == $('#program_studi_id').val()){
                    duplicate = true;
                }
            });
            if(!duplicate){
                program_studi_create.push({
                    index: ++i,
                    program_studi_id: $('#program_studi_id').val(),
                    alur_7_semester: $('#alur_7_semester').val(),
                    alur_8_semester: $('#alur_8_semester').val(),
                    jenis_mata_kuliah_id: $('#jenis_mata_kuliah_id').val()
                });
                $('#tabelProgramStudi tr:last').after('<tr id="' + i + '">' +
                    '<td>' + $('#program_studi_id option:selected').text() + '</td>' +
                    '<td>' + $('#jenis_mata_kuliah_id option:selected').text() + '</td>' +
                    '<td>' + $('#alur_7_semester').val() + '</td>' +
                    '<td>' + $('#alur_8_semester').val() + '</td>' +
                    '<td class="actionCpmk"><a onclick="event.preventDefault();btnDeleteTabel(' +
                    i +
                    ');" data-toggle="modal">' +
                    '<i class="fa fa-trash"></i></a></td>' +
                    '</tr>'
                );
            }
            else{
                alert("Program Studi sudah ada");
            }
        });
    });

    function btnDeleteTabel(id) {
        $('tr#' + id).remove();
        var index = program_studi_create.findIndex(function (item) {
            return item.index == id;
        });
        program_studi_create.splice(index, 1);
    }

</script>
@endpush
