@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-sm-6">
                <h4>Data Mata Kuliah</h4>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('mata-kuliah')}}">Mata Kuliah</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('mata-kuliah')}}/{{$detail_mata_kuliah->id}}">Show</a></li>
                </ol>
            </div>
            <div class="col-md-6">
                <a class="btn btn-warning float-sm-right mr-1" href="{{url('mata-kuliah')}}/{{$detail_mata_kuliah->id}}/edit">
                    <i class="fa fa-edit"></i>
                    Edit Mata Kuliah
                </a>
                <button onclick="event.preventDefault();formDeleteMataKuliah();" data-toggle="modal" type="button"
                    class="btn btn-danger float-sm-right mr-1">
                    <i class="fa fa-trash"></i>
                    Delete Mata Kuliah
                </button>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <h6 class="">Kode</h6>
            </div>
            <div class="col-md-auto">
                <h6>: {{$detail_mata_kuliah->mataKuliah->kode}}</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h6 class="">Nama</h6>
            </div>
            <div class="col-md-auto">
                <h6>: {{$detail_mata_kuliah->mataKuliah->nama}}</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h6 class="">SKS</h6>
            </div>
            <div class="col-md-auto">
                <h6>: {{$detail_mata_kuliah->mataKuliah->sks}}</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h6 class="">Program Studi</h6>
            </div>
            <div class="col-md-auto">
                <h6>: {{$detail_mata_kuliah->programStudi->nama}}</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h6 class="">Jenis Mata Kuliah</h6>
            </div>
            <div class="col-md-auto">
                <h6>: {{$detail_mata_kuliah->jenisMataKuliah->nama}}</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h6 class="">Alur 7 Semester</h6>
            </div>
            <div class="col-md-auto">
                <h6>: {{$detail_mata_kuliah->alur_7_semester}}</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h6 class="">Alur 8 Semester</h6>
            </div>
            <div class="col-md-auto">
                <h6>: {{$detail_mata_kuliah->alur_8_semester}}</h6>
            </div>
        </div>

        <div class="row mb-1 mt-2">
            <div class="col-auto">
                <h6>Program Studi Lain:</h6>
            </div>
        </div>

        <div class="row mb-1">
            <div class="col-8">
                <table width="100% data-toggle=" class="table myTable table-hover" id="tableCpmk">
                    <tr>
                        <th width="20%">Program Studi</th>
                        <th width="20%">Jenis Mata Kuliah</th>
                        <th width="20%">Alur 7 Semester</th>
                        <th width="20%">Alur 8 Semester</th>
                    </tr>
                    @foreach($detail_mata_kuliah->mataKuliah->programStudi as $item)
                        @if($item->id != $detail_mata_kuliah->program_studi_id)
                        <tr>
                            <td>{{$item->nama}}</td>
                            <td>{{$jenis_mata_kuliah->find($item->pivot->jenis_mata_kuliah_id)->nama}}</td>
                            <td>{{$item->pivot->alur_7_semester}}</td>
                            <td>{{$item->pivot->alur_8_semester}}</td>
                        </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    @include('mata_kuliah.delete');
</section>
<!-- /.content -->
@endsection
@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnDelete").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'DELETE',
                url:'{{url("mata-kuliah")}}/{{$detail_mata_kuliah->mata_kuliah_id}}',
                success: function (data) {
                    alert(data.message);
                    if(!data.error){
                        window.location.replace("{{url('mata-kuliah')}}");
                    }
                },
                error: function (data) {
                    console.log(data);
                    var errors = $.parseJSON(data.responseText);
                    var stringException = "";
                    if(data.status == 422){
                        console.log(errors);
                        $.each(errors.errors, function (key, value) {
                            stringException += value + "\n";
                        });
                    }
                    else{
                        stringException = data.message;
                    }
                    alert(stringException);
                }
            });
        });
    });
    function formDeleteMataKuliah() {
        $.ajax({
            method: 'GET',
            url:'{{url("get-mata_kuliah")}}',
            data: {
                mata_kuliah_id: "{{$detail_mata_kuliah->mata_kuliah_id}}",
            },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                console.log(data.mata_kuliah);
                $("#delete-data").html("<p>Kode: " + data.mata_kuliah[0].kode + "</p>" +
                "<p>Nama: " + data.mata_kuliah[0].nama + "</p>" +
                "<p>SKS: " + data.mata_kuliah[0].sks + "</p>" );
                // $("#id_delete").val(data.isi_soal.id);
                $("#delete-error-bag").hide();
                $('#modalDeleteMataKuliah').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
</script>
@endpush