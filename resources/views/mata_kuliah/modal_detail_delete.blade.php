<!-- Delete Task Modal Form HTML -->
<div class="modal fade" id="modalDeleteDetailMataKuliah">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formDeleteDetailMataKuliah">
                <div class="modal-header">
                    <h4 class="modal-title" id="delete-title-detail_mata_kuliah" name="title">Apakah anda yakin menghapus data ini?</h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="delete-data-detail_mata_kuliah"></div>
                    <p class="text-warning"> <small> Data akan terhapus selamanya. </small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="id-delete-detail_mata_kuliah" type="hidden" value=""> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button> 
                    <button class="btn btn-danger" id="btnDeleteDetailMataKuliah" type="button">
                    <i class="fa fa-trash"></i>
                    Hapus </button>
                </div>
            </form>
        </div>
    </div>
</div>
