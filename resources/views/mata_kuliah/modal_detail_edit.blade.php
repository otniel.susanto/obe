<!-- Edit Modal HTML -->
<div class="modal fade" id="modalEditDetailMataKuliah">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formEditDetailMataKuliah">
                <div class="modal-header">
                    <h4 class="modal-title"> Edit Detail Mata Kuliah</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-detail-error-bag">
                        <ul id="edit-detail-errors"> </ul>
                    </div>
                    
                    <label for="edit-program_studi_id">Program Studi</label>
                    <select id="edit-program_studi_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>

                    <label for="edit-jenis_mata_kuliah_id">Jenis Mata Kuliah</label>
                    <select id="edit-jenis_mata_kuliah_id" class="form-control">
                        <option disabled selected></option>
                        @foreach($jenis_mata_kuliah as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endforeach
                    </select>
                    
                    <label for="edit-alur_7_semester">Alur 7 Semester</label>
                    <input type="text" class="form-control" id="edit-alur_7_semester" placeholder="Alur 7 Semester">
                    <label for="edit-alur_8_semester">Alur 8 Semester</label>
                    <input type="text" class="form-control" id="edit-alur_8_semester" placeholder="Alur 8 Semester">


                </div>
                <div class="modal-footer">
                    <input id="id-edit-detail_mata_kuliah" type="hidden" value="0">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i> 
                    Cancel
                    </button>
                    <button class="btn btn-info" id="btnUpdateDetailMataKuliah" type="button" value="add">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
