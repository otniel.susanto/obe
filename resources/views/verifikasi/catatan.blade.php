<!-- Create Modal HTML -->
<div class="modal fade" id="modalCatatanVerifikasi">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formCatatanVerifikasi">
                <div class="modal-header">
                    <h4 class="modal-title">Catatan Verifikasi</h4>
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="catatan-error-bag">
                        <ul id="catatan-verifikasi-errors"> </ul>
                    </div>

                    <label for="catatan">Catatan</label>                    
                    <textarea rows="5" class="form-control" id="catatan" 
                     placeholder="Masukkan catatan untuk soal ujian ini"></textarea>

                </div>
                <div class="modal-footer">
                    <input onclick="event.preventDefault();" class="btn btn-default" data-dismiss="modal" type="button" value="Cancel">
                    <button type="button" class="btn btn-info" id="btnSimpanCatatan">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
