@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1>Verifikasi</h1>
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item active"><a href="{{url('verifikasi')}}">Verifikasi</a></li>
        </ol>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Filter Section -->
        <form action="{{url('verifikasi')}}" method="get">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <select name="program_studi_id" id="program_studi_id" class="select2 form-control">
                            <option disabled selected></option>
                            @foreach($program_studi as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->program_studi_id) selected @endif
                            >{{$item->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="tahun_ajaran_id" id="tahun_ajaran_id" class="select2 form-control">
                            <option disabled selected></option>
                            @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->tahun_ajaran_id) selected @endif
                            >{{$item->keterangan}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <input name="kode_mata_kuliah" type="search" class="form-control" placeholder="Kode MK" value="{{$request->kode_mata_kuliah}}">
                    </div>
                    <div class="col-md-3">
                        <input name="nama_mata_kuliah" type="search" class="form-control" placeholder="Nama MK" value="{{$request->nama_mata_kuliah}}">
                    </div>
                    <div class="col-md-2">
                        <select name="status" id="status" class="select2 form-control">
                            <option disabled selected></option>
                            <option value="Belum" @if($request->status == "Belum") selected @endif >Belum terverifikasi</option>
                            <option value="Menunggu" @if($request->status == "Menunggu") selected @endif>Sedang diperiksa</option>
                            <option value="Sudah" @if($request->status == "Sudah") selected @endif>Sudah terverifikasi</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-secondary float-right" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                        <button class="btn btn-primary float-right mr-1" type="submit" id="btnFilter"><i class="fa fa-search"></i> Cari</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
        <table width="100%" data-toggle="table" data-pagination="true" class="table myTable table-striped table-hover">
            <thead>
                <tr>
                    <th data-sortable="true">No.</th>
                    <th data-sortable="true">Program Studi</th>
                    <th data-sortable="true">Tahun Ajaran</th>
                    <th data-sortable="true">Mata Kuliah</th>
                    <th data-sortable="true">Aktivitas Ujian</th>
                    <th data-sortable="true">Tanggal / Jam</th>
                    <th data-sortable="true">Status</th>
                    <th data-sortable="true">Diverifikasi Oleh</th>
                </tr>
            </thead>
            <tbody>
                @foreach($assessment as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->kelas[0]->programStudi->nama}}</td>
                    <td>{{$item->kelas[0]->tahunAjaran->keterangan}}</td>
                    <td><a href="{{url('verifikasi')}}/{{$item->id}}">{{$item->kelas[0]->mataKuliah->kode}} - {{$item->kelas[0]->mataKuliah->nama}}</a></td>
                    <td><a href="{{url('verifikasi')}}/{{$item->id}}">{{$item->aktivitas}}</a></td>
                    <td>
                    {{\Carbon\Carbon::parse($item->tanggal)->isoFormat('D MMMM YYYY / HH:mm')}} WIB
                    </td>
                    <td>
                        @if(!$item->verifikasi_id)
                            <span class="bg-danger">Belum terverifikasi</span>
                        @elseif(is_null($item->verifikasi->verified_at))
                            <span class="bg-warning">Dalam proses revisi <br> (Last update: 
                            {{\Carbon\Carbon::parse($item->verifikasi->updated_at)->isoFormat('D MMMM YYYY / H:m')}} WIB)</span>
                        @elseif(!is_null($item->verifikasi->verified_at))
                            <span class="bg-success">Sudah terverifikasi</span><br>
                            <a href="{{url('get-verifikasi')}}/{{$item->id}}">
                            <i class="fa fa-download"></i>
                            Download File Verifikasi
                            </a>
                        @endif
                    </td>
                    <td>
                        @if(isset($item->verifikasi))
                            @if(!is_null($item->verifikasi->verified_at))
                                {{$item->verifikasi->dosen->npk}} - {{$item->verifikasi->dosen->name}}
                            @else
                                -
                            @endif
                        @else
                            -
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        // Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi"
        });
        $('#tahun_ajaran_id').select2({
            allowClear: true,
            placeholder: "Pilih Tahun Ajaran"
        });
        $('#status').select2({
            allowClear: true,
            placeholder: "Status"
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('verifikasi')}}");
        });
        //Filter Section End
    });
</script>
@endpush