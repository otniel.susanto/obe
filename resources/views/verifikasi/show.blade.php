@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-sm-3">
                <h1>Data Ujian</h1>
                <ol class="breadcrumb ml-1">
                    <li class="breadcrumb-item active"><a href="{{url('verifikasi')}}">Verifikasi</a></li>
                    <li class="breadcrumb-item active"><a href="{{url('verifikasi/')}}">Show</a></li>
                </ol>
            </div>
            <div class="col-md-9">
                @if(is_null($assessment->verifikasi_id) || is_null($assessment->verifikasi->verified_at))
                    <a href="{{url('assessment')}}/{{$assessment->id}}/edit" class="btn btn-warning float-sm-right mr-1">
                        <i class="fa fa-edit"></i>
                        Edit Penilaian (Assessment)
                    </a>
                @endif
                <a href="{{url('get-soal')}}/{{$assessment->id}}" class="btn btn-info float-sm-right mr-1" target="_blank">
                    <i class="fa fa-download"></i>
                    Download Soal
                </a>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="">Mata Kuliah <span class="float-right">: </span></h5>
                </div>
                <div class="col-md-8">
                    <h5>{{$assessment->kelas[0]->mataKuliah->kode}} - <br>
                        {{$assessment->kelas[0]->mataKuliah->nama}}</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h5 class="">Nama <span class="float-right">: </span></h5>
                </div>
                <div class="col-md-8">
                    <h5>{{$assessment->aktivitas}}</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h5 class="">Tanggal/ Jam <span class="float-right">: </span></h5>
                </div>
                <div class="col-md-auto">
                    <h5>{{\Carbon\Carbon::parse($assessment->tanggal)->isoFormat('D MMMM YYYY / H:m')}} WIB</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h5 class="">Kelas Paralel <span class="float-right">: </span></h5>
                </div>
                <div class="col-md-8">
                    <h5>
                        @foreach($assessment->kelas as $key=>$item)
                        @if($key > 0 )
                        ,
                        @endif
                        {{$item->kp}}
                        @endforeach
                    </h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h5 class="">Tahun Ajaran <span class="float-right">: </span></h5>
                </div>
                <div class="col-md-8">
                    <h5>{{$assessment->kelas[0]->tahunAjaran->keterangan}}</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h5 class="">Status Verifikasi <span class="float-right">: </span></h5>
                </div>
                <div class="col-md-8">
                    <h5>
                    @if(!$assessment->verifikasi_id)
                        <span class="bg-danger">Belum terverifikasi</span>
                    @elseif(is_null($assessment->verifikasi->verified_at))
                        <span class="bg-warning">Dalam proses revisi <br> 
                        Soal telah dilihat pada tanggal: 
                        {{\Carbon\Carbon::parse($assessment->verifikasi->updated_at)->isoFormat('D MMMM YYYY / H:m:s')}} WIB <br>
                        {{$assessment->verifikasi->dosen->name}}</span>
                    @elseif(!is_null($assessment->verifikasi->verified_at))
                        <span class="bg-success">Sudah terverifikasi</span> <br>
                        <a href="{{url('get-verifikasi')}}/{{$assessment->id}}">
                        <i class="fa fa-download"></i>
                        Download File Verifikasi
                        </a>
                    @endif
                    </h5>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h5 class="">Catatan:</h5>
            <h5>
            @if(isset($assessment->verifikasi)) 
                {{$assessment->verifikasi->catatan}} 
            @endif </h5>
        </div>
    </div>

        <div class="row mt-1 mb-1">
            <div class="col-auto">
                <h5>Isi Soal</h5>
            </div>
        </div>

        <div class="table-responsive">
            <table width="100%" class="table myTable table-bordered table-hover table-striped" 
            id="tableIsiSoal">
                <tr>
                    <th width="5%">Nomor <br> Soal</th>
                    <th width="10%">Skor Maksimal</th>
                    <th width="10%">Taksonomi Bloom</th>
                    <th width="10%">Keterangan</th>
                    <th width="15%">Kompetensi / Kemampuan Akhir yang diharapkan</th>
                    <th width="50%">Rubrik</th>
                </tr>
                @foreach($assessment->isiSoal as $item)
                <tr>
                    <td>{{$item->nomor_soal}}</td>
                    <td>{{$item->skor_maks}}</td>

                    <td>
                        @if(isset($item->taksonomiBloom))
                            {{$item->taksonomiBloom->level}} - {{$item->taksonomiBloom->keterangan}}
                        @else
                            <span class="bg-danger">Belum ada data</span>
                        @endif
                    </td>
                    <td>{{$item->keterangan}}</td>
                    <td>{{$item->kompetensi}}</td>
                    <td>{{$item->rubrik}}</td>
                </tr>
                @endforeach
            </table>
        </div>
        <!-- Button Verifikasi dan Beri Catatan -->
        @if($user->jabatan != "Dosen")
            @if(is_null($assessment->verifikasi_id) || is_null($assessment->verifikasi->verified_at))
                <div class="row">
                    <div class="col-6">
                        <button onclick="event.preventDefault();formCreateVerifikasi({{$assessment->id}});" data-toggle="modal"
                            type="button" class="btn btn-primary mb-1">
                            <i class="fa fa-signature"></i>
                            Verifikasi
                        </button>
                        <button onclick="event.preventDefault();formCatatanVerifikasi({{$assessment->id}});" data-toggle="modal"
                            type="button" class="btn btn-warning mb-1">
                            <i class="fa fa-edit"></i>
                            Beri Catatan
                        </button>
                    </div>
                </div>
            @endif
        @endif
    </div>
    @if($user->jabatan != 'Dosen')
        @include('verifikasi.create')
        @include('verifikasi.catatan')
    @endif
</section>
<!-- /.content -->
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        $("#btnCreate").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("verifikasi")}}',
                data: {
                    assessment_id: {{$assessment->id}}
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formCreateVerifikasi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
        $("#btnSimpanCatatan").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: 'POST',
                url:'{{url("verifikasi-catatan")}}',
                data: {
                    assessment_id: {{$assessment->id}},
                    catatan: $('#catatan').val(),
                },
                dataType: 'json',
                success: function (data) {
                    alert(data.message);
                    $("#formCatatanVerifikasi .close").click();
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        });
    });

    function formCreateVerifikasi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("verifikasi")}}/' + id + '/edit',
            success: function (data) {
                $('#modalCreateVerifikasi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    function formCatatanVerifikasi(id) {
        $.ajax({
            method: 'GET',
            url:'{{url("verifikasi")}}/' + id + '/edit',
            success: function (data) {
                console.log(data);
                if(data.verifikasi !== null){
                    $('#catatan').val(data.verifikasi.catatan);
                }
                $('#catatan-error-bag').hide();
                $('#modalCatatanVerifikasi').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

</script>
@endpush
