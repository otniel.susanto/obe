<!DOCTYPE html>
<html>
<head>
	<title>Rekap Kompetensi dan Verifikasi</title>
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 12pt;
		}
	</style>
	<h3 style="text-align:center;"><b>REKAP KOMPETENSI DAN VERIFIKASI</b></h3>
	<hr>
	<h4>Rekap kompetensi/kemampuan akhir yang diharapkan</h4>
	<table style="border: 1px solid black; width:100%; border-collapse: collapse;">
		<thead style="color:white; background-color:black;">
			<tr>
				<th style="padding:10px; text-align:center; word-wrap: break-word; width:10%;">Nomor Soal</th>
				<th style="padding:10px; text-align:center; word-wrap: break-word; width:30%;">Level pada <br>Taksonomi Bloom</th>
				<th style="padding:10px; text-align:center; word-wrap: break-word; width:60%;">Kompetensi/Kemampuan Akhir yang Diharapkan</th>
			</tr>
		</thead>
		<tbody>
			@foreach($assessment->isiSoal as $item)
				<tr>
					<td style="padding: 10px; border: 1px solid black; text-align:center;">{{$item->nomor_soal}}</td>
					<td style="padding: 10px; border: 1px solid black; text-align:center;">{{$item->taksonomiBloom->level ?? ''}}: {{$item->taksonomiBloom->keterangan ?? ''}}</td>
					<td style="padding: 10px; border: 1px solid black;">{{$item->kompetensi ?? ''}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div style="border: 5px solid black; display:inline-block; width:300px; margin-top:100px;">
		<table style="width:60%; border: white;">
			<tr style="border: white;">
				<td colspan="3" style="text-align:center;">Soal telah diverifikasi</td>
			</tr>
			<tr style="border: white;">
				<td style="width:100px;">Pada tanggal</td>
				<td style="width:1px;">:</td>
				<td style="width:150px;">{{\Carbon\Carbon::parse($assessment->verifikasi->verified_at)->isoFormat('D/M/YYYY')}}</td>
			</tr>
			<tr style="border: white;">
				<td style="width:100px;">Oleh</td>
				<td style="width:1px;">:</td>
				<td style="width:150px;">{{$assessment->verifikasi->dosen->name}}</td>
			</tr>
			<tr style="border: white;">
				<td style="width:100px;">Jabatan</td>
				<td style="width:1px;">:</td>
				<td style="width:150px;">{{$assessment->verifikasi->dosen->jabatan}}</td>
			</tr>
			<tr style="border: white;">
				<td style="width:100px;">Tanda tangan</td>
				<td style="">: </td>
				<td><img style="width:50px; height:50px" src="{{asset('tanda-tangan')}}/{{$assessment->verifikasi->dosen->nama_file_ttd}}" alt="Gambar Tanda Tangan"></td>
				
			</tr>
		</table>
	</div>
</body>
</html>