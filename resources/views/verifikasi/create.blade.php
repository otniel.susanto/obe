<!-- Create Verifikasi Modal Form HTML -->
<div class="modal fade" id="modalCreateVerifikasi">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="formCreateVerifikasi">
                <div class="modal-header">
                    <h4 class="modal-title" id="create-title" name="title"> Verifikasi Soal Ujian </h4> 
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button"> × </button>
                </div>
                <div class="modal-body">
                    <div id="verifikasi-wrapper">
                        <p>Apakah anda yakin data ujian ini sudah benar dan layak untuk diverifikasi?</p>
                    </div>
                    <p class="text-warning"> <small> </small> </p>
                </div>
                <div class="modal-footer"> 
                    <input id="id_assessment" type="hidden" value=""> 
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times"></i> 
                        Cancel
                    </button> 
                    <button class="btn btn-success" id="btnCreate" type="button"> 
                        <i class="fa fa-signature"></i> 
                        Verifikasi 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
