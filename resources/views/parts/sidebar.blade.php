<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4 text-sm">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
        <img src="{{asset('img/iconUbaya.png')}}" alt="Logo Ubaya" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">SI Penilaian OBE</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="https://my.ubaya.ac.id/img/krywn/{{$user->npk}}_m.jpg" class="" alt="User Image">
        </div>
        <div class="info text-md">
            <a href="{{url('profile')}}" class="d-block">{{$user->name}}</a>
        </div>
      </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Penilaian (Assessment) -->
                <li class="nav-item">
                    <a href="{{url('assessment')}}" class="nav-link 
                        @if(str_contains(url()->current(), 'assessment')))
                            active
                        @endif">
                        <i class="fa fa-star"></i> 
                        <p>Penilaian (Assessment)</p>
                    </a>
                </li>
                <!-- Verifikasi Ujian -->
                <li class="nav-item">
                    <a href="{{url('verifikasi')}}" class="nav-link 
                        @if(str_contains(url()->current(), 'verifikasi')))
                            active
                        @endif">
                        <i class="fa fa-signature"></i> 
                        <p>Verifikasi Ujian</p>
                    </a>
                </li>

                <!-- Laporan -->
                <li class="nav-item has-treeview
                    @if(str_contains(url()->current(), 'score-card') || 
                    str_contains(url()->current(), 'course-scoring-sheet') || 
                    str_contains(url()->current(), 'rekap-cpl') )
                        menu-open
                    @endif">
                    <a href="#" class="nav-link">
                        <p>Laporan <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <!-- Score Card -->
                        <li class="nav-item">
                            <a href="{{url('score-card')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'score-card') && !str_contains(url()->current(), 'course-score-card'))
                                    active
                                @endif">
                                <i class="fa fa-table"></i> 
                                <p>Score Card (Student)</p>
                            </a>
                        </li>
                        <!-- Course Score Card -->
                        <li class="nav-item">
                            <a href="{{url('course-score-card')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'course-score-card'))
                                    active
                                @endif">
                                <i class="fa fa-table"></i> 
                                <p>Score Card (Course)</p>
                            </a>
                        </li>
                        <!-- Course Scoring Sheet -->
                        <li class="nav-item">
                            <a href="{{url('course-scoring-sheet')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'course-scoring-sheet')))
                                    active
                                @endif">
                                <i class="fa fa-file-excel"></i> 
                                <p>Course Scoring Sheet</p>
                            </a>
                        </li>
                        <!-- Rekap CPL (Mata Kuliah) -->
                        <li class="nav-item">
                            <a href="{{url('rekap-cpl')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'rekap-cpl')))
                                    active
                                @endif">
                                <i class="fa fa-file-excel"></i> 
                                <p>Rekap CPL</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Semesteran -->
                <li class="nav-item has-treeview 
                    @if(str_contains(url()->current(), 'tahun-ajaran') || 
                    str_contains(url()->current(), 'kelas'))
                        menu-open
                    @endif">
                    <a href="#" class="nav-link">
                        <p>Data Per Semester <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <!-- Kelas Paralel-->
                        <li class="nav-item">
                            <a href="{{url('kelas')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'kelas')))
                                    active
                                @endif">
                                <i class="fa fa-square-root-alt"></i> 
                                <p> Kelas Paralel</p>
                            </a>
                        </li>
                        <!-- Tahun Ajaran -->
                        <li class="nav-item">
                            <a href="{{url('tahun-ajaran')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'tahun-ajaran')))
                                    active
                                @endif">
                                <i class="fa fa-calendar-alt"></i>
                                <p>Tahun Ajaran</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- Kurikulum -->
                <li class="nav-item has-treeview 
                    @if(str_contains(url()->current(), 'level-kontribusi') || 
                    str_contains(url()->current(), 'rps'))
                        menu-open
                    @endif">
                    <a href="#" class="nav-link">
                        <p>Kurikulum<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <!-- RPS -->
                        <li class="nav-item">
                            <a href="{{url('rps')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'rps')))
                                    active
                                @endif">
                                <i class="fa fa-file-alt"></i> 
                                <p> RPS</p>
                            </a>
                        </li>
                        <!-- Level Kontribusi -->
                        <li class="nav-item">
                            <a href="{{url('level-kontribusi')}}" class="nav-link 
                                @if(str_contains(url()->current(), 'level-kontribusi')))
                                    active
                                @endif">
                                <i class="fa fa-exchange-alt"></i> 
                                <p>Matriks CPL vs CPMK</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Data Master -->
                <li class="nav-item has-treeview
                    @if(str_contains(url()->current(), 'mata-kuliah') || (str_contains(url()->current(), 'cpl') && ! str_contains(url()->current(), 'rekap-cpl')) ||
                    str_contains(url()->current(), 'taksonomi-bloom') || 
                    str_contains(url()->current(), 'mahasiswa') || 
                    str_contains(url()->current(), 'program-studi') || str_contains(url()->current(), 'jenis-penilaian') ||
                    (str_contains(url()->current(), 'level') && !(str_contains(url()->current(), 'level-kontribusi'))))
                        menu-open
                    @endif">
                    
                    <a href="#" class="nav-link">
                        <p>Data Master <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <!-- Program Studi -->
                        <li class="nav-item">
                            <a href="{{url('program-studi')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'program-studi'))
                                active
                            @endif">
                                <i class="fa fa-school"></i> 
                                <p>Program Studi</p>
                            </a>
                        </li>
                        <!-- Mata Kuliah -->
                        <li class="nav-item">
                            <a href="{{url('mata-kuliah')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'mata-kuliah')))
                                active
                            @endif">
                                <i class="fa fa-chalkboard"></i> 
                                <p>Mata Kuliah</p>
                            </a>
                        </li>
                        <!-- Mahasiswa -->
                        <li class="nav-item">
                            <a href="{{url('mahasiswa')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'mahasiswa')))
                                active
                            @endif">
                                <i class="fa fa-user-graduate"></i>
                                <p>Mahasiswa</p>
                            </a>
                        </li>
                        <!-- Taksonomi Bloom -->
                        <li class="nav-item">
                            <a href="{{url('taksonomi-bloom')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'taksonomi-bloom')))
                                active
                            @endif">
                                <i class="fa fa-award"></i>
                                <p>Taksonomi Bloom</p>
                            </a>
                        </li>
                        <!-- Jenis CPL -->
                        <li class="nav-item">
                            <a href="{{url('jenis-cpl')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'jenis-cpl')))
                                active
                            @endif">
                                <i class="fa fa-border-all"></i>
                                <p>Jenis CPL</p>
                            </a>
                        </li>
                        <!-- Kategori CPL -->
                        <li class="nav-item">
                            <a href="{{url('kategori-cpl')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'kategori-cpl')))
                                active
                            @endif">
                                <i class="fa fa-boxes"></i>
                                <p>Kategori CPL</p>
                            </a>
                        </li>
                        <!-- CPL -->
                        <li class="nav-item">
                            <a href="{{url('cpl')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'cpl') && !(str_contains(url()->current(), 'jenis-cpl')) && !(str_contains(url()->current(), 'kategori-cpl')))
                                active
                            @endif">
                                <i class="fa fa-graduation-cap"></i> 
                                <p>CPL</p>
                            </a>
                        </li>
                        <!-- Level -->
                        <li class="nav-item">
                            <a href="{{url('level')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'level') && !(str_contains(url()->current(), 'level-kontribusi')))
                                active
                            @endif">
                                <i class="fa fa-angle-double-up"></i>
                                <p>Level</p>
                            </a>
                        </li>
                        <!-- Jenis Penilaian -->
                        <li class="nav-item">
                            <a href="{{url('jenis-penilaian')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'jenis-penilaian'))
                                active
                            @endif">
                                <i class="fa fa-sun"></i>
                                <p>Jenis Penilaian</p>
                            </a>
                        </li>
                        <!-- Jenis Mata Kuliah -->
                        <!-- <li class="nav-item">
                            <a href="{{url('jenis-mata-kuliah')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'jenis-mata-kuliah'))
                                active
                            @endif">
                                <i class="fa fa-sun"></i>
                                <p>Jenis Mata Kuliah</p>
                            </a>
                        </li> -->
                    </ul>
                </li>
                <!-- Settings -->
                <li class="nav-item has-treeview
                    @if(str_contains(url()->current(), 'kelompok-nilai') ||
                    str_contains(url()->current(), 'users') )
                        menu-open
                    @endif">
                    
                    <a href="#" class="nav-link">
                        <p>Settings<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <!-- Kelompok Nilai-->
                        <li class="nav-item">
                            <a href="{{url('kelompok-nilai')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'kelompok-nilai'))
                                active
                            @endif">
                                <i class="fa fa-layer-group"></i> 
                                <p>Kelompok Nilai</p>
                            </a>
                        </li>
                        <!-- Users-->
                        <li class="nav-item">
                            <a href="{{url('users')}}" class="nav-link 
                            @if(str_contains(url()->current(), 'users'))
                                active
                            @endif">
                                <i class="fa fa-users"></i> 
                                <p>Users</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- Help -->
                <li class="nav-item">
                    <a href="{{url('help')}}" class="nav-link 
                    @if(str_contains(url()->current(), 'help'))
                        active
                    @endif">
                        <i class="fa fa-question-circle"></i> 
                        <p>Help</p>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
