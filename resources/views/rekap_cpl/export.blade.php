<table id="tableRekapCpl">
    <thead>
        @if($program_studi !== null)
        <tr>
            <th colspan="4" style="vertical-align:center">Program Studi: {{$program_studi->nama}}</th>
        </tr>
        @endif
        @if($mata_kuliah !== null)
        <tr>
            <th colspan="4" style="vertical-align:center">Mata Kuliah: {{$mata_kuliah->kode . ' - ' . $mata_kuliah->nama}}</th>
        </tr>
        @endif
        <tr>
            <th colspan="4" style="vertical-align:center">Tahun Ajaran: {{$tahun_ajaran->keterangan}}</th>
        </tr>
        <tr></tr>
        <!-- ROW Pertama / SEMESTER, Capaian Pembelajaran Lulusan (CPL) -->
        <tr>
            <th rowspan='4' style="width:5px; text-align:center; vertical-align:center">No.</th>
            <th rowspan='3' colspan='3' style="text-align:center; vertical-align:center">Mata Kuliah</th>
            <th colspan="{{($cpl_unique->count() + $cpl_unique->groupBy('program_studi_id')->count())*3}}" class="text-center">Capaian Pembelajaran Lulusan (CPL)</th>
        </tr>
        <!-- ROW KEDUA / Program Studi CPL -->
        <tr>
            @foreach($cpl_unique->groupBy('program_studi_id') as $itemCpl)
                <th colspan="{{($itemCpl->count() + 1)*3}}" style="text-align:center; vertical-align:center">{{$itemCpl[0]->programStudi->nama}}</th>
            @endforeach
        </tr>
        <!-- ROW KETIGA / KODE CPL -->
        <tr>
            @foreach($cpl_unique as $key=>$itemCpl)
                <th colspan="3" style="text-align:center; vertical-align:center">{{$itemCpl->kode}}</th>
                @if(isset($cpl[$key+1]))
                    @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                        <th colspan="3" style="text-align:center; vertical-align:center">Total</th>
                        @php 
                            $totalSkorMaks = 0;
                        @endphp
                    @endif
                @endif
            @endforeach
            <th colspan="3" style="text-align:center; vertical-align:center">Total</th>
        </tr>
        <!-- ROW KEEMPAT / ROW SKOR MAKS -->
        <tr>
            <th style="text-align:center; vertical-align:center">Kode MK</th>
            <th style="text-align:center; vertical-align:center">Nama Mata Kuliah</th>
            <th style="text-align:center; vertical-align:center">KP</th>
            @php 
                $totalSkorMaks = 0;
            @endphp
            @foreach($cpl_unique as $key=>$itemCpl)
                <th style="text-align:center; vertical-align:center">Max</th>
                <th style="text-align:center; vertical-align:center">Avg</th>
                <th style="text-align:center; vertical-align:center">%</th>
                @if(isset($cpl[$key+1]))
                    @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                        <th style="text-align:center; vertical-align:center">Max</th>
                        <th style="text-align:center; vertical-align:center">Avg</th>
                        <th style="text-align:center; vertical-align:center">%</th>
                    @endif
                @endif
            @endforeach
            <th style="text-align:center; vertical-align:center">Max</th>
            <th style="text-align:center; vertical-align:center">Avg</th>
            <th style="text-align:center; vertical-align:center">%</th>
        </tr>
    </thead>
    <!-- Isi Tabel -->
    <tbody>
        @php $countKelas = 0; @endphp
        @foreach($kelas as $itemKelas)
            <tr>
                <td>{{++$countKelas}}</td>
                <td>{{$itemKelas->mataKuliah->kode}}</td>
                <td>{{$itemKelas->mataKuliah->nama}}</td>
                <td>{{$itemKelas->kp}}</td>
                @php
                    $totalSkorMaksCpl = 0;
                    $totalAverageSkorMahasiswa = 0;
                @endphp
                @foreach($cpl_unique as $keyCpl=>$itemCpl)
                    @php
                        $totalSkorMahasiswa = 0;
                    @endphp
                    @if($cpl->where('id', $itemCpl->id)->firstWhere('kelas_id', $itemKelas->id) !== null)
                        @php
                            $getCpl = $cpl->where('id', $itemCpl->id)->where('kelas_id', $itemKelas->id)->first();
                            $skorMaksCpl = $getCpl->skor_maks;
                            $totalSkorMaksCpl += is_numeric($skorMaksCpl) ? $skorMaksCpl : 0;
                        @endphp
                        <!-- Jumlah Nilai Mahasiswa -->
                        @foreach($itemKelas->mahasiswa->where('program_studi_id', $itemCpl->program_studi_id) as $key=>$itemMahasiswa)
                            @php
                                $skorMahasiswa = $level_kontribusi[$itemKelas->id][$itemCpl->id][$itemMahasiswa->id] ?? '-';
                                $totalSkorMahasiswa += is_numeric($skorMahasiswa) ? $skorMahasiswa : 0 ;
                            @endphp
                        @endforeach
                        <!-- Nilai Mahasiswa dicari rata-ratanya -->
                        @php
                            if($itemCpl->id == 13){

                            }
                            $averageSkorMahasiswa = $totalSkorMahasiswa > 0 ? round($totalSkorMahasiswa / 
                                $itemKelas->mahasiswa->where('program_studi_id', $itemCpl->program_studi_id)->count(),2) : '-';
                        @endphp
                        <td>{{$skorMaksCpl ?? '-'}}</td>
                        <td>{{$averageSkorMahasiswa}}</td>
                        <td>{{$averageSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                            round($averageSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : '-' ) : '-'}}</td>
                        @php
                            $totalAverageSkorMahasiswa += is_numeric($averageSkorMahasiswa) ? $averageSkorMahasiswa : 0;
                        @endphp
                        <!-- Cek Program Studi Beda atau sama -->
                        @if(isset($cpl[$keyCpl+1]))
                            <!-- Print Total kalo Prodi Beda -->
                            @if($itemCpl->program_studi_id != $cpl[$keyCpl+1]->program_studi_id)
                                <td>{{$totalSkorMaksCpl}}</td>
                                <td>{{$totalAverageSkorMahasiswa}}</td>
                                <td>{{$totalAverageSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                                    round($totalAverageSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : '-' ) : '-'}}</td>
                                @php
                                    $totalSkorMaksCpl = 0;
                                    $totalAverageSkorMahasiswa = 0;
                                @endphp
                            @endif
                        @endif
                    @else
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    @endif
                @endforeach
                <td>{{$totalSkorMaksCpl}}</td>
                <td>{{$totalAverageSkorMahasiswa}}</td>
                <td>{{$totalAverageSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                    round($totalAverageSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : '-' ) : '-'}}</td>
            </tr>
        @endforeach
    </tbody>
</table>