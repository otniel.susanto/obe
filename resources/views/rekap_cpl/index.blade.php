@extends('index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <h1 class="mb-1">Rekap Nilai CPL (Program Studi / Mata Kuliah)</h1>
        <!-- Filter Section -->
        <form action="{{url('rekap-cpl')}}">
            <div class="form-group row">
                <div class="col-md-3">
                    <select id="program_studi_id" name="program_studi_id" class="form-control" placeholder="Pilih Program Studi">
                        <option disabled selected></option>
                        @foreach($program_studi as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->program_studi_id)
                                    selected
                                @endif
                            >{{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <select id="mata_kuliah_id" name="mata_kuliah_id" class="form-control" placeholder="Pilih Mata Kuliah">
                        <option disabled selected></option>
                        @foreach($mata_kuliah as $item)
                            <option value="{{$item->id}}" 
                                @if($item->id == $request->mata_kuliah_id)
                                    selected
                                @endif
                            >{{$item->kode}} - {{$item->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select id="tahun_ajaran_id" name="tahun_ajaran_id" class="form-control" placeholder="Pilih Tahun Ajaran" required>
                        <option disabled selected></option>
                        @foreach($tahun_ajaran as $item)
                            <option value="{{$item->id}}"
                                @if($item->id == $request->tahun_ajaran_id)
                                    selected
                                @endif
                            >{{$item->keterangan}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <input name="kp" type="search" class="form-control" placeholder="KP" value="{{$request->kp}}">
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" type="submit" id="btnCari"><i class="fa fa-search"></i> Cari</button>
                    <button class="btn btn-secondary" type="button" id="btnReset"><i class="fa fa-eraser"></i> Reset</button>
                </div>
            </div>
        </form>
        <!-- Filter Section End -->
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @if($kelas === null || $kelas->count() < 0)
        <h5>Belum ada data, silakan mencari data lainnya</h5>
        @else
        <!-- Keterangan Rekap CPL -->
        <div>
            <!-- Keterangan Mata Kuliah -->
            @if($request->program_studi_id != "" && $request->program_studi_id !== null)
                <h5>Program Studi: <br> {{$kelas[0]->programStudi->nama}}</h5>
            @endif
            @if($request->mata_kuliah_id != "" && $request->mata_kuliah_id !== null)
                <h5>Mata Kuliah: <br> {{$kelas[0]->mataKuliah->kode}} - {{$kelas[0]->mataKuliah->nama}}</h5>
            @endif
            <h5>Tahun Ajaran: <br> {{$kelas[0]->tahunAjaran->keterangan}}</h5>
            <span>
                <form action="{{url('rekap-cpl-export')}}" method="get">
                    <input type="hidden" name="program_studi_id" value="{{$request->program_studi_id}}">
                    <input type="hidden" name="mata_kuliah_id" value="{{$request->mata_kuliah_id}}">
                    <input type="hidden" name="tahun_ajaran_id" value="{{$request->tahun_ajaran_id}}">
                    <input type="hidden" name="kp" value="{{$request->kp}}">
                    <button type="submit" class="btn btn-success float-sm-right mb-1">
                    <i class="fa fa-file-excel"></i>
                    Export ke File Excel</button>
                </form>
            </span>
        </div>
        <!-- Tabel Rekap CPL -->
        <div class='table-responsive'>
            <table data-toggle="table" data-pagination="true" class="table myTable table-bordered table-hover" id="tableCourseScoreCard">
                <thead>
                    <!-- ROW Pertama / SEMESTER, Capaian Pembelajaran Lulusan (CPL) -->
                    <tr>
                        <th data-sortable="true" rowspan='4' class='text-center align-middle'>No.</th>
                        <th rowspan='3' colspan='3' class='text-center align-middle'>Mata Kuliah</th>
                        <th colspan="{{($cpl_unique->count() + $cpl_unique->groupBy('program_studi_id')->count())*3}}" class="text-center">Capaian Pembelajaran Lulusan (CPL)</th>
                        <!-- <th rowspan='2' class='text-center align-middle'>Total Skor</th> -->
                    </tr>
                    <!-- ROW KEDUA / Program Studi CPL -->
                    <tr>
                        @foreach($cpl_unique->groupBy('program_studi_id') as $itemCpl)
                            <th colspan="{{($itemCpl->count() + 1)*3}}" class="text-center align-middle">{{$itemCpl[0]->programStudi->nama}}</th>
                        @endforeach
                    </tr>
                    <!-- ROW KETIGA / KODE CPL -->
                    <tr>
                        @foreach($cpl_unique as $key=>$itemCpl)
                            <th colspan="3" class='text-center align-middle'>{{$itemCpl->kode}}</th>
                            @if(isset($cpl[$key+1]))
                                @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                                    <th colspan="3" class='text-center align-middle'>Total</th>
                                    @php 
                                        $totalSkorMaks = 0;
                                    @endphp
                                @endif
                            @endif
                        @endforeach
                        <th colspan="3" class='text-center align-middle'>Total</th>
                    </tr>
                    <!-- ROW KEEMPAT / ROW SKOR MAKS -->
                    <tr>
                        <th data-sortable="true" class='text-center align-middle'>Kode MK</th>
                        <th data-sortable="true" class='text-center align-middle'>Nama Mata Kuliah</th>
                        <th data-sortable="true" class='text-center align-middle'>KP</th>
                        @php 
                            $totalSkorMaks = 0;
                        @endphp
                        @foreach($cpl_unique as $key=>$itemCpl)
                            <th>Max</th>
                            <th>Avg</th>
                            <th>%</th>
                            @if(isset($cpl[$key+1]))
                                @if($itemCpl->program_studi_id != $cpl[$key+1]->program_studi_id)
                                    <th>Max</th>
                                    <th>Avg</th>
                                    <th>%</th>
                                @endif
                            @endif
                        @endforeach
                        <th>Max</th>
                        <th>Avg</th>
                        <th>%</th>
                    </tr>
                </thead>
                <!-- Isi Tabel -->
                <tbody>
                    @php $countKelas = 0; @endphp
                    @foreach($kelas as $itemKelas)
                        <tr>
                            <td>{{++$countKelas}}</td>
                            <td>{{$itemKelas->mataKuliah->kode}}</td>
                            <td>{{$itemKelas->mataKuliah->nama}}</td>
                            <td>{{$itemKelas->kp}}</td>
                            @php
                                $totalSkorMaksCpl = 0;
                                $totalAverageSkorMahasiswa = 0;
                            @endphp
                            @foreach($cpl_unique as $keyCpl=>$itemCpl)
                                @php
                                    $totalSkorMahasiswa = 0;
                                @endphp
                                @if($cpl->where('id', $itemCpl->id)->firstWhere('kelas_id', $itemKelas->id) !== null)
                                    @php
                                        $getCpl = $cpl->where('id', $itemCpl->id)->where('kelas_id', $itemKelas->id)->first();
                                        $skorMaksCpl = $getCpl->skor_maks;
                                        $totalSkorMaksCpl += is_numeric($skorMaksCpl) ? $skorMaksCpl : 0;
                                    @endphp
                                    <!-- Jumlah Nilai Mahasiswa -->
                                    @foreach($itemKelas->mahasiswa->where('program_studi_id', $itemCpl->program_studi_id) as $key=>$itemMahasiswa)
                                        @php
                                            $skorMahasiswa = $level_kontribusi[$itemKelas->id][$itemCpl->id][$itemMahasiswa->id] ?? '-';
                                            $totalSkorMahasiswa += is_numeric($skorMahasiswa) ? $skorMahasiswa : 0 ;
                                        @endphp
                                    @endforeach
                                    <!-- Nilai Mahasiswa dicari rata-ratanya -->
                                    @php
                                        if($itemCpl->id == 13){

                                        }
                                        $averageSkorMahasiswa = $totalSkorMahasiswa > 0 ? round($totalSkorMahasiswa / 
                                            $itemKelas->mahasiswa->where('program_studi_id', $itemCpl->program_studi_id)->count(),2) : '-';
                                    @endphp
                                    <td>{{$skorMaksCpl ?? '-'}}</td>
                                    <td>{{$averageSkorMahasiswa}}</td>
                                    <td>{{$averageSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                                        round($averageSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : '-' ) : '-'}}</td>
                                    @php
                                        $totalAverageSkorMahasiswa += is_numeric($averageSkorMahasiswa) ? $averageSkorMahasiswa : 0;
                                    @endphp
                                    <!-- Cek Program Studi Beda atau sama -->
                                    @if(isset($cpl[$keyCpl+1]))
                                        <!-- Print Total kalo Prodi Beda -->
                                        @if($itemCpl->program_studi_id != $cpl[$keyCpl+1]->program_studi_id)
                                            <td>{{$totalSkorMaksCpl}}</td>
                                            <td>{{$totalAverageSkorMahasiswa}}</td>
                                            <td>{{$totalAverageSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                                                round($totalAverageSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : '-' ) : '-'}}</td>
                                            @php
                                                $totalSkorMaksCpl = 0;
                                                $totalAverageSkorMahasiswa = 0;
                                            @endphp
                                        @endif
                                    @endif
                                @else
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                @endif
                            @endforeach
                            <td>{{$totalSkorMaksCpl}}</td>
                            <td>{{$totalAverageSkorMahasiswa}}</td>
                            <td>{{$totalAverageSkorMahasiswa > 0 ? ( $totalSkorMaksCpl > 0 ? (
                                round($totalAverageSkorMahasiswa / $totalSkorMaksCpl * 100, 2) . '%') : '-' ) : '-'}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</section>
@endsection

@push('child-scripts')
<script>
    $(document).ready(function () {
        //Filter Section
        $('#program_studi_id').select2({
            allowClear: true,
            placeholder: "Pilih Program Studi",
        });
        $('#mata_kuliah_id').select2({
            allowClear: true,
            placeholder: "Pilih Mata Kuliah",
        });
        $('#tahun_ajaran_id').select2({
            placeholder: "Pilih Tahun Ajaran",
        });
        $("#btnReset").click(function(){
            window.location.replace("{{url('course-score-card')}}");
        });
        //Filter Section End
    });
</script>
@endpush
